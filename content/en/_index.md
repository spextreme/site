---
title: Paulin's Pages
description: My collection of writing and information.
date: 2020-11-01T04:15:05+09:00
draft: false
landing:
  height: 100
  title:
    - Paulin's Pages
  titleColor:
  textColor:
  spaceBetweenTitleText: 25
footer:
  sections:
    - title: General
      links:
        - title: About Me
          link: /about
    - title: Resources
      links:
        - title: GitLab
          link: https://gitlab.io/
        - title: Rowan
          link: https://rowan.edu/
  contents: 
    align: left
    applySinglePageCss: false
    markdown:
      |
      ## Stephen Paulin
      Copyright © 2020. All rights reserved.
      <div>Icons made by <a href="https://www.flaticon.com/authors/icon-pond" title="Icon Pond">Icon Pond</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

sections:
  - bgcolor: teal
    type: card
    description: "This site hosts content I want to share including my course material for classes I teach, writing that I'm working on and other things I want to share.   Enjoy."
    header: 
      title: Welcome
      hlcolor: "#8bc34a"
      color: '#fff'
      fontSize: 32
      width: 220
    cards:
      - subtitle: Course Material
        subtitlePosition: center
        description: "Course material for the class I teach at Rowan University."
        image: images/courses.png
        color: white
        button: 
          name: Course Content
          link: courses
          size: large
          color: 'white'
          bgcolor: '#283593'
      - subtitle: Writing
        subtitlePosition: center
        description: "A section for things I've written or stuff I'm working on. This can include draft work and writing I'm currently experimenting on."
        image: images/writing.png
        color: white
        button: 
          name: Writing
          link: writing
          size: large
          color: 'white'
          bgcolor: '#283593'
      - subtitle: Blog
        subtitlePosition: center
        description: "Not really a blogger but a place to store some random thoughts."
        image: images/blog.png
        color: white
        button: 
          name: Blog
          link: blog
          size: large
          color: 'white'
          bgcolor: '#283593'
---
