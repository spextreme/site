---
title: "Exceptions"
date: 2021-02-17
description: Exception handling and dealing with uncertainty.
draft: false
collapsible: false
weight: 15
---

## Introduction

When creating an application, you will always think that your implementation will work perfectly.   However, there are times when things don't work as planned, and your application may get completely messed up because of something completely out of your control.

If things worked perfectly all the time, you'd never lose power, your cell phone battery wouldn't run out of juice, and your car wouldn't run out of gas (or electricity).   In programming, you may create an application that wants to read a file from the computer or make a connection over the Internet.   Well, what happens if your application can't access the file on the computer because someone deleted it or the Internet address you want to reach shut down in the middle of reading data?

These are called exceptions, and they will occur, and there will be nothing you can do to prevent it.  You'll just have to accept that they will happen and decide what to do when they occur.

In this section, we'll talk about a specialized concept in Java called Exception Handling and the details around using it with the *try catch* block.

## Exception Handle

Exceptions are errors or unexpected events that occur during your applications execution.   These events occur and trigger something to be 'thrown'.  The concept of throwing an exception is to mean that something happened in one spot, and it is unable to be handled there, so it is thrown (up the call stack) to someone else to handle it.

This is done because the place the error occurs may not be within your control (e.g. failed to read from a server).  As the person calling this, you can be sure that if that method call doesn't work, you'll at least get notified of the error to handle the case.

At any point, your application may encounter an exception, but it's usually not random.   You'll have specific areas that are more prone to errors (i.e. files, network access, etc.), and then some are just coding problems (i.e. Null Pointer, Illegal Arguments, etc.).  Understanding exceptions is important, and in some cases, the language will force you to handle exceptions because the object you're using says it will potentially throw one.

As you develop methods and algorithms, you'll find cases where you might want to throw an error to alert someone that they have an error they should deal with that your code won't.

Overall, the Java Virtual Machine (JVM) will catch all exceptions.  If your application doesn't handle it, then the JVM will catch it, report the message, and exit.

## Exception Class

As everything in Java comes back to an object, Exceptions are also an object themselves.  The foundation of all exception starts from *Throwable*.   The *Error* and *Exceptions* classes then branch off of *Throwable*.

![Exception Hierarchy](/images/intro-to-java/exception-hierarchy.png)

\* The above is not a complete list but has a solid core set to start with.  

The *Error* class is typically thrown when a critical error occurs, like an internal error, a JVM issue, or running out of memory.   Applications will not typically handle these since they are more related to bigger issues with the runtime environment and not the application.

The *Exception* type is for application exceptions and is generally handled within the application.   

Overall, in this context, there are two types of exceptions: 
- Checked Exceptions
- Unchecked Exceptions

### Unchecked Exceptions

An unchecked exception is one that can be thrown and your application isn't forced to handle it.   They are children of *Error* and *RuntimeException* and will occur while the application is running (runtime).   *Error* exceptions are system critical errors that should not be handled in an application.   The *RuntimeEexception* serves as a superclass for exceptions that usually occur as programming errors.  This usually means you have a potential bug or your program had some unexpected event.

For example, *NullPointerException* is one of the main ones many programmers run in to.  This occurs when you create a variable to hold an instance of an object, and the object is *null*, but an attempt is made to use the method on the object.

```
String myString = null;

myString.equals( "Hello" );
```

In the above, it is pretty obvious *myString* is *null* and calling equals will throw the *NullPointerException*.  These things may not be as obvious and you can potentially work around the potential risk in most cases (i.e. check for null before use). In the above, a better way to write this so *NullPointerException* can never happen would be to reverse it.

```
String myString = null;

"Hello".equals( myString );
```

Again all *RuntimeException*s don't have to be handled and can usually be dealt with by good coding practices.

Here is a short list of some of the *RuntimeExceptions* you may run into (not a complete list).

- OutOfMemoryError
- StackOverflowError
- Runtime Exception
- IllegalArgumentException
- NullPointerException
- ArithmeticException
- IndexOutOfBoundsException
- ArrayIndexOutOfBoundsException
- StringIndexOutOfBoundsException
- ClassCastException
- IllegalArgumentExcption
- IllegalThreadStateException
- NumberFormatException
- SecurityException
- ConcurrenctModificationException

### Checked Exceptions

Checked exceptions are ones that must be handled by the programmer and are known as compile time exceptions.  This means you must either catch them or throw them out of your method to be handled further up the call stack.   They are known as compile time exceptions because the compile will report an error if you don't handle them.

Checked exceptions all extend from *Exception* and will require you to wrap them in a *try catch* block to be able to handle them, or you'll have to use the *throws* keyword on your method to inform the compiler you plan to pass it out of your method.

Here is a short list of some of the checked exceptions you may encounter.  This is not a complete list.

- ClassNotFoundException
- InstantiationException
- IllegalAccessException
- NoSuchMethodException
- IOException
- EOFException
- FileNotFoundException
- InterruptedIOException
- NotSerializableException
- SocketException
- ConnectException
- MalformedURLException
- UnknownHostException
- SQLException

## *try-catch*

When dealing with any exceptions (checked or unchecked), you can handle them by using the *try-catch* block around the section of code that will produce the exception.

To catch an exception you'll use the following model:

```java
try {
  // Normal code to process
} catch( <exception> ) {
  // Code to use to process exception
}
```

An example of handling an exception that occurs is below.  In this case, the URL object expects a web address, and we've forgotten to provide the protocol (http://) which will cause this to throw a *MalformedURLException*.  Since we handle the *Exception*, this will be caught since *MalformedURLException* is a child of *Exception*.

```java
public void exceptionHandlingTryCatch() {

 try {

   new URL("spextreme.gitlab.io/site/courses/intro-to-java/");

 } catch (final Exception e) {

   System.out.println("We gave a bad URL.  Should have been "
       + "https://spextreme.gitlab.io/site/courses/intro-to-java/");
 }

}
```

In cases where a method may throw different types of exceptions, you can list multiple catch blocks to handle them.  

```java
try {

   new URL("spextreme.gitlab.io/site/courses/intro-to-java/");

 } catch (final MalformedURLException me) {

   System.out.println("Url is malformed.");
   
 } catch (final Exception e) {

   System.out.println("Some error occurred.");
 }
```

In this case, we handled the *MalformedException* first if that is what is thrown.  However, if any other exception type is thrown, then the second will handle it.  Remember inheritance.  If we put the *Exception* type first and then the *MalformedURLException* it would never be reached because a *MalformedURLException* is an *Exception* (since the *MalformedURLException* extends from it).

### Finally

No, this doesn't mean it's over, it just means there is another part to the *try catch* block and that is the *finally* clause.    It's modeled after try something, catch it, and finally finish this process.  But really, what it does is make sure that no matter what happens you're given one last chance to do some processing.

The reason for this is, if you open a file, you are holding on to something in your application (file descriptor), and if you have an exception occur, it immediately jumps to the catch block.   This could be bad because you still have that open file descriptor. Finally was designed to make sure you are given an opportunity to perform any cleanup associated with the exception.

```java
 public void exceptionHandlingTryCatchFinally() {

    FileInputStream fis = null;

    try {

      fis = new FileInputStream("testfile.txt");
      final int data = fis.read();

      System.out.println("First Byte of Data is: " + data);

    } catch (final Exception e) {

      System.out.println("Had an error finding the file or reading it.");
    } finally {

      if (fis != null) {
        try {

          fis.close();
        } catch (final Exception e2) {
          // ignore since we are really done.
        }
      }
    }
  }
```

This code is a little more complicated, but it shows how exceptions make code much more complex.   It takes a fair amount to be sure your application doesn't have a catastrophic event.  In this case, we are opening a file (*FileInputStream*), which occurs by making this call.   The next line attempts to read the contents.  If an error occurs while reading, then the catch clause will print out an error message.  Remember we opened the file for reading, so now we have that open file descriptor being held and leaving it that way can corrupt the file.   The *finally* block is going to run and because we have an open file, the *fis* variable is not *null* and we ask it to *close()* which in turn releases the file.  Since *close()* throws another exception, we handle that by just ignoring it since we are done processing and no more risk should exist.

*finally* is a nice option because, no matter what happens in the *try* or the *catch*, the *finally* will run.  This can help clean up and undo anything that may have been in progress when the exception occurred.

As the code above shows, working with open connections (files, networking, etc...) can be messy.  Java realized this, and in current versions, a new option called *try-with-resource* exists.

## *try-with-resource*

This is a specialized version of the *try* statement to work with resources that implement *AutoCloseable*.   This way the JVM can automatically clean up the resources associated with the object used when the code completes successfully or has an exception.  It removes the extra calls necessary to handle cleanup like closing a stream.

The basic syntax is:

```java
try( <instance of AutoCloseable> ) {
   // Normal processing
} catch( <exception> ) {
   // Exception processing
}
```

The following example is the same as the one in the Finally section, but it uses the *try-with-resource*, which, as you can see, removes a lot of extra code to handle the cleanup.  The beauty of *try-with-resource* is that it will automatically handle the close logic in both the successful and exception paths.   This has become the new way to handle exceptions on *AutoClosable* instances.

```java
public void exceptionHandlingTryWithResource() {

 try (FileInputStream fis = new FileInputStream("testfile.txt")) {

   final int data = fis.read();

   System.out.println("First Byte of Data is: " + data);

 } catch (final Exception e) {

   System.out.println("Had an error finding the file or reading it.");
 }
}
```

## Throwing Exceptions

Now that we understand how to handle exceptions by catching them, there is another path to handle exceptions we can introduce.  This is the concept of throwing an exception.

When you have a method that needs to produce an exception or a method call being used is already producing an exception, it is possible to tag your method as a producer of exceptions.  This is done by marking your method with the *throws* keyword and the type of exception or exceptions to be thrown.

```java
public void exceptionHandlingThrowsMultiple() throws MalformedURLException, URISyntaxException,
      FileNotFoundException, BadThingsHappenedException {

 final URL url = new URL("file:///tmp/file.txt");
 new FileInputStream(new File(url.toURI()));
}
```

As shown in the example above, 4 exceptions are thrown from this method, *MalformedURLException*; *URISyntaxException*; *FileNotFoundException*; *BadThingsHappenedException*.   Each exception is separated by a comma when multiple are listed. We could have just as easily said we are throwing just the *Exception* type which would cover all of them and do this as a single exception defined after the *throws*.  

This allows the method to skip directly handling any of these exceptions with the *try catch* and simply throw the exceptions to whomever calls this method.

This is a case that is sometimes more beneficial because the method may need to produce an exception so that the caller can be informed and process an alternate path.

It is also possible to create an exception and throw it from your method.  In this case you will use the *throw* keyword and build a exception yourself.

```java
public void exceptionHandlingThrowException() throws BadThingsHappenedException {

 throw new BadThingsHappenedException("This isn't implemented yet");
}
```
In this case, we create an exception *BadThingsHappenedException*, and our method throws it to the caller.  In this way, we can make any exception that exist and pass it to the caller to have them handle it.

## Custom Exceptions

Since *Exception*s are just an object in Java, it's possible to create our own *exception* to notify users of something the library or application is producing.   This may be done to support a different path for handling errors, or it may be because a custom exception type fits better (makes more sense) in our application.

Making a custom exception is as simple as extending from Exception.   Most exceptions are nothing more then a unique type but may also carry a more detailed message.

Here is an example of a custom exception:

```java
/**
 * A custom exception for when bad things happen.
 */
public class BadThingsHappenedException extends Exception {

  /**
   * Default constructs this object.
   */
  public BadThingsHappenedException() {

  }

  /**
   * Constructs this object with a message.
   *
   * @param message The message to add to this exception.
   */
  public BadThingsHappenedException(String message) {

  }
}
```

As this shows, simply extending from *Exception* gives us everything we need to be an exception.   We can now throw this from any method and we can catch and handle it anywhere that is needed.

## Stack Trace

The last section to understanding exceptions is to know what they report out when printed to the screen.   This data is available in the object and can be access through its methods, but typically, the output of the exception is placed on the screen or in a log file to help users more easily troubleshoot when problems do occur.

When an exception occurs, it happens in a class on a specific line.   The exception will record the stack trace of how it got to that line of code.   That means from the first call into the *main* method through to each call that got to the method that threw the exception.   The stack trace holds the full history so that you have information about how it got to that line of code, the methods that were called, and the line numbers they were on.   This includes the object's full name (usually the package + name) and a lot of detail.  If you have access to the source code, it is usually very accurate to the line and method name.

Here is an example of the stack trace in a simple case:

```
java.net.MalformedURLException: no protocol: spextreme.gitlab.io/site/courses/intro-to-java/
   at java.base/java.net.URL.<init>(URL.java:627)
   at java.base/java.net.URL.<init>(URL.java:523)
   at java.base/java.net.URL.<init>(URL.java:470)
   at com.spextreme.intro.to.java.exceptions.ExceptionSamples.exceptionHandlingThrows(ExceptionSamples.java:43)
   at com.spextreme.intro.to.java.exceptions.ExceptionSamples.main(ExceptionSamples.java:22)
```

This is not a very deep exception since we throw it right in the *main*, but the point is that it shows all the details.  You'll notice that the *MalformedURLException* prints some details about what is wrong (no protocol because we are missing the http://).   Then it bubbles up to line 43 of our class to the main method of our class on line 22. The *<init>* are the constructor calls made in instantiating the *URL* instance.   

This can be very helpful when you have runtime exceptions and in troubleshooting what may have been coded incorrectly.   But in the real world these are usually records that the developers will ask for to debug the customers problems.   

Generally these are things stored in log files and not presented to the end user.   Nicer, more compact messages are provided to the user with a reference to see the log for more details.

## Conclusion

As you can see, Exception Handling is very important and with so much uncertainty this is a good thing.  With the ability to handle exceptions we can protect the application from unexpected failure and our users from losing data.

As you build an application, make sure you keep in mind things that may fail unexpectedly.   Working with resources outside of your application like the file system or network may be needed to handle unexpected cases.

Any time you do use exception handling, also check to make sure any resources held are released and cleaned up.  Otherwise your application can corrupt data, and it may make it unrecoverable.
