---
title: "Exceptions - Video"
date: 2021-02-17
description: Exception handling and dealing with uncertainty.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/exceptions-screen.png)](https://youtu.be/a0nnHo35jII)

## Slides

{{< revealjs >}}

## Exceptions

- Application aren't perfect
- Neither are the systems they run on
- You can't plan for everything
- This is why exceptions exist

---

## Exception Handling

- Exceptions are unexpected errors
- Trigger something to be *thrown*
- Error occurs, caught further up the stack
- Can happen at any point
- JVM will handle all by shutting down

---

## Exception Class Structure

- All starts from Throwable
   - Errors
   - Exceptions

--

## Exception Class Structure

![Exception Hierarchy](/site/images/intro-to-java/exception-hierarchy.png)

---

## Unchecked Exceptions

- Runtime
- Not forced to handle
- Error and RuntimeException

```
String myString = null;

myString.equals( "Hello" );
```

--

## Unchecked Exceptions

- OutOfMemoryError
- StackOverflowError
- Runtime Exception
- IllegalArgumentException
- NullPointerException
- ArrayIndexOutOfBoundsException
- ClassCastException
- IllegalArgumentExcption
- SecurityException
- ConcurrenctModificationException

---

## Checked Exceptions

- Compile time
- Must be handled
   - catch
   - throw
- Extended from Exception

--

## Checked Exceptions

- ClassNotFoundException
- NoSuchMethodException
- IOException
- EOFException
- FileNotFoundException
- InterruptedIOException
- SocketException
- MalformedURLException
- UnknownHostException
- SQLException

---

## *try-catch*

- Keywords to handle exceptions
- Handle many with separate catch blocks
- Polymorphism works here
- *try* contains what should be done
- *catch* contains what to do on exception

```java
try {
  // Normal code to process
} catch( < exception > ) {
  // Code to use to process exception
}
```

--

## *try-catch* Example

```java
try {

   new URL("spextreme.gitlab.io/site/courses/intro-to-java/");

 } catch (final MalformedURLException me) {

   System.out.println("Url is malformed.");

 } catch (final Exception e) {

   System.out.println("Some error occurred.");
 }
```

--

## Finally

- Part of the *try-catch* syntax
- Runs in all cases (go and exception path)
- Run something no matter what
- Example - Open a File
   - File locked
   - Exception on Read
   - Need to release the lock

--

## Finally Example

```java
try {
  fis = new FileInputStream("testfile.txt");
  final int data = fis.read();
  System.out.println("First Byte of Data is: " + data);
} catch (final Exception e) {
  System.out.println("Had an error reading it.");
} finally {
  if (fis != null) {
   try {
     fis.close();
   } catch (final Exception e2) { /*ignore*/ }
  }
}
```

--

## *try-with-resource*

- Specialized version of *try-catch-finally*
- *AutoCloseable* resources
- Will close the resource automatically in all cases
- Removes the need for the finally

```java
try( < instance of AutoCloseable > ) {
   // Normal processing
} catch( < exception > ) {
   // Exception processing
}
```

--

## *try-with-resource* Example

```java
try (FileInputStream fis = new FileInputStream("testfile.txt")) {

final int data = fis.read();

System.out.println("First Byte of Data is: " + data);

} catch (final Exception e) {

System.out.println("Had an error finding the file or reading it.");
}
```
---

## Throwing Exceptions

- Throw the exception from method
- Force caller to handle it
- *throws* keyword
- Method stops at the exception point

```java
public void exceptionHandlingThrowsMultiple()
    throws MalformedURLException, URISyntaxException,
           FileNotFoundException, BadThingsHappenedException {

 final URL url = new URL("file:///tmp/file.txt");
 new FileInputStream(new File(url.toURI()));
}
```

--

## Throw Exception

- Method can create an exception
- Then *throw* it

```java
public void exceptionHandlingThrowException()
            throws BadThingsHappenedException {

 throw new BadThingsHappenedException(
            "This isn't implemented yet");
}

---

## Custom Exceptions

- Exception are still just an object
- Extend from one of the existing
- Most are just a unique type

--

## Bad Things Happened

```java
public class BadThingsHappenedException extends Exception {

  public BadThingsHappenedException() { }

  public BadThingsHappenedException(String message) { }
}
```

---

## Stack Trace

- Exceptions report important details
- Package/Class/Method that caused it
- Line number it occurred on
- The call stack of how it got there

--

## Stack Trace Sample

```
java.net.MalformedURLException: no protocol: spextreme.gitlab.io/site/courses/intro-to-java/
   at java.base/java.net.URL.< init >(URL.java:627)
   at java.base/java.net.URL.< init >(URL.java:523)
   at java.base/java.net.URL.< init >(URL.java:470)
   at com.spextreme.intro.to.java.exceptions.ExceptionSamples.exceptionHandlingThrows(ExceptionSamples.java:43)
   at com.spextreme.intro.to.java.exceptions.ExceptionSamples.main(ExceptionSamples.java:22)
```

---

## Conclusion

- Applications will fail
- Exceptions are important
- *try-catch-finally*
- *throw* / *throws*
- Stack Trace

{{< /revealjs >}}