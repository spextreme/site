---
title: "Inheritence - Video"
date: 2021-02-07
description: What is inheritance and how its a major part of OOP.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/inheritance-screen.png)](https://youtu.be/G84SHhk2ecI)

## Slides

{{< revealjs >}}

## Inheritance

- OOP Concept
- Generalization to specialization
   - Shape -\> Rectangle, Circle, Polygon
- "is a" relationship
   -  A rectangle is a shape
- Parent - Child
   - Superclass - Subclass

--

## Extends

```java
<scope> class <name> extends <superclass> {
   <...>
}
```

- *extends* keyword
- Access to all methods/fields excluding *private*
- Constructs not inherited
- *super* can call any non-private superclass items
- Leverage the *@Override annotation*
- *final* classes/methods can not be extended
- All inherit *Object*

---

## Polymorphism

- means "Many forms"

![Shape Inheritance](/site/images/intro-to-java/shape-inheritence.png)

```java
Shape square = new Square();
Shape circle = new Circle();
Shape polygon = new Polygon();
```

---

## Abstract

- *abstract* keyword
- Define a class that can't be instantiated
- It is only partially completed
   - Defines the method signature but does not implement it
- Abstract classes can be extended
   - subclass must implement undefined methods

--

## Abstract Example

```java
public abstract class Shape {

  private int height;
  private int width;
  private int x;
  private int y;

  /**
   * Draws the shape based on its specific rules.
   */
  public abstract void draw();

  // Code left out for simplicity
}
```

--

## Abstract Example Part 2

```java
public class Rectangle extends Shape {

  /**
   * {@inheritDoc}
   */
  @Override
  public void draw() {

    System.out.println(String.format(
        "I am a Rectangle (%i, %i, %i, %i).",
        getWidth(), getHeight(), getX(), getY()));
  }
}
```

--

## Abstract Benefits

- Central code (reduce duplication)
- Reuse defined methods
- Reduce testing and maintenance
- Polymorphic relationship usage

---

## Interfaces

- Like a contract
- Defines the public methods
- Implementations must implement the methods
- *interface* keyword

```java
< scope > interface < name > {
   < methods >;
}
```

--

## Interface Example

```java
public interface IShape {

  /**
   * Draws the shape based on its specific rules.
   */
  void draw();

}
```

--

## Interface Example 2

```java
public abstract class Shape implements IShape {

  private int height;
  private int width;
  private int x;
  private int y;

  // Code left out for simplicity
}
```

--

## Interface Example 3


```java
IShape square = new Square();
IShape circle = new Circle();
IShape polygon = new Polygon();
```

--

## Interface

- Simplistic
- Defines the API (contract)
- Separations from definition and implementation
   - Define the rules
   - Others implement what it means

---

## Conclusion

- Inheritance - parent/child relationship
- Polymorphism - "Many Forms"
- Abstract classes
- Interfaces

{{< /revealjs >}}