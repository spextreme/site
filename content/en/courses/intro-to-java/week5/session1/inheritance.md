---
title: "Inheritance"
date: 2021-02-07
description: What is inheritance and how it's a major part of OOP.
draft: false
collapsible: false
weight: 15
---

## Inheritance

Inheritance is a concept in object oriented programming (OOP) that allows for a parent-child relationship.   The child class will inherit all the contents of the parent to allow them to access, use and extend the capabilities provided.   Think about the concept we have in naming things like a shape.  A shape is descriptive for a large collection of items, but then it can be specialized as a square, circle, or polygon.

This is the same model we may use for objects.   The topmost object may be a shape, then a specialization of a shape may be created for the square and another for the circle.  Each of them can share a location (x,y) and a size(width, height), but drawing them would be very different.

Inheritance is designed around this model.  It is used to represent the "is a" relationship (i.e. A square is a Shape).  Inheritance describes the parent as a superclass and the child as the subclass.  This will make a little more sense when we cover the keywords used for inheritance.

The first keyword to understand is the *extends* keyword.  This is used to have a class extend another.  

```java
<scope> class <name> extends <superclass> {
   <...>
}
```

The subclass is the one that extends from a superclass.  With this, the subclass will then be able to access all fields and methods that are not marked as private.   Private fields, as we discuss, are hidden from the subclass and not inherited.  The constructors are also not directly inherited.   A subclass may call on a constructor by using the *super* keyword.  The *super* keyword may be used to call anything from the superclass.

To call a constructor you would use the *super()* or *super(<params>)* form to call the appropriate constructor.   When calling a superclass's constructor, it must be the first statement in the subclass's constructor.

A subclass may also overload any method defined in the superclass by matching the signature of the method.  This allows the subclass to override what it is doing with that method.  It is also possible for the subclass to call the superclass's method using the *super* keyword (i.e. *super.\<method\>(<params>)*).  It is also recommended to put the @Override annotation above the method so the compiler can generate an error when the method isn't properly aligned with the superclass's method signature.

One word of caution is that some methods or classes may not able to be inherited or extended.   If they are defined as *final* on the class or method then it will block them from being extended (class) or overridden (method).

As we originally mentioned, everything in Java inherits from something.  If nothing is defined, then the default inheritance of the *Object* class is automatic.  This is why all classes have the *toString*, *equals*, and a other methods from the *Object* class.

### Polymorphism

Polymorphism means many forms and is highly related to object oriented programming (OOP) because of inheritance.   Using polymorphism allows Java to use the superclass definition to hold any of the subclasses that extend from the superclass.   If we take our Shape example again we have the following:

![Shape Inheritance](/images/intro-to-java/shape-inheritence.png)


With this, we can then define a variable and place any of the subclass types into it.

```java
Shape rectangle = new Rectangle();
Shape square = new Square();
Shape circle = new Circle();
Shape polygon = new Polygon();
```

With Polymorphism this is allowed because a *Rectangle* is a *Shape* ("is a" relationship).  And what this can do is, when we call *shape.draw()*, because it is a *Rectangle*, it will draw the rectangle.

### Abstract

Now that we understand inheritance and polymorphism, it's time to understand what Java can do leveraging these concepts.   When a class is defined as an *abstract* class, it means it is a partial definition of a class and is somehow incomplete. The first rule is an abstract class must *extend*, and second, it must finish any methods that are defined but not implemented by the abstract class.  What this means is the abstract implementation can define the method with a complete signature (return type, name and parameters), but the subclass has to implement it.  Going back to the *Shape* class, we have:

```java
public abstract class Shape {

  private int height;
  private int width;
  private int x;
  private int y;

  /**
   * Draws the shape based on its specific rules.
   */
  public abstract void draw();

  // Code left out for simplicity
}
```

In the above examples, notice the *abstract* keyword added to the class as well as the one method defined with no content.   This is how Java knows that this is an abstract class and that any class that extends it must implement the *draw()* method.

```java
public class Rectangle extends Shape {

  /**
   * {@inheritDoc}
   */
  @Override
  public void draw() {

    System.out.println(String.format("I am a Rectangle (%i, %i, %i, %i).", getWidth(), getHeight(),
        getX(), getY()));
  }
}
```

For the class that extends this, notice how it added the keyword *extends* and the class to extend from.   The *@Override* was added to the method this class had to implement to help the IDE/Compiler report errors if we don't properly align to the method signature.   Also notice that this has access to the public getter methods defined in the Shape class.

Abstract is a beneficial way to take a large portion of code, implement it once, and then have it usable by the many (polymorphic) objects that extend it.   It can save a lot of effort, testing, and coding.

### Interfaces

An interface is like making a promise to follow the defined set of rules.   With an interface, you can define the contract that must be implemented by anyone who decides to abide by it.

When an interface is defined, it lists all the public methods without defining any implementation or fields.   The methods must contain the complete signature (return type, name and parameters) and can even be fully documented (Javadoc).  Scope may only be defined as *public* or it can be excluded as it is always public.  Remember, an interface doesn't define the implementation portion, so the method signature is terminated with a semi-colon.   

The interface can then be implemented by a class, in which case, the class must implement all of the methods defined.   It inherits the method signatures and any Javadoc comments bound to them.   

To define an interface, the *interface* keyword is used in place of the *class* keyword.  It must abide by all the same naming rules and lives in a '.java' file.

```java
<scope> interface <name> {
   <methods>;
}
```

We'll continue with the *Shape* example and define an *interface* for the *draw()* method.

```java
public interface IShape {

  /**
   * Draws the shape based on its specific rules.
   */
  void draw();

}
```

To then use the interface and bind it to a class, we'll use the *implements* keyword.

```java
public abstract class Shape implements IShape {

  private int height;
  private int width;
  private int x;
  private int y;

  // Code left out for simplicity
}
```

All that was done here was binding the interface *IShape* to the *Shape* abstract class using the *implements* keyword.   Also, since the *IShape* defines the *draw()* method, the abstract no longer needs to be defined here (although it can be if desired).  Since the interface defines the method, any user of the interface or abstract class will have to implement the *draw()* method.

Using an interface it is identical to using any other object, but because of polymorphism, anything that is an *IShape* can be bound to the variable.

```java
IShape square = new Square();
IShape circle = new Circle();
IShape polygon = new Polygon();
```

Overall, interfaces are very simplistic, but the power that they bring for defining Application Programming Interfaces (APIs) and messaging communications is very helpful in complex application and library development.  Also, defining an interface allows the implementation to be done differently for different use cases without affecting the overall users of the implementation since the contract (method signatures) of the interface is controlled.   It is the first step in freeing the usage from the implementation.

## Conclusion

There is a lot in this section about inheritance.   Understanding the parent-child relationship is important because it opens the door to interfaces and abstract classes.   Also, understand polymorphism is beneficial because of what it can mean.

As we get into collections, this concept will become very important and the benefits will be greatly seen.
