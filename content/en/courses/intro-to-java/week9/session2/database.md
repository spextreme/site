---
title: "Database"
date: 2021-03-21
description: Understanding and managing large amounts of data.
draft: false
collapsible: false
weight: 15
---

## Introduction

Data is a major part of our lives.  If you do a Google search for any subject you'll pretty much find hundreds of documents about it.   Each person has a large collection of data that companies are collecting like your date of birth, gender, etc...    Data is all around us.

Storing data requires some method to save it but then make it easy to search and perform other things like joining different related information together.

This is where Databases come in.  They are designed and well optimized to hold content in a structured or unstructured form.   Structured data can be helped and managed in what is called a Relational Databases.   Unstructured data (like a collection of web pages) can be stored in non-relational databases.

In this section will go through the different types of databases, their query language (how to search) and some cases of how and when they may be used.


## Relational Database

Relational data is also known as structured data.  Therefore a Relational Database is designed to hold data that is able to be structured.  This means that it can be easily organized in a table with rows and columns.   Each row represent a record that has the same set of columns for each record.

The below example of a person record, holding their name address, email and cellular phone number.

|firstname|lastname|email|cell|address|
|---------|--------|-----|----|-------|
|John|Smith|john.smith@email.com|123-123-1234|123 Street Rd, Some Town, NJ, 12345|
|Jane|Doe|jane.doe@email.com|890-890-7890|15 Upside Down, Major Town, CA, 54321|

As the table shows each record can be easily mapped to the same set of columns.

This is what Relation Databases are designed to do.  They hold data in a collection of tables and then has support to relate data together across different tables.  For example, we could change our table from above to have a second table holding the phone numbers and allow the person to reference the phone numbers that belong to them since many people have multiple numbers and some share a number.  

### Entity Relationship Diagram (ERD)

When working with a relational database, you'll typically want to see all the tables and how they are related.   This is called an Entity Relationship Diagram or ERD.   An ERD is a way to represent the design of a database using a set of tables and their relationship.

![ERD Person and Contact](/images/intro-to-java/simple-erd.png)

As the above shows, an ERD makes describing the database much easier to understand.   The symbols help describe the relationship but understanding them in the begin isn't required.   As the above shows, we have two tables and they are related.

ERD are also very nice when the structure becomes much more complex.

![ERD Medical Tracking System](/images/intro-to-java/medical-tracking-system-erd.png)

Things can get very complicated but visually this can help to simplify the understanding.  The above is a project that I been working on for a little bit and hoping to build out at some point.


### SQL

SQL, Structured Query Language, is the syntax used for creating, altering and interacting with a Relational Database.   It is a specialized syntax, similar to the Java language, that is used to work with most Relational Databases.  This means that there are specialized keywords used to work with the database.   We will not cover them in great detail but give a basic overview of the main commands.  There are full semester database course, so the goal here is to just introduce the concepts.

#### Select

The *SELECT* keyword is used to query data in a database.  This may be used to lookup data in a table, join data across different tables, and perform summarization of the data.  It provides a way to filter content to select only specific records using the *WHERE* clause.   

```sql
/* SELECT all persons */
SELECT * FROM introtojava_sample.person;

/* SELECTS all persons with the last name Doe */
SELECT * FROM introtojava_sample.person WHERE lastname='Doe';
```
The *SELECT* statement is a very powerful capability of a Relational Database.   It provides many features and supports many ways to query data.

#### Insert

The *INSERT* keyword is used to place data into a table and create new records (rows).   The *INSERT* statement takes in the columns and the data to align them.

```sql
/* Insert John Doe into the person table. */
INSERT INTO introtojava_sample.person ( id, firstname, lastname, email, address ) VALUES (1, 'John', 'Deo', 'john.doe@email.com', '123 street rd, some city, NJ 12345');
/* Insert Jane Doe into the person table. */
INSERT INTO introtojava_sample.person ( id, firstname, lastname, email, address ) VALUES (2, 'Jane', 'Doe', 'jane.doe@email.com', '123 street rd, some city, NJ 12345');
/* Insert Bob Smith into the person table. */
INSERT INTO introtojava_sample.person ( id, firstname, lastname, email, address ) VALUES (3, 'Bob', 'Smith', 'bob.smith@email.com', '567 Long Road, alt city, NJ 12346');
```

As the examples show this will insert three new records into the Person table populating the id, firstname, lastname, email, and address with the aligned data.   Notice each insert statement ends with a *;* (Similar to Java and ends each SQL command).  Also notice that strings are represented using the single quote (*'*).

#### Update

*UPDATE* is used to edit or change records in a table.  It uses the *WHERE* clause to find the set of records to be updated and then aligns the column name to what the new value should be.

```sql
UPDATE introtojava_sample.person  SET lastname='Doe' WHERE id=1;
```

In the above we are going to update the record with the unique id of 1 and change the lastname to the correct spelling of 'Doe'.   *UPDATE* may be used to modify a collection of rows to the same value.  If the above didn't include the *WHERE* clause, all our records would have had the last name of 'Doe'.

#### Delete

*DELETE* is used to remove content from a table.  It may be used to remove a collection of records by filtering with the *WHERE* clause.  It can also remove all content from a table by excluding the *WHERE* clause.

```sql
DELETE FROM introtojava_sample.person WHERE id=1;
```

This will remove the record with the id of 1.

#### Create

The *CREATE* statement is used to build a new database schema or create a new table in an existing database schema.   A database schema is a single database that is used to hold a collection of tables.  Most database vendors (MariaDB, MySQL, Oracle, MS SQL Server) can hold many database schemas in a single server. 

To create a table you'll provide the database name and the table name and then define each of the columns.   SQL provides multiple data types like int, string (varchar), bit, and many others.  In our example, we use an int to represent our primary key (unique number for each entry that can not be null) and then 3 strings.   For SQL you have to define a string with VARCHAR and the size of the string (number of characters).

```sql
/* Creates a new database */
CREATE SCHEMA `introtojava_sample`;

/* Creates a table in the introtojava_sample schema */
CREATE TABLE `introtojava_sample`.`person` (
  `id` INT NOT NULL,
  `firstname` VARCHAR(64),
  `lastname` VARCHAR(64),
  `email` VARCHAR(128),
  `address` VARCHAR(500),
  PRIMARY KEY (`id`));
```

The two separate create statements build the database *introtojava_sample* and the table within that database *person*.   This is how we build out the structure of our database to hold all the content we need to store. 

#### Drop

*DROP* is used to remove a table or database schema.   This will remove them and all the record contained within will be lost.  This is a very destructive command.

```sql
/* Remove the table person */
DROP TABLE introtojava_sample.person;

/*Remove the database schema introtojava_sample */
DROP SCHEMA introtojava_sample;
```

As mentioned the above two examples remove the table *person* and then the schema *introtojava_sample*.

### Relational Database Summary

This barely scratches the surface on Relational Databases but does highlight the power it provides.   Relational Databases hold structure data very well and the *SELECT* command provides an infinite method to find and query data.

## Non-Relational Database (NoSql)

NoSQL databases are designed to store unstructured data and move away from the Relational Database concepts and the SQL syntax.   Since all data doesn't easily fit into a database (think of a web page document), NoSQL database were created to better manage this type of information.

The core of a NoSql database is around the concept of everything being modeled as a *key-value* pair.  This is similar to a Map in Java.  The important part in this is that the *key* is what makes the record unique and the *value* can hold any type of data.

NoSQL database can still perform relationships between the data but it is managed much differently.  When working with NoSQL the organization of information becomes much more important.  Duplicating information across records is a more natural action in NoSQL databases to better support querying.

We will not go any further in NoSQL databases due to the advance nature of them.

## Java & Databases

Java has always supported working with databases through the use of a connection model called JDBC (Java Database Connectivity).   JDBC is an API (Application Program Interface) used to support database connections in a standard way through Java.  The nice part is that the API states the contract that then all different database vendors support.  This provides your application with a consistent way to talk to the database and if you need to use a different database (MySql to MS SQL Server) you won't have to change your code.

JDBC is designed around querying and saving data but it can be used to create and drop content as well.   This concept is call CRUD (Create, Read, Update, Delete).

JDBC is part of the core of the Java Framework under the package *java.sql*.

The basic model for JDBC is to 
1. Open a connection
2. Use the connection to perform CRUD operations
3. Close the connection

Here is a simplistic example

```java
Connection conn = null;

 try {
   // Would need a real connection setup with account and database populated.
   conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/introtojava_sample", "user",
       "password");

   final Statement stmt = conn.createStatement();

   final ResultSet rs = stmt.executeQuery("SELECT * FROM Person");
   while (rs.next()) {
     final int numColumns = rs.getMetaData().getColumnCount();
     for (int i = 1; i <= numColumns; i++) {
       // Different then arrays columns start at 1.
       System.out.println("COLUMN " + i + " = " + rs.getObject(i));
     }
   }
 } catch (final SQLException e) {

   System.out.println("Database error: " + e.getMessage());

 } finally {

   try {

     if ((conn != null) && !conn.isClosed()) {

       conn.close();
     }

   } catch (final SQLException e) {
   }

 }
```

As this shows, it opens the connections, queries for all the *persons* in the table, and then gets back a *ResultSet* of the items found.   This then loops through the *ResultSet* and for each record, loops across the columns and outputs each column index and data.   

This also shows there is a lot of exception handling required.  This is because of the database being an external system so connection errors may occur.  It also could happen that another system could be changing the database and the underlying data structure.   Lots of unknowns when working with databases connections.

## Conclusion

As this demonstrates, a database is a powerful way to manage and collect data.  Connecting to it with Java can leverage JDBC to simplify the setup and standardize the connection details.   Any application that will work with data, which this includes most, will leverage a database at some point.