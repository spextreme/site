---
title: "Database - Video"
date: 2021-03-21
description: Understanding and managing large amounts of data.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/databases-screen.png)](https://youtu.be/2GkjmjGCOow)

## Slides

{{< revealjs >}}

## Introduction

- Data is important and everywhere
- Data storage needs a special solution
- Database are optimized for data storage
- Structured and Unstructured data

---

## Relational Database

- Structured data
- Tables
   - Rows/Columns
   - Joining

--

## Relational Database Table

|firstname|lastname|cell|
|-|---|---| 
|John|Smith|123-123-1234|
|Jane|Doe|890-890-7890| 

---

## Entity Relationship Diagram (ERD)

- A graphical representation of the database
- See all the tables and their relationships
- Special symbols to represent things

-- 

## Simple ERD

![ERD Person and Contact](/site/images/intro-to-java/simple-erd.png)

-- 

## Complex ERD

![ERD Medical Tracking System](/site/images/intro-to-java/medical-tracking-system-erd.png)

---

## SQL

- Structured Query Language
- Database language
- Special keywords
- Supports CRUD - Create, Read, Update, Delete
- Only an overview

--

## Select

- Query data from tables
- Can be filtered - *WHERE*
- Join multiple tables

```sql
/* SELECT all persons */
SELECT * FROM introtojava_sample.person;

/* SELECTS all persons with the last name Doe */
SELECT * FROM introtojava_sample.person 
      WHERE lastname='Doe';
```

--

## Insert

- Adds data to a table
- Creates a new row

```sql
/* Insert John Doe into the person table. */
INSERT INTO introtojava_sample.person 
   ( id, firstname, lastname, email, address ) 
 VALUES 
   (1, 'John', 'Deo', 'john.doe@email.com', 
            '123 street rd, some city, NJ 12345');
```

--

## Update

- Change rows in a table
- Use *WHERE* to filter

```sql
UPDATE introtojava_sample.person 
   SET lastname='Doe' 
   WHERE id=1;
```

--

## Delete

- Removes rows
- Use *WHERE* to filter

```sql
DELETE FROM introtojava_sample.person WHERE id=1;
```

--

## Create

- Used to make a new schema or table
- Defines the name 
- For tables, defines the columns

```sql
CREATE SCHEMA `introtojava_sample`;

CREATE TABLE `introtojava_sample`.`person` (
  `id` INT NOT NULL,
  `firstname` VARCHAR(64),
  `lastname` VARCHAR(64),
  `email` VARCHAR(128),
  `address` VARCHAR(500),
  PRIMARY KEY (`id`));
```

--

## Drop

- Removes a schema or table
- Destructive command

```sql
DROP TABLE introtojava_sample.person;

DROP SCHEMA introtojava_sample;
```

---

## Relational Database Summary

- Barely scratched the surface
- Very powerful for managing data

---

## Non-Relational Database (NoSql)

- Designed for unstructured data
   - Think about a web page or document
- Key/Value pairs
   - Key is unique
   - Value can store anything
- Supports relationships
- Duplicates data

---

## Java & Databases

- JDBC - Java Database Connectivity
- A standard API for database access
- CRUD interactions
- *java.sql* package
- Independent of database and vendor

--

## Connection

- Open a connection
- Use the connection to perform CRUD operations
- Close the connection

--

## Connection Code

```java
...
conn = DriverManager.getConnection(
   "jdbc:mysql://localhost:3306/introtojava_sample", 
      "user", "password");

final Statement stmt = conn.createStatement();

final ResultSet rs = stmt.executeQuery("SELECT * FROM Person");
...
conn.close();
...
```

---

## Conclusion

- Databases are very powerful tools for managing data
- JDBC simplifies working with a database
- Most applications leverage a database


{{< /revealjs >}}