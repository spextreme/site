---
title: "Build Software - Video"
date: 2021-03-13
description: Overview of Maven and building software outside of an IDE.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/building-screen.png)](https://youtu.be/hAcXBQU5GwY)

## Slides

{{< revealjs >}}

## Introduction

- IDE helps developers
- IDE builds software automatically
- Building outside of an IDE 

---

## Build Tool

- Many build tool solutions
- Focus on building and auditing software
- Scriptable and highly configurable
- Compile, test, package
- Deployment 

---

## Maven

- Software Project Management & Comprehension tool
   - More then building
   - Build, document and report
- XML file called a pom.xml
   - Project Object Model

--

## Maven Lifecycle
  
- Clean
- Build
- Site

--

## Maven Lifecycle Clean & Site
  
- Clean
   - clean
- Site
   - site
   - site-deploy

--

## Maven Lifecycle Build
  
- Build
   - validate
   - compile
   - test
   - package
   - verify
   - install
   - deploy

--

## Running Maven

- *mvn* command
- Pass in the goals

```bash
mvn clean package
```

--

## Maven Plugins

- Plugin architecture
- Each plugin focus on one task
   - Do that task well
   - Reduce complexity
   - Increase stability
- Maven Central Artifact Repository

--

## Dependencies

- Best Dependency Management solution
- Used for third-party packages
   - JUnit
- Downloads and caches dependencies
- Natively works with Eclipse

---

## Building Software

- Can be very complicated
- Compiling
- Testing
- Audits
- Reporting

--

## Standard Directory Structure

```bash
+-- pom.xml
+-- src
    +-- main
    |   +-- java
    |   +-- resources
    +-- test
        +-- java
        +-- resources
```

--

## Sample Pom

```
&lt;project xmlns="http://maven.apache.org/POM/4.0.0"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
   http://maven.apache.org/xsd/maven-4.0.0.xsd"&gt;

   &lt;modelVersion&gt;4.0.0&lt;/modelVersion&gt;

   &lt;groupId&gt;com.spextreme.examples&lt;/groupId&gt;
   &lt;artifactId&gt;threads&lt;/artifactId&gt;
   &lt;version&gt;1.0.0-SNAPSHOT&lt;/version&gt;
   &lt;packaging&gt;jar&lt;/packaging&gt;

&lt;/project&gt;
```

---

## XML

- Extensible Markup Language
- Structured syntax
- Good for configuration
- tag based
   - <tag> and </tag>
   - tag can be any string

```xml
&lt;project&gt;
   &lt;modelVersion>4.0.0&lt;/modelVersion&gt;
&lt;/project&gt;
```

---

## Conclusion

- Maven is a powerful build tool
- pom.xml and standards based
- Dependency Management
- Compiling, testing, deployment

{{< /revealjs >}}