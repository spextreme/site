---
title: "Build Software"
date: 2021-03-13
description: Overview of Maven and building software outside of an IDE.
draft: false
collapsible: false
weight: 1
---

## Introduction

Everything we have talked about thus far is about writing code and how to make the computer perform the actions we desire. 

In this section, we will actually talk about how we take our code and turn it into something that can be more easily built outside of our IDE. In the previous sections we never worried about compiling because our IDE performed all of that for us. When we get into larger projects we will need a way to perform our builds more frequently, run our tests that we have created and create a package that can be distributed.

## Build Tool

There are many build tools on the market today. Build tools are largely focused on providing capabilities to script the steps necessary to compile, test, and run other tools on our code.  There are many tools that support these capabilities.  The market leaders are feature rich and provide good dependency management and a way to customize the steps of the build.

Each build tool on the market today support a way to produce the product and create a package of it for deployment.   We will not cover all the tools but will instead focus on one that provides all the capabilities that should be expected by any solution chosen.

## Maven

Maven is know as a software project management and comprehension tool.   This is because Maven can build, document and report on your software.    While it does serve as a build tool, it does provide many more capabilities for creating your product and helping to improve the quality of your solution.

Maven uses XML (see below) as it's build file syntax and it is placed into a file called Project Object Model (POM). The *pom.xml* holds the contents that define what Maven will execute when it is run. Maven is largely set up to perform most of the necessary steps automatically with little effort from the developer. For example if you need to compile and package your software it mostly requires you to define your artifact name, version, and package type.

Maven uses a lifecycle model to define the steps being performed during its execution.  There are three distinct lifecycles in Maven: clean; build; site. 

**Clean:**
- clean

**Build:**
- validate
- compile
- test
- package
- verify
- install
- deploy

**Site:**
- site
- site-deploy

The *clean* lifecycle is provided to remove the contents so it will be back to a state that contains none of the generated content.  This really means the target directory (The place Maven places all the generated content) is deleted.

The *site* lifecycle steps provide a way to generate reports and documentation for your product.  Maven will automatically create content for a full web site around your product.

The *build lifecycle is the main one that will be used but it provides all the steps around compiling, testing and ultimately deploying your project.

When running Maven, you pass in a lifecycle goal.   The goals are any of the lifecycle sub items (clean, compile, package, etc...).   

```bash
mvn clean package
```

This will ultimately run the clean lifecycle step using the clean goal.  The *package* goal will cause Maven to run validate, compile, test and package performing all those goals of the lifecycle.

### Maven Plugins

Maven is largely based on a plug-in architecture, where each independent plug-in is focused on performing one task. For example the compile plug-in is solely focused on compiling your Java source code. The Surefire plug-in is designed around running your  tests and producing the JUnit report.

Maven plugins are published in a central repository (Maven Central or others) so they can be downloaded at build time when needed.  It only pulls down the plugin if the build needs it.

It is also very important that the plugin is focused on doing only one task.  This means it is focused on doing the one thing well and reduced complexity and potential errors.

### Dependencies

Maven has a very well defined Dependency Management solution.  As your product becomes more complex, you'll leverage third party libraries and products to help speed up your development.   These third party libraries (JUnit is one we've used) are called dependencies that you're product requires.  It needs them to compile and run your product. 

Maven is built around this with a concept called Dependency Management.  It allows you to define the dependencies in the *pom.xml* and Maven will then use this to make sure it is available in your build environment.  It downloads the items from an artifact repository (Maven central for example) and puts them in a local cache for your build to use.   The next time you build it will only pull down what is new or missing.

This way, it frees you from having to worry about getting all the dependencies installed locally.   This will also work natively inside of Eclipse.

## Building Software

As mentioned, building software can be very complicated based on the product being produced and may involve many steps and specialized solutions.  This included the steps needed to compile the product, the tests executions needed and the dependencies required to support your product.

With Maven, building software from the start is simplified because of their standards.   Maven defines a directory structure layout that is the default organization for source, test and resources.


```bash
+-- pom.xml
+-- src
    +-- main
    |   +-- java
    |   +-- resources
    +-- test
        +-- java
        +-- resources
```

As shown there is a *pom.xml* at the root, with a *src* directory.  The source directory is broken into *main* and *test* to separate and organize the different types of source.   Under *main*/*test* there is a place for the Java code (*java*) and *resources*.   Your package and source content will go under the *java* directory.  Resources is used to cover anything else that your product may need.  This could include images, files, etc...

With this layout and the *pom.xml*, Maven has enough information to build your product, run tests and produce a package.  All that you have to focus on is building the code and the tests.

To get started with Maven, the first thing to do is create a *pom.xml* file.  A sample POM is below.

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

   <modelVersion>4.0.0</modelVersion>

   <groupId>com.spextreme.examples</groupId>
   <artifactId>threads</artifactId>
   <version>1.0.0-SNAPSHOT</version>
   <packaging>jar</packaging>

</project>
```

In the above file, it has the default header for a Maven project which is what Maven uses to define the overarching product. The next part defines the group, artifact, version which is known as the GAV.  This defines the details of the product, it's name and version. The group is usually the base package name that you use like *edu.rowan* as an example.  Just know that the GAV is there to uniquely identify your product when deploying or referencing as a dependency. The artifact name will be your artifact ID combined with the version.

With this little bit in place, Maven will compile your product, run the tests and produce a jar (a zip of the compile class files) and place all this in the *target* directory.

Maven does allow you to customize all the steps and add other things but for basic applications this is all that will be required.

# XML

Extensible Markup Language (XML) is structure language to write parsable documents.  It uses a tag based model to hold a key that then wraps other content which may be used to describe information.   Tags are anything that go between the <> signs in a document.   One rule of XML is that it must have a begin and end tag.   The end tag only varies slightly by adding a </>.  A sample XML document may be

```xml
<project>
   <modelVersion>4.0.0</modelVersion>
</project>
```

As this shows, it has the tag *project* and *modelVersion*.  *modelversion* is a child of *project* and the *project* is the root element.  There is no limit to the nesting of elements.

Many tools use XML as their method of configuration.   Maven is no exception as show in the *pom.xml*.