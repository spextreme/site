---
title: "Testing"
date: 2021-02-14
description: Testing what you develop with a test first mind set.
draft: false
collapsible: false
weight: 15
---

## Introduction

Writing code requires a lot of work, especially for more complex solutions and advanced capabilities.   When you develop an application, you should make sure it is properly working before your customers start to use it.  It is horrible when you get a customer call that says your application doesn't work.   The customer may not be happy, and you will have a lot of questions and little time to figure out why it didn't work. Also, regaining their trust will be very difficult.

When a customer calls and says, "Your app doesn't work.  I can't do anything," you'll ask what isn't working.  "Your app" they'll say.   How helpful is that answer?  Not very.  But then again, you're really at fault because your app isn't working for the customer, and it's not their job to figure it out.  

So how do you limit this type of problem?   Testing is the solution.  There are multiple levels of testing and lots of solutions, depending on your application and what you're application does.

There are 3 main levels of testing:
- Unit Testing
- Integration Testing
- System/Acceptance Testing


### System/Acceptance Testing

System/Acceptance Testing is focused on proving to the end user the application is ready for production.  It is focused on meeting the requirements and selling off the system to the requirements.   It usually involves putting the full system in place that is as close to production as necessary.  It may even include real equipment and other things required by the system.

This testing is usually very time consuming, may be expensive, and if an issue is found at this level, it's very costly to fix.

### Integration Testing

Integration testing is responsible for starting to test the application with other parts that it works with. This may be to test how it works with other applications or maybe how it integrates with hardware that it is designed for.   Parts of the system may also be simulated because it may be too costly to use them or set them up.  

This is also an area where performance testing may be performed to check how the system runs, where it may need to be improved, or how it handles loads.  This is mainly focused on how the application works.

### Unit Testing

Unit testing is focused on the parts that make up an application.   It is focused around writing tests that prove a line of code and the method that contains them work as designed.   It also attempts to minimize the need for outside dependencies.

In practice, Unit Tests should run extremely fast, require little setup, and be automated so they can be run frequently.

Another aspect of Unit Tests is the concept of Test Driven Development (TDD) which helps with designing objects for test. 

## Test Driven Design (TDD)

Test Driven Design, aka TDD, is a concept where tests are developed before the code.   This means, you write a test, then write the code that will satisfy the test.   Once you have the test passing, you'll update the code to clean it up (remove duplication, etc...).

This is called the Red, Green, Refactor method and is the core of TDD.    When you write the test first and you run it you'll end up with a failed test (Red).    Then you write the code to get the test to pass (Green).  Then cleaning it up is the last part (Refactor).

### Defining For Tests

As you get into TDD, you should begin to notice that your object design changes to become more testable.  Following TDD causes this because you focus on making the object cater to the test.   As the objects become dependent on other objects you'll end up needing to pass in dependencies.   This is a concept called **Dependency Injection** and will help make objects easier to use and test.   Dependency Injection is about designing the object to be handed what it needs so that it never creates the things on its own.  

This means your objects should always have setter methods or the constructor should take in what it needs.   This way it can be handed what it needs when it's created.    When testing, you can change the dependencies by simply creating a mock to duplicate the required objects to support your testing.

## JUnit

For unit testing there is a concept called xUnit Frameworks where the x is the language being used.   For example JUnit is a Java Unit test framework that has been around for almost as long as Java.  It has been the main xUnit framework that has defined and establish the other frameworks for other language.   CPPUnit, HtmlUnit, DbUnit, etc....   They all serve the same purpose, and that is to simplify unit testing for the given language.

JUnit is the de facto standard and its concepts are very easy to understand.   To use JUnit, you'll include the *junit.jar* file which Eclipse has built in and will auto add it to your project when you create the first test.  As you get further into this course, we'll use dependency management to pull it in for use outside of Eclipse.  For now, using Eclipse is good enough.

JUnit, as mentioned, is very easy to understand.  It has a test runner which is there to search for and run any defined tests.   Second, it has a collection of assert\* statements (assertTrue, assertEquals, assertNull, etc...) to help perform the comparisons to prove the code is working as designed.   That is pretty much all that is needed to get started, but know there is much more to it.

### Mocking

As the code gets more complex, you'll run into cases where the object you're testing will need other objects (dependencies) to fully make it work.  These dependencies will make testing your object much harder because controlling the input and output will rely on the dependent objects to work in a controlled way.  This isn't always easy to archive especially when you may not have developed the other object (Java Framework, Third Party Package, etc.).

This case has given rise to a concept called creating Mock Objects.   Mock Objects are using the real class definition to create a fake instance which is under your control.  There are plenty of Mock libraries but they all do similar things.  They take an object and allow you to define what should be taken in by a method (matching) and return the results you define.

Mocks make tests easier to control the inputs and outputs, and allow you to fully test the methods use of the dependency. Using a good framework will also help make tests much easier to read.  

One of the current top frameworks is [Mockito](https://site.mockito.org/).

## TDD Walk-Through

Now that we understand *Dependency Injection*, *Unit Testing*, *TDD*, and *JUnit* we can run through an example to build a product using TDD.  

Application development always starts with the "what you want" to build.  These are called the *Requirements*.   They define the needs, which for this walkthrough will start with a set of requirements to build some shapes.

### Requirements

1. A rectangle to hold a position of X and Y
2. A rectangle to hold a width and height 
3. Rectangle can calculate the area
4. A circle to hold a position of X and Y
5. A circle to hold a width and height 
6. Circle can calculate the area

### Step 1

It begins with choosing a requirement to implement.

**Requirement 1: A rectangle to hold a position of X and Y**

```java
class RectangleTest {
  @Test
  void testPositionXandY() {
    final Rectangle rectangle = new Rectangle();

    assertEquals(0, rectangle.getX());
    assertEquals(0, rectangle.getY());

    rectangle.setX(10);
    rectangle.setY(50);
    
    assertEquals(10, rectangle.getX());
    assertEquals(50, rectangle.getY());
  }
}
```

At this point, this test will not even compile since the Rectangle class doesn't yet exist.  Now, let's create the class to allow this to compile and make this test pass.

```java
public class Rectangle {
  private int x = 0;
  private int y = 0;

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }
}
```

Now we can run the test and see what happens.

![Req1: Test Pass](/images/intro-to-java/tdd-req1-testresult-pass.png)


Finally we check if anything should be refactored.   Is there any duplication or things that can be cleaned up?  Not in this case, so we'll move on to the next requirement.

### Step 2

Now we have our first set of code.  At this point we should store it in our source control system to save our place in case we have an issue or mess things up.  After that, we pick the next requirement to work.

**Requirement 2: A rectangle to hold a width and height**

This one should be easy.  We'll first define the test again.

```java
@Test
void testWidthAndHeight() {
 final Rectangle rectangle = new Rectangle();

 assertEquals(0, rectangle.getWidth());
 assertEquals(0, rectangle.getHeight());

 rectangle.setWidth(25);
 rectangle.setHeight(45);

 assertEquals(25, rectangle.getWidth());
 assertEquals(45, rectangle.getHeight());
}
```

Now create the code to satisfy it.

```java
public class Rectangle {
  private int height = 0;
  private int width = 0;
  private int x = 0;
  private int y = 0;

  public int getHeight() {
    return height;
  }

  public int getWidth() {
    return width;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }
}
```

Now we can run the tests and make sure everything passes.

![Req2: Test Pass](/images/intro-to-java/tdd-req2-testresult-pass.png)

Again, we check for duplication and areas to refactor.   Do you notice any?   In this case, our test now has some duplication.  Both test methods have the same line.

```java
final Rectangle rectangle = new Rectangle();
```

JUnit provides a method for defining something that should be setup for each test by annotating with the @BeforeEach clause.   We can move the construction of the rectangle into the setup block so it is constructed for us.   This way it's newly constructed for each test, removing the extra lines from our test.

```java
class RectangleTest {
  
  private Rectangle rectangle = null;
  
  @BeforeEach
  void setUp() throws Exception {
    rectangle = new Rectangle();
  }

  @Test
  void testPositionXandY() {
    assertEquals(0, rectangle.getX());
    assertEquals(0, rectangle.getY());

    rectangle.setX(10);
    rectangle.setY(50);

    assertEquals(10, rectangle.getX());
    assertEquals(50, rectangle.getY());
  }

  @Test
  void testWidthAndHeight() {
    assertEquals(0, rectangle.getWidth());
    assertEquals(0, rectangle.getHeight());

    rectangle.setWidth(25);
    rectangle.setHeight(45);

    assertEquals(25, rectangle.getWidth());
    assertEquals(45, rectangle.getHeight());
  }
}
```

Now that we have removed the duplication, we run the test again to make sure our refactor didn't break anything.  The key to refactoring is to not change the functionality, just the organization.  Requirement 2 is now complete, so we'll store that in our source control and pick another requirement.

![Req2 Test Pass Refactor](/images/intro-to-java/tdd-req2-testresult-refactor-pass.png)

### Step 3

A circle and rectangle seem like the exact same thing.   Thus, we can attack the fourth and fifth requirement pretty much by duplicating the rectangle, which will meet two more requirements.  

**Requirement 4: A circle to hold a position of X and Y** 
**Requirement 5: A circle to hold a width and height**

To begin this we will just copy Rectangle and RectangleTest and rename each to Circle and CicrcleTest.  We'll then make internal updates to replace any construction of Rectangle to be Circle in the test so we end up with the following test and implementation.

```java
class CircleTest {

  private Circle circle = null;

  @BeforeEach
  void setUp() throws Exception {
    circle = new Circle();
  }

  @AfterEach
  void tearDown() throws Exception {}

  @Test
  void testPositionXandY() {
    assertEquals(0, circle.getX());
    assertEquals(0, circle.getY());

    circle.setX(10);
    circle.setY(50);

    assertEquals(10, circle.getX());
    assertEquals(50, circle.getY());
  }

  @Test
  void testWidthAndHeight() {
    assertEquals(0, circle.getWidth());
    assertEquals(0, circle.getHeight());

    circle.setWidth(25);
    circle.setHeight(45);

    assertEquals(25, circle.getWidth());
    assertEquals(45, circle.getHeight());
  }
}
```

And the Circle Class implementation.

```java
public class Circle {
  private int height = 0;
  private int width = 0;
  private int x = 0;
  private int y = 0;

  public int getHeight() {
    return height;
  }

  public int getWidth() {
    return width;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }
}
```

And we run the tests again and everything should pass.  We are now up to 4 total tests. 

![Req4 & 5: Test Pass](/images/intro-to-java/tdd-req4_5-testresult-pass.png)

Since we copied the *Rectangle* we now have a lot of duplication in this.   There is a lot we can do to correct it and this is why we need all those tests.   So the first step is to strip out the duplication of the *Rectangle* and the *Circle*.   This can be done by creating a class which we'll call *Shape* as the parent of the *Rectangle* and *Circle*.  It will look like the following and remove all of the content from the *Rectangle* and the *Circle*.

```java
public class Shape {
  private int height = 0;
  private int width = 0;
  private int x = 0;
  private int y = 0;

  public int getHeight() {
    return height;
  }

  public int getWidth() {
    return width;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }
}
```

This makes the *Rectangle* and *Circle* implementation look like this:


```java
public class Rectangle extends Shape {
}
```

```java
public class Circle extends Shape {
}
```

Now if we rerun the tests we get:

![Req4 & 5: Refactored Test Pass](/images/intro-to-java/tdd-req4_5-testresult-refactory1-pass.png)

Finally, we need to clean up the tests, since they are highly redundant.  For this we'll take the *RectangleTest*, rename it to *ShapeTest*, and delete the *CircleTest*.   We then rerun the tests and have gone back down to 2 tests.

![Req4 & 5: Refactor Test Pass](/images/intro-to-java/tdd-req4_5-testresult-refactory2-pass.png)

Wow, that was a lot to change and refactor, but now we have a parent, *Shape*, that controls all the data for each of our objects.   The test proves everything is working, and now we are confident that everything is correct and simplified.   We covered most of the requirements, so let's finish this off.

### Step 4

Picking another requirement, we need to figure out to calculate area.

**Requirement 3: Rectangle can calculate the area**

We know area of a rectangle is length times width, so we should be good to go.   We'll start by adding this to *Rectangle* to meet the requirement

```java
class RectangleTest {

  @Test
  void testGetArea() {
    final Rectangle rectangle = new Rectangle();

    rectangle.setHeight(2);
    rectangle.setWidth(5);

    assertEquals(10, rectangle.getArea());
  }
}
```

We have to create a new test for *Rectangle* now that we have a special area method in the *Rectangle* class.  Here is the code to satisfy the test.

```java
public class Rectangle extends Shape {

  public double getArea() {
    return getHeight() * getWidth();
  }
}
```

![Req3: Test Pass](/images/intro-to-java/tdd-req3-testresult-pass.png)

### Step 5

We have one requirement left.  Let's take it and complete this so we can be paid.

**Requirement 6: Circle can calculate the area** 
 
Since we just implemented area on a rectangle, we can do the same thing with the circle, since its just calculated slightly differently.  We should be smart about this.  Since we know *Rectangle* and *Circle* already extend Shape, and they both need area, we should move it up to the *Shape* object.   However, we know they have to be unique in each object.  This means we have a couple of options.  *Shape* could implement a method that does nothing other than throw an exception if used (UnimplementedMethodException), or we could make the *Shape* abstract.  Going the abstract approach seems better since *Shape* really can't exist on its own.  Thus, we start by converting the *Shape* to an abstract class and renaming to be more specific.  Next, we define the method we had in *Rectangle* in *AbstractShape* and make *Rectangle* and *Circle* override it.

```java
public abstract class AbstractShape {
  private int height = 0;
  private int width = 0;
  private int x = 0;
  private int y = 0;

  /**
   * Constructs this object.
   */
  public AbstractShape() {}

  /**
   * Calculates the area.
   *
   * @return The area.
   */
  public abstract double getArea();

//Rest cut for simplify
```

Now we can rerun our tests to prove we didn't break anything. 


![Req6: Test Refactor 1 Pass](/images/intro-to-java/tdd-req6-testresult-refactory1-pass.png)

Now that this is cleaned up, we can implement our *Circle* area and finish this off.  Run our test and tell the customer their requirements are met with proof.

```java
class CircleTest {

  @Test
  void testGetArea() {
    final Circle circle = new Circle();

    circle.setHeight(1);
    circle.setWidth(1);

    // The 5 checks how far in the precision of the result should be matched.
    assertEquals(Math.PI, circle.getArea(), 5);
  }
}
```

And finally the last implementation.

```java
public class Circle extends AbstractShape {

  @Override
  public double getArea() {
    return Math.PI * Math.pow((getWidth() / 2), 2);
  }
}
```

And lets prove we are all done.

![Req6: Test 1 Pass](/images/intro-to-java/tdd-req6-testresult-pass.png)

This is a full sequence of TDD.  Along the way, we built a complete set of objects with tests that prove it will work.  Since we used JUnit, they run fast and can be automated.   Also, the code is well organized and lean and should be easy to maintain.   Finally, since we source controlled along the way we have history of how we got to this point.  All in the day of a good software developer.

## Conclusion

As shown, TDD is a powerful concept in designing code and implementing with confidence that what you are building works.   Unit testing doesn't take the place of full scale testing (integration, system, acceptance) but it is the first step in creating high quality code.   It also will give you the confidence that the code works and as you evolve it over time you'll be comfortable with making changes.   Code should evolve and change over time; therefore, having good tests is very important and beneficial.

*JUnit* is a great framework designed to simplify unit testing and make the effort of building tests much easier.   Combined with mocking frameworks like *Mockito*, there is no limiting what can be done.