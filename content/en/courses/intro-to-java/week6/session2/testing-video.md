---
title: "Testing - Video"
date: 2021-02-17
description: Testing what you develop with a test first mind set.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/testing-screen.png)](https://youtu.be/WzXg-zgWZVs)

## Slides

{{< revealjs >}}

## Writing Code

- Writing code is complex 
- Writing complex applications is error prone
- Proving they work is difficult
- Having customers find issues can be disastrous
- How do you prove it works

--

## Testing

- Testing code is important
   - Probably more important then what you write
- Different types of testing
   - System/Acceptance Testing
   - Integration Testing
   - Unit Testing

--

## System/Acceptance Testing

- Focuses on the full system and parts
   - Hardware, Software
- Test as close to production as possible
- Meeting the requirements
- Customer sell off
- Time consuming and may be expensive

--

## Integration Testing

- Test the application with other parts
- Some parts may still be simulated
- Performance and Load testing

--

## Unit Testing

- Unit - Small parts of the application 
   - Lines of Code
- Reduce need for dependencies
- Test in isolation
- Usually fast and easy setup
- Should be automated

---

## Test Driven Development

- TDD
- A way of writing code starting with the test
- Red - Green - Refactor
- Create the test
   - Then write the code to satisfy it
      - Then remove duplication
--

## Defining For Tests

- Test first mind set changes the way you write code
- Dependencies between objects causes coupling
- Testing a method that relies on something else
- TDD drives objects to be constructed outside 
   - Then passed into the using object
   - Called **Dependency Injection**

---

## JUnit

- xUnit framework
- The standard in Unit Testing
- Simple framework
   - Test Runner
   - Assert methods
- Integrated into Eclipse

--

## Mocking

- Dependencies are inevitable
   - One object uses another object
- Testing can be more difficult
- Mocking allows a way to fake the dependency
- Puts the test in control of the input and output
- Mockito 

---

## TDD Walk-Through

- Starts with what you want to build
- Turns into a set of Requirements
- Then building it
- Finally proving it works 
- Give to the customer
- This will walk through how that works with TDD

-- 

## Requirements

1. A rectangle to hold a position of X and Y
2. A rectangle to hold a width and height 
3. Rectangle can calculate the area
4. A circle to hold a position of X and Y
5. A circle to hold a width and height 
6. Circle can calculate the area

--

## Requirement 1

**A rectangle to hold a position of X and Y**

--

## Red - Write the test

```java
class RectangleTest {
  @Test
  void testPositionXandY() {
    final Rectangle rectangle = new Rectangle();

    assertEquals(0, rectangle.getX());
    assertEquals(0, rectangle.getY());

    rectangle.setX(10);
    rectangle.setY(50);
    
    assertEquals(10, rectangle.getX());
    assertEquals(50, rectangle.getY());
  }
}
```

--

## Green - Write the code

```java
public class Rectangle {
  private int x = 0;
  private int y = 0;

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }
}
```

--

## Green - Run the test

![Req1: Test Pass](/site/images/intro-to-java/tdd-req1-testresult-pass.png)

--

## Refactor

- Check the code for duplication
- Refactor if needed

Nothing here

--

## Requirement 2

**A rectangle to hold a width and height**

--

## Red - Write the test

```java
@Test
void testWidthAndHeight() {
 final Rectangle rectangle = new Rectangle();

 assertEquals(0, rectangle.getWidth());
 assertEquals(0, rectangle.getHeight());

 rectangle.setWidth(25);
 rectangle.setHeight(45);

 assertEquals(25, rectangle.getWidth());
 assertEquals(45, rectangle.getHeight());
}
```

--

## Green - Write the code

```java
public class Rectangle {
  private int height = 0;
  private int width = 0;
  
  //Previous Code not shown

  public int getHeight() {
    return height;
  }

  public int getWidth() {
    return width;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public void setWidth(int width) {
    this.width = width;
  }
}
```

--

## Green - Run the test

![Req2: Test Pass](/site/images/intro-to-java/tdd-req2-testresult-pass.png)

--

## Refactor

- Test has some duplication

```java
final Rectangle rectangle = new Rectangle();
```

- JUnit - @BeforeEach

--

## Refactored Code

```java
class RectangleTest {
  
  private Rectangle rectangle = null;
  
  @BeforeEach
  void setUp() throws Exception {
    rectangle = new Rectangle();
  }

  @Test
  void testPositionXandY() {
    assertEquals(0, rectangle.getX());
    assertEquals(0, rectangle.getY());

    rectangle.setX(10);
    rectangle.setY(50);

    assertEquals(10, rectangle.getX());
    assertEquals(50, rectangle.getY());
  }

  @Test
  void testWidthAndHeight() {
    assertEquals(0, rectangle.getWidth());
    assertEquals(0, rectangle.getHeight());

    rectangle.setWidth(25);
    rectangle.setHeight(45);

    assertEquals(25, rectangle.getWidth());
    assertEquals(45, rectangle.getHeight());
  }
}
```

--

## Green - Rerun test

![Req2 Test Pass Refactor](/site/images/intro-to-java/tdd-req2-testresult-refactor-pass.png)

--

## Requirement 4 & 5

**A circle to hold a position of X and Y**

**A circle to hold a width and height**

--

## Red - Write the test

```java
class CircleTest {

  private Circle circle = null;

  @BeforeEach
  void setUp() throws Exception {
    circle = new Circle();
  }

  @AfterEach
  void tearDown() throws Exception {}

  @Test
  void testPositionXandY() {
    assertEquals(0, circle.getX());
    assertEquals(0, circle.getY());

    circle.setX(10);
    circle.setY(50);

    assertEquals(10, circle.getX());
    assertEquals(50, circle.getY());
  }

  @Test
  void testWidthAndHeight() {
    assertEquals(0, circle.getWidth());
    assertEquals(0, circle.getHeight());

    circle.setWidth(25);
    circle.setHeight(45);

    assertEquals(25, circle.getWidth());
    assertEquals(45, circle.getHeight());
  }
}
```

--

## Green - Write the code

```java
public class Circle {
  private int height = 0;
  private int width = 0;
  private int x = 0;
  private int y = 0;

  public int getHeight() {
    return height;
  }

  public int getWidth() {
    return width;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }
}
```

--

## Green - Run the test

![Req4 & 5: Test Pass](/site/images/intro-to-java/tdd-req4_5-testresult-pass.png)

--

## Refactor

- Circle is a copy of Rectangle
- Can we reduce that
- Sure we can
- Both are a Shape
   - *Inheritance*

--

## Refactor - Shape

```java
public class Shape {
  private int height = 0;
  private int width = 0;
  private int x = 0;
  private int y = 0;

  public int getHeight() {
    return height;
  }

  public int getWidth() {
    return width;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }
}
```

--

## Refactor - Rectangle/Circle

```java
public class Rectangle extends Shape {
}
```

```java
public class Circle extends Shape {
}
```

--

## Green - Run the test

![Req4 & 5: Refactored Test Pass](/site/images/intro-to-java/tdd-req4_5-testresult-refactory1-pass.png)

--

## Refactor - Test Cleanup

- RectangleTest rename to ShapeTest
- CircleTest deleted

--

## Green - Run the test

![Req4 & 5: Refactor Test Pass](/site/images/intro-to-java/tdd-req4_5-testresult-refactory2-pass.png)

--

## Requirement 3

**Rectangle can calculate the area**

--

## Red - Write the test

```java
class RectangleTest {

  @Test
  void testGetArea() {
    final Rectangle rectangle = new Rectangle();

    rectangle.setHeight(2);
    rectangle.setWidth(5);

    assertEquals(10, rectangle.getArea());
  }
}
```

--

## Green - Write the code

```java
public class Rectangle extends Shape {

  public double getArea() {
    return getHeight() * getWidth();
  }
}`

--

## Green - Run the test

![Req3: Test Pass](/site/images/intro-to-java/tdd-req3-testresult-pass.png)

--

## Refactor

- Looking good

--

## Requirement 6

**Circle can calculate the area** 

--

## Little Thought

- Rectangle has getArea()
- Circle is going to need it
- We have a parent Shape
- Options
   - Implement getArea() in Shape 
   - Shape becomes abstract and defines it

--

## Abstract Shape

```java
public abstract class AbstractShape {
  private int height = 0;
  private int width = 0;
  private int x = 0;
  private int y = 0;

  /**
   * Constructs this object.
   */
  public AbstractShape() {}

  /**
   * Calculates the area.
   *
   * @return The area.
   */
  public abstract double getArea();

//Rest cut for simplify
```

--

## Green - Run the test

![Req6: Test Refactor 1 Pass](/site/images/intro-to-java/tdd-req6-testresult-refactory1-pass.png)

--

## Red - Write the test

```java
class CircleTest {

  @Test
  void testGetArea() {
    final Circle circle = new Circle();

    circle.setHeight(1);
    circle.setWidth(1);

    // The 5 checks how far in the precision of the 
    // result should be matched.
    assertEquals(Math.PI, circle.getArea(), 5);
  }
}
```

--

## Green - Write the code

```java
public class Circle extends AbstractShape {

  @Override
  public double getArea() {
    return Math.PI * Math.pow((getWidth() / 2), 2);
  }
}
```

--

## Green - Run the test

![Req6: Test 1 Pass](/site/images/intro-to-java/tdd-req6-testresult-pass.png)

--

## Refactor

- All done
- Code is clean
- Test pass

--

## TDD Wrap Up

- TDD drove what objects we needed
   - Discovered solutions along the way
- General idea of what to build
   - TDD flushes out the details
- Tests keep you confident 
   - Comfortable with change

---

## Conclusion

- TDD is powerful
- Unit testing is very important
- Improves comfort with change
- JUnit is the standard in Unit testing
- Mocking can help define tests

{{< /revealjs >}}