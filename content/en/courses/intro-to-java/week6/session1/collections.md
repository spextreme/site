---
title: "Collections"
date: 2021-02-17
description: The java collections framework.
draft: false
collapsible: false
weight: 15
---

## Introduction

As discussed, primitive data types (int, char, boolean, etc...) are designed to hold a single value.   This is beneficial, but when you need a large collection of data it can be very difficult to create all those variables.   

An array exists to hold a collection of like variables that are independently index.  This allows creating a single variable to hold all the values that can then be referenced by their index.  Arrays are beneficial, but they are limited to only holding one data type at a time, being a fixed size, and being unable to easily grow.

Java has built a powerful framework around arrays called Collections and improved the way they work.

In this section, we'll cover arrays in detail and dive into the powerful [Java Collections Framework](https://docs.oracle.com/javase/8/docs/technotes/guides/collections/index.html).


## Arrays

An array is based on primitive data types (int, char, boolean, etc...), and it's generally intended to only hold one of those types at a time.   This is because, when an array is created, it's given a data type and a size.

To create an array, the open and closed square brackets *[]* are used.  When defining an array, the [] are used on the type side of the variable to denote that it will be an array.  On the right side of the equals, where you create the array, you specify a number between the brackets which tells it how many elements to allocate for this array.

```java
char[] charArray = new char[5];
```

This will create an array that can store up to 5 chars.  To store data, the [] brackets are used to place the index value in for assigning the value to the spot in the array.

```java
charArray[0] = 'h';
charArray[1] = 'e';
charArray[2] = 'l';
charArray[3] = 'l';
charArray[4] = 'o';
```

When an array is created, it starts at the index value of zero and counts up.  In the above, we put each character into a given index inside the array using [] again to identify the index number.   If we tried to place something in the array that is past index 4 (say index [5]), we would get a *RuntimeException* of *IndexOutOfRange* because the array was only established to hold 5 (0-4) items.

An alternate method for defining an array allows you to create it with the elements populating it.  This can be helpful because you won't need to figure out how many elements to allocate since this path will allocate what is defined.

```java
char[] charArray = {'h', 'e', 'l', 'l', 'o'};
```

This does the same as above but in a single line.

Another benefit to arrays in Java is that they provide a method to get the number of spaces allocated for the given array.  Java arrays provide a property (not a method), *length* that can be used to get the size of an array.

```java
int size = charArray.length; //will return 5
```

This can be very useful when setting up a loop to iterate over the elements of the array.   Remember the for loop with the index value.  Here is a good use for it with an array to print out each element.

```java
for (int i = 0; i < charArray.length; i++) {
   System.out.print(charArray[i]);
}
```

The key parts to remember are:
- Arrays only hold a single data type
- Arrays start at index 0
- [] are used to define the type, create, and used the index.
- *length* property gets the size

## Collections

A collection is a group of items that are somehow related.  In Java, a collection is a base interface that provides a foundation for a much more dynamic and powerful array.  Remember, an array is a set of things that has been pre-allocated to hold a single type of data.  

Java solved many of the limitations of an array with Java Collections Framework.  A collection is a dynamic array of items that can grow if more items are added and it needs more space.   

The Java Collection Framework is designed to hold *Object*s so any type may be used so long as it is an object.   If you need to hold primitive types, they must be converted to their object form ( new Integer(1), new Float(1.3f), etc.).

The Collection interface also hosts a set of methods to work with the content it hosts.

- add - Used to add an element.
- clear - Removes all elements to empty the collection.
- contains - Returns *true* if the given item is in the collection.
- isEmpty - Returns *true* if the collection has no elements.
- iterator - Useful for looping over the data set.
- remove - Removes the given item from the collection.
- size - Gets the number of items in the collection.
- toArray - Converts the collection to an array.

\* this is not a full list.  See the [Javadoc](https://docs.oracle.com/javase/8/docs/api/java/util/Collection.html) for all methods.

There are a lot of extensions from the Collections interface as well as a lot of implementations.   We'll cover a few of the basics and most used instances.

### List

*List* is another interface defined from the *Collection* interface.  The *List* exists as an ordered collection (also known as a sequence).  This defines the type of collection to allow the user to control the exact order of the items in the collection.  This also provides a why to search for items in the list as well as get items at their index value like an array.

This has all the methods of collection but adds a few to support the new capability. 

- add - Additional add method to support putting an item in at a specific index.
- get - Gets an item from the given index.
- indexOf - Finds the first index value of the given item or -1 if not found.
- lastIndexOf - Finds the last index value of the given item or -1 if not found.
- remove - Additional remove to remove an item from a given index.
- set - Allow setting a value at the specific index.
- subList - Pull out a section of the list between a starting and ending index value.

\* this is not a full list.  See the [Javadoc](https://docs.oracle.com/javase/8/docs/api/java/util/List.html) for all methods.

As you can see, the *List* provides a lot more on top of *Collection* to allow the developer to control the contents more easily and work with it like an array.

#### ArrayList

The *ArrayList* is a complete concrete implementation of the *List* interface.  It can contain *null* items and duplicates, and will dynamically grow as more items are added.   The *ArrayList* is one of the more heavily used collections items because of how close it is to an array, ease of use, and the dynamic growth support.   

#### Other List Implementations

There are many other list implementations, and it's always best to look at the Javadoc and see what implementations are built from the List interface.   A few to know about include:

- [LinkedList](https://docs.oracle.com/javase/8/docs/api/java/util/LinkedList.html) - An implementation of a link list.
- [Stack](https://docs.oracle.com/javase/8/docs/api/java/util/Stack.html) - A last in first out (LIFO) model.
- [Vector](https://docs.oracle.com/javase/8/docs/api/java/util/Vector.html) - Like ArrayList but thread safe

### Set

The *Set* is another interface defined from the *Collection* interface.   The *Set* is defined to make a collection of items that contain no duplicate items.   What this means is that no two items can be *equal* as defined by the object equal method.  This also means that only one item on the list can be *null*.

This does not define any new relative methods over the *Collection* interface.

There are a few implementations of the *Set* interface worth noting.

- [HashSet](https://docs.oracle.com/javase/8/docs/api/java/util/HashSet.html) - Uses a hash table to back the set unique items and searching.
- [LinkedHashSet](https://docs.oracle.com/javase/8/docs/api/java/util/LinkedHashSet.html) - A hash set implementation with the link list model.

### Map

The *Map* interface is used to define a key/value pair representation of a collection.  The key is an identifier within the map to the value it holds.    The key must be unique within the map as a key can only represent one value at any given time.   

The map also makes three separate collections views available.  One is the set of keys (*Set*), one is of the values (*Collection*) and the last is of the key/value pairs (*Map*).

### Iterator

An *Iterator* is an interface defined for supporting stepping (iterating) through a collection of items.   When working with any collection, it is possible to use an *iterator* to step through the items.

In this example, it creates an *ArrayList* with 3 *String* instances.   The for loop then creates a new *intr* variable that is set to the lists iterator.    The *Iterator* interface has a few useful methods for getting the next item (*next()*) and telling if the iterator has more items (*hasNext()*).  Also has a method for removing the current item (*remove()*).  With the for loop's initialization block and conditional block, this fits very well.

```java
final List<String> list = new ArrayList<>();

list.add("Hello");
list.add("World");
list.add("one");

for (final Iterator<String> intr = list.iterator(); intr.hasNext();) {

   System.out.println(intr);
}
```

As we talked about, its also easier to create a for loop with the collections framework by using the *for-each* method.  Here is the same example from above as a *for-each* loop:

```java
final List<String> list = new ArrayList<>();

list.add("Hello");
list.add("World");
list.add("one");

for (final String str : list) {

   System.out.println(str);
}
```

The *for-each* is the most useful method at this point.  Its addition to the language, it is much simpler to use, reduces casting, and reduces the need for extra checking.

### Generics

In all the examples above, Generics was used but it wasn't really explained.  In Java, the Collections Framework supports using Generics to simplify the need to cast an object to the proper type (From *Object* back to its original type).   Generics allow a collection item to be set to a defined type so casting isn't necessary and type checking is enforced.

Casting is used to take an object type and convert it to another type.  This will only directly work if the object types are compatible (inheritance).   For example to cast an *Object* to a *String* will only work if the item is a *String*.  In this example we'll create an ArrayList holding *Object*s which means it could be anything.  We will add *String*s to this list and then cast them from *Object* to *String* in a loop.

```java
final List str = new ArrayList<>();

str.add("Hello");
str.add("World");

for (final Object obj : str) {

   // Casting from object to string only works cause
   // our list is made up of only strings.
   final String objStr = (String) obj;

   System.out.println(objStr);
}
```

Now if we were to add another type of object (say *Integer*) this code would throw a *ClassCastException* when it hits that item in the list.  

Generics were created to reduce the need to perform casting as well as type checking the collection to prevent this case form occurring.

By using the object name plus the *<>* characters we can add a type that the collection will hold. The above code would then be converted to the following.

```java
final List<String> str = new ArrayList<>();

str.add("Hello");
str.add("World");

for (final String objStr : str) {

   System.out.println(objStr);
}

```

As this shows, adding *<String>* to the list object definition means this will only hold *String* objects.  If the *.add()* was called with something that wasn't a *String*, then the compiler would report the error before any attempt was made to use it.   It also guarantees that our loop will only return *String* instances and the objects will be of that type so no casting is required.

Generics are a very powerful and beneficial capability in the Collections framework and the overall language.   This is just scratching the surface, but since this is only the basics, this will get us started.

## Conclusion

Arrays, Collections, Generics, oh my!  This was a lot of ground to cover, but this is one of the main things that we'll be heavily leveraging in almost any application.   Storing data is very important to most applications and being able to hold them, loop through them, and even search to find information will become very useful.

The Java Collections Framework is one of the more important advancements in the language and provides many objects that we only started to touch on.  As you develop more applications, you'll find there are a lot of objects in the Collections Framework that will provide an implementation for your needs.
