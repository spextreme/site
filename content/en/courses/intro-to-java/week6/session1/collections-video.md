---
title: "Collections - Video"
date: 2021-02-17
description: The java collections framework.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/collections-screen.png)](https://youtu.be/6tL9iuUreBA)

## Slides

{{< revealjs >}}

## Collections Framework

- Primitive types hold a single value only
- Collections of data would be difficult this way
- Arrays provide a way to group a single type together
- Arrays and the Java Collections Framework

---

## Arrays

- Based on primitive data types
- Can be used with objects
- Holds one data type
- Fixed size
- *[]* used for all array work
- Start at index 0

--

## Array Creation

```java
char[] charArray = new char[5];

charArray[0] = 'h';
charArray[1] = 'e';
charArray[2] = 'l';
charArray[3] = 'l';
charArray[4] = 'o';
```

or

```java
char[] charArray = {'h', 'e', 'l', 'l', 'o'};
```

--

## Array Elements

```java
System.out.println( charArray[0] ); // h
System.out.println( charArray[3] ); // l
System.out.println( charArray[5] ); // Error: Array out of bounds
```

--

## Array Length

- length property
- Not a method
- Gets the allocated size

```java
int size = charArray.length;
```

--

## Arrays + Loops

- Loops are great for arrays

```java
for (int i = 0; i < charArray.length; i++) {
   System.out.print(charArray[i]);
}
```

---

## Collections

- A group of related items
- Java Collections Framework
- Based on a core interface *Collection*
- Dynamic by nature
- Holds Objects (no Primitive Types)

--

## Collection Method

- add - Adds an element
- clear - Removes all elements
- contains - *true* if item in collection
- isEmpty - *true* if no elements
- iterator - looping
- remove - Removes item from collection
- size - Gets the number of items
- toArray - Converts to an array

--

## List

- Another interface
- Ordered
- Supports searching
- Extends from Collection

--

## List Methods

- add - Add item at a specific index
- get - Gets item from index
- indexOf - Finds the first item's index
- lastIndexOf - Finds the last item's index
- remove - Remove from index
- set - Set at index
- subList - Section of the list

--

## ArrayList

- Concrete implementation of List
- Dynamic growth
- May contain *null*s
- Heavily used
- Improved over the array []

--

## *List* Implementations

- LinkedList- An implementation of a link list
- Stack - A last in first out (LIFO) model
- Vector - Like ArrayList but thread safe

--

## Set

- Interface for unique items
- Extends *Collection*
- No new defined methods
- Can have only one *null*
- All items must be ! *equal()*

--

## Set Implementations

- HashSet - Hash table backed of unique items
- LinkedHashSet - Link list model

---

## Map

- Interface for Key/Value pairs
- Key must be unique
- Key can not be *null*
- Provides 3 Collections
   - Keys - Set
   - Values - Collection
   - Key/Values - Map

---

## Iterator

- Interface for stepping through a collection
- Used in loops
- Interface define a
   - *next()*
   - *hasNext()*
   - *remove()*
- Useful in a for loop

--

## Iterator For Loop

```java
final List< String > list = new ArrayList<>();

list.add("Hello");
list.add("World");
list.add("one");

for (final Iterator< String > intr = list.iterator();
               intr.hasNext(); ) {

   System.out.println(intr.next());
}
```

---

## For Each

```java
final List< String > list = new ArrayList<>();

list.add("Hello");
list.add("World");
list.add("one");

for (final String str : list) {

   System.out.println(str);
}
```

---

## Generics

- Collections make heavy use of
- Generics used to define the type
- Compile time type checking
- Reduce casting
- Very powerful

--

## Without Generics

```java
final List str = new ArrayList<>();

str.add("Hello");
str.add("World");

for (final Object obj : str) {

   // Casting from object to string only works cause
   // our list is made up of only strings.
   final String objStr = (String) obj;

   System.out.println(objStr);
}
```

--

## With Generics

```java
final List< String > str = new ArrayList<>();

str.add("Hello");
str.add("World");

for (final String objStr : str) {

   System.out.println(objStr);
}

```
---

## Conclusion

- Arrays []
- Collection
   - List
   - Set
   - Map
- Iterator
- Generics

{{< /revealjs >}}