---
title: "Annotations - Video"
description: "Understanding Java Annotations."
date: 2021-04-05
draft: false
enableToc: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/annotations-screen.png)](https://youtu.be/uvm4LuDa9FA)

## Slides

{{< revealjs >}}

## Annotations

- Been using them throughout the course
   - *@Override*, *@Test*
- Unique feature to Java
- Doesn't related to the code you write

---

## Purpose

- Metadata for your code
- Not related to your code
- Processed by other tools and frameworks

---

## Using

- Bound to Classes, Methods or Fields
- *@*

```java
  @Deprecated
  public void outdatedMethod() {
    // This is no longer needed.
  }
```

--

## Single Parameter

- Annotations may take in a parameter
- ( Value )

```java
@SuppressWarnings({"rawtypes", "unchecked"})
public void aMethodWithAWarning() {

 // This would report a warning in Eclipse because we didn't 
 // provide the list with a type (rawtype warning).
 // Using the annotation we can tell Eclipse to ignore this 
 // warning.
 final List unTypedList = new ArrayList();

 // Unchecked is reported when passing a string into a list 
 // that is not typed as a String. 
 // Rawtype lists are actually Object based since anything 
 // may be place in the list then.
 unTypedList.add("Hello");
}
```

--

## Multiple Parameter

- Annotations may take in multiple parameters
- Parameter = Value

```java
  @BeanProperty(description = "A simple java bean example", 
                     expert = false, required = false)
  private String myBean() {
    
    return "BeanProperty";
  }
```

--- 

## Declaring

- Creating a custom annotation is pretty easy
- *@interface*

```java
public @interface MyCustomAnnotation {
  String owner();

  String permissions();

  boolean required();
}
```

--- 

## Conclusion

- Metadata for your code
- @ symbol above or next to the item to bind to
- Used by tools or frameworks
- Easy to create with the *@interface* keyword

{{< /revealjs >}}