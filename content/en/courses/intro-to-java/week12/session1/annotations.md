---
title: "Annotations"
description: "Understanding Java Annotations."
date: 2021-04-05
draft: false
enableToc: false
weight: 15
---

## Introduction

Throughout the course, we've been using Java Annotations for different things (@Override, @Test, etc...) but we haven't really discussed annotation or how they work.

Java Annotations are an interesting concept and provide a unique feature for the language and tools that use them.  They don't directly relate to the code you're writing but they provide other tools capabilities to help in your development or building your application.

## Purpose

Java Annotation are a way to provide metadata about your code.  Metadata is extra information provided that may then be used by other tools.   Annotations are not related to your code (meaning they aren't used by your code as part of its execution) but are then used by other tools or frameworks.  This isn't to say your code can't make use of them but normally you won't be parsing the information directly to make your application function.

Annotations are processed by other tools like an IDE or frameworks that parses your code and finds the annotations.   It then used them to perform an action for whatever the tools purpose is.  For example, by placing the @Override annotation, Eclipse will parse it and will perform validation on your code to verify it is properly setup as a implementation of the parents method signature.    If Eclipse finds the annotation and the method doesn't align then it reports an error.  There are many defined annotations in the Java language to support IDE and other tools.

As we've discussed, frameworks are also a big user of annotations.   Spring Framework for example leverages many different annotations to parse the code and perform dynamic mapping.  For example, there is an annotation called *@Autowire* that can be placed on a class's field, and Spring will find that, determine the type of object that its bound to and will automatically construct an instance of that type, and bind it to the field.  By simply placing this one little annotation, Spring can hand objects around to support loading of your application and simplify the construction and initialization (called Dependency Injection).

## Using

Now that we understand the purpose of annotations, we can better understand how to leverage them in code.  Annotations may be bound to classes, methods or fields, and are always prepended with the *'@'* (at) symbol.  Java will bind the annotation to the class, method, or field that it is aligned to (just in front of or the line above).

```java
  @Deprecated
  public void outdatedMethod() {
    // This is no longer needed.
  }
```

Annotations may also contain extra data in the form of parameters.  This means that the annotation will look more like a method call by passing parameters in. In the following example we are going to use the *@SupressWarnings* to tell Eclipse to ignore (not report on) the *rawtypes* and *unchecked* errors that would be reported for having a list without a type and adding a *String* where the type is unknown. 

```java
@SuppressWarnings({"rawtypes", "unchecked"})
public void aMethodWithAWarning() {

 // This would report a warning in Eclipse because we didn't provide the
 // list with a type (rawtype warning).
 // Using the annotation we can tell Eclipse to ignore this warning.
 final List unTypedList = new ArrayList();

 // Unchecked is reported when passing a string into a list that is not typed as a String.
 // Rawtype lists are actually Object based since anything may be place in the list then.
 unTypedList.add("Hello");
}
```

In providing parenthesis, you are technically passing in arguments like a method.   The main difference is you're really just setting values for the annotation to use (see Declaring for more details).   If the annotation takes in multiple variables, then you'll have to use a variable = value model.

```java
  @BeanProperty(description = "A simple java bean example", expert = false, required = false)
  private String myBean() {
    
    return "BeanProperty";
  }
```

This shows how the @BeanProperty is taking in three values to set the description, expert and required values.   They all use a Java types (*String*, *boolean*, etc..) so it should look familiar.

## Declaring

Defining your own custom annotation is very easy in Java.   Why you'd want to is very limited but if you're building a tool or a framework this may be something you want to do.

To create an annotation you'll simply define it as follows

```java
public @interface MyCustomAnnotation {
  String owner();

  String permissions();

  boolean required();
}
```

As this shows, you use the *@interface* to define the annotation and then put the type with the variable name like a method definition.  This will then make the parameters be *owner*, *permissions* and *required*.

```java
@MyCustomAnnotation(owner = "SP extreme", permissions = "Use as an example only", required = true)
public class ExampleAnnotationsUse {
   ...
}
```

## Conclusion

Annotation are a way to provide metadata about your code so that other tools or frameworks can use it for other purposes.   Generally, your code will not use the annotations directly.

You may provide annotations to help your IDE perform some actions like ignore warnings or verify the code is properly defined.  You may add them to support unit testing like the @Test annotation provided by the JUnit framework.

Annotations can be very helpful and have provided Java a unique solution to allow tools to post-process your applications code and add helpful solutions.
