---
title: "Wrap Up"
description: "Closing out on the remainder of introductory topics."
date: 2021-04-10
draft: false
enableToc: false
weight: 15
---

## Almost Done

At this point in the course we have pretty much covered most of the introductory topics and then some extra things.  This includes Java basics, the syntax, conditionals, looping, enums, and objects.   We also touched on many other things included source control, IDE (Eclipse), testing and frameworks (JUnit, Spring, etc...).   We really did cover a lot of topics.

At this point there are a few straggling concepts that we should cover to round out all the topics and skills learned.  This will cover the remaining topics:

- Recursion
- Classpath
- Garbage Collection
- Inner Classes
- Anonymous Classes

## Recursion

Recursion is a concept of a method that will call itself.   This means that the method will call itself over and over again until it hits a condition that cause it to return.   

Recursion is very useful in some cases, like traversing a file system tree.  Think about your operating system and the directory tree.  You have a directory that contains other directories and files.   So the function may first go into a directory get a list of items and if its of type directory it will go into that directory, get a list of files and repeat.  It will then only stop going down when it hits the lowest directory that contains no other directories.

The risk of this type of method is that you never hit an end to make it break the recursive call.  This means you just infinitely keep calling the function.  This will eventually cause the JVM to terminate with an error called a *StackOverflowError*.

A simple example of a bad recursion function would look something like:

```java
public void infiniteRecursion(int counter) {

 System.out.println(String.format("infiniteRecursion(%s)", counter));

 infiniteRecursion(++counter);

}
```

This would run infinitely but eventually the JVM will realize it and kill itself with a *StackOverflowError*.

```bash
...

infiniteRecursion(5411)
infiniteRecursion(5412)
infiniteRecursion(5413)
Exception in thread "main" java.lang.StackOverflowError
   at java.base/java.util.regex.Pattern$GroupHead.match(Pattern.java:4791)
   at java.base/java.util.regex.Pattern$Branch.match(Pattern.java:4736)
   at java.base/java.util.regex.Pattern$Branch.match(Pattern.java:4734)
   at java.base/java.util.regex.Pattern$Branch.match(Pattern.java:4734)
   at java.base/java.util.regex.Pattern$BranchConn.match(Pattern.java:4700)
   at java.base/java.util.regex.Pattern$GroupTail.match(Pattern.java:4850)
   at java.base/java.util.regex.Pattern$BmpCharPropertyGreedy.match(Pattern.java:4331)
   at java.base/java.util.regex.Pattern$GroupHead.match(Pattern.java:4791)
   at java.base/java.util.regex.Pattern$Branch.match(Pattern.java:4736)
   at java.base/java.util.regex.Pattern$Branch.match(Pattern.java:4734)
   at java.base/java.util.regex.Pattern$BmpCharProperty.match(Pattern.java:3951)
   at java.base/java.util.regex.Pattern$Start.match(Pattern.java:3606)
   at java.base/java.util.regex.Matcher.search(Matcher.java:1729)
   at java.base/java.util.regex.Matcher.find(Matcher.java:773)
   at java.base/java.util.Formatter.parse(Formatter.java:2702)
   at java.base/java.util.Formatter.format(Formatter.java:2655)
   at java.base/java.util.Formatter.format(Formatter.java:2609)
   at java.base/java.lang.String.format(String.java:2897)
   at com.spextreme.examples.recursion.RecursionExamples.infiniteRecursion(RecursionExamples.java:19)
   at com.spextreme.examples.recursion.RecursionExamples.infiniteRecursion(RecursionExamples.java:21)
   at com.spextreme.examples.recursion.RecursionExamples.infiniteRecursion(RecursionExamples.java:21)

...
```

A proper recursive function must have some method for stopping the recursive call.  This is usually performed by some condition that will ultimately end the recursive call so it can climb back up the call stack.  Here is a example of a recursive call that will return once the number ten (10) is hit.

```java
public void limitedRecursion(int counter) {

 System.out.println(String.format("limitedRecursion(%s)", counter));

 if (counter >= 10) {
   return;
 }

 limitedRecursion(++counter);
}
```

This will call itself ten times and then it will return which will cause each other method to return until the program hits the first time called and the main method and finally exits.

```bash
limitedRecursion(1)
limitedRecursion(2)
limitedRecursion(3)
limitedRecursion(4)
limitedRecursion(5)
limitedRecursion(6)
limitedRecursion(7)
limitedRecursion(8)
limitedRecursion(9)
limitedRecursion(10)
```

Recursive methods are very beneficial for some use cases but they should be coded with a true termination in mind.   Understand how the method will return will make infinite recursion much less frequent.

One last note is that all of this is called *Direct Recursion* which means the method calls itself.   There is a concept of *Indirect Recursion*, where the first method (methodA) calls a second method (methodB) that calls the first method (methodA).   Use this with caution as it can be harder to follow.

## Classpath

In most of our work, we've create a single project that has a class or two and will run inside the IDE very easily.  As our application become more complex, uses outside libraries or frameworks, making sure all the classes are available, can become challenging.   When a Java application runs, it looks for a class when you attempt to construct it (*new MyClass();*).   Internally what Java is doing is it is looking for that defined class file that lives on the hard drive in some directory so it can be loaded and interpreted for the instantiation.

Java does this with a concept call *Classloading*.   This means that when you call *new* the class is search for by Java and then loaded into memory to construct it.   For it to find the class it will search the *Classpath*.

The classpath is a list of directories or Jar (Java Archive) files and will parse the contents to look for the matching class declaration to what is being requested.   In all of our cases this year, Eclipse has automatically done this for you and because our examples are very simplistic its a simple list to search including the Java Framework and your package.   In fact the only case where it needed anything extra was for the Testing section when we added the *Junit.jar* to our project which Eclipse automatically added it to the *Classpath*.

When running on the command line, the Classpath is a variable taken in by the JVM, where you can provide the list of file system paths to a directory that holds *.class* files and/or a list of jar files.    The JVM will then use this list to search for any classes being requested.

```bash
java.exe -classpath "/home/username/repos/wrap up/bin" com.spextreme.examples.recursion.RecursionExamples
```

## Garbage Collection

We've briefly talked about memory and how it is organized around bytes of data.   When we create a variable, a set of bytes are allocated to hold the object or primitive data in memory.   Over time as the application creates more variables and executes, that memory starts to fill up.  The JVM is only given a specific amount of memory so there is a limit to the total thing that may be allocated.  If we keep creating objects eventually we'll hit an *OutOfMemoryError*.

Fortunately Java provides a concept called *Garbage Collection* which cleans up and reclaims memory that was used by an object when they are no longer being referenced.  This means, that the JVM is running a background process looking for objects that were created, and no longer have anyone using them.   For example, when you create a variable in a method, that variable is being referenced until the method is finished.  Once it is, any variable defined is no longer referenced and the JVM will mark it for Garbage Collection during the next cycle.

This is done completely in the background and one of the main benefits of Java.   You should still be cognizant of the objects and variables you created to make sure you're optimally using memory but this feature is a nice benefit to simplify your application.

## Inner Classes

Everything in Java is a class as we've seen throughout the course.  Objects, Interfaces, Enums are all structured in a similar way and are themselves objects. 

Inner classes are no different, but they are written inside of another class definition and are not contained in their own file.  The below example shows the *InnerClass* was created inside of the *NormalClass*.

```java
public class NormalClass {

  private class InnerClass {

    public String name;
    public int value;

    public InnerClass(String name, int value) {
      this.name = name;
      this.value = value;
    }
  }

  private String altName;
  private final InnerClass inner;

  public NormalClass(String name, String altName, int value) {
    inner = new InnerClass(name, value);
  }

  public String getAltName() {
    return altName;
  }

  public InnerClass getInner() {
    return inner;
  }

  public String getName() {
    return getInner().name;
  }

  public int getValue() {
    return inner.value;
  }
}
```

Inner classes are nice because you can make quick use of a class that may not need to be outside of the current class that its created in.  This can keep code a little more organized and compact.  An interesting fact about inner class is they can access the outer classes private members.  One last benefit is that this may be much easier to maintain because it is held within the using class. 


## Anonymous Classes

Going a step further, *Anonymous classes* are like *Inner classes* but they aren't given a direct name.   If you remember our Threading example, this is a good use case of an Anonymous class.

```java
public class StandardClass {

  public void runThread() throws Exception {
    
    Thread t = new Thread(new Runnable() {

      @Override
      public void run() {

        System.out.print("Anonymous class Runnable for a simple thread execution complete.");
      }
    });

    t.start();
  }
}
```

As this shows, the *new Runnable* call creates an class with the *run()* method defined but doesn't actually name the Runnable instance.    This is equivalent if we created a class that implemented Runnable but this way we don't need the extra file or to construct the class.   This helps simplify the code and keeps the *Runnable* instance private to the method.

The main thing to realize, is that an *Anonymous class* must extend or implement something to be defined.  There is no way to create a standard class anonymously.   In defining an *Anonymous class*, the interface or abstract methods must be fully implemented since this is defining the class implementation.  


## Conclusion

In this section we covered a lot of little things that we've used without even knowing it (Classpath, garage collection) and introduced some new concepts that help simplify the code (inner/anonymous classes).   Recursion is also one of the core foundation program concepts that was also covered.

This completes all the primary course material for using Java.   At this point you should have all the tools necessary to build working Java applications.  Good luck and happy coding.

We'll have one more concept that we'll cover that tie everything you learned together and how in today's world of software development Automation is king in saving the developers time and effort in many of the steps in producing good quality software.