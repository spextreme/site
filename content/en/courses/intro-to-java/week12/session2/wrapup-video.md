---
title: "Wrap Up - Video"
description: "Closing out on the remainder of introductory topics."
date: 2021-04-10
draft: false
enableToc: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/wrapup-screen.png)](https://youtu.be/sRdvKLtkPlM)

## Slides

{{< revealjs >}}

## Almost Done

- Covered most introductory topics
- Java basics, syntax, conditions, etc...
- Covered other topics 
   - Source Control, Eclipse, Testing, etc..
- Few remaining topics to go through

---

## Recursion

- A method calling itself
   - Think about a file system 
- Infinite Recursion
   - *StackOverflowError*
- Needs a method to stop 
   - Conditionals
- Direct vs Indirect Recursion

--

## Infinite Recursion

```java
public void infiniteRecursion(int counter) {

 System.out.println(String.format("infiniteRecursion(%s)", 
                                                   counter));

 infiniteRecursion(++counter);

}
```

--

## Proper Recursion

```java
public void limitedRecursion(int counter) {

 System.out.println(String.format("limitedRecursion(%s)", 
                                                   counter));

 if (counter >= 10) {
   return;
 }

 limitedRecursion(++counter);
}
```

--- 

## Classpath

- Eclipse did this for us
- Java has to look up each class to construct it
  - file system
  - Jar Files
- Classloader
- Know where to find it from the classpath

```bash
java.exe -classpath "/home/username/repos/wrap up/bin" 
   com.spextreme.examples.recursion.RecursionExamples
```

--- 

## Garbage Collection

- Each variable we create, eats up memory
- Memory is finite
  - *OutOfMemoryError*
- Java automatically manages memory
   - GC runs in the background (JVM Thread)

--- 

## Inner Classes

- A class defined inside another class
- Doesn't use its own file
- Can keep code organize
- Easier to maintain related items
- Has access to the outclasses private variables
 
--

## Inner Class Example

```java
public class NormalClass {

  private class InnerClass {

    public String name;
    public int value;

    public InnerClass(String name, int value) {
      this.name = name;
      this.value = value;
    }
  }

  private String altName;
  private final InnerClass inner;

  public NormalClass(String name, String altName, int value) {
    inner = new InnerClass(name, value);
  }
  
  ...
}
```

--- 

## Anonymous Classes

- Like *Inner classes* but not named
- Implement or extend a interface/class

--

## Thread Example

```java
public class StandardClass {

  public void runThread() throws Exception {
    
    Thread t = new Thread(new Runnable() {
      @Override
      public void run() {
        System.out.print("Anonymous ...");
      }
    });

    t.start();
  }
}
```

--- 

## Conclusion

- Bunch of little things we've used
  - Classpath
  - Garbage Collection
- New concepts
  - Inner Classes
  - Anonymous Classes
  - Recursion

{{< /revealjs >}}