---
title: "Loops - Video"
date: 2021-02-04T10:08:56+09:00
description: For loops, while loops, and other iterating methods.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/loops-screen.png)](https://youtu.be/MPBXwUDngcA)

## Slides

{{< revealjs >}}

## Introduction

- Iterate over (loop)
- Working with collections of data
- Repetitive tasks

---

## Increment/Decrement Operators

- Increment: ++
- Decrement: \--

```java
number = number + 1;
number++;

number = number - 1;
number--;
```

--

## Prefix/Postfix

- Prefix

```java
 ++number;
 --number;
```

- Postfix

```java
 number++;
 number--;
```

---

## While

```java
while( <condition> ) {
  statements;
}
```

- Runs until the condition is *false*
- Pre-test loop
- Any number of statements to execute
- The while loop must change the condition
   - Otherwise it's an infinite loop

--

## Infinite Loop

```java
public static void infinitLoop() {

 final int counter = 0;

 while (counter < 20) {

   System.out.println(counter + " - Done Yet...");
 }
}
```

--

## Infinite Loop Fixed

```java
public static void infinitLoopFixed() {

 int counter = 0;

 while (counter < 20) {

   System.out.println(counter + " - Done Yet...");
   counter++; // This was missing
 }
}
```

---

## Do-While

```java
do {
  statements;
} while( <condition> );
```

- Runs until the condition is *false*
- Post-test loop
   - Runs at least once before checking condition
- Any number of statements to execute
- Loop must change the condition

--

## Do-While Example

```java
public static void doWhileLoopExample() {

 int counter = 0;

 do {

   System.out.println("Only once");
   counter++;
 } while (counter < 1);
}
```

---

## For

```java
for( initialization; test; update ) {
   statements;
}
```

- Pre-test
- Any number of statements to execute
- Control Variable
   - Initialize
   - Test condition
   - Update

--

## For Example

```java
public static void forLoopCounter() {

 System.out.println("For Counter");

 for (int i = 0; i < 20; i++) {
   System.out.println("\t" + i);
 }
}
```

--

## For Loop

- Control Variable
   - Do not change inside loop
- Initialize/Update multiple variables

--

## For Loop Example 2

```java
public static void forLoopMultipleCounter() {

 System.out.println("Multiple Var For Counter");

 for (int i = 0, j = 50; (i < 10) || (j > 10); i++, j--) {
   System.out.println(String.format("\ti: %d, j: %d", i, j));
 }
}
```

---

## For Each Loop

- Collections
   - Arrays, Lists, etc..
- Iterates over each item in the list

```java
for (variable : collection) {
  statements;
}
```

--

## For Each Loop Example

```java
public static void forEachExample() {

 final String[] array = new String[] {"Hello", "World", "!",
      "Loops", "are", "powerful"};

 System.out.println("For Each item: ");

 for (final String str : array) {
   System.out.println(String.format("\t %s", str));
 }
}
```

---

## Break and Continue

- *break* - Terminates loop immediately
- *continue* - Restarts loop
- Generally a bad coding practice
- Good for error cases

--

## Break Example

```java
public static void whileWithBreakExample() {

 int counter = 0;

 System.out.println("While with break: ");
 while (true) {

   if (counter > 20) {
     break;
   }

   System.out.println(counter);
   counter++;

 }
}
```

--

## Continue Example

```java
public static void whileWithContinueExample() {

 int counter = -1;

 System.out.println("While with Continue: ");
 while (counter < 20) {

   counter++;

   if ((counter % 2) != 0) {
     continue;
   }

   System.out.println(counter);
 }
}
```

---

## Conclusion

- Computers are good at repetitive tasks
- Loops are powerful and useful
- Collections of data
- For/For Each will be heavily used
   - Learn it

{{< /revealjs >}}