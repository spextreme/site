---
title: "Loops"
date: 2021-02-04T10:08:56+09:00
description: For loops, while loops, and other iterating methods.
draft: false
collapsible: false
weight: 15
---

## Introduction

For a program to be useful, it may need to work with large amounts of data or perform repetitive tasks.   

In this section, we'll cover the concept of loops.  A loop is something that starts, performs an action, then does it again until some condition is met.  Java provides a few methods to implement loops which we'll detail in this section.

## Increment/Decrement Operators

To begin, we need to understand a few basic operators that are used to increment or decrement numbers.

Java has a shorthand set of operators to perform the basic +1 and -1 calculation.  They are called the increment and decrement operators.

|Operator|Name|Equivalent to|Example|
|--------|----|-------------|-------|
| ++ | increment | number = number + 1; |number++; |
| \-- | decrement | number = number - 1; |number\--; |

With these, you can quickly add or subtract one from a number.   These are heavily used with looping, which is why they are introduced here.

There is one last piece to understand with the ++ and -- operators.  Placement determines when the increment and decrement occur.  You can prefix or postfix the operators to the variable.

When the operators is prefixed, the variable will be increment/decrement prior to the rest of the equation being evaluated.  When the operator is postfixed, the variable will increment/decrement after the rest of the equation has been evaluated.  Once we get into loops, this will become more clear.

## While

A *while* loop is one of the Java looping keywords.  The *while* loop will iterate as long as the condition is *true*.

```java
while( <condition> ) {
  statements;
}
```

The beauty of the *while* loop is that you can set it up to run until the condition is met.  This is called a pre-test loop because it first performs the condition check, then runs the statements within the loop body if the condition evaluates to *true*.   It will then check the condition again and execute the loop body.  This will be repeated until the condition evaluates to *false*.

This means that your condition variable should be changed inside the loop once the loop meets the exit criteria.   Typically, you'll have some boolean variable defined outside of the loop and set to *true*, then inside the loop, have a check (*if* statement for example) that sets the variable to *false* so the next condition check will cause the *while* loop to exit.

Oftentimes, this step is forgotten and you'll hit a case where the loop runs forever (*infinite loop*).   This causes your application to appear as if it is hung, or you may hear the computer fan kick into high gear.   This is something every programmer hits at times, and you kick your self every time.

A *while* loop will execute 0 or more times.   Remember since this is a pre-test loop, the condition starting out as *false* will mean the *while* loop never starts.

Below is an example of an infinite loop.  It will set the counter to 0, then go into the *while* loop.  Since 0 < 20 this will run the loop.   For each iteration, 0 will still be less then 20.  The main issue is that the counter is not being incremented.  

```java
public static void infinitLoop() {

 final int counter = 0;

 while (counter < 20) {

   System.out.println(counter + " - Done Yet...");
 }
}
```

To fix this issue, we need to use the increment operator (++) to update the counter on each iteration.   It performs the same steps as the infinite loop, but this time, by incrementing the counter, the condition will be hit after 20 iterations through the loop.

```java
public static void infinitLoopFixed() {

 int counter = 0;

 while (counter < 20) {

   System.out.println(counter + " - Done Yet...");
   counter++; // This was missing
 }
}
```

## Do-While

A *do-while* loop is a post-test loop because it will run the loop body first, then check the condition.  Overall it is similar to the *while* loop.

```java
do {
  statements;
} while( <condition> );
```

The following example loops as long as the counter is less then 1.  In this case, it runs the loop, increments to 1 and then checks the condition. It is showing that even when the condition isn't met, a post-test loop will run at least once.

```java
public static void doWhileLoopExample() {

 int counter = 0;

 do {

   System.out.println("Only once");
   counter++;
 } while (counter < 1);
}
```

## For

The *for* loop is probably the most used loop in Java.  It allows a developer to initialize a variable, perform a test and handle the incrementing in one line.  It is highly versatile and applicable in many cases.

```java
for( initialization; test; update ) {
   statements;
}
```

This is a pre-test loop since the *for* loop is going to perform the test condition before running through the loop the first time.

Notice that the *for* loop has a very unique syntax with the multiple parts (and semi-colons).   This is what makes the *for* loop unique and quite powerful.

The initialization section is a way to defined the control variable (the thing you're using in the condition check).   This allows the variable to be defined and initialized, so it will begin the loop in a fresh state.  This uses the standard variable definition syntax (*int i = 0;*).

The test section is the conditional check that must return a boolean value.  This is equivalent to a condition in an *if* statement or *while* loop.  Generally, the control variable created in the initialization section is used, but it is not required to be.

Finally, the update section is called after the loop completes and will update the control variable.  This doesn't have to be used, but it is very convenient.  Let's look at a simple index counter example.

```java
public static void forLoopCounter() {

 System.out.println("For Counter");

 for (int i = 0; i < 20; i++) {
   System.out.println("\t" + i);
 }
}
```

In the example, we initialize the index (*i*) by defining the variable and setting it to 0.  Next we check that *i* is less then 20 and run the output.   Finally we incremented *i* by one, perform the check and print out the next value.   This repeats until *i* is incremented to 20 and then the loop exits (stopping so the last output number is 19).

A word of caution, the control variable should not be updated inside the *for* loop if using the update section.  This could affect the loop execution and cause more complex code that is difficult to maintain.

Finally, a *for* loop may initialize and update multiple variables in the initialization and update sections of the *for* loop.

```java
public static void forLoopMultipleCounter() {

 System.out.println("Multiple Var For Counter");

 for (int i = 0, j = 50; (i < 10) || (j > 10); i++, j--) {
   System.out.println(String.format("\ti: %d, j: %d", i, j));
 }
}
```

## For Each

Later on in the course, we'll be focusing on "collections" which are just a way to talk about a list of items or an array.   This may be a list of numbers (1 to 100), a list of names registered for the class, or pretty much anything.

Since Java has a complex collections model available, the *for each* was born out of the constant use of indexes (from the *for* statement above) and having to iterate over these lists and cast the object to a type.   The *for each* simplifies the creation of a for loop to iterate over collections and arrays.

If you remember, we talked about arrays before.  To define an array we use the square brackets [] to create an array of some type.  

The *for each* has the following syntax

```java
for (variable : collection) {
  statements;
}
```

In the example, we create an array of some *String*s and then use the *for each* to iterate over each one and output it.

```java
public static void forEachExample() {

 final String[] array = new String[] {"Hello", "World", "!", "Loops", "are", "powerful"};
 System.out.println("For Each item: ");

 for (final String str : array) {
   System.out.println(String.format("\t %s", str));
 }
}
```

## Break and Continue

There are times, when working with loops, you'll need to stop it prematurely or skip over some statements during an interation.   

The *break* keyword can be placed inside of a loop, and it will stop executing the loop at the point *break* is called and exit out of the loop.   This can be useful to abnormally terminate a loop.

This is a poor example of setting a loop to run infinitely but then adding in an *if* check to break out of the loop when we hit 20.   

```java
public static void whileWithBreakExample() {

 int counter = 0;

 System.out.println("While with break: ");
 while (true) {

   if (counter > 20) {
     break;
   }

   System.out.println(counter);
   counter++;

 }
}
```

Generally using the *break* in this manner is a bad coding practice, and the condition check would be better solution. There are good reasons to use *break*, however.  This is primarily for abnormal termination like an error that occurs or something unique to the loop (i.e. failed to read data). 

The *continue* statement is the other keyword that may be used to allow a loop to remain executing, but at the point it's called, it prevents the remainder of the loop block from being executed and immediately jumps back to the start of the loop.

This may be useful when a subcondition is met, so you need to stop processing the current tasks, but you're not ready to exit the loop because you need to move on to the next item in the collection.

Here is a forced example to cause this to skip outputting all the odd numbers:

```java
public static void whileWithContinueExample() {

 int counter = -1;

 System.out.println("While with Continue: ");
 while (counter < 20) {

   counter++;

   if ((counter % 2) != 0) {
     continue;
   }

   System.out.println(counter);
 }
}
```

Generally, using the condition statement is a bad coding habit also, as it can make code hard to understand and difficult to maintain.

## Conclusion

Iterating and performing repetitive actions is one of the most beneficial things a computer can do.  It doesn't get bored or tired of doing the same action.  

This is why loops are so powerful and useful in computing and you'll find many uses for them.  As we get further in and start working with larger amounts of data (array, lists, sets, etc...) these loops will help provide a method to do complex actions on the data.

If you only focus on learning one loop out of this, the *for/for each* loop will be the one you want to master.
