---
title: "Operators & enums"
date: 2021-02-07
description: This reviews all of the operators and introduces enumerations.
draft: false
collapsible: false
weight: 15
---

## Introduction

At this point we've coverage a large amount of the Java keywords and the syntax of the language.   In this section were going to finish out everything related to operators and introduce a specialized classed called an *enum* (enumeration). 

Overall Java is made of two key parts, the language syntax and the collection of objects they have built into the Java framework.  The language syntax is the structure of the code, the keywords, primitive types, and operators.   The framework is all of the objects that exist as part of the main language to help build application faster and more efficiently.   The framework has evolved for over 20+ years and has a large array of objects available.

For now, we'll rehash some of the parts we've already discussed, fill in missing sections, and cover a new concept around enumerations which is helpful in the readability of code.

## Operators

### Arithmetic Operators

A computer is really good at doing arithmetic and we only glossed over it in different sections.   However it's worth explaining because Java supports these operators plus some additional ones to make things easier to do calculations.   

| operator | description | example |
|----------|-------------|---------|
| + | Addition - Add two numbers together. | x + y; | 
| - | Subtraction - Subtract a number from another. | x - y; |
| * | Multiplication - Multiply two numbers together. | x * y; |
| / | Division - Divide a number by another. | x / y; |
| % | Modulus (Mod) - Returns the divisions remainder. | x % y; |
| ++ | Increment - Increment by one. | x++; |
| -- | Decrement - Decrement by one. | x--; |

### Binary Operators

Binary is the lowest form for a computer and we touched on it when we discussed memory and other parts of a computer.  Since a computer only knows binary (0 and 1) there are operators that support working on binary values.   We will not go into much detail here as they are typically used for much more advanced algorithms development which is past the scope of this class.  This is only listed for completeness in covering operators.

| operator | description | example |
|----------|-------------|---------|
| & | Bitwise AND - Copies a bit to the result if it exists in both values. | x & y; |
| \| | Bitwise OR - Copies a bit to the result if it exists in either value. | x \| y; |
| ^ | Bitwise XOR - Copies the bit if it is set in one operand but not both. | x ^ y; |
| ~ | Bitwise Compliment - Unary operator that has the effect of flipping the bits. | x ~ y; |
| \<\< | Left Shift - The left operands value is moved left by the number of bits specified by the right value.  | x \<\< y; | 
| \>\> | Right Shift - The left operands value is moved right by the number of bits specified by the right value. | x \>\> y; | 
| \>\>\> | Zero Fill Right Shift - The left operands value is moved right by the number of bits specified by the right value. | x \>\>\> y; |


### Assignment Operators

We've made heavy use of the assignment operator so far but there is still a couple we haven't covered.

| operator | description | example |
|----------|-------------|---------|
| = | Assigns the value to a variable. | int x = 2; | 
| += | Adds the value to the number and then assigns the result to the variable. |  x += y; | 
| -= | Subtracts the value from the number and then assigns the result to the variable. | x -= y;  | 
| *= | Multiplies the value to the number and then assigns the result to the variable. | x *= y; | 
| /= | Divides the value from the number and then assigns the result to the variable. | x /= y; | 
| %= | Modules of the value and the number and assigns the result to the variable. |  x %= y; | 
| \<\<= | Left shift *AND* and assignment operator. |  x \<\<= y; | 
| \>\>= | Right shift *AND* and assignment operator. |  x \>\>= y; |  
| &= | Bitwise *AND* and assignment operator. | x &= y; |
| ^= | Bitwise exclusive OR and assignment operator. | x ^= y; |
| \|= | Bitwise inclusive OR and assignment operator. | x ^= y; |

### Relational Operators

These are the ones used by conditional statements to obtain a *true* or *false* result.  These are highly important to know due to the heavy use in *if* statements and the varying types of loops.

| operator | description | example |
|----------|-------------|---------|
| == | Equal to - Check for equality between two things. | x == y; | 
| != | Not equal to - Checks if the two items are not equal. | x != y; | 
| > | Greater than - Checks if the left value is greater than the right value. | x > y; | 
| < | Less than - Checks if the left value is less than the right value. | x < y; | 
| >= | Greater than or equal to - Checks if the left value is greater than or equal to the right value. | x >= y; | 
| <= | Less than or equal to - Checks if the left value is less than or equal to the right value. | x <= y; | 

### Logical Operators

These are used to handle conditional based operations like *and* and *or* of a boolean value set.  They are used in the condition set of *if* and loops.

| operator | description | example |
|----------|-------------|---------|
| && | And - Returns *true* if both sides are *true*. | x == y && x == z; | 
| \|\| | Or - Returns *true* if one side is *true*. | x == y || x == z; | 
| ! | Not - Reverses the result. | !(x == y); | 


### Conditional Operators

The conditional operators is a shorthand way of performing an *if then* statement.

| operator | description | example |
|----------|-------------|---------|
| (exp) ? (if true) : (if false) | If then - Based on the value, if *true*, then the first portion is executed, otherwise the second portion will be executed. | x == y ? true : false; | 

## Types

As we discussed, Java has 8 primitive types.  Everything else is an object and part of the overall framework.

### Primitive Types

| type | example |
|------|---------|
| boolean | boolean x = true; |
| byte | byte x = 32; |
| char |  char x = 'a'; |
| short | short x = 8; |
| int | int x = 32; |
| long | long x = 64L; |
| float | float x = 32f; |
| double | double x = 64.1d; |

## Objects

An object is the primary model for Java where a class represents the blueprint for an object to be created.   The class can then be instantiated, thus creating an object, which holds the data (fields) and performs the defined actions (methods).

To define a class, the java keyword *class* is used.  A class is contained within a '.java' file and anything inside the class definition is part of it.  This included fields and methods.    

Scope is also an important part of a class definition as it controls what an outside user of this class can access.  *public*, *private*, *protected* and package private provide the ability to define method, field, and class level access.

## Java Keywords

We have covered a lot of the java keywords.  We haven't gotten all of them and we probably will not get to all of them in this class.  However here is a list of them for reference.

| keywords | | | | |
|---------|-|-|-|-|
| abstract | default | if         | private      | this |
| assert*  | do      | implements | protected    | throw |
| boolean  | double  | import     | public       | throws |
| break    | else    | instanceof | return       | transient* |
| byte     | enum    | int        | short        | try |
| case     | extends | interface  | static       | void |
| catch    | final   | long       | strictfp*    | volatile* |
| char     | finally | native*    | super        | while |
| class    | float   | new        | switch       |  |
| continue | for     | package    | synchronized |  |

\* these are ones we will not cover as they get more into advance level programming with Java.  You can always Goggle them if you are curious or want to know more.

## Enumeration

An Enumeration (Enum) is a specialized object that was added to Java late in its evolution (Java v1.5).  The setup for an *enum* is similar to a class in that it goes into its own file (with the '.java' extension) and then is defined like a class but with the *enum* keyword.

```java
<scope> enum <name> {
   <comma separated list of types>;
}
```

An *enum* is a list of names that may then be use as if they were a integer.  This means that they are given a unique identifier within the context of the defined *enum*.  This allows them to be used in *switch* statement as the case values which can be very helpful in making code easier to understand.   Since they ultimately relate to an integer they can also be more easily compared in condition statements using equality (*==*).

When defining the items of an *enum*, the names can be any string and included numbers (as long as not the first character in the string) and it may use the underscore (_) character.    Spaces and other special characters are not allowed which is similar to all other names items (classes, fields, methods, etc...).

An example of defining an *enum*.

```java
public enum FamilyMemberType {
  /**
   * A child of the parents.
   */
  Child,
  /**
   * Represents the father of a family.
   */
  Father,
  /**
   * Represents the mother of a family.
   */
  Mother,
  /**
   * Undetermined type.
   */
  Other
}
```

### Switch statement

The switch statement can leverage *enum* values to help make it much more descriptive.   Below is an example of using the *FamilyMemberType* in a *switch* statement.

```java
public static void switchWithEnum(FamilyMemberType member) {

 switch (member) {
   case Child:
     System.out.println("I am a child.");
     break;

   case Father:
     System.out.println("I am the father.");
     break;

   case Mother:
     System.out.println("I am the mother.");
     break;

   case Other:
   default:
     System.out.println("undefined");
     break;
 }
}
```

### Comparisons

Comparing *enum* values is as simple as using the relational operators.   This would be equivalent in a while or do-while condition.

```java
public static void ifWithEnum(FamilyMemberType member) {

 if ((member == FamilyMemberType.Mother) || (member == FamilyMemberType.Father)) {
   System.out.println("I'm a parent");
 } else if (member == FamilyMemberType.Child) {
   System.out.println("I'm a child");
 } else {
   System.out.println("Undetermined");
 }
}
```

### Methods

An *enum* is very easy to define but Java makes it work similar to an object and defines a few methods on the *enum* generated by the compiler.  Since an *enum* is an object, the *toString*, *equals* and other object defined methods are available.  They work exactly like any other object but the *toString* is going to output the *enum* *name* as it is defined.   The *equals* will compare it as if it is a number because each *enum* is given an *ordinal* (number) value.

A few methods defined by the enum are *name*, *ordinal*, *valueOf*, and *values*.

#### *name*

The name is defined as the name of the *enum* when it was created. In our example above it was 'Father', 'Mother', 'Child', 'Other'.  This defined name is what the *name* (and *toString()*) method will return.  This is convenient because it makes it easy to provide a string representation of the *enum*.

```java
System.out.println("I am a " + FamilyMemberType.Father.name()); // output: 'I am a Father'
```

#### *ordinal*

Each *enum* value is automatically created as an object.   The Java language in turn auto assigns a number which is represented as the ordinal value.   You should never rely on the number because reordering the *enum* or adding new items may change the ordinal value for the next compilation.  This is why referencing the *enum* by name is much more beneficial and standard practice.

```java
System.out.println("I am number " + FamilyMemberType.Father.ordinal()); // output: 'I am number 1'
```

#### *valueOf*

At times, you may be using an enumeration but need to read something in.  For example, maybe we save the configuration of our application in a file, and one of the configuration tokens is a *enum*.  If we save this to a file, it may be in the file as the String name of the *enum*.   The next time it is read in we have to convert the String name back into the defined *enum*.  

To support this, the *enum* provides a static *valueOf* method that will read in a String and attempt to match it to one of the defined values for the *enum*.  It will return the *enum* matching that name exactly (case is important here).   If the given String doesn't match the name of any of the items then an exception is thrown (Exceptions will be covered later in this course).

```java
System.out.println("Value of got: " + FamilyMemberType.valueOf("Mother"));
```

#### *values*

This is a method used to obtain an array of all the defined types in the *enum*.

```java
for (final FamilyMemberType member : FamilyMemberType.values()) {
   System.out.println(member.name());
 }
```

## Conclusion

There is a lot of parts to Java, and this completed the set of operators supported by the language.  It included basic arithmetic, logical, relational, and binary operators.

Finally we ended on the *enum* type which can be very beneficial in simplifying code for other developers by making named values for comparisons, and conditional operations.  The *enum* is a specialized type in Java and comes with a lot of useful features built in.









