---
title: "Operators & enums - Video"
date: 2021-02-07
description: This reviews all of the operators and introduces enumerations.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/ops-enum-screen.png)](https://youtu.be/h5T9nFWpMrc)

## Slides

{{< revealjs >}}

## Introduction

- Covered much of the Java Language
- Most of the Operators
- Little bit of the Framework
- Closing out on the Operators
- Learning about Enumerations

---

## Arithmetic Operators

| operator | description | example |
|----------|-------------|---------|
| + | Addition | x + y; |
| - | Subtraction | x - y; |
| * | Multiplication | x * y; |
| / | Division | x / y; |
| % | Modulus (Mod) | x % y; |
| ++ | Increment | x++; |
| -- | Decrement | x--; |

---

## Binary Operators

| operator | description | example |
|----------|-------------|---------|
| & | Bitwise AND | x & y; |
| \| | Bitwise OR | x \| y; |
| ^ | Bitwise XOR | x ^ y; |
| ~ | Bitwise Compliment | x ~ y; |
| \<\< | Left Shift  | x \<\< y; |
| \>\> | Right Shift | x \>\> y; |
| \>\>\> | Zero Fill Right Shift | x \>\>\> y; |

---

## Assignment Operators

| operator | example |
|----------|---------|
| = |  int x = 2; |
| += |  x += y; |
| -= | x -= y;  |
| *= | x *= y; |
| /= | x /= y; |
| %= |  x %= y; |

---

## Relational Operators

| operator | description | example |
|----------|-------------|---------|
| == | Equal to | x == y; |
| != | Not equal to | x != y; |
| > | Greater than | x > y; |
| < | Less than | x < y; |
| >= | Greater than or equal to | x >= y; |
| <= | Less than or equal to | x <= y; |

---

## Logical Operators

| operator | description | example |
|----------|-------------|---------|
| && | And | x == y && x == z; |
| \|\| | Or | x == y || x == z; |
| ! | Not | !(x == y); |

---

## Conditional Operators

- A simple form for inline *if then*

```java
(exp) ? (if true) : (if false)
```

---

## Primitive Types

| type | example |
|------|---------|
| boolean | boolean x = true; |
| byte | byte x = 32; |
| char |  char x = 'a'; |

--

## Primitive Types

| type | example |
|------|---------|
| short | short x = 8; |
| int | int x = 32; |
| long | long x = 64L; |
| float | float x = 32f; |
| double | double x = 64.1d; |

---

## Objects

- Core of the Java language
- Class - Blue print for creating objects
- Instantiation means to create an object
- Scope is important for access and usage

---

## Java Keywords

- Keyword are the language part
- Covered many so far
- Still have some to do
- Will not cover all

--

## Java Keywords
| keywords | | | | |
|---------|-|-|-|-|
| abstract | default | if         | private      | this |
| assert*  | do      | implements | protected    | throw |
| boolean  | double  | import     | public       | throws |
| break    | else    | instanceof | return       | transient* |
| byte     | enum    | int        | short        | try |

--

## Java Keywords Cont

| keywords | | | | |
|---------|-|-|-|-|
| case     | extends | interface  | static       | void |
| catch    | final   | long       | strictfp*    | volatile* |
| char     | finally | native*    | super        | while |
| class    | float   | new        | switch       |  |
| continue | for     | package    | synchronized |  |

---

## Enumeration

- Specialized Java type
- Is an object
- Added in Java 1.5
- *enum* keyword
- Setup exactly like a class

```java
< scope > enum < name > {
   < comma separated list of types >;
}
```

--

## Enumeration

- List of types
- Each given a unique number
- Useful in switch and conditional statements
- Cleaner code.
- Same rules for naming as other types

--

## Enum Example

```java
public enum FamilyMemberType {
  Child,
  Father,
  Mother,
  Other
}
```
--

## Switch statement

```java
switch (member) {
 case Child:
   System.out.println("I am a child.");
   break;
 case Father:
   System.out.println("I am the father.");
   break;
 case Mother:
   System.out.println("I am the mother.");
   break;
 case Other:
 default:
   System.out.println("undefined");
   break;
}
```

--

## Comparison Example

```java
public static void ifWithEnum(FamilyMemberType member) {

 if ((member == FamilyMemberType.Mother)
         || (member == FamilyMemberType.Father)) {
   System.out.println("I'm a parent");
 } else if (member == FamilyMemberType.Child) {
   System.out.println("I'm a child");
 } else {
   System.out.println("Undetermined");
 }
}
```

--


## Enum Methods

- Its just an object
- Java puts methods in an enum
- Simplify some things
- *name*, *ordinal*, *valueOf*, and *values*

--
## *name*

- Defines the name
- Used in *toString()*

```java
System.out.println("I am a " + FamilyMemberType.Father.name());
// output: 'I am a Father'
```

--

## *ordinal*

- Automatic creation
- Unique number for each item
- Number is called the ordinal value
- Should use the ordinal directly
   - Can be renumbered if list changed

```java
System.out.println("I am number "
     + FamilyMemberType.Father.ordinal());
// output: 'I am number 1'
```

--

## *valueOf*

- Convert string to an enum
- Must match the exact name (case sensitive)
- Static method *valueOf*
- Exception thrown if no match

```java
System.out.println("Value of got: "
      + FamilyMemberType.valueOf("Mother"));
```
--

## *values*

- An array of each defined type

```java
for (final FamilyMemberType member : FamilyMemberType.values()) {
   System.out.println(member.name());
 }
```

---

## Conclusion

- Operators
   - That all of them
   - arithmetic, logical, relational, and binary operators
- Enum
   - Specialized object
   - Useful in making code easier to read/maintain
   - Conditionals, switch, comparisons
   - Just another object

{{< /revealjs >}}