---
title: "Review notes"
date: 2021-03-30
description: Review Details
draft: true
collapsible: false
weight: 15
---

{{< revealjs >}}

## Exam Details

- April 1st
- Complete before 11:59
- 60 Minutes from start
- Any time that day
   - Class time I'll be available

---


## Review: Java

- Does java run everywhere?  Why?

[link](http://localhost:1313/site/courses/intro-to-java/week1/session1/programming/)

---

## Review: OOP

- What is OOP?
- How do we defining a field?
- How do we call a method?
- Why Comment
- What is Javadoc?

[link](http://localhost:1313/site/courses/intro-to-java/week1/session2/content/)

---

## Review: IDE

- What does an IDE offer?
- Name one

[link](http://localhost:1313/site/courses/intro-to-java/week2/session1/ide-scm/)

---

## Review: Strings

- How do we define a string?

[link](http://localhost:1313/site/courses/intro-to-java/week2/session2/scope-strings/)

---

## Review: Scope

- What are the 4 scopes?

[link](http://localhost:1313/site/courses/intro-to-java/week2/session2/scope-strings/)

---

## Review: Classes

- How do we instantiate a class?

[link](http://localhost:1313/site/courses/intro-to-java/week3/session1/classes/)

---

## Review: Static

- What does static mean and do?

[link](http://localhost:1313/site/courses/intro-to-java/week3/session1/classes/)

---

## Review: Condition

- How do we define an if statement?
- What is a conditional?

[link](http://localhost:1313/site/courses/intro-to-java/week3/session2/conditional/)

---

## Review: For Loops

- Types of a for loop

[link](http://localhost:1313/site/courses/intro-to-java/week4/session1/loops/)

---

## Review: Enum

- What is an Enum

[link](http://localhost:1313/site/courses/intro-to-java/week4/session2/ops-enums/)

---

## Review: Try-Catch

- How do you handle an exception?

[link](http://localhost:1313/site/courses/intro-to-java/week5/session2/exceptions/)

---

## Review: Arrays

- How do we create a standard array?

[link](http://localhost:1313/site/courses/intro-to-java/week6/session1/collections/)

---

## Review: Threads

- What is a Thread?

[link](http://localhost:1313/site/courses/intro-to-java/week7/session2/threading/)

---

## Review: Refresher

- All these are important

[link](http://localhost:1313/site/courses/intro-to-java/week8/session2/java-refresher/)

   
{{< /revealjs >}}