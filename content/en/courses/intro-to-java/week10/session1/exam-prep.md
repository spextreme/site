---
title: "Exam Prep"
date: 2021-03-23
description: Preparing for the exam.
draft: false
collapsible: false
weight: 15
---

## Introduction

There is no material for this session but it is highly recommended you attend class since we'll be doing a live review during class.  This will also be the oppertunity to ask question and get any last minute help before the exam.


## Exam Details

The exam is 25 total questions (Multiple Choice and True/False).  They cover everything we've talked about up to Threads (Week 7).  You'll access the exam through Canvas by logging in and going to Quizes.  Once you start the exam you'll have 60 minutes to complete it.  This should be plenty of time.

The exam will be available the morning of April 1st.  It must be completed by 11:59pm.   After that time you will not be able to take the exam and will get a 0 for it.  Remember it is 20% of your grade.




