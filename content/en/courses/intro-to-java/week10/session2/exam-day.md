---
title: "Exam Day"
date: 2021-03-23
description: Exam time.
draft: false
collapsible: false
weight: 15
---

## Introduction

The exam occurs during this class session (April 1st) .  The exam can be done any time during that day but it is highly recommended to attend class and work on it during that time.  

## Rules

1. You have 60 minutes from first signing in to the exam to compete it.
1. There are 25 *multiple choice* and *true/false* total questions.
1. I will answer questions in private chat or break out rooms during class time only.  If you take the exam outside of class time then I may not be available.
1. It must be submitted before 11:59pm of April 1st.  If you don't complete it by that time a 0 will be given and there will be no option to make it up.  Remeber this is 20% of your grade.
1. You must log into Canvas and access the exam with your account.
1. This is "Open Book" so you may use the course material, Google, etc...   


## Conclusion

Good luck.   You should have plenty of time but make sure you at least put an answer for each question and submit it when you're complete before 11:59pm.