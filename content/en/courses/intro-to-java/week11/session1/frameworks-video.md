---
title: "Libraries & Frameworks - Video"
date: 2021-03-26
description: The value of Libraries and Frameworks.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/frameworks-screen.png)](https://youtu.be/C5NW5JAPOw0)

## Slides

{{< revealjs >}}

## Introduction

- Building real world applications is hard
- Lots of up front things needed
- There are many libraries and frameworks available

---

## Library

- Single package (jar)
- Collection of classes
- Provides a focused capability
- Many available (Open Source) for use
- Used as dependencies on your application

--

## Library Examples

- Log4j 2
- SLF4J
- JUnit
- Mockito
- Apache Commons
- Google Guava
- Hibernate

---

## Framework

- Like a library
- Usually bigger
- Focused on making the language easier

--

## Framework Examples

- JUnit
- Spring Framework
- Hibernate
- Google Web Toolkit

---

## Benefits

- Save development time and effort
- Build your app faster
- Usually better tested and higher quality
- Usually more advanced then specialized implementations
- Built by a community

---

## Drawbacks

- May be buggy or incomplete
- May be challenging to integrate and use
- May be overkill for your needs
- Could be bloated and not well optimized

---

## Conclusion

- Very helpful in building applications
- Solve problems more easily
- Faster development time
- Guides your development toward better software


{{< /revealjs >}}