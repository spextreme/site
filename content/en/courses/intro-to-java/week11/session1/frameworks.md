---
title: "Libraries & Frameworks"
date: 2021-03-25
description: The value of Libraries and Frameworks.
draft: false
collapsible: false
weight: 15
---

## Introduction

When it comes time to start building a real world application, you'll find that there is a lot of up front things you may need to create.   A real world application will need standard things that all applications need like a main method, logging for reporting problems, utility functions to handle things, and much more.

Since Java has been around for so long, many libraries and frameworks have been created to simplify development.  This allows you to leverage those libraries to save yourself development time.   For example, Logging for Java (Log4j) is a a excellent simple logging library to help save you the time of building your own (or using *System.out* everywhere).  It is feature rich and highly configurable and has a clean interface.

As you progress further you'll find that many of the things you want to code will lead you down a path of needing to build something else.   A search in Google, will typically find that someone has already solved the problem or created a library for you to leverage.

There are also very large frameworks that cover a much bigger basis for application development.  For example, the Spring Framework is a huge framework for building big and small applications, network services, and much more.  

In today's session, we'll talk about libraries and frameworks, why the are important and how the help simplify your development.

## Library

We'll begin with a library and what it really means.   A library is usually a single package (.jar) that provides a collection of classes for you to use.   Continuing with our Log4j example, it is a library provided by Apache to offer basic logging functions to write out errors, warnings, informational, and debug type of messages.   It handles the formatting of the output, all parts of logging and enabling what level to report.  This then allows you to simple use its classes to have it perform the logging and now you won't need to write anything to implement logging in your application.

This is why Libraries are valuable, because they provide an implementation so you don't have to.   Also with many of these being provided as open source, they offer a community based development model that allows them to be advanced and become more stable over time.  The more popular libraries also provide very good documentation and are much less error prone due to their popularity and use.

Libraries are typically included as a dependency on your project.  This is how we used the JUnit.jar (which itself is a test framework that comes in a library form).   This is why Dependency Management is important because as your project grows you'll include many more libraries for it to take advantage of.

Some very popular libraries
- Log4j 2 - A logging framework on its second go around.
- SLF4J - Abstraction over the many Java logging frameworks.
- JUnit - The de facto standard in Java Unit Testing.
- Mockito - Lean and clean UI for mocking during unit testing.
- Apache Commons - Collections extensions, Java helpers, JDBC helpers, IO Utils, Logging Utils and math/stat components.
- Google Guava - I/O Utils, Concurrency, String Utilities, Caching and hashing.
- Hibernate - A Object Relation Mapper for mapping Objects to Database tables.

## Framework

A framework is like a library but it is focused on something larger and making the language easier to use.   A framework is usually implemented to help make life easier when building something using the given language.  It may implement something for the language to help users more easily create applications around it.  For example a framework may be created to make it easier to impalement a web application using Java.

One of the most popular frameworks for Java is the Spring Framework which will work as our example.   The Spring Framework at its core implemented a core concept called Inversion of Control and leverages Dependency Injections.   What this means is it will handle creating objects for you and manages their lifecycle.  It will then hand them to other objects to use so you can focus on the implementation.  This simplifies the development cycle because you're focus on building objects that work and then gluing it all together with Spring to create the application.   Spring however is much bigger in that it has also built a collection of objects that exist to simplify your development.  If you need something like a database Object Relational Mapper (Hibernate) it can glue it into your application and simplify its use.   The framework is very large and heavily used today.

Some popular frameworks
- JUnit - The de facto standard in Java Unit Testing.
- Spring Framework - Lightweight framework for building apps of all types.
- Hibernate - A Object Relation Mapper for mapping Objects to Database tables.
- Google Web Toolkit - Bridging the gap between the backend and frontend.

## Benefits

Libraries and Frameworks are very beneficial to developers and they are constantly evolving and growing.  Some reasons to use them included:

- Save development time and effort so you can build your app faster.
- Usually better tested and higher quality then rolling your own due to the community support.
- A general solution is usually more advanced then implementing a focused solution.
- Many built with a community of people offer a more collective solution then a single focused one.

## Drawbacks

As with anything, nothing is perfect.   There are some drawbacks that should be understood.

- Some things may be buggy or incomplete depending on popularity and who they came from.
- Could be more challenging to integrate and use due to the complexity built in to the solution.
- May be overkill for your needs.
- Could be bloated and not well optimized for your use case.

## Conclusion

Libraries and Frameworks are very powerful and helpful in building applications.  There are many solutions today that will help save you time and get your application built faster.   The open source community contributes to millions of products across the world today that help create these libraries and frameworks.    Also with a good quality framework, it can help get rid of much of the boiler plate code many applications require.   It can also help guide you toward a better solution that is easier to maintain in the future.

If you're going to build anything of worth today, Spring Framework is the best place to start because of all that is provides.  It is very expansive so it will take some time to learn but it will be well worth the effort.