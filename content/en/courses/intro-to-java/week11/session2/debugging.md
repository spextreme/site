---
title: "Debugging"
date: 2021-04-01
description: Debugging software and the Debugger.
draft: false
collapsible: false
weight: 15
---

## Introduction

All code will have issues which can manifest from mistakes by the developer, logic errors, and in some cases random issues due to the environment (slowness of a computer, network issues, etc...).   Therefore you'll always have to deal with **bugs** in an application.   Bugs are those things that make your application not work as expected.

Trying to figure out the issue and why your application is failing can be a very difficult task.  This is where debuggers can help because they provide a method to walk through the code line by line and see what the varaibles are set to as it executes.  

The Eclipse IDE provides a very good debugger, to allow you to visualize the execution of the code, control what line to stop on, how to progress and what the value of your variables are at the current point in time.   All these make debugging code a little bit easier.   But remember, the best code, has a lot of tests to reduce bugs so you don't have to rely on the debugger to find them.

## Bugs

Software problems occur when the application doesn't work as it was intended.   An interesting fact is the first computer bug was found on September 9, 1947 when an actual bug, a moth, had gotten into the Mark II computer and was causing it to consistently produce incorrect results.

Today, bugs are usually induce in code because of mistakes, incorrect coding or programming errors.  This could be something as simple as flipping a conditional check or it can be much more complex as an algorithm with a very challenging mathematical formula.

## Eclipse Debugger

The Eclipse Debugger is integrated into the Eclipse IDE and is a set of views joined with the editor.  There is a perspective preconfigured for the debugger which uses the editor as the main piece to show the current executing lines and add break points.  You may customize this layout like any other Eclipse display.   The views of interest that support the Eclipse debugger are: 

- Breakpoints
- Debug
- Variables

![Eclipse Debugger](/images/intro-to-java/eclipse-debugger-full.png)

### Breakpoints

Breakpoints are used to tell the debugger when and where to stop.  You place a breakpoint in the Eclipse editor by double click in the left most section of the editor on the line that you want to pause executing.  When you do this, a small dot appears which symbolizes the breakpoint and the line it's attached to.

![Eclipse Breakpoint in editor](/images/intro-to-java/eclipse-debugger-breakpoint-editor.png)

Notice the small blue dot on line 37, this means that when the program is executing, and it reaches that line, the Eclipse debugger will pause the running application at that line.

The Breakpoint View shows all the defined breakpoints that are currently set across all loaded projects in Eclipse. 

![Eclipse Breakpoint View](/images/intro-to-java/eclipse-debugger-breakpoints-view.png)

This provides a convienent list of all the defined breakpoints.  This allows you to enable and disable breakpoints, edit or remove them and if you double click on one it will jump to it in the editor.  This view also allows you to create specialized breakpoint that will break on a specific type of exception.

Breakpoints are the heart of debugging because they provide you an entry point into the executing code.

### Debug View

![Eclipse Breakpoint View](/images/intro-to-java/eclipse-debugger-debug-view.png)

The Debug view shows you the current executing processes with all of its threads and the call stack to the point where you are stopped.  In the above view, we are running the *ExampleConditions* java application and we have a single thread that is paused (suspended).   It then shows the call stack of how we got to the line that we paused on.   The code started at main on line 56 and then went into the ifExamples method (which takes two integers as input) and then hit line 37.   Selecting a line in this view will cause the Eclipse editor to jump to that line and highlight it.   This view and the text editor are synchronized so your can see the code execute as you execute different lines.

The other important part related to this view is the toolbar buttons that allow you to control what occurs next after you suspend the application. 

![Eclipse Breakpoint View](/images/intro-to-java/eclipse-debugger-debug-actions.png)

Each button symbolizes a path from the current breakpoint.  From left to right we have:

- Play - This will cause the execution to continue until the next breakpoint is hit or the end of the application is reached.
- Pause - If the application is currently executing, the pause will cause it to stop on whatever line it is currently processing when the button is pressed.
- Stop - This terminates the process at the current point.   This is like taking the rug out from under the application and killing it immediately.
- Disconnect - Allows you to disconnect the debugger so the application can run without it being attached any more.
- Step Into - This is used to step to the next executing line.  If that is a method call, then pressing this will cause it to go down into the methods first line.
- Step Over  - This is used to step to the next executing line.  If that is a method call, then this will execute all the statements in the method and return from it and move to the next line without going down into the method.
- Step Return - This is used to cause the application to finish processing its current point to the end of the method and then jumping up to the method that called it.   This way you can go back up the call stack.

With these buttons and the view of the current executing call stack, it provides you all the control you'll need to determine how to proceed.

## Variables

![Eclipse Breakpoint View](/images/intro-to-java/eclipse-debugger-variable-view.png)

As we've discussed, variables are what hold all the data in your application and reference the location in memory.   The variable view provides a method to see exactly what those variables are referencing in memory at the current execution point the application is suspended at.  This shows all the variables available to the current executing line, which means the method variables and any of the class variables will be shown in that view.

This view shows the variable names on the left with the current value on the right.  If the variable is an object or an array, the view allow you to expand the item to see what variables it contains (Object) or what the elements of the array are holding.

This is a excellent part of debugging because it gives you insight into what your application is really doing.   During debugging, this will be something you reference heavily so you can check that your variables are referencing the value you expect.  It can also be nice to see how things change as each line gets executed using the step actions (step into, step over, step return).  The view will highlight any lines when the current execution changes the value.  As you step from line to line, you can see how the variables change and see what your application is doing.

## Conclusion

This only touched on how to use the debugger but learning how to set a breakpoint, stepping into, out of and over will take you really far.   Any time you need to figure out what your application is doing at a any point, and visualizing how its progressing will go a long way when learning and troubleshooting code.

The Eclipse debugger is very easy to use and get started with.  The Debugging perspective will set up the editor and views so you can get started right away.   Seeing the code in the editor and being able to step through it is a nice feature provided by Eclipse.