---
title: "Debugging - Video"
date: 2021-04-03
description: Debugging software and the Debugger.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/debugging-screen.png)](https://youtu.be/-fCcxppNhB4)

## Slides

{{< revealjs >}}

## Debugging

- Bugs happen
   - Developers make mistakes
   - Logic errors
   - Other issues
- Debugging code can be difficult
   - Eclipse Debugger can help

---

## Bugs

- First bug found on Sept 9, 1947 in the Mark II
- Anything that causes the application to not work
- Examples
   - Flipping a conditional
   - Complex calculation
   - Network communications failure
   
---

## Eclipse Debugger

- Integrated into the IDE
- Few views + the Editor
- Default Perspective
- Customizable layout

--

## Eclipse Debugger

![Eclipse Debugger](/site/images/intro-to-java/eclipse-debugger-full.png)

---

## Breakpoints

- Tell debugger where to stop
- Suspends execution when the code is hit
- Shown in the editor
- All listed in the Breakpoint view

--

## Breakpoints - Editor

![Eclipse Breakpoint in editor](/site/images/intro-to-java/eclipse-debugger-breakpoint-editor.png)

--

## Breakpoints - View

![Eclipse Breakpoint View](/site/images/intro-to-java/eclipse-debugger-breakpoints-view.png)

---

## Debug View

- Running applications
- Threads
- Current call stack

![Eclipse Breakpoint View](/site/images/intro-to-java/eclipse-debugger-debug-view.png)

--

## Debug View Toolbar

![Eclipse Breakpoint View](/site/images/intro-to-java/eclipse-debugger-debug-actions.png)

- Play
- Pause
- Stop
- Disconnect
- Step Into
- Step Over
- Step Return

---

## Variables

- Variables hold data
- Variables are pointers to memory locations
- View allows seeing this data

![Eclipse Breakpoint View](/site/images/intro-to-java/eclipse-debugger-variable-view.png)

---

## Conclusion

- Breakpoints stop the execution 
   - To get you into debugging
- Good for seeing the execution
   - How the data changes
- Step Into, Step Over, Step Return

{{< /revealjs >}}