---
title: "Files & Streams - Video"
date: 2021-02-22
description: Reading, writing and working with files.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/files-screen.png)](https://youtu.be/csQ5qNcY0Ls)

## Slides

{{< revealjs >}}

## Files

- Applications need to read/write files
- Files are good for 
   - Storing data
   - Holding configuration
- Hard drives are persistent storage
- Types
   - Text Files
   - Binary Files

--

## Text Files

- ASCII
   - American Standard Code for Information Interchange
   - Standard set of characters
- Opens with a text editor
- Examples
   - Configuration
   - Web pages
   - Documentation
   - much more...

--

## Binary Files

- Machine readable
- Data is not easily read
- Application specific
- Standard formats exist
- Example
   - Images - png, gif
   - Video - wav, mp4
   - Music - avi, mp3

---

## Java File

- Objects to work with files
- Abstraction to work with any file system
- Accessing directories and files 
- Reading/Writing

--

## Java Packages

- java.io
   - File
   - Input/Output Streams
- java.nio
   - Path
   - Files

--- 

## *java.io.File*

- Represents a directory or file
- Attributes 
  - name
  - size
  - ownership
  - permissions
  - creation / modification
- Abstracts the path (/ vs \\)

--

## File Helpers

- Static Fields
   - pathSeparator 
      - /
      - \\
   - separator
      - :
      - ;

--

## File Methods

- createNewFile
- delete
- exists
- getName
- getParentFile
- isDirectory/isFile
- length
- listFiles
- mkdirs
- toPath

--

## java.nio.file.Path

- Interface for handle file system usage
- Represents the directory tree and the file
- *Files*
   - Static methods 
   - Works with *Path*

--

## Path Methods

- getFileName
- getFileSystem
- getName
- getParent
- getRoot
- toFile

--- 

## Streams

- Used to read/write content
- IO - Input Output
- Exceptions

--

## Input Stream

- Reading content
- Abstract class for all input streams
- Reads byte data
- Implementations designed for different purpose

--

## Input Stream Classes

![InputStream Instances](/site/images/intro-to-java/inputstream.png)

--

## Input Stream Example

```java
try (FileInputStream inputStream = 
         new FileInputStream("InputFile.txt")) {

   int b = 0;

   while ((b = inputStream.read()) != -1) {
     System.out.print((char) b);
   }
   System.out.println();

 } catch (final Exception e) {
   e.printStackTrace();
 }
```

--

## Output Stream

- Writing content
- Abstract class for all output streams
- Writes byte data out
- Basis for all implementations

--

## Output Stream Classes

![OutputStream Instances](/site/images/intro-to-java/outputstream.png)

--

## Output Stream Example

```java
try (FileOutputStream outputStream = 
         new FileOutputStream("OutputFile.txt")) {

   outputStream.write("Put me in a file please.".getBytes());
   System.out.println("Wrote file to: OutputFile.txt");

 } catch (final Exception e) {
   e.printStackTrace();
 }
```

--

## Chaining Streams

- Streams are powerful
- Constructor takes in a stream
- Chaining together to adjust input/output

--

## Chaining Streams Example

```java
BufferedInputStream inputStream =
  new BufferedInputStream(new FileInputStream("InputFile.txt"))
```

---

## Conclusion

- Files are abstracted in Java
- Streams provide reading and writing support
- Chaining streams together to improve capability

{{< /revealjs >}}