---
title: "Threading - Video"
date: 2021-03-03
description: What are threads and how do we make the application run many things at once.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/threads-screen.png)](https://youtu.be/7I7G3hou0RE)

## Slides

{{< revealjs >}}

## Thread

- Main method is Java's first thread
   - Runs one instruction at a time
- Threads aka Concurrency
- *Thread* object
- *Runnable* object

--- 

## Runnable

- Interface to tell threads what to run
- Single method *run()*
- Can be used for executing anything

--

## Runnable Instance

```java
public class CountingRunnable implements Runnable {

  @Override
  public void run() {

    for (int i = 1; i <= 60; i++) {

      System.out.println("[Counter Thread] " + i + " s");

      try {
        Thread.sleep(1000);
      } catch (final InterruptedException e) {
        // ignore since we're just using it to pause.
      }
    }
  }
}
```

---

## Create A Thread

- Instantiate a *Thread* Class
- Provides something to run (*Runnable*)
- Call *start*
- Runs until it ends or is forced to terminate

```java
Thread thread = new Thread(new CountingRunnable());

thread.start();
```

--

## Threads

![Debugger View of Threads](/site/images/intro-to-java/threads-debugger.png)

---

## Sleep

- A way to pause a thread
- Takes an amount of time to sleep
- Static method on *Thread*
- Throws interrupt exception

---

## Synchronization

- Handles concurrency issues
- Bounds to a resource
- Resource is locked upon use
- Protects the resource

--

## Concurrency Thoughts

- Think about how the thread uses the data
- Protecting the data is important
- Concurrent reads/modify can throw exceptions
- Mistakes can be difficult problems to solve

---

## *wait* & *notify*

- Methods on the *Object* class
   - Every object therefore has it
- Allows control over the resources access

--

## Wait

- Control how/when a Thread will access a resource
- Calling thread pauses
- Similar to sleep but not end time
   - Must be notified to be woken up

--

## Notify/Notify All

- Notifies the waiting thread it can start again
- Doesn't guarantee it can access the resource
   - Has to release the resource 
- *notifyAll* notifies all waiting threads

---

## Conclusion

- Concurrency 
- Very useful 
- More challenging and more to worry about
- *synchronized* to protect resources
- *wait* and *notify* for fine grain control

{{< /revealjs >}}