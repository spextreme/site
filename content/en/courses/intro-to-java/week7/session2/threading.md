---
title: "Threading"
date: 2021-03-03
description: What are threads and how do we make the application run many things at once.
draft: false
collapsible: false
weight: 15
---

## Introduction

All applications start from *main()* which is a single thread your application runs within.   A thread only runs one instruction at a time until it completes all the instructions provided.   Therefore, a *Thread* provides a method to execute some sequence of instructions at the same time the main (or other threads) are running.  This means that if you want your application to run more than one thing at a time, you'll need to create more threads than just the main execution thread.  Threads, in essence, provide your application with different execution paths.

Java has a nice thread model that makes it easy to spawn additional Threads within your application.  It all begins with an object called *Thread* and what it will run with a *Runnable*.

### Runnable

*Runnable* is an interface that has a single method called *run()*, which will be used to trigger the content within the *run* method when the *Thread* starts.   Since *Runnable* is an interface, it may be added to any class that wishes to make itself able to be run in a *Thread*.

Creating a *Runnable* is as easy as implementing it in a class.  Since it's an interface with one method, the only thing needed is adding the code to the run method of what you want the thread to execute.  In the below example, notice the *implements Runnable* on the class line, and the *public void run()* method.   That came from the *Runnable* interface, and then the code in the run method will count from 1 to 60 and output the number every second (see [*Thread.sleep()*](courses/intro-to-java/week7/session2/threading/#sleeping)).

```java
public class CountingRunnable implements Runnable {

  @Override
  public void run() {

    for (int i = 1; i <= 60; i++) {

      System.out.println("[Counter Thread] " + i + " s");

      try {

        Thread.sleep(1000);

      } catch (final InterruptedException e) {
        // ignore since we're just using it to pause.
      }
    }
  }
}
```

## *Thread*

The *Thread* class is what Java uses to represent the thread in your application. If you run it in the debugger, you will notice the available threads running your application.

![Debugger View of Threads](/images/intro-to-java/threads-debugger.png)

As the debugger view shows, there are a couple of threads in this example.   The one labeled 'main' is the main executable application while the other two threads are the test framework and another to execute a network server thread the test started.

Creating a thread can be done by instantiating a *Thread* instance.  In many cases, you'll supply a *Runnable* instance for it to execute.  After creating the instance with something to run, you'll call the *start()* method.

```java
Thread thread = new Thread(new CountingRunnable());

thread.start();
```

This shows creating a *Thread* with a *CountingRunnable* instance which has implemented the *Runnable* interface.   When the *start()* method is then called, it will execute the code in the *CountingRunnable* run method. 

With *Thread*s and *Runnable*s you will be able to create more complex logic in your application.   A real life example is how a web server works. A web server is waiting for a page request.  When it receives it, it spawns a thread to process that request, while the main thread goes back to listening for the next request.  The request then processes the page request and returns the content to the user.  Once the response is made, the thread exits.  Threads provide a lot of capability, but they do make things more complicated and offer some risk to data.

### Sleeping

There are times when you'll need a thread, even the main one, to pause for a period of time.   Pausing a thread will halt it until the amount of time passes.  This can be done to allow other threads to do work, or maybe the paused thread needs to delay for a period before it can continue.   The example above (under [*Runnable*](courses/intro-to-java/week7/session2/threading/#runnable)) used *Thread.sleep()* to pause for one thousand milliseconds (1 second) so the counter prints once per second.   If that sleep wasn't present, then the computer would print out 1 to 60 as quick as it can.

*sleep()* is a static method, and when you call it, it takes in the total number of milliseconds to pause for.

```java
Thread.sleep(500);
```

This will pause for half a second and then continue processing.   Also note that the *sleep* method will throw a checked exception of *InterruptedException* if something prematurely cancels the sleep. 

## Synchronization

Getting into thread programming opens a door to new possibilities within your application, but it also brings with it a new area to be cautious about.   By running things concurrently, if each thread is going to interact with a resource, there is a risk if they both modify the content.  If multiple threads change data on the shared resources, then this has the potential to corrupt the content, and one or all of the threads throw errors.   

For example, if there is a list of 6 items, and two threads try to delete an item, one at the end of the list and one in the middle, then the potential exists that this program will fail.   The first execution may work perfectly.  However, the second run causes an *IndexOutOfBoundsException*. The reason for this is that, during the first execution, the thread order was the second one deleted from the end and then the first thread deleted from the middle.   During the second run, the first thread deleted from the middle while the second thread had the index of the last item at 5, but then, when it tried to remove index 5, it had been changed so that there was one less on the list.

As this shows, having multiple threads can be difficult to manage data when both want to manipulate it.   Luckily, Java provides a keyword to help with this.    When resources are told to be *synchronized*, then the application will block when two threads attempt access data.  Typically, this is more important for modifying, deleting or adding content.   For reading, it usually won't matter since the data stays constant, but if one thread modifies the list while another is reading, then there is a potential issue.  

By wrapping with the *synchronized* block, this tells the JVM when two threads access this resource, the first one will request a lock on the resource and begin processing it.  Once it is done with the resource, it releases the lock, which will then allow the second thread to obtain the lock and process.  Putting this on a variable will protect that variable when it is accessed by multiple threads.

To continue with the example, if the list is wrapped with *synchronized*, then the first thread will lock the resources and perform its work on the list (delete a row).  The second thread will wait for the list resource to be unlocked before it will continue.  By using *synchronized*, this code will now always work since each thread will access the list before or after the other thread.

*synchronized* is a great way to protect resources.  Just remember, when you get into thread programming, to really think about what it will do and how the data is shared.   Good planning and thinking how multiple threads will use the data can save you hours in debugging random errors.  This is where a lot of logic errors get induced and make it difficult to identify the issue.

## *wait* & *notify*

This is more of an advanced topic, but it is good to have a cursory understanding of how they relate to an *Object* and concurrency programming.   The *Object* class contains the methods *wait()*, *notify()*, and *notifyAll()* which means any object in Java will inherite them and be able to use them.   These methods may be used to perform different actions related to Thread programming.   

### wait

The *wait* method causes the calling thread to give up execution and sleep while it waits for another thread to notify it that it should continue.  It's a way for the calling thread to release the lock on a resource to give a chance for another thread to work with it.  This is so you can control how threads will block and release a resource.

### notify

This will notify the waiting thread that it can go back to work and start processing. It doesn't guarantee the waiting thread will continue because the notifying thread has to also release the lock on the resource.  Once the resource is released, the waiting thread will continue.

The *notifyAll* is identical to the *notify* but will tell all waiting threads to continue.

## Conclusion

Thread programming can be useful when your application needs to run concurrent processing.   It does make resource management a little more challenging with the need to protect those resources.  Using *synchronized* to wrap around a variable, Java will make sure the data is protected from multiple threads.   *wait* and *notify* provide a little more control over this protection.
