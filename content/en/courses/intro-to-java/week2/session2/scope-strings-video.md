---
title: "Scope & Strings - Video"
date: 2021-01-27T10:08:56+09:00
description: Introduction to Scope and a in depth on Strings.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/scope-system-string-screen.png)](https://youtu.be/ZZt7a41wl2c)

## Slides

{{< revealjs >}}

## Introduction

- Scope
- System Object
- Strings In Depth

---

## Scope

- Think about sharing information
   - Bank account or personal details
- OOP this is true with who can access object data
- Layers of an application
   - Application
      - packages
         - classes
            - fields
            - methods
               - variables
--

## 4 Scope Attributes
   - public
   - protected
   - private
   - package private

--

## public

- *public* keyword
- Can be access by any layer

```java
public class ShapePublic {

  // A field defined as public.
  public int width = 0;

  // A get method defined as public.
  public int getWidth() {
    return width;
  }
}
```

--

## private

- *private* keyword
- Internal to an object
- Accessible only to methods, fields in the object

```java
public class ShapePrivate {
  // A field defined as private.
  private int width = 0;

  // A get method defined as public to allow access to the field.
  public int getWidth() {
    return width;
  }

  // A set method to update the private field.
  public void setWidth(int width) {
    this.width = width;
  }
}
```

--

## protected

- *protected* keyword
- Hides fields/methods from others
- Objects that inherit can access

```java
public class ShapeProtected {
  // A field defined as protected.
  protected int width = 0;
}

public class Square extends ShapeProtected {
  // A get method defined for the protected shape width field.
  public int getWidth() {
    return width;
  }

  // A set method defined for the protected shape width field.
  public void setWidth(int width) {
    this.width = width;
  }
}
```

--

## package private

- Public to items in the same package
- Items outside the package its private

```java
package com.spextreme.scope;

public class ShapePackagePrivate {
  // A field defined as package private.
  int width = 0;

  // A get method defined as public to allow access to the field.
  public int getWidth() {
    return width;
  }

  // a set method to update the field but only things in this package (scope) can access it.
  void setWidth(int width) {
    this.width = width;
  }
}
```

--

## Scope Summary

| Modifier | Class | Package | Subclass | World |
| -------- | ----- | ------- | -------- | ----- |
| public | Y | Y | Y | Y |
| protected | Y | Y | Y | N |
| package private | Y | Y | N | N |
| private | Y | N | N | N |

---

## System

- Java is platform agnostic
- System means the JVM
- *System* is an object representing it
- Created by the JVM
- Always available

--

## Streams

- Used for input/output of data
   - Networking, files, console
- System creates *in*, *out* and *err* streams
- *in* for input
- *out*, *err* for output

--

## Input Stream

- Use to read input from console
- Contains read methods

```java
System.in
```
--

## PrintStream

- *err* and *out* are both *PrintStream* objects
- Both used to output
- *err* separate to allow filtering errors
- Access to the output stream

```java
System.out
System.err
```
--

## PrintStream Methods

- print
- printf
- println

---

## Strings

- Most common thing in Java
- Strings are hard for computers
- *String* is an array of characters
- Java makes *String* core to the language
- *String* is an object

--

## Creation

- Two ways to create a String

```java
String firstString = "My First String";

String secondString = new String( "My second String" );

--

## Concatenation

- Joining things together
- Creates a new object
- Strings are immutable

```java
String a = "Hello";
String b = "World";

String newString = a + " " + b;
```

--

## Formatting

- Provides a way to format a string
- Multiple ways to format
- Concatenation

```java
final String pre = "The name of the professor for";
final String className = "Intro to Java";
final int courseNum = 1140;
final String post = "is";
final String name = "Steve";

String finalString =
  pre + " cs0" + courseNum + "-" +
     className + " " + post + " " + name + ".";
```

--

## String.format

- Methods of String
- Uses format specifiers
   - %d
   - %s
   - many more

```java
final String pre = "The name of the professor for";
final String className = "Intro to Java";
final int courseNum = 1140;
final String post = "is";
final String name = "Steve";

final String finalString =
  String.format("%s cs0%d-%s %s %s.", pre,
     courseNum, className, post, name);
```

--

## Methods

- Many methods in a String
- See the Javadoc for a full list
- Can't cover all of them

--

## equality

- Strings are an array of characters
- Hello and hello are not equal
   unless ignoring case
- Returns *true* or *false*

```java
"hello".equals("Hello"); // false
"hello".equals("hello"); // true
"hello".equals("world"); // false
"hello".equalsIgnoreCase("Hello"); // true
```

--

## contains

- Checks if a pattern is matched within the string
- Returns *true* or *false*

```java
"The cow jumped over the moon".contains("jump"); // true
"The cow jumped over the moon".contains("hello"); // false
```

--

## length

- Returns a positive integer
- Number of characters in string

```java
"The cow jumped over the moon".length(); // 28
```

--

## indexOf

- Returns the index where pattern begins
- 0 base index

```java
"Hello World!".indexOf("World"); // 6
"Hello World!".indexOf(" "); // 5
"Hello World!".indexOf("Intro"); // -1
```
--

## lastIndexOf

- Returns the last index where pattern begins
- 0 base index

```java
"Hello world!  What a big world it can be and it may go round and round.".lastIndexOf("world"); // 25
"Hello world!  What a big world it can be and it may go round and round.".lastIndexOf("round"); // 65
"Hello world!  What a big world it can be and it may go round and round.".lastIndexOf("not"); // -1
```
--

## isEmpty/isBlank

- Empty
   - Checks contains no characters
- Blank
   - Checks no characters
   - No white space

```java
"".isEmpty(); // true
"   ".isEmpty(); // false
"Hello World!".isEmpty(); // false
"".isBlank(); // true
"   ".isBlank(); // true
"Hello World!".isBlank(); // false
```

--

## substring

- Cuts a section out of the string
- Two forms
   - Beginning index
   - Beginning/Ending index

```java
"This is a string. It contains two sentences.".substring(18); //It contains two sentences.
"This is a string. It contains two sentences.".substring(0,17); //This is a string.
"This is a string. It contains two sentences.".substring(17); // It contains two sentences.
```
--

## toLowerCase/toUpperCase

- Converts string to
   - Lower case
   - Upper case

```java
"Hello World!".toLowerCase(); // hello world!
"Hello World!".toUpperCase(); // HELLO WORLD!
```

--

## valueOf\*

- Converts the input to a String
- Supports all types

```java
String.valueOf(true); // true
String.valueOf(false); // false
String.valueOf('a'); // a
String.valueOf(new char[] {'a', 'b', 'c'}); // abc
String.valueOf(342.92384d); // 342.92384
String.valueOf(3.1415f); // 3.1415
String.valueOf((short)1); // 1
String.valueOf(13); // 13
String.valueOf(1234567890123414L); // 1234567890123414
String.valueOf(object); // java.lang.Object@368239c8
```

---

## Conclusion

- Scope
   - Very important for controlling data
- System
   - Useful early on
- Strings
   - You'll use these often
   
{{< /revealjs >}}