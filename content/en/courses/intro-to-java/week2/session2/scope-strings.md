---
title: "Scope & Strings"
date: 2021-01-27T10:08:56+09:00
description: Introduction to Scope and a in depth on Strings.
draft: false
collapsible: false
weight: 15
---

## Introduction

In this section, we'll cover more foundational items including Scope, the *System* object and Strings.


## Scope

In programming, content may be shared across different parts of the application while other content may need to be hidden.  If you think about a bank account, you are fine with yourself seeing the account and its balance, but you probably don't want a stranger to have access to your account or any of the details of it.

In object oriented programming, this is also true.  There may be content internal to an object that shouldn't be accessible by the outside world.  There may also be content you want to share but limit who it is shared with.

This is where scope comes in and will help to control access to content.  Scope has 4 main types:

- public
- protected
- private
- package private

To understand what these mean, we need to understand what is involved and the different layers an application includes.

From the lowest level we have a variable and where it is stored.   If the variable is inside a method, it's only accessible to the content within the method.  

The next layer up is the class level, which is where fields (variables) and methods are defined.   This is where controlling scope begins.   The class may then be part of a package, which contains other objects.   Next you have the other packages, and finally you have your application.  If we think of this as a tree it would branch as follows.

- Users
  - Application
     - packages
         - classes
            - fields
            - methods
               - variables

A method owns its variables without exception.  A class owns the fields and methods it defines within it.  Packages are a way to group classes together.  Finally you have your application.

Users are the outside world who may be accessing your application, or connecting to it from another application.  All of this relates to scope and controlling who can do what at each level.
          
### Public

As the name suggests, this means it's fully available and can be seen by all layers.   Other objects within the same package, and the total collection of objects in the application, can access the item marked as public.  The keyword to mark a field, method, or class is *public*.

```java
public class ShapePublic {

  // A field defined as public.
  public int width = 0;

  // A get method defined as public.
  public int getWidth() {
    return width;
  }
}
```

### Private

The *private* keyword placed in front means that this can not be accessed outside of the class.   A method or field defined as private hides itself in the class and only the other fields/methods within the class can access it.  

This is used typically to hide the class fields, but then make a method public to access the field's value.  Methods may also be made private so they may only be used by other methods within the class.  This is done mainly to control access to data and make sure that others can't access or change the data without the object's knowledge. 

The following makes a variable private, but then adds two methods to allow other objects to set (called a setter method) and get (called a getter method) the data the object controls.  This is a standard model in Java.

```java
public class ShapePrivate {

  // A field defined as private.
  private int width = 0;

  // A get method defined as public to allow access to the field.
  public int getWidth() {
    return width;
  }

  // A set method to update the private field.
  public void setWidth(int width) {
    this.width = width;
  }
}
```

### Protected

Protected has a special meaning when it comes to objects.   Protected means the variable or method can not be accessed from outside of the object.  In this context it is identical to private.  But where it's different is if another object inherits or extends (inheritance to be covered later in the course) this object, then it may access the items marked as protected.

```java
public class ShapeProtected {

  // A field defined as protected.
  protected int width = 0;
}

public class Square extends ShapeProtected {

  // A get method defined for the protected shape width field.
  public int getWidth() {
    return width;
  }

  // A set method defined for the protected shape width field.
  public void setWidth(int width) {
    this.width = width;
  }
}
```

As the code shows, the *ShapeProtected* defines the fields but then the *Square* object has access to it and can manipulate or get its value.  This is only possible because of the *extends* keyword used.  Otherwise the *width* variable will not be accessible to anything outside the object.

## Package Private

By now you should have a good guess what a package private will mean.   This means that any object in the same package can access the content.  The interesting part of package private is that it has no keyword.  To set a field or method as package private, you simple don't provide a keyword in front of the type.

```java
package com.spextreme.scope;

public class ShapePackagePrivate {

  // A field defined as package private.
  int width = 0;

  // A get method defined as public to allow access to the field.
  public int getWidth() {
    return width;
  }

  // a set method to update the field but only things in this package (scope) can access it.
  void setWidth(int width) {
    this.width = width;
  }
}
```

### Summary

As shown, there are 4 main scope definitions.   Each serves a purpose for controlling access to fields and methods in a class or the class itself.

Here is a quick table to show scope as defined by each type.

| Modifier | Class | Package | Subclass | World |
| -------- | ----- | ------- | -------- | ----- |
| public | Y | Y | Y | Y |
| protected | Y | Y | Y | N |
| package private | Y | Y | N | N |
| private | Y | N | N | N |


## System

We talked about the console when we showed how an IDE integrates one to support output from an  application.   The console is just a text output area and could be a terminal on your computer as well.

When you need to output data, whether to the screen, a network, or a file, its all done using a concept called a output stream.   Streams are used to read (input) and write (output) data.  There are a host of input and output streams in Java to support reading and writing for many different models (files, network, etc.).

Also in Java, because of its platform agnostic solution, it needed to provide a way to interact with the operating system so it can access things like a terminal window.   Java created an object called *System* which is design to be a general access point to some basic things in the OS.  The Java Virtual Machine (JVM) is the system in this context.

Now we know there are streams and a System object.   You can access the System object without creating one because the JVM has made it available and set it up.  Then all the methods on it are available.  System has three steams available: *in*; *out*; *err*;  

The *in* stream is for inputting content from the console or terminal.  It may be used to accept user input.  It contains *read* methods to access the input data.

```java
System.in
```

The *err* and *out* are both output streams which are used to output information from the application.  The *err* stream is a special type, so things going to the console are tagged differently.  This is generally not really used by beginning developers.

```java
System.out 
System.err
```

*System.out* gives you access to an output stream.  An output stream contains a host of methods on it to write data.  Since this is backed by a PrintStream object, most of the methods you'll use start with the word *print*.

- print
- printf
- println

The main thing to note is they all send output to the console.  *print* does nothing more then output what is fed to it unchanged.  *println* outputs and appends a new line character on the end of the stream to send the cursor to the next line.  The *printf* will use format specifiers (discussed under String formatting) to format the output as it is written.

This way, your applications have the ability to write data out to the user for any purpose.

## Strings

Strings are one of the most common things you'll find in programming, mainly because it's what we as humans understand.   The interesting part, however, is that strings for a computer are very challenging because they mean very little to a computer.

Think of a string as a sequence of characters strung together.  This is how a computer handles them.   In most older languages, strings are an array of characters (char []).   Java was one of the first languages to make Strings a more natural capability.

A Java String is an object, but holds some special abilities within the language.   This means it can be created more easily then a normal object.

### Creation

To create a string is similar to creating a variable.   If you remember from [Fields](courses/intro-to-java/week1/session2/content/) the creation of one is: 

```
<type> <name> = <value>
```

String is the type, so creating a string is done by putting the words in double quotes (")

```java
String firstString = "My First String";
```

Strings may also be created like an object.  This is done by using the *new* keyword.

```java
String secondString = new String( "My second String" );
```

In most cases the first method is used for simplicity.

### Concatenation

What would you say if I told you strings can be added together?  Well, this is a true statement, but it is technically called String concatenation.   Concatenation means to join things together.  In Java, concatenation is the act of taking a string and appending it with another String to create a new string. 

For example, lets start with two strings:

```java
String a = "Hello";
String b = "World";
```

If we add *a* and *b* together with a space, we'll get a new string 'Hello World'

```java
String newString = a + " " + b;

System.out.println(newString);
```

```bash
Hello World
```

The thing to make clear here is that *a*, *b*, and *newString* are all separate strings (objects in memory).

This is important because a Java String is immutable.  This means, once it's created it can't be changed or modified.  Therefore, any concatenation done will always create a new String object.

### Formatting

Strings are often outputted to the user and are generally incomplete, needing parts of the output to be filled in.   In this, the String object provides ways to format the String.

#### Format by Concatenation

The String instance has a lot of ways to format output. As was shown, it is possible to use concatenation to format a string by joining all the parts together.

```java
final String pre = "The name of the professor for";
final String className = "Intro to Java";
final int courseNum = 1140;
final String post = "is";
final String name = "Steve";

String finalString = pre + " cs0" + courseNum + "-" + className + " " + post + " " + name + ".";

System.out.println(finalString);
```

```bash
The name of the professor for cs01140-Intro to Java is Steve.
```

As this shows, it will concatenate all the parts together, saving it to a new string *finalString*.   While this is done often and works perfectly fine, it may not be as efficient since it has to create a new String for each concatenation.

#### Format by *String.format*

An alternate method is to use the *String.format* method.  This is a method that is part of *String* object and can format the string together in a similar way.

```java
final String pre = "The name of the professor for";
final String className = "Intro to Java";
final int courseNum = 1140;
final String post = "is";
final String name = "Steve";

final String finalString =
  String.format("%s cs0%d-%s %s %s.", pre, courseNum, className, post, name);

System.out.println(finalString);
```

```bash
The name of the professor for cs01140-Intro to Java is Steve.
```

The *String.format* handles all the joining and does it more efficiently.  The *format* call returns a String.   

The *String.format* takes in format specifiers to help guide the output being passed.   The specifiers are the percent character in the format string.  Below are the available specifiers.

|Specifier|Datatype|Output|
|---------|--------|------|
|%a|float|hex of a float|
|%b|any|boolean|
|%c|char|character|
|%d|integer|decimal|
|%e|float|scientific notation|
|%f|float|decimal|
|%g|float|decimal|
|%h|any|hex|
|%n|none|platform line separator|
|%o|integer|octal number|
|%s|any|String|
|%t|date/time|A prefix for data/time conversion used with data/time output specifiers.|
|%x|integer|hex string|

The above table is for reference, and you'll usually look these up when you need to use them.  But if you remember two, then remember %s and %d to handle Strings and Integers.

### Methods
The String class is one of the most used in Java, and it is also one of the more complex due to the amount of capabilities it offers.   Here, we go through some of the more useful methods for the String class.  This is not a complete list, and you can review the Javadoc for [String](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/String.html) if you want the full details.

#### equals/equals ignore case

As we should now be aware, Strings are nothing more than an array of characters.  But because a String is also an object, two objects by default are not equal unless they are both pointing to the same address in memory (remember this from the [computer memory](courses/intro-to-java/week1/session1/computers/#main-memory) discussion).

Therefore, String has a method to compare to see if two strings contain the exact same sequence of characters.   For example "Hello".equals("Hello") will return *true* but "Hello".equals("hello") will be *false* because the h and H characters are different.   

To solve the case problem, there is a second equals method, *equalsIgnoreCase* that will make those equal (*true*).

```java
final String helloLower1 = "hello";
final String helloLower2 = "hello";
final String helloUpper = "Hello";
final String worldLower = "world";

System.out.println("\"" + helloLower1 + "\".equals(\"" + helloUpper + "\"); // "
  + helloLower1.equals(helloUpper));
System.out.println("\"" + helloLower1 + "\".equals(\"" + helloLower2 + "\"); // "
  + helloLower2.equals(helloLower2));
System.out.println("\"" + helloLower1 + "\".equals(\"" + worldLower + "\"); // "
  + helloLower1.equals(worldLower));
System.out.println("\"" + helloLower1 + "\".equalsIgnoreCase(\"" + helloUpper + "\"); // "
  + helloLower1.equalsIgnoreCase(helloUpper));
```

```java
"hello".equals("Hello"); // false
"hello".equals("hello"); // true
"hello".equals("world"); // false
"hello".equalsIgnoreCase("Hello"); // true
```

#### contains

The *contains* method is designed to perform a search within the string to see if it contains a sequence of characters.  If that exact pattern exists in the String, then it will return *true*.  Otherwise it will return *false*.

```java
final String myString = "The cow jumped over the moon";
final String contains = "jump";
final String notContains = "hello";

System.out.println(
  "\"" + myString + "\".contains(\"" + contains + "\"); // " + myString.contains(contains));
System.out.println("\"" + myString + "\".contains(\"" + notContains + "\"); // "
  + myString.contains(notContains));
```

```java
"The cow jumped over the moon".contains("jump"); // true
"The cow jumped over the moon".contains("hello"); // false
```

#### length

Sometimes you'll need to figure out how long a String is, meaning how many characters are contained in the String.   The *length* method will return a positive number of characters this String contains.

```java
final String myString = "The cow jumped over the moon";

System.out.println("\"The cow jumped over the moon\".length(); // " + myString.length());
```

```java
"The cow jumped over the moon".length(); // 28
```

#### indexOf/lastIndexOf

The *indexOf* method is used to find the starting location of a sequence of characters in the String.   This means it will search for the pattern to be an exact match and return the index where the pattern begins.  Note, however, the returned index is based on a 0 starting point (first character is at index 0).

```java
final String myString = "Hello World!";

System.out.println("\"" + myString + "\".indexOf(\"World\"); // " + myString.indexOf("World"));
System.out.println("\"" + myString + "\".indexOf(\" \"); // " + myString.indexOf(" "));
System.out.println("\"" + myString + "\".indexOf(\"Intro\"); // " + myString.indexOf("Intro")); // not found
```

```java
"Hello World!".indexOf("World"); // 6
"Hello World!".indexOf(" "); // 5
"Hello World!".indexOf("Intro"); // -1
```

There are times, however, that a sequence may appear multiple times and you need to find the one that occurs last in the String.   This is where the *lastIndexOf* comes in.

```java
final String myString =
   "Hello world!  What a big world it can be and it may go round and round.";

System.out.println(
   "\"" + myString + "\".lastIndexOf(\"world\"); // " + myString.lastIndexOf("world"));
System.out.println(
   "\"" + myString + "\".lastIndexOf(\"round\"); // " + myString.lastIndexOf("round"));
System.out
   .println("\"" + myString + "\".lastIndexOf(\"not\"); // " + myString.lastIndexOf("not")); 

```

```java
"Hello world!  What a big world it can be and it may go round and round.".lastIndexOf("world"); // 25
"Hello world!  What a big world it can be and it may go round and round.".lastIndexOf("round"); // 65
"Hello world!  What a big world it can be and it may go round and round.".lastIndexOf("not"); // -1
```

#### isEmpty/isBlank

It's hard to believe a concept so simple wasn't in the early versions of the String class.  The *isEmpty* method is used to determine if the String doesn't contain anything (length equals 0).  The isBlank was added to check if it is empty or contains only white space.

```java
final String emptyString = "";
final String blankString = "\t "; // \t is a tab
final String aString = "Hello World!";

System.out.println("\"" + emptyString + "\".isEmpty(); // " + emptyString.isEmpty());
System.out.println("\"" + blankString + "\".isEmpty(); // " + blankString.isEmpty());
System.out.println("\"" + aString + "\".isEmpty(); // " + aString.isEmpty());
System.out.println("\"" + emptyString + "\".isBlank(); // " + emptyString.isBlank());
System.out.println("\"" + blankString + "\".isBlank(); // " + blankString.isBlank());
System.out.println("\"" + aString + "\".isBlank(); // " + aString.isBlank());
```

```java
"".isEmpty(); // true
"   ".isEmpty(); // false
"Hello World!".isEmpty(); // false
"".isBlank(); // true
"   ".isBlank(); // true
"Hello World!".isBlank(); // false
```

#### Substring

Strings are interesting things, and sometimes there is a need to manipulate them.  This may mean you need to pull out a section of the string, or you want to cut the end off.   There are a lot of cases where you'll need to rip apart a string.

```java
final String fullString = "This is a string. It contains two sentences.";

System.out.println("\"" + fullString + "\".substring(18); //" + fullString.substring(18));
System.out.println("\"" + fullString + "\".substring(0,17); //" + fullString.substring(0, 17));

final int index = fullString.indexOf(".") + 1;
System.out.println(
  "\"" + fullString + "\".substring(" + index + "); //" + fullString.substring(index));
```

```java
"This is a string. It contains two sentences.".substring(18); //It contains two sentences.
"This is a string. It contains two sentences.".substring(0,17); //This is a string.
"This is a string. It contains two sentences.".substring(17); // It contains two sentences.
```

#### toLowerCase/toUpperCase

As we discussed, upper and lower case letters are different when representing the same character.  This is because each character is represented by a different byte so comparing 'a' and 'A' is really doing a comparison of the 8 bits that make them up.

|char|byte|
|-|---------|
|a|0110 0001|
|A|0100 0001|

This shows that, while Strings seem like a sequence of characters, they are really a sequence of bits, and comparison and work is really done on the bytes.   

In some cases, you may want to ignore case altogether, as shown with the *equalsIgnoreCase* method.  But the *toLowerCase* and *toUpperCase* offer another solution.

As the names describe, these will convert the entire string to lower or upper case character sets.

```java
final String fullString = "Hello World!";

System.out.println("\"" + fullString + "\".toLowerCase(); // " + fullString.toLowerCase());
System.out.println("\"" + fullString + "\".toUpperCase(); // " + fullString.toUpperCase());
```

```java
"Hello World!".toLowerCase(); // hello world!
"Hello World!".toUpperCase(); // HELLO WORLD!
```

#### ValueOf\*

Java has 8 primitive types and then all those other objects that exist or may not even exist yet.   Everything in Java can be turned into a String.  The language is set up to handle this on objects by allowing every object to define a *toString* method.  Primitives are a little different, but they are ultimately backed by objects too.

The String class contains a collection of *valueOf(\*)* methods to handle each primitive type and an Object type.  Note that when an object doesn't define how to be converted to a String, the objects memory address is outputted as the string.

- valueOf(boolean)
- valueOf(char)
- valueOf(char []) // [] is an array of
- valueOf(double)
- valueOf(float)
- valueOf(int)
- valueOf(long)
- valueOf(Object)

```java
System.out.println("String.valueOf(true); // " + String.valueOf(true));
System.out.println("String.valueOf(false); // " + String.valueOf(false));

System.out.println("String.valueOf('a'); // " + String.valueOf('a'));
System.out.println("String.valueOf(new char[] {'a', 'b', 'c'}); // "
  + String.valueOf(new char[] {'a', 'b', 'c'}));

System.out.println("String.valueOf(342.92384d); // " + String.valueOf(342.92384d));
System.out.println("String.valueOf(3.1415f); // " + String.valueOf(3.1415f));
System.out.println("String.valueOf((short)1); // " + String.valueOf((short) 1));
System.out.println("String.valueOf(13); // " + String.valueOf(13));
System.out
  .println("String.valueOf(1234567890123414L); // " + String.valueOf(1234567890123414L));
System.out.println("String.valueOf(object); // " + String.valueOf(new Object()));
```

```java
String.valueOf(true); // true
String.valueOf(false); // false
String.valueOf('a'); // a
String.valueOf(new char[] {'a', 'b', 'c'}); // abc
String.valueOf(342.92384d); // 342.92384
String.valueOf(3.1415f); // 3.1415
String.valueOf((short)1); // 1
String.valueOf(13); // 13
String.valueOf(1234567890123414L); // 1234567890123414
String.valueOf(object); // java.lang.Object@368239c8
```