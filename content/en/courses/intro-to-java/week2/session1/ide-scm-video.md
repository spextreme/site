---
title: "IDEs, SCMs - Video"
date: 2021-01-19T10:08:56+09:00
description: Introduction to IDEs and Source Control.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/eclispe-git-screen.png)](https://youtu.be/BblZTLV_JOA)

## Slides

{{< revealjs >}}

## Introduction

- Developers enjoy creating things
- New ideas and reinventing solutions
- Some from necessity

---

## IDE

- Integrated Development Environment
- Large collection of capabilities integrated together
- Help developers create and understand code
- Make development easier
- Help with testing and debugging

--

## Text Editing

- It all start with a simple text editor
- Writing code manually is time consuming
- IDEs advance a text editor in many ways
   - Color Coding
   - Auto completion
   - Templating
   - Many more capabilities

--

## Eclipse Text Editor

![Eclipse Text Editor](/site/images/intro-to-java/ide-texteditor.png)

--

## Compiling

- Compiling is an extra step that required
- Time consuming
- IDEs build in a compiler
- Compile as you type
- Syntax checking in real time
- Highlight errors
- Ready to run

--

## Eclipse Compiler Error

![Eclipse Error In Code](/site/images/intro-to-java/ide-codingerror.png)

--

## Running

- IDEs allow executing the application
- Console window to show output

--

## Eclipse Compiler Error

![Eclipse Error In Code](/site/images/intro-to-java//ide-executing.png)

--

## So Much More

- Debugging
- Plugins
- And more

---

## SCM

- Source Control Management
- Very important in code development
- Tracks revisions of content
- Ability to show change history
- Used by Open Source and Companies alike

---

## Git

- Open Source SCM
- Built to handle the Linux Kernel
- Used by most technology companies today
- We'll use Git for class

--

## Repository

- Represents the storage system
- Git uses this to hold the history
- Local file system '.git' directory

``` bash
drwxr-xr-x 1 paulin 197121    0 Dec 28 19:31 .git/
-rw-r--r-- 1 paulin 197121  290 Jan 21 18:41 HelloWorld.java
```

--

## Distributed

- Many SCMs designed around a central server
- Git is the only that isn't
- It's designed to be distributed
- Connect to multiple repos and share between them
- Many companies offering solutions (Git Lab, Git Hub, etc...)

--

## Distributed Example

- GitLab is where this is hosted

![Eclipse Error In Code](/site/images/intro-to-java//ide-gitlab-repository.png)

--

## Commands

- Clone
- Status
- Add
- Remove
- Push
- Pull

--

## Cloning

- Clone - Create a Duplicate
- Git's main concept for mirroring a repo
- Used to get a copy of a repo locally
- *clone* is the command

--

## Cloning Example

```bash
$ git clone https://gitlab.com/spextreme/intro-to-java-sample-repo.git

Cloning into 'intro-to-java-sample-repo'...
remote: Enumerating objects: 50, done.
remote: Counting objects: 100% (50/50), done.
remote: Compressing objects: 100% (38/38), done.
remote: Total 50 (delta 10), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (50/50), done.
Checking connectivity... done.
```

--

## Status

- Git knows what's different
- Compares the working directory to the .git history
- *git status* is used to find out what's different

--

## Status Example

```bash
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   basics/hello.world/HelloWorld.java

no changes added to commit (use "git add" and/or "git commit -a")
```

--

## Stage & Save

- Git has a staging area
- Will only save what is staged
- *git add*
   - Adds content to the queue
- *git remove*
   - Queues a remove of an item
- *git commit*
   - saves the queued changes

--

## Stage & Save Example

```bash
$ git add NewFile.java

$ git rm RemoveFile.java
rm 'RemoveFile.java'

$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   NewFile.java
        deleted:    RemoveFile.java
```

--

## Push & Pull

- Works with remotes
   - Origin is usually the default
- Push
   - Sends local repo changes to remote
- Pull
   - Gets remote changes and updated local repository
   - Merges changes to working copy

--

## Push Example

```bash
$ git push
Counting objects: 3, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 339 bytes | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To git@gitlab.com:spextreme/intro-to-java-sample-repo.git
   0523580..d307473  master -> master
```

--

## Pull Example

```bash
$ git pull
remote: Enumerating objects: 4, done.
remote: Counting objects: 100% (4/4), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), done.
From gitlab.com:spextreme/intro-to-java-sample-repo.git
   d307473..e60bd87  master     -> origin/master
Updating d307473..e60bd87
Fast-forward
 RemoteChange.java | 5 +++++
 1 file changed, 5 insertions(+)
 create mode 100644 RemoteChange.java
```

---

## Conclusion

- IDEs are powerful and feature rich
   - Save time and effort
   - Helps learning
- SCM are important to development
   - Improve comfort with change
- Eclipse and Git will be used a lot

{{< /revealjs >}}