---
title: "IDEs, SCMs"
date: 2021-01-19T10:08:56+09:00
description: Introduction to IDEs and Source Control.
draft: false
collapsible: false
weight: 15
---

## Introduction

Developers like to create things and build software they think others will find useful.  Some come from necessity, others are an idea someone had and thinks would be a good solution.

In this section we'll cover topics centered around Integrated Development Environments (IDEs) and source control.  You may wonder why these topics are coming up early.  This is because they are very useful tools to help you develop and will save you a lot of time and effort as well as help you learn.

## Integrated Development Environment (IDE)

Integrated Development Environments (IDEs) are tools that contain a large collection of capabilities that help developers.  All coding begins with a text editor, then you need to compile the code, then you can finally run it.   An IDE will help do all of these things but adds extra capabilities to color code the syntax, auto complete code, and even debug it.  There is much more to an IDE, so lets break down the different parts.

### Text Editing

Code is written using any standard text document (like our Hello World program) with a .java file extension.  This allows any text editor to be used (Notepad, vi, etc...).

However, writing all the code manually, you'll have to know the words and syntax of the language and hit a lot of keys.  This can even be a challenge for seasoned developers, especially when using libraries from other developers.   

Developers are by nature lazy, so what an IDE has done with a Text Editor is add support to help auto complete the syntax.  It can generate template code like a class or a method, fill in javadoc, and even guess what you're starting to type.  This way you can more quickly complete the syntax or use it to generate files and methods to save time and keystrokes.

While auto completion is one of the main things the IDE text editor will provide, it also will color code and format the syntax for you.   This makes it much easier to read the code because variables, methods, and keywords area all uniquely shown.   Auto-formatting is another great feature because it frees you from having to hit tab or space to make it look well organized.

![Eclipse Text Editor](/images/intro-to-java/ide-texteditor.png)

This shows how our simple Hello World program color codes the syntax and is formatted.  The comments are easy to see, the keywords stand out, and the strings are also easy to identify.  As you can see, this does improve the ability to understand the code.

IDEs have a lot of other features around this, but we won't go into all the details.  Just know there is a lot to discover as you get further into your development activities.

### Compiling

As mentioned, an IDE provides the developer with the ability to compile the code without the need to run the compiler manually.   It doesn't end there, however.   Most IDEs integrate a compiler so that it is running as you type code.   This means that as you're typing the code, it can perform syntax checking and highlight when you have an error or something may not be correct.

![Eclipse Error In Code](/images/intro-to-java/ide-codingerror.png)

As shown in this screenshot, the editor marks the line that has an error as well as underlines where the error may be found.  In this case, we forgot to put the 'n' on the println call.  

It can even go further with this by showing potential errors or warn you of bad practices.   There are lots of things the IDE can check.

The last part of compiling is that as you save the file, it will automatically generate the .class file.  Remember from the Hello World, we ran javac to compile the .java file into a .class file.   The IDE will be doing this constantly, so it makes it easier to launch our application.

### Running

The beauty of the compiling constantly being done in the background is that it means when it's time to run your application it is all ready to start.

![Eclipse Error In Code](/images/intro-to-java/ide-executing.png)

As this shows, not only can the program be launched, the IDE has a console window to output the information from the program.  When a program runs and needs to output information to the screen, this is done with a console.  

### The Works

We haven't even scratched the surface of an IDE yet.   There are so many other features in an IDE that it would take up an entire book to cover them all.  Here are some others worth mentioning.

#### Debugging

Now that we can easily run the application, it's sometimes nice to step through the code line by line to evaluate what the code is doing.  The IDE usually includes a built in debugger.  The nice part about this is that the debugger will show the code in the text editor, provide a memory view to show the variables and their value stored in memory and allow you to step into or step over the line of code.  This means a method call can be jumped into (Step Into).   Stepping over is when you want the method to run but you don't need to go into the method.   This is all triggered by setting a break point in the code.   This means that when the application executes that line of code, it will stop and wait for the user to determine what to do.   Debuggers are a great way to find issues, see what your program is doing, and trace along as it executes.

#### Plugin

Many IDEs provide a way for developers to extend the capability of the IDE.  For example, maybe we want to check our code for "code smells" (Bad coding practices).   There are tools that will do this, and the makers of tools like SpotBugs make their tool runable in the IDE.   This way it can be more easily integrated for the developer and provide inline feedback along with the compiler.

## Source Control Management System (SCM)

Source control is one of the main things that will be very important when writing code.   In the beginning, when you start, you wouldn't even consider it, but once understood, it becomes a way for you to become much more comfortable with change.

Source control is designed to manage content and track the revision of files as changes are made.   Think about it this way, you write your first program, Hello World.   You save it to the source control system then make a change.   You try it out, and it fails miserably.   You have two options:  hope your editor has a lot of undo options, or revert the changes back to what you put in the source control system.

As you write more complex code, this case becomes much more of a normal except that a lot of times, changes are made by a team, and at some point the product breaks.  This is where Source Control can save the day.  You have 3 developers each working on changes and at some point after about 5 changes you find an issue with the product.   Using the source control you can see who changed what and when, and revert back to any point.   

Open source products and companies alike all use an SCM system to manage their source. It's great for collaboration as well as tracking changes and recovering from breakage.

### Git

Git is an implementation of a Source Control Management (SCM) system designed around collaboration and sharing.   It was created to manage the Linux kernel and has become the main source control system for most of the industry.

Git is one of the main open source solutions, and major companies host Git for all to use.  In fact, this entire site I've setup is hosted using Git.

In this course we'll be using Git to share content and access many of the examples that have been established.  

#### Repository 

To understand how Git works, we must first gain access to a repository.  A repository is the term used to represent something that is being source controlled.  This is how Git will then manage the content and the revisions as things change.

On the local file system, Git does this by creating a folder for the repository, and then there is a *.git* directory where it stores the repository content.   Everything else in the directory is the content at some point in time.  

For example, we have a repository with our HelloWorld.java (noticing yet how the Hello World example is useful) the following structure would exist on the file system.

``` bash
drwxr-xr-x 1 paulin 197121    0 Dec 28 19:31 .git/
-rw-r--r-- 1 paulin 197121  290 Jan 21 18:41 HelloWorld.java
```

Notice that the .git directory, which is what Git is using to manage the historical changes of HelloWorld.java, is at the root of this directory.  The file HelloWorld.java is there in its newest state.

The repository is where all interaction can occur.  This allows you to add content (add), save changes (commit), resync with the remote repository (fetch/pull) and push changes to a remote (push).

#### Distributed

Most source control management systems are designed around a central server that hosts the main system.   Users then locally interact with the server to get a copy of the content to their local system but then everything is done using that server for connections and sharing changes.

Git is really different since the *.git* directory is a full repository with all the history of changes and the content.   You don't need a server to interact with since it's all local.

This is why Git is called a distributed Source Control Management system.  If you have a local repository and you want to give a copy to your friend, they could get a copy of your repository on their computer.  Now there are two identical copies.   Git has a way to connect your repository to the copy on your friend's computer.  Your copy is called local, and the other is called remote.  

Git doesn't stop there though.   If your repository lives on 100 other computers, you would have 100 remotes. Git then provides ways to interact with each of these remotes, and those remotes can interact with you.

This is how Open Source and companies work with Git to manage their source.  Typically they use some server to host a main Git repository (GitLab, GitHub, etc.), and then all the users interact with it.  This way users can collaborate and all work with a single well known address.

In this class, we'll be using Git and connecting to GitLab where all this content is stored.  If you would like to browse the repository you can visit it at [Gitlab.com/spextreme](https://gitlab.com/spextreme/intro-to-java-sample-repo).

![Eclipse Error In Code](/images/intro-to-java/ide-gitlab-repository.png)

#### Cloning a Repository

Now that we know what a repository is and have one that we can browse with our web browser, how do we get a copy of it locally?  This is a concept called cloning.

Cloning a repository is the act of making a 100% duplicate copy (a clone) of the remote repository.   Git provides a command *clone* to perform this.  

```bash
$ git clone https://gitlab.com/spextreme/intro-to-java-sample-repo.git

Cloning into 'intro-to-java-sample-repo'...
remote: Enumerating objects: 50, done.
remote: Counting objects: 100% (50/50), done.
remote: Compressing objects: 100% (38/38), done.
remote: Total 50 (delta 10), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (50/50), done.
Checking connectivity... done.
```

Now that we have run the command *git clone* we have a complete copy of the repository on our local system in a folder called intro-to-java-sample-repo.

```bash
$ cd intro-to-java-sample-repo/
$ ls -al intro-to-java-sample-repo/
total 18
drwxr-xr-x 1 paulin 197121    0 Jan 22 19:20 .git/
-rw-r--r-- 1 paulin 197121   59 Jan 22 19:20 .gitignore
drwxr-xr-x 1 paulin 197121    0 Jan 22 19:20 basics/
drwxr-xr-x 1 paulin 197121    0 Jan 22 19:20 comments/
-rw-r--r-- 1 paulin 197121 1071 Jan 22 19:20 LICENSE
-rw-r--r-- 1 paulin 197121  410 Jan 22 19:20 README.md
drwxr-xr-x 1 paulin 197121    0 Jan 22 19:20 scope/
```

From here we can now fully interact with git using any command.

#### Understanding Status

As we work with the content in the source repository, Git knows when the repository has changed.  So how do we find out?   The *git status* command will give us all the details.

```bash
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   basics/hello.world/HelloWorld.java

no changes added to commit (use "git add" and/or "git commit -a")
```

This is the *git status* command which gives a lot of information including the files that were modified, helpful hints on what we may want to do, and even how to undo the change.

This is one of the commands you'll use most often.

#### Staging and Saving

Now that we know how to find out what's different in our repository, we need a way to save these changes.   Git automatically will know when something changes.  It does this by knowing that the local copy of something doesn't match the repositories saved content.

There are two steps to perform in saving any content to the repository.  
- Staging the changes
- Committing them

The *git add* and *git remove* commands provide a way to stage changes.   *add* is used to add files to the staging area, and remove is used to stage a file for deletion.   

As an example, we'll begin with a repository that has our HelloWorld.java and a RemoveFile.java.

```bash
$ ls -al
total 9
drwxr-xr-x 1 paulin 197121   0 Jan 23 14:31 .git/
-rw-r--r-- 1 paulin 197121 135 Jan 23 14:30 HelloWorld.java
-rw-r--r-- 1 paulin 197121   0 Jan 23 14:31 RemoveFile.java
```

Next we'll add a file NewFile.java and we'll remove the RemoveFile.java.

```bash
$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        NewFile.java

nothing added to commit but untracked files present (use "git add" to track)

$ git add NewFile.java

$ git rm RemoveFile.java
rm 'RemoveFile.java'

$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   NewFile.java
        deleted:    RemoveFile.java
```

As the example shows, we start with a *git status* to see what is different, and we have a new file that git doesn't yet control.   Doing the *git add NewFile.java* puts it in the staging queue.  Next we remove the RemoveFile.java with the *git rm RemoveFile.java* which tells the staging area this fill is to be removed.  Finally we end with another status command to confirm our changes are properly staged.  The status command shows we are ready to commit two changes and it gives us a hint if we want to unstage anything.  Also note, while not shown in this example, *git add* is used to stage modified files too.

Now that we have everything staged, its time to put these into the Git repository.  This is done by committing the changes with the *git commit* command.    When a commit is done it is required and best practice to commit the changes with a comment to describe what is being saved.   The better your message, the easier it is for others to know what was done.  It may also help when you have to go back in time to understand a change.

```bash
$ git commit -m "Added a new file and removed the outdated file that is not being used"
[master d307473] Added a new file and removed the outdated file that is not being used
 2 files changed, 4 insertions(+), 4 deletions(-)
 create mode 100644 NewFile.java
 delete mode 100644 RemoveFile.java
```

The output shows that we affected the git repository on master and it created a unique revision with the hash code d307473.  Other details include what was changed, and some metrics on the updates and the comment we added.  Remember that commit is only taking the content that is staged and saving it.  This means if we didn't add the NewFile.java, then commit would have only removed the RemoveFile.java.

Now everything is back to a clean state in the folder, and Git is managing everything again.

```bash
$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
nothing to commit, working directory clean
```

#### Pushing and Pulling

Now that we can affect our local repository, there may be times when we want to share our local changes to a remote or we may even want to get changes from a remote.

If you remember from the cloning discussion, we talked about a remote repository being on Gitlab.   If you go into your repository that you cloned and do a *git remote -v* it will provide the remote you're connected to.  

```bash
$ git remote -v
origin  git@gitlab.com:spextreme/intro-to-java-sample-repo.git (fetch)
origin  git@gitlab.com:spextreme/intro-to-java-sample-repo.git (push)
```

This command simply shows what remote we are connected to. By default, after performing a clone, Git will automatically establish an *origin* which is where you cloned from.   This is the default remote for all cloned repositories.  When we described that you could connect to another repository you could define a second remote with unique name.   Then you can do remote commands between different repositories (distributed part of Git).   In this course we'll focus everything on the default origin.

There are two things you can really do with a remote.  
1. Push changes 
2. Pull changes

If we start with the changes we recently made (added and removed a file) these changes are only in our local repository.   If we wanted to put them on GitLab, we would have to tell Git to take all the content of our repository and *push* it to GitLab (origin remote).

```bash
$ git push
Counting objects: 3, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 339 bytes | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To git@gitlab.com:spextreme/intro-to-java-sample-repo.git
   0523580..d307473  master -> master
```

This push took the local repository content and sent it to GitLab, made the updates in GitLab and reported the details back.  As you can see, the master branch was updated and the changes we had locally made are now on GitLab.

When others publish changes using this method, you may need to get their changes locally.   For example, let's say someone else published changes and pushed them to GitLab (RemoteChange.java).  In this case someone else made a new file, added and committed it, then pushed the changes to GitLab.

So now we want to get those updates local in our repository.    The simplest way to do that is to go into our repository and do a *git pull*.

```bash
$ git pull
remote: Enumerating objects: 4, done.
remote: Counting objects: 100% (4/4), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), done.
From gitlab.com:spextreme/intro-to-java-sample-repo.git
   d307473..e60bd87  master     -> origin/master
Updating d307473..e60bd87
Fast-forward
 RemoteChange.java | 5 +++++
 1 file changed, 5 insertions(+)
 create mode 100644 RemoteChange.java
```

This shows we reached out to Gitlab, synchronized our local repository, and pulled down all the changes locally.   It then merged the changes into the files we already had on our file system.

Pushing and Pulling are two of the few acts that require a remote (network) connection.   Most actions are done only on the local repository (status, add, commit).

## Conclusion

In this section, we talked about Integrated Development Environments (IDEs), and Source Control Management (SCM) Systems.   In this class we'll be using these tools to help facilitate the development process (Eclipse IDE) and collaborate on items (Git SCM).

For Git the most important thing to remember is that every command in Git starts with the word *git*.   After that you supply the git command to run.  The most important commands to know are clone, status, add, remove, commit, push, and pull.  Git has many other commands, but this is a good starter for what we'll be doing.

