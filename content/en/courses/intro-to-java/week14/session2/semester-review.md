---
title: "Semester Review"
description: "Going over what we discuss this semester as a basic review of Java."
date: 2021-04-18
draft: false
enableToc: false
weight: 15
---

## Introduction

In this session we'll focus on most of the Java basic topics that we've gone through this semester.  We'll focus on 

- Primitives
- Strings
- Variables/Fields
- Classes
- Conditions
- Loops
- Threads

You may also feel free to ask any questions on any topic your heart desires.  This is all for you since this is our last class.

I'd like to thank all of you for a great semester.  I hope you gained something from this course and I wish you all the best of luck in your future course work and careers.   Stay safe and if you ever have questions feel free to reach out.


