---
title: "Project Demo"
description: "A demonstration of Java project."
date: 2021-04-18
draft: false
enableToc: false
weight: 15
---

## Introduction

This will be a virtual class that will be recorded showcasing a Java application (Spring Framework microservice) which uses a Raspberry Pi (a cheap Linux computer) to control the power to a device (for now turning on LED lights).

In this, I'll walk through the Raspberry Pi setup, a breadboard (like a circuit board), how its setup and the Java application that makes it work.    The goal will be to show how everything we discussed this semester fits together to make a working application that is deployed.

## Slides

{{< revealjs >}}

## Smart Swimmer

- A simple microservice that acts as an on/off switch
- Three main API calls
   - On/Off
   - Status
- Simple web request to run against a named motor
- Plan is to control a 240 circuit for pool motors
   - Filter
   - Cleaner

---

## Hardware

- Raspberry Pi 
- Solid State Relay (SSR)
- Jandy/Polaris Motor

--

## Raspberry Pi

![Raspberry Pi](/site/images/intro-to-java/smart-swimmer-raspberry-pi-b.jpg)

- Small cheap computer (~$30)
- WiFi
- USB power
- GPIO interface
   
   
--

## GPIO

![GPIO](/site/images/intro-to-java/smart-swimmer-gpio.png)

- General Purpose Input/Output
- Each pin has a special purpose

--

## Filter

![Jandy Filter](/site/images/intro-to-java/smart-swimmer-jandy-filter-motor.png)

- Motor spins a plate that cause suction
- Pulls water through pipes into the filter 
- Pushes water back into the pool

--

## Cleaner

![Polaris Booster Pump](/site/images/intro-to-java/smart-swimmer-polaris-booster-motor.jpg)

- Pushes water through a small pipe into the pool
- Pool cleaner hose connected to fitting
- High pressure cause cleaner to move around
- Also causes suction in the cleaner to pull up dirt

--

## Solid State Relay (SSR)

![SSR](/site/images/intro-to-java/smart-swimmer-ssr.jpg)

- On/Off Switch
- High Power 24-480V

--

## How It Fits Together

![Hardware Model](/site/images/intro-to-java/smart-swimmer-hardware-layout.png)

---

## Smart Swimmer Microservice

- Small simple application
- Spring Framework
- RESTful interface

--

## API

- On
   - .../rest/v1/on?name=cleaner 
- Off
   - .../rest/v1/off?name=cleaner
- Status
   - .../rest/v1/status

--

## Server

- Server runs waiting for requests
- Upon a request, spawns a thread to handle the request
- Responds to request with results (JSON)
- Built in documentation

---

## Overall Goal

- Home Automation
   - Weather Indexer
   - Smart Swimmer
   - Smart Scheduler
   - Watering Can
---

## Live Demo

- Raspberry Pi
- Smart Swimmer Server
- Smart Swimmer Code
- Debugging
- Automated Build
- Web UI

   
{{< /revealjs >}}

## Demo

The below is a recording of a live demo show during class.  During the demo the video of the equipment was displayed on the left side of the screen but unfortunately Zoom didn't record its own window and placed the small version of it in the upper right corner.  Sorry for that but if you make the video larger you should still be able to see how the service turns on/off the different circuits.

[![Watch the video](/images/intro-to-java/videos/smart-swimmer-demo-screen.png)](https://youtu.be/nW8Xuc2x6VI)


