---
title: "Recap"
description: "Going over any topics desired."
date: 2021-04-13
draft: false
enableToc: false
weight: 15
---

## Introduction

For this week, we'll just be reviewing some topics that you picked.  If you have others you want to talk about please email me or ask them during class.  I plan to go over:

- [Inheritance](/courses/intro-to-java/week5/session1/inheritance/)
- [Unit Testing](/courses/intro-to-java/week6/session2/testing/)

If more topics come up, I'm happy to cover them.
