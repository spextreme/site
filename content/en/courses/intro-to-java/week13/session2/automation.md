---
title: "Automation"
description: "Automation is critical in the development lifecycle."
date: 2021-04-17
draft: false
enableToc: false
weight: 15
---

## Introduction

Now that we have completed all the topics in this course, let's talk about how it all fits together.   Code can become very complicated over time, so it is very important to focus on Keeping It Simple Stupid ((KISS principle)[https://en.wikipedia.org/wiki/KISS_principle]) and maintainability.   As we've learned testing becomes invaluable in maintaining the code and using it as a design tool (TDD) will help keep it simple.  Focusing on simplicity and using objects, helps developers make things easier.

Source control helps keep the code controlled, while Unit Testing and TDD (Test Driven Development) help in creating testable code and high quality software.  With this, Continuous Integration (CI) has emerged to help developers more frequently build and run the tests on any changes automatically.  The goal is to build and test frequently, and making it as simple for developers to get feedback when things fail.  Automation is important because it frees the developers to focus on code and increases the testing and finding defects early.

Continuous Integration has lead the way in modern development to the act of Continuous Deployment (CD).  This takes automation to the next level, in that, when the CI system's build succeeds, it then pushes the product to a release area where customers begin using it.   For example, Netflix developers are constantly making changes to the services they provided.   Every little change is built automatically by their CI system.   When it passes, it is then tested automatically as close to production as possible.  If it continues to pass the gambit of tests, then it gets pushed out to production so customers can start using it.   In major online companies today this is standard practice.

## Continuous Integration

As described, Continuous Integration (CI) is the activity of constantly merging in source code changes and having an automation system build and test them frequently.  This provides the developer/teams with constant feedback that their changes are good and the software is stable.   If a merge cause the automation system to fail, then you know exactly what change broke it and its much easier to find and fix.

Finding issues early with a change is very important because you want to make sure the quality of your product is good for your users.  If users are the ones finding your issues then you probably will lose many of your users.  The later you find an issue, the harder it will be to debug/resolve as well as increase the cost to fix.

CI provides the best alternative to find issue quickly and early.  However this is only as good as the quality of tests created. Remember TDD and Unit tests are very important.

## Jenkins

[Jenkins](https://www.jenkins.io/) is a CI system that was designed long ago to perform builds.  The creator, Kohsuke Kawaguchi, back in 2004 (previously called Hudson) created it because he was tired of other developers finding bugs with his code. It was designed around the concept of continuously building on change and reporting issues when problems happened.

Jenkins (which is written in Java) is a web based application server that will connect to many different types of source repositories, monitor for change, and when detected, will perform any type of action necessary.  For example, it can detect a change we push up to Git, then perform our Maven build and run our collection of JUnit tests.  Jenkins can then report the results of this build, show our test trends, publish our Javadoc and even push our build items to any location we want.

Jenkins now has a concept of placing the configuration for your repository's build in a file that it stored in your repository with the code.  This concept is called configuration as code so that it can be managed in the repository and source change like source code.   Jenkins calls this a Jenkins Pipeline which is a way to script all the steps Jenkins should perform into a sequence of events.  Jenkins will then monitor your repository for change, and read this file and execute the desired steps.  Jenkins then renders this in a view for showing the results and linking to all the data produced.

![Jenkins Pipeline](/images/intro-to-java/jenkins-pipeline-smart-swimmer.png)


## Conclusion

Any software development activity should be focused on building tests to prove the software is working as desired, and it should be automated to increase the feedback loop on changes.  When you are first starting out this may seem like a lofty goal but focusing on it from the start will help improve the quality of your software and make you a better developer.  It will also make you much more comfortable with change.