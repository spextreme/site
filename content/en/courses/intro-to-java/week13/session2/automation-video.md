---
title: "Automation - Video"
description: "Automation is critical in the development lifecycle."
date: 2021-04-17
draft: false
enableToc: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/automation-screen.png)](https://youtu.be/3_-PN0X47vc)

## Slides

{{< revealjs >}}

## Introduction

- Completed all the topics
- Code becomes complicated
   - KISS principle
   - Maintainability
- Continuous Integration
- Continuous Delivery

--- 

## Continuous Integration

- Continuously integrate changes
- Build/Test/Audit automatically
- Rapid feedback loop
- Quality of tests affects defect mitigation
- Feeds into Continuous Deployment

--

## Continuous Deployment

- Automating the target deployment
- Follows from successful CI

--- 

## Jenkins

- Created in 2004 by Kohsuke Kawaguchi
- Written in Java
- Monitors the SCM and triggers on change
- Build/Test/Audit and much more
- Configuration as code
   - Jenkins Pipeline
   
--

## Jenkins Pipeline

![Jenkins Pipeline](/site/images/intro-to-java/jenkins-pipeline-smart-swimmer.png)

--- 

## Conclusion

- Testing and Automation are very important
- Fast feedback loop
- Focus on this from the start
- Improves development and quality of software
- Makes developers comfortable with change

{{< /revealjs >}}