---
title: "Computers"
date: 2021-01-10T10:08:56+09:00
description: A high level overview of how computer hardware works.
draft: false
collapsible: false
weight: 35
---

## Introduction

As we progress through this course, we'll be learning about programming and how to make computers perform the actions that we specifically ask them to.   Everything in today's world is largely based on computer technology and the software that runs on them.   This section will focus on the hardware that makes a computer function.   

## Hardware

Computers are a collection of physical pieces of hardware put together to make it function.  Hardware includes things like the Central Processing Unit (CPU), hard drive, memory and many other things.   There may also be hardware items that are external to the computer like the keyboard, mouse, monitor, printer, camera and much more.

### Central Processing Unit

The main part of a computer is the Central Processing Unit, also known as the CPU.   The CPU is the heart of the computer but functions more like our brains.   It is the piece of hardware that processes the instructions that the software tells it to.  The CPU takes an instruction as input and produces the desired output.   This is how your computer processes all requests.

![CPU pins](/images/intro-to-java/cpu-pins.jpg)

The above is a picture of a CPU chip looking at the site that plugs into the motherboard.  The motherboard is a circuit board that the CPU is mounted to and connects it to other parts of the computer including power, memory, hard drive and much more.

![CPU In Motherboard](/images/intro-to-java/cpu-in.png)

This is a CPU seated into the motherboard.  Once it is connected to the system it will be able to receive electrical signals and send them out.   These signals are how a computer processes the instructions told to it by the executing application.

The CPU is made up of two parts, including the Control Unit and the Arithmetic Logical Unit (ALU).  The control unit is responsible for coordinating all of the computer's operations.   The ALU is used to perform mathematical operations.  The CPU takes in a single instruction as input, processes it and then sends the resulting output.  CPUs are singularly focused, meaning they can only handle one instruction at a time.  The speed  at which it processes an instruction is controlled by the ticks of a clock, or clock speed.  This means that one tick in the clock is 1 hertz.  Therefore if you have a 2 Gigahertz (GHz) processor, it can carry out two billion cycles per second.

The CPU can do a lot, but its focus is to process any instruction that it is told to handle.


### Motherboard

The motherboard, as mentioned, holds the CPU.  But what is a motherboard?  It is the main circuit board that holds a collection of components (transistors, processor, slots, memory, network, USB ports, and much more).   The main purpose of the motherboard is to allow all the electronic components of a computer to communicate with one another.  This is how the CPU can access the memory, a network connection or the hard drive.

![Motherboard](/images/intro-to-java/motherboard.png)

Most motherboards contain slots to allow other parts of the computer to be connected.  This is how memory can be added or changed on the computer.  It may also have expansion slots to insert cards that can perform other functions like graphics.

Motherboards come in many shapes and sizes and vary by purpose.  However, one thing is true for all of them: power, a CPU, and memory are the main parts needed to make the motherboard and computer function.  The motherboard is how these things communicate and get power routed between them.

### Main Memory

The memory of a computer is how the computer stores information currently being processed.  The computer's memory is called Random Access Memory (RAM) because it is there to handle current execution applications and their data being processed by the CPU.

![Memory](/images/intro-to-java/memory.png)

RAM can be read from or written to and is used to store and access information.  Memory is organized into units called bytes.   A byte is made up of 8 bits.  A bit is how a computer represents something and it has the state of being on (1) or Off (0).  Grouping bits into a collection of 8 allows it to represent a number or a character.  A byte is capable of representing 256 unique combinations which is how the numbers and characters are represented inside of the computer.  For example, to represent a 0 as a byte the sequence is 0000 0000, 1 is 0000 0001, 2 is 0000 0010 and so on.

The combinations of these 1 (ones) and 0 (zeros) allow the computer to manage any piece of information.  In memory, this is how the executing applications store any piece of information for later access.  RAM can store as many bytes as the size will allow.  For example a 4-gigabyte (Gb) memory can store four billion bytes.

The thing to realize, however, is that memory, specifically RAM, in the computer is volatile.  This means it is only active so long as the computer is powered on.   Restarting the computer, turning it off or it losing power will cause the RAM to lose any information currently stored in memory.

RAM holds information by organizing the data into an address space.  Think of this as a collection of columns and rows where the index of the column/row is the address for that cell.  That address then holds a value within it.  

Assume we have the following table which represents the memory of a computer.

|    |    |    |    |    |    |    |    |    |    |
|----|----|----|----|----|----|----|----|----|----|
| <sup>0</sup>  | <sup>1</sup>  | <sup>2</sup>  | <sup>3</sup>  | <sup>4</sup>  | <sup>5</sup>  | <sup>6</sup>  | <sup>7</sup>  | <sup>8</sup>  | <sup>9</sup>  |
| <sup>10</sup> | <sup>11</sup> | <sup>12</sup> | <sup>13</sup> | <sup>14</sup> | <sup>15</sup> | <sup>16</sup> | <sup>17</sup> | <sup>18</sup> | <sup>19</sup> |
| <sup>20</sup> | <sup>21</sup> | <sup>22</sup> | <sup>23</sup> | <sup>24</sup> | <sup>25</sup> | <sup>26</sup> | <sup>27</sup> | <sup>28</sup> | <sup>29</sup> |

To store the number 143 in memory it may be placed at address 14.

|    |    |    |    |    |    |    |    |    |    |
|----|----|----|----|----|----|----|----|----|----|
| <sup>0</sup>  | <sup>1</sup>  | <sup>2</sup>  | <sup>3</sup>  | <sup>4</sup>  | <sup>5</sup>  | <sup>6</sup>  | <sup>7</sup>  | <sup>8</sup>  | <sup>9</sup>  |
| <sup>10</sup> | <sup>11</sup> | <sup>12</sup> | <sup>13</sup> | <sup>14</sup> 143 | <sup>15</sup> | <sup>16</sup> | <sup>17</sup> | <sup>18</sup> | <sup>19</sup> |
| <sup>20</sup> | <sup>21</sup> | <sup>22</sup> | <sup>23</sup> | <sup>24</sup> | <sup>25</sup> | <sup>26</sup> | <sup>27</sup> | <sup>28</sup> | <sup>29</sup> |

Now if the computer needs to access the value it would reference the address 14 to pull out the value. 

### Data Storage

Almost all computers have some form of a long term storage system.  This is usually in the form of a hard drive or solid state drive.

A hard drive is typically made of a collection of thin disks packed together with a read head that floats over them.   These drives spin while the read/write head moves in and out to read or write to the disks.   These are what most desktops have in them, and they can permanently store information.

![Hard Drive](/images/intro-to-java/harddrive.png)

More modern computers and most laptops or handheld devices (phones) use an electronic storage system called a Solid State Drive (SSD).  The are typically much smaller then a Hard Drive and can store much more information.  They also are quieter (nothing spinning) and last longer.   For that, however, they are usually much more expensive.

![Solid State Drive](/images/intro-to-java/ssd.jpg)

There are many other media types include Universal Serial Bus (USB) drives which are small drives (similar to SSD) that are on a small stick that can plug in to a USB port on your computer.   Not too long ago, Compact Disk (CDs) and Digital Versatile Disc (DVDs) were common but have slowly been replaced.

In all cases, these types of storage are designed to allow long term persistent storage methods.  That means the executing applications can save data it produces to be loaded again later.  This includes loading the data after rebooting from the system.

### Input Devices

A computer wouldn't be much if a user couldn't interact with it.   Input devices are how we can provide data to the computer.  This may come from a device like the keyboard where you can type characters, and a word processing application can record what is typed into a document. 

![Keyboard](/images/intro-to-java/keyboard.png)

Most computer systems have some sort of input device that allows you to move a cursor around the screen and click on things.  This is called a mouse or maybe a touch screen where your finger moves and clicks.

![Mouse](/images/intro-to-java/mouse.png)

Other forms of input may come from a microphone which allows you to speak, and the microphone transmits the signal to the computer, which interprets it.

![Microphone](/images/intro-to-java/microphone.png)

There are other types of input devices like cameras, scanners, and tablets that may also be connected to provide input into the computer.

### Output Devices

Output devices are how we can visually see what the computer is doing.  It presents on the monitor a graphical user interface (GUI) for us to interact with.  This is how we watch videos, see what is being typed, and visualize as we move the cursor around the screen.

The speakers are another primary output device which allow you to hear sounds made by the computer.  This may be in the form of an auditory warning like a beep or audio from a video or music file.

Printers are another peripheral used for output.  These allow the computer to transfer the contents to a piece of paper.    This could even be a 3D printer where the output tells the printer points to melt plastic onto a point to form a 3D rendering.

## Software

Software is what makes a computer useful.   Software can be used to do just about anything.  The thing to know, though, is that software is nothing more than a collection of instructions to tell the CPU what to do.

One of the first software items that you will interact with is the computer's Operating System.  This is what provides you with the visual display and ability to run other software applications, and it's how you'll interact with the computer in all ways.

Other software that sit on top of the operating system are called applications.  This includes things like your word processor, web browser, games, and many other things.


### Operating System

An operating system (OS) is the main foundational software on every computer system.  It proves the interaction with all the hardware components attached to the system, a user interface that displays on the screen, network access to communicate and access the Internet, and everything in between.

There are many operating systems available today including:
- Android
- MacOS
- IOS
- Linux
- Windows

This is not an exhaustive list, but it provides a few of the main options.

All of these provide a graphical user interface for you to use.   As you may have noticed, the Android and IOS are operating systems on phones.  The others are on desktop or laptop computers.

### Application Software

Applications are pieces of software that run on top of the operation system.   They are usually designed to provide a user function.  For example web browsers, like Chrome or Firefox, are designed to provide a way to communicate over the Internet, make a request for a page, and then display it to you.

Software can be written to perform any task.  It can also be written in a multitude of languages.

Languages are how humans write software that gets turned into a computer application.  There are many languages available but they all provide a way for someone to write code that gets compiled (or interpreted) for the computer to run and use.

Some popular languages include

- Java
- C++
- Python
- HTML
- JavaScript
- and many more

In the remainder of this course, we'll get into details on how program languages function. We will focus on Java and how it works to create a software application.
