---
title: "Programming"
date: 2021-01-10T10:08:56+09:00
description: Basics of programing with details on algorithms, binary and languages.
draft: false
collapsible: false
weight: 45
---

## Programming

As we touched on in the first topic on Computers, software is what makes computers usable.  Software is an application that exists to perform a function by instructing the computer on what should be done.  Making an application is where programming comes in.

Programming is the act of writing code in a high level language that will be converted into a machine readable format (Machine Code).  The convention is known as compiling the human readable code into a format that the machine will understand.   A programmer (aka developer) is the person who writes the code.

In this section we'll cover topics on programming, discover what it really is, and lay the foundation for what we'll be doing throughout this course.

### Algorithms

When a programmer begins, they develop source code (usually just called code) which is a collection of instructions that tells the computer what to perform.   Putting these instructions together forms the basis of the application being developed.   As things become more complex, there are logical groupings formed that become the basis for algorithms.

Algorithms are nothing more than a collection of instructions to be executed by the computer.  However, the term algorithm is usually used to describe a specific function that is created to perform something more complex.  For example, a function to calculate the circumference of a circle may take a couple of lines of code, but as a set, this would be called an algorithm.

Algorithms become the basis for functions in a program.  Usually a program may contain many algorithms within a complete application.

### Binary

Binary is a base 2 numbering system that contains only two digits, 0 (zero) and 1 (one).   This is how a computer interprets everything.   As we've mentioned, a CPU interprets these instructions as 0 and 1 when it's processing them just as memory stores these bits in chunks called bytes.   Binary is an important part in all computer systems.

![Binary](/images/intro-to-java/binary.png)

What's also interesting is that each varying type of CPU interprets binary information differently.  One binary sequence written for a CPU will not work well with a different CPU.

With this challenge of highly variable binary patterns for CPUs and humans not able to easily read/write binary, programming languages were invented to solve these issues.

### Languages

A programming language was created to use words to allow humans to write complex applications without the need to create binary messages.   Writing a command like 'System.out.println( "Hello World" );' is much easier then a vary long string of 1 and 0. 

It would look something like this
```
01010011 01111001 01110011 01110100 01100101 01101101 00101110 01101111 
01110101 01110100 00101110 01110000 01110010 01101001 01101110 01110100 
01101100 01101110 00101000 00100000 00100010 01001000 01100101 01101100 
01101100 01101111 00100000 01010111 01101111 01110010 01101100 01100100 
00100010 00100000 00101001 00111011 
```

As you can see, using words is much easier. That sequence may not even be correct on some CPUs.   Programming languages can handle conversion from the text form much easier.   By compiling the application, the code is converted into the platform-specific machine code.

Languages pave the way to simplify the creation of complex algorithms and applications.

## Java

In this course, we'll focus on Java, how it functions as a language, and how it is converted to the machine code necessary to run the applications.

Java is one of the worlds most advanced program languages to date.  Started in 1991 at Sun Microsystems (now Oracle) by a team led by James Gosling, it was originally a way to have a platform-independent method for different devices to communicate.  However, since this was around the time of the explosion of the World Wide Web, their focus changed to make Java a better option for web development.

### Platform Independence

As discussed, code has to be compiled to work on the target system which can be quite challenging for many different systems (Windows, Linux, MacOS).   Java changed all of that by focusing on creating a language that will work on any platform by writing the same code.

When this concept was first created, it was revolutionary because every other language had to be converted to machine language for the target platform.   Java created a new way to handle this by using their compiler to turn the code into a specialized format called Bytecode.   Then they wrote an interpreter called the Java Runtime Environment (JRE) to read that Bytecode and run it on the target platform.   This means that the uniqueness of each platform is abstracted by the JRE so developers can write code once, compile it once, and run it anywhere there is a Java Virtual Machine (JVM).

Today every environment supports Java and has a JVM available.  This makes writing Java code attractive because the platform variability is completely removed.   Writing something once and distributing it on all platforms is a great feature.

### Write Once, Run Anywhere

This became the tag line of Java, and it's very true.   Platform variability is one of the biggest challenges for developers, and in many cases the developers will sometimes not target some platforms because of it.

With Java, that obstacle is removed and now the focus can be on creating a great application.  Writing and testing the code on one platform, then being able to distribute anywhere, makes it one of the best choices.

### Java Variants

Java has become very popular and has changed its packaging many times over.   This causes some confusion when you try to get started.  Just know that in most cases the different versions are designed for the environment more than the development.

The main part to understand is that there is a Java Runtime Environment and Java Development Kit.   


#### Java Runtime Environment (JRE)

The JRE is a version of Java which provides enough to run it on a target platform.  This includes the JVM application, 'java' command line and associated tools to allow execution of any Java compiled application.  It doesn't provide any of the tools to create an application however.

This is what is available on most platforms to allow the 'Run Anywhere' to work.

#### Java Developers Kit (JDK)

The JDK usually includes the JRE, but it also contains all the parts necessary to develop Java applications.  This includes the Java framework source code, a compiler, and a host of other development tools.

The JDK is distributed in different forms to support different styles of application development.

- Java Standard Edition (Java SE) - Provides the foundation for most Java Development.
- Java Enterprise Edition (Java EE) - Provides tools for creating large business applications.
- Java Micro Edition (Java ME) - A small optimized version of Java for targeting smaller devices like phones.

In this course the JDK SE is all you will need.

### Java + Open Source

Open Source software has become a major player in the software market.  Open Source is an excellent option and usually provides high quality software with many large commercial companies building and supporting Open Source software.   

Open Source software's goal is to provide free access to the source code to share, contribute, and learn from.  The community can then benefit from others contributing to the source by advancing the application.

Java itself has been open source for most of its existence.   The OpenJDK (https://openjdk.java.net/) is the Open Source version freely available.  We'll focus on using the OpenJDK for all development. Please note that Oracle also distributes a version of Java with a more restrictive license.

### Getting and Installing Java

Its finally time to start getting ready to build Java applications.   To get started, go to the OpenJDK download site and get Java.

#### Windows
1. https://jdk.java.net/java-se-ri/11 - You can get any version, but for this course we'll focus on OpenJDK 11.
1. Unzip to any place on your computer.  I recommend 'C:\Program Files\Java\'.
1. Open the Windows Start Menu -> Control Panels -> System.
1. Choose Advanced System Settings in the left panel.
1. The System Properties dialog will appear.  Click Environment Variables to open the environment dialog.
1. Look under system variable and look for JAVA_HOME.  If not there click 'New...'.  If there, select it and click 'Edit...'
1. If New was selected, enter JAVA_HOME in the Variable name: field.
1. Go back to the folder you installed the JDK, and go into the java installation directory and copy the path link. This might look like 'C:\Program Files\Java\jdk-11'.
1. Paste the value you copied (i.e. 'C:\Program Files\Java\') into the system variable value field for the JAVA_HOME variable.
1. Click OK to save the changes.
1. Next find the variable Path in the system variables list.
1. Select it and click edit.
1. In the variable value field, prepend the value '%JAVA_HOME%/bin;' to the front leaving all other values intact.
1. Click OK, click OK again, and click OK a third time.
1. Close the control panels.
1. Go to the start menu and type 'cmd' and open a command prompt.
1. In the command prompt that opens, type 'java -version' and confirm you get the open jdk version you installed.

#### Linux
1. Open a command prompt
1. Run the command 'yum update'
1. Search for Java by typing 'yum search openjdk'. 
1. Look for the openJDK version to install. It should be something like 'java-11-openjdk.x86_64 : OpenJDK Runtime Environment 11'
1. After finding that, you'll want to install the base package and the development package. This is done by doing a 'yum install java-11-openjdk java-11-openjdk-devel'
1. Once complete, run 'java -version' and confirm you now have a working Java install.

#### MacOS
1. In Finder go to Go -> Utilities from the menu bar.
1. Double-click the Terminal application.
1. In the terminal window, type 'brew update'
1. Next type 'brew tap adoptopenjdk/openjdk'
1. Finally type 'brew install adoptopenjdk11'
1. Now check the install with 'java -version'
