---
title: "Intro"
date: 2021-01-10T10:08:56+09:00
description: The video and slides for week 1 - Session 1 introduction to the course
draft: false
collapsible: false
weight: 10 
---

## Video

[![Watch the video](/images/intro-to-java/videos/intro-screen.png)](https://youtu.be/6hckmmO0oeM)

## Material

### Intro to Java

An overview of computers and programming with Java

---

### About Me

- Steve Paulin
   - Programming forever
   - Passion for
      - Skiing
      - Woodworking
      - Programming
      - Creating Things
- Contact
   - spextreme at gmail dot com
   - paulin59 at rowan dot edu

---

### Syllabus

- Expectations and details of this course
- Grading details
- Homework
- Rowan Policies

[Syllabus](https://spextreme.gitlab.io/site/courses/intro-to-java/week1/syllabus/)

--

#### Expectations

- Review material before each class
- Attend class and participate
- Attempt programming assignments
- Make mistakes and learn something

--

#### Homework

- Material will be available online
- Review material before class
- Class time will be for questions and programming

--

#### Rowan Policies

- Academic Integrity and Plagiarism
- Attendance
- Classroom and Online Decorum
- Statement of Accommodations
- Diversity
- Sexual Misconduct and Harassment Reporting
- Success Network and Tutoring Center
- Withdrawal Policy

---

### Course Topics

- Coding, Compiling and Running Java Applications
- Logical constructs
- Defining classes and methods
- Working with Arrays
- Exception handling programming techniques
