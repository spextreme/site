---
title: "Programming - Video"
date: 2021-01-17T10:08:56+09:00
description: The video and slides for week 1 - session 1 on programming
draft: false
collapsible: false
weight: 40 
---

## Video

[![Watch the video](/images/intro-to-java/videos/programming-screen.png)](https://youtu.be/Jk276fF0SVY)

## Slides

{{< revealjs >}}
## Programming

- Software makes computers useful
   - An application to perform something
- Programming is the act of writing code
   - High level language converted to machine code
      - Conversion is know as compiling
- Programmer - the person who writes the code

--

## Algorithms

- Code is a collection of instructions
- Instructions grouped together form an application
- This code can become quite complex
- Algorithms is a term to describe a collection of instructions
   - Usually a specific function within in application
- Usually become the bases if methods in an application

--

## Binary

- Base 2 numbering system
- Contains only two digits, 0 and 1
- CPU, Memory uses this model
- This is very important to understand
- Each CPU my interpret binary data differently
   - One binary sequence in one system may not work in another
- Humans are not easily able to read/write binary data

--

## Languages

- Created to allow humans to write machine code (binary)
- Uses a natural language to write instructions
- Pave the foundation for advanced algorithms/applications
- Compiling converts the language to machine code

---

## Java

- One of the most advanced programming languages to date
- Sun Microsystems (now Oracle) created in 1991
   - James Gosling team lead
- Original created to have a platform independent way to communicate
- Changed focus to web technology due to the timing

--

## Platform Independence

- Code has to be compiled to work for a specific system/CPU
   - Windows, Linux, MacOS
- Java changed this model
- Java Compiler creates ByteCode
- Java Runtime interprets the ByteCode to run on the platform
- The Runtime is the variation per platform

--

## Write Once Run Anywhere

- The tag line of Java
- This is true because Java Runtime is everywhere
- Changed the focus to Applications
   - Writing and testing once for all platforms

---

## Java Variants

- Can be very confusing due to Java change over time
- Mostly different variants target different environments
- Focus on
   - Java Runtime Environment (JRE)
   - Java Developers Kit (JDK)

--

## Java Runtime Environment (JRE)

- JRE minimal needed to run any java application
- JVM - Java Virtual Machine
   - 'java' command line
- Usually default installed on most systems

--

## Java Developers Kit (JDK)

- Used by developers to compile Java code
- Contains he java framework source code
- Other development tools
- Different editions to focus purpose
   - Standard Edition (Java SE)
   - Enterprise Edition (Java EE)
   - Micro Edition (Java ME)
- We will use JDK SE

--

## Java + Open Source

- Open Source is a major player in the industry
- Can provide high quality software solutions
- Provides free access to source to share, contribute and learn
- Java is open source
   - OpenJDK

---

## Getting & Installing Java

- See the write-up for details
  - Windows
  - Linux
  - MacOs

{{< /revealjs >}}