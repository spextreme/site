---
title: "Computers - Video"
date: 2021-01-10T10:08:56+09:00
description: The video and slides for week 1 - session 1 on computers
draft: false
collapsible: false
weight: 30 
---

## Video

[![Watch the video](/images/intro-to-java/videos/computers-screen.png)](https://youtu.be/Z56fIiWCtRc)

## Slides

{{< revealjs >}}

## Computers - Introduction

- How do computers work
- What makes them function
- Everything is based on computer technology
- Hardware is the foundation

---

## Hardware

- Computers are a collection of hardware
- Hardware is the physical content in a computer
   - CPU - Central Processing Unit
   - Hard Drive
   - Memory
   - Much more
- External hardware
   - Keyboard
   - Mouse
   - Monitor
   - etc...

--

## Central Processing Unit

- This is known as the CPU
- The main part of a computer
- Works like the brain
- CPU takes in instructions and produces an output
- CPU sits in the mother board

--

## CPU Image
![CPU pins](/site/images/intro-to-java/cpu-pins.jpg) ![CPU pins](/site/images/intro-to-java/cpu-in.png)

--

## Motherboard

- The main circuit board holding a collection of components
   - Transistors, processor, slots, memory, network, usb ports, etc...
- Allows electrical components to communicate
   - CPU can access memory
- Contains slots for other parts to connect
   - Memory
   - Expansion Slots
- Many shapes and sizes

--

## Motherboard Image
![Motherboard](/site/images/intro-to-java/motherboard.png)

--

## Main Memory

- Used to store information
- Called RAM - Random Access Memory
- Read and Write
- RAM is volatile
- Organized into units called Bytes
   - A byte is 8 bits (1 and 0)
   - Grouping bits into a collection.
   - Byte is capable of representing 256 unique combinations
- Memory organizes the data into address spaces

--

## RAM
![Memory](/site/images/intro-to-java/memory.png)

--

## Memory Storage

- Memory organizes the data into address spaces
- Think of it like a collection of rows/columns

Example : Store 143 in memory @ address 14

|    |    |    |    |    |    |    |    |    |    |
|----|----|----|----|----|----|----|----|----|----|
| <sup>0</sup>  | <sup>1</sup>  | <sup>2</sup>  | <sup>3</sup>  | <sup>4</sup>  | <sup>5</sup>  | <sup>6</sup>  | <sup>7</sup>  | <sup>8</sup>  | <sup>9</sup>  |
| <sup>10</sup> | <sup>11</sup> | <sup>12</sup> | <sup>13</sup> | <sup>14</sup> 143 | <sup>15</sup> | <sup>16</sup> | <sup>17</sup> | <sup>18</sup> | <sup>19</sup> |
| <sup>20</sup> | <sup>21</sup> | <sup>22</sup> | <sup>23</sup> | <sup>24</sup> | <sup>25</sup> | <sup>26</sup> | <sup>27</sup> | <sup>28</sup> | <sup>29</sup> |

--

## Data Store

- Long term/permanent storage
- Hard Drive
   - Collection of disks packed together
   - Read/write head floats
   - Spins why the head moves in and out
- Solid State Drive (SSD)
   - Electronic storage
   - Smaller in sized
   - Store much more information
   - Usually much more expensive

--

## Hard Drive

![Hard Drive](/site/images/intro-to-java/harddrive.png)

--

## Solid State Drive

![Solid State Drive](/site/images/intro-to-java/ssd.jpg)

--

## Other Media Types

- Universal Serial Bus (USB)
   - Similar to SSD
   - Smaller
   - Plug into a USB port
- Compact Disk (CD)
- Digital Versatile Disc (DVD)

--

## Input Devices

- Method for humans to input information
- Keyboard
   - Input characters to the computer
- Mouse/Finger/Stylist
   - Move a pointer around the screen
   - Select things
- Microphone
   - Sound input

--

## Output Devices

- Visualize what the computer is doing
- Display on a screen
   - User interface
   - Watch a video
- Speakers allow us to hear sounds
- Printer allows content to a piece of paper
   - 3D printers mold plastic into a shape

---

## Software

- Makes a computer useful
- Can do just about anything
- Nothing more then a collection of instructions
- Tells the CPU what to do

--

## Operating System

- OS
- Base software on all computers
- Provides the interface to the hardware
- Example OS's
   - Android
   - MacOS
   - IOS
   - Linux
   - Windows
   - many more

--

## Application Software

- Software that runs on top of the OS
- Provide user functionality
   - Web browser (Chrome, Firefox)
- Can be written perform any task
- Languages are how humans write software
   - Many languages available
      - Java, C++, Python, many more

{{< /revealjs >}}