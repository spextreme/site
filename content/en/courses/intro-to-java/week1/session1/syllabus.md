---
title: "Syllabus"
date: 2021-01-10T10:08:56+09:00
description: The course and administrative details.
draft: false
collapsible: false
weight: 10
---

## Instructor
Steve Paulin - spextreme at gmail dot com

## Course
| Class                      | Code     | Meeting Times | Locations |
| -----                      | ----     | ------------- | --------- |
| Intro Sci Programming-Java | CS-01104 | T/Th 3:30-6:15     | T - Bunce 156 / Th Entprz 517 |
 
## Material

All the material is accessible through my GitLab site.

https://spextreme.gitlab.io/site/courses/intro-to-java/

## Course Description  

Introduction to Java focuses on the fundamentals of programming using the Java language.  It explains the basics including primitive types, the Java syntax, loops, conditionals and concepts on object oriented programming.  Other topics include using an integrated development environments (IDE) to assist in development and source control with Git to manage change.

## Course Topic Overview
The course covers all topics of fundamental programming. At the end of the class, you will understand:
1. Code, compile and run a Java program.
2. Programming techniques for console input and output.
3. Apply logical constructs for branching and loops.
4. Define classes and methods.
5. Create and access arrays.
6. Employ exception-handling programming techniques.

The syllabus/schedule are subject to change.

## Grading
| Item | % of Points |
| ---- | ----------- |
| Classroom Assignments | 60% |
| Homework Assignments | 20% |
| Exam | 20% |
| Total | 100% |

| Letter Grade | Pct |
| ------------ | --- |
| A    | 97-100   |
| A-   | 96- 90   |
| B+   | 87-89    |
| B    | 83-86    |
| B-   | 80-82    |
| C+   | 77-79    |
| C    | 73-76    |
| C-   | 70-72    |
| D+   | 67-69    |
| D    | 65-66    |
| Fail | Below 65 |

## Course Activities

The course activities will include videos/documentation where new material is presented and explained. During our meeting time, you will have the opportunity to ask questions about the material, get in depth explanations of concepts as we work through coding exercises on the concepts.  

## Student Evaluation
1. Attendance - You are expected to attend every class since the class assignments are the largest percentage of the grade. Missing a class will hamper your learning objectives and your overall performance in the course.
2. Class Assignments - Assignments are done during the class meeting time which is why attendance is important.  Each time we meet, we'll develop applications to exercise what has been covered in the material.   If you are not present in class, you cannot submit the assignments.
3. Homework Assignment - Each week, it is expected that all material for the giving meeting time has been reviewed.

## Canvas
Canvas is officially the location to use to submit assignments and review your grades.  Canvas also offers discussion area and collaboration tools.  It is recommended to log in regularly.
https://rowan.instructure.com/

## Exam
There will be one Exam near the end of the course to evaluate concepts that have been covered.

## Homework
It is the expectation that before each class the material has been reviewed (watched the videos or read through he documentation) before that given class.   This will allow you to have the foundation to work the course material during our class meeting time.

## University-Wide Policies
You should be familiar with all the University-wide policies. They are Included in Student Handbook which can be found at
http://www.rowan.edu/studentaffairs/main_office/publications/Handbook_Planner.cfm

### Acceptable Use Policies
By registering and taking this course, you are agreeing to follow all Rowan computer and network acceptable use policies for faculty, staff, and students. Detailed information may be found on the Rowan Acceptable Use Policy, Computer Lab Usage Policy, Workstation Use Policy, and Mobile Electronic Device Policy. Or, access all Rowan Information Resources & Technology Policies and Procedures.

### Academic Integrity and Plagiarism
It is the student's responsibility to be fully familiar with the Rowan Academic Integrity Policy and the Student Code of Conduct. Students will be held accountable to strictly adhere to these policies. By the act of assignment/assessment submission, it will be assumed that students thereby affirm that they have neither received nor provided any unauthorized assistance with the completion of their work.
Plagiarism and cheating in ANY form is considered academic dishonesty and will NOT be tolerated.
ALL academic integrity violations will be reported via the Report of an Academic Integrity Violation (RAIV) Form and will result in penalties that may include a failing course grade and/or probation, suspension, or expulsion from Rowan.

### Attendance
Regular attendance for this class is required and expected. You are responsible for any missed material and completing all work by the assigned due dates. You should notify the instructor of your absence as soon as possible. For more information on student attendance responsibilities, see the Rowan Attendance Policy.

### Classroom Decorum
All students are held to certain standards of conduct and are expected to help promote the learning environment by respecting their instructor/classmates and by adhering to the Rowan Classroom Behavior Policy.

### Statement of Accommodation
Your academic success is important. If you have a documented disability that may have an impact upon your work in this class, please contact me. Students must provide documentation of their disability to the Rowan Academic Success Center in order to receive official University services and accommodations. The Academic Success Center can be reached at 856-256-4234. The Center is located on the 3rd floor of Savitz Hall. The staff is available to answer questions regarding accommodations or assist you in your pursuit of accommodations as per the Rowan Accommodation Policy. We look forward to working with you to meet your learning goals.

### Rowan University's Statement on Diversity
Rowan University promotes a diverse community that begins with students, faculty, staff and administration who respect each other and value each other's dignity. By identifying and removing barriers and fostering individual potential, Rowan will cultivate a community where all members can
learn and grow. The Rowan University community is committed to a safe environment that encourages intellectual, academic, and social interaction and engagement across multiple intersections of identities. At Rowan University, creating and maintaining a caring community that
embraces diversity in its broadest sense is among the highest priorities.

### Rowan University Sexual Misconduct and Harassment Reporting, and Title IX
Rowan University and its faculty and staff are committed to assuring a safe and productive educational environment for all students. Title IX makes it clear that sexual misconduct and harassment based on sex and gender is a Civil Rights offense subject to the same kinds of accountability and the same kinds of support applied to offenses against protected categories such as race, national origin, etc.
University faculty and staff members are required to report any instances of sexual misconduct or harassment, to the University's Title IX Coordinator so that the appropriate resources and support options are provided. What this means is that as your professor, I am required to report any incidents of sexual misconduct and harassment that are directly reported to me, or of which I am somehow made aware.
If you are the victim of sexual misconduct or harassment, Rowan encourages you to reach out to these resources:

### Confidential Resources
The Wellness Center, Winans Hall, 856-256-4333, http://www.rowan.edu/wellness

### Non-Confidential Resources
Office of Student Equity and Compliance (OSEC), Savitz Hall 203, 856-256-5830
Public Safety, Bole Annex, 856-256-4911
Other reporting information is available here: http://go.rowan.edu/titleix

### Rowan Success Network and Rowan Tutoring Center

The Rowan Success Network (RSN) powered by Starfish� is the online
communications program designed to make it easier to connect you with the
resources to be successful at Rowan. Throughout the semester, you may receive emails from the RSN team regarding your academic performance. You should pay attention to these emails and consider taking the recommended actions.

The University Tutoring Center is also available free of charge to all Rowan students. You may utilize the scheduling tools on RSN Starfish� to make appointments, including tutoring and advising, at your convenience.

### Class Withdrawal Policy

| Date | Process | Transcript |
| ---- | ------- | -------- |
| Regular Drop/Add | Drop/add courses easily without forms or approvals | Course will not be shown (if dropped) |
| Late Drop/Add | Drop/add courses with form and instructor signature | Course will not be shown (if dropped) |
| Regular Withdrawal | Withdraw from course with form and instructor signature | Course grade of W |
| Late Withdrawal | Withdraw for extenuating circumstances having Dept. of Computer Science approval with withdrawal form, instructor signature, and department chair signature | Course grade of WP/WF (as determined by instructor) |
| Hardship Withdrawal | NO withdrawals except for extreme circumstances with department chair and dean withdrawal form signatures | Course grade of WP/WF (as determined by instructor) |

Refer to Rowan Registrar dates and deadlines chart for the specific semester at: https://sites.rowan.edu/registrar/registration-information/registration-dates.html



