---
title: "OOP..."
date: 2021-01-19T10:08:56+09:00
description: Overview of Object Oriented Programming, Commenting and more.
draft: false
collapsible: false
weight: 15
---

## Object Oriented Programming (OOP)

Object Oriented Programming (OOP) is a concept of using human-based objects to represent parts of an application.   This allows applications to be more easily understandable by humans, as well as more modular.

For example, if you think about a web page, it may have a layout, logo, title, links and content.   If we were to represent it as a collection of objects, we may break it down into those pieces.   A logo may be represented by its position (x, y) and its size (width, height).

### What is an Object

In OOP, an object has two capabilities that define it.   It must be able to manage data and perform operations.  


#### Fields

Fields (aka variables or properties) are very important to any program because they are a way of storing data with a easy reference to the information.  As was discussed in the Computers section, memory holds binary information.   A variable is a named reference to that information stored in memory.

In Java, fields are given a type which helps identify the format of data being stored.  There are 8 primitive data types in Java.

| Data Type | Size | Range |
| --------- | ---- | ----- |
| byte| 1 byte | -128 to +127 |
| char| 1 byte | -128 to +127 |
| boolean| 1 byte | 0 or 1 |
| short| 2 byte | -32768 to +32767 |
| int| 4 byte | -2<sup>31</sup> to +2<sup>31</sup>-1 |
| long| 8 byte | -2<sup>64</sup> to +2<sup>64</sup>-1 |
| float| 4 byte | 3.40282347 x 10<sup>38</sup> to 1.40239846 x 10<sup>-45</sup> |
| double| 8 byte | 1.7976931348623157 x 10<sup>308</sup>, 4.9406564584124654 x 10<sup>-324</sup> |

The range is not as important when you start programming but may become valuable to make sure your application doesn't overtake the available memory of the computer.

##### Definition

Creating a variable is very simple by using the following format

```
<type> <name> = <value>;
``` 

The type is focused on the Data Type talked about in the introduction.  This is how you define the type of data that this variable will hold so the system can allocate the right amount of space for it.

The name is a way to describe the purpose of this as well as the way to reference it later in the application.  The name can be any string of characters but can't contain any special characters other then an underscore (_).   Typically a name is represented as words using camel case (first letter of each word is capitalized except the starting character is lowercase i.e. 'myVariable').  Also note that numbers may be used in the variable as long as they are not the first character. 

Finally, the value follows the equal sign and is the data to be held by this variable in memory.   The value being provided must match the defined type that has been set.  This means you can't place a char in the variable if the type is defined as an int.

To terminate the variable definition the line ends with a semi-colon (;).  This is actually true for all java lines.

##### Examples

The following are examples for creating each type of variable.

``` java
  byte myByte = 10;

  boolean myBoolean = true;

  char myChar = 's';

  short myShort = 100;

  int myInteger = 10000;

  long myLong = 1000000000L;

  float myFloat = 3.1415f;

  double myDouble = 9.8214213;
```

As these show, defining a variable is very simple.  Just remember the name = value pattern.


#### Methods

The actions that can be performed on an object are called Methods.   These are named items that can be called to take an action.   This may be to set a value, perform a calculation, or return some data.   They are nothing more than how the object will do work upon requested.

Methods take the form of 

```
<scope> <return type> <name> ( <parameters> ) { }
```

Lets begin with the *name*.  The name will be used to call this method.  It can be any string as long as it doesn't contain special characters or spaces.   Typcially the name is a set of words where the first letter is capitalized.  This is called Camel Case. For example, getName, calculateTrajectory.

The return type can be any primitive or object but may also be set to the keyword *void* to mean nothing is going to be returned.  

Scope will be covered at a later time.

The section after the method name '()' is where 0 or more parameters may be defined.   Parameters are also typed like fields.   You can have as many parameters as need seperating each by a comma.   

Finally the part included in the { ... } is where the method instructions go.  This is how you define what the method will perform when called.

```java
public String createGreeting( String name ) { 

   return "Hello " + name "!";
}
```

The above example when called, will take a String *name* as a parameter, concatinate it with the word 'Hello ' and append it with an '!' and return the new String as a response.

## Basics of Programming

Programming is about understanding how to tell a computer to perform some action.  It could be as simple as printing some text on a screen, or it could be more complex like drawing an image.    In all cases, it starts with a single instruction.

An instruction is a line of code that does something.   Whether it is to create a variable to hold data, or to perform an operation, there isn't much more than that.

With modern languages like Java, which are object based (Object Oriented Programming), they provide an easier method for people to understand how to create these instructions to tell the computer what to do.  As we discussed in the [computers](courses/intro-to-java/week1/session1/computers/) section, they only know how to perform binary (on/off) operations. So programming in objects is much easer for us to do.

### First program

In every programming language there is that first program, and it is the simple "Hello World" example (Yes every language has this).   Why is this the first program?   This is because it is very simple (usually a couple lines of code) and will successfully run.  It's also good because it is hard to get wrong and gives the ability to compile and run with minimal code.  Here is a simple Hello World for a Java.

``` java
public class HelloWorld {

  public static void main(String[] args) {

    System.out.println("Hello World! - Intro to Java");
  }
}
```

As you can see, this is more than a couple lines but the main thing to notice is it creates an object 'HelloWorld', a main method (the part that JVM will call when it runs), and a simple print line to output to the screen.

While this is very simple, it highlights the main parts of a Java application.  To run this we have to then:   

- Compile - 'javac HellowWorld.java'
- Running - 'java HelloWorld'

``` bash
Hello World! - Intro to Java
```

That is all we need to do to compile and run our first Java application.

## Commenting

All languages have a way to apply comments to the code.  Comments are a way to document more details about the code developed.  It is often best practice to put documentation in the code to describe the variables, methods, and class details.

In Java there are two types of commenting, standard comments and Javadoc.

### Standard Comments

There are two forms of standard comments in Java code: a single line comment and a multiline comment.

#### Single Line

This form is used to comment one line at a time.  To use a single line comment, a double slash is used (//) and the comment begins from any place that it appears.

```java
// This is a comment
```

As mentioned, it doesn't have to be on the start of a line, as it may be placed after code.  Just note that the // starts the comment and it will go to the end of the line.

```java
int width = 100; // This is in pixels
```

#### Multiline

There are times when you may need to have comments wrap across multiple lines or embedded within a section of code. The multiline comment block supports this.  This is done by using the slash + star (/*) to start the comment and star + stash (*/) to end the comment.

```java
/* 
This is a multiline comment
that can span as many lines as needed.
*/
````

This may also be used to embed a comment in code.

```java
for(int i /* index */ = 0; i < total /* all records */; i++ ) {
}
```

### Javadoc

When Java was created, it had a built in capability to support generating documentation for the code.  It uses a tool called *javadoc* to parse the code, to build a collection of web pages for the classes and the details of the class.  The beauty of this is that it uses the code to generate the details, which means it is always accurate to the code.

Since this is parsing the code to generate the documentation, it may not be descriptive enough or may be lacking the details required.   Javadoc created a special commenting model to support adding details to the pages it generates.  This way a class that gets generated can include details about the package, class, methods and/or fields.  

The slash + double star (/**) starts a javadoc comment and the star + slash (*/) ends it.   Placing this on a class, method, or field will cause the Javadoc parser to use it for the description of the associated type.

```java
/**
 * A class level comment.
 */
public class ShowComments {

  /**
   * A comment for a methods with an input parameter and return type description.
   *
   * @param param The parameter comment.
   * @return The return comment.
   */
  public int methodComment(int param) {

    return param + 1;
  }
}
```

To generate this into a collection of web pages we can run the javadoc tool on it.  The output will be a set of web pages (Actually it's a full site so there are a lot of pages generated).   To run the javadoc tool on this:

```bash
javadoc ../src/comments/ShowComments.java
```

There is a lot more to javadoc but this gives a basic understanding of its use.

### Always Comment

In this section, we talked about different methods for commenting your code.  It is always a good practice to include Javadoc for all classes and public methods at a minimum.   This way the documentation can be generated and help with the maintenance and understanding of the code and potential use of it.  
