---
title: "OOP... - Video"
date: 2021-01-19T10:08:56+09:00
description: Overview of Object Oriented Programming, Commenting and more.
draft: false
collapsible: false
weight: 10
---
## Video


[![Watch the video](/images/intro-to-java/videos/oop-prog-commenting-screen.png)](https://youtu.be/BRiNhMSxbHA)

## Slides

{{< revealjs >}}

## Object Oriented Programming (OOP)

- Using objects to represent parts of the application
- More understandable by humans
- Improve design and modularity
- Self contained items to help organize functionality

--

## What is an object

- Two main parts of an object
   - Fields
   - Methods

--

## Fields

- Fields hold data in memory
- Typed Key/Value pairs
- Fields, Properties, Variables - all the same thing

--

## Field Types

- The format of the data being stored
- Put in front of the key
- Java primitive Types
   - byte, char, boolean, short, int, long, float, double
- May be other objects
   - String, List, etc...
--

## Field Definition

``` java
type name = value;
```
- Type safety
- Name describes the variable
   - Camel Case - myVariable
   - Numbers may be used but not the first character
   - No special characters except _
- Value is after the = and what should be held
   - Must match the type

--

## Methods

- An action to perform on an object
- A named item that may be called
- How the object can do work
- May return or pass in data

```java
public String createGreeting( String name ) {

   return "Hello " + name "!";
}
```

---

## Basics of Programming

- Telling a computer what to do
- A set of instructions
- Java uses keywords that become those instructions
- Object based programming is much easier to create instructions

--

## First Program

- Always starts with a "Hello World" example
- Done for simplicity and easy to run
- Nice for testing things out

``` java
public class HelloWorld {

  public static void main(String[] args) {

    System.out.println("Hello World! - Intro to Java");
  }
}
```

--

## Compiling and running

- Main method is called first
- The println outputs to the console
- Compile with
   - javac HelloWorld.java
- Run with
   - java HelloWorld

``` console
Hello World! - Intro to Java
```
---

## Commenting

- All language support it
- Used to document code for others to understand
- Used to describe the code
- Useful for maintenance and others to use

--

## Standard Comments

- Two main forms of comments in Java
   - Single Line
   - Multiline

--

## Single Line

``` java
// this is a comment
int width = 100; // This is in pixels
```
- // starts a comment
- Runs to the end of the line
- Can appear at any point on a line

--

## Multiline

- Sometimes its better to write multiple lines
- Can wrap lines in comment block
- Also useful for embedding in code
- Start /* and end */ set

``` java
/*
This is a multiline comment
that can span as many lines as needed.
*/

for(int i /* index */ = 0; i < total /* all records */; i++ ) { }

```
--

## Javadoc

- A build in capability of Java
- Parses the code to generate HTML documentation
- Unique commenting model to allow custom details
- Packages, Classes, Methods and Fields
- /** and */

--

## Javadoc Example

``` java
/**
 * A class level comment.
 */
public class ShowComments {
  /**
   * A comment for a methods with an input parameter
   * and return type description.
   *
   * @param param The parameter comment.
   * @return The return comment.
   */
  public int methodComment(int param) {
    return param + 1;
  }
}
```

--

## Always Use Comments

- Comments are highly valuable
- Best practice to use Javadoc
- Comment complex code
- Great for maintenance

{{< /revealjs >}}