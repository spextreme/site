---
title: "Intro To Java"
description:  "Introduction to Java, focuses on the fundamentals of programming using the Java language.  It explains the basics including primitive types, the Java syntax, loops, conditionals, flow control and concepts on object oriented programming.  Other topics include using an integrated development environments (IDE) to assist in development and source control with Git to manage change."
date: 2020-11-01T10:08:56+09:00
draft: false
collapsible: true
weight: 1
---