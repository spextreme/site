---
title: "Conditional - Video"
date: 2021-02-01T10:08:56+09:00
description: Details about conditional statements and decision structures.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/conditional-screen.png)](https://youtu.be/Tn6KuH7C-VQ)

## Slides

{{< revealjs >}}

## Introduction

- Making decisions
   - Easy?
   - Hard?
- Computer making a decision
   - Easy?
   - Hard?
- More important part of programming
- Condition is *true* or *false*

---

## if

```java
if( < condition equals true > ) {
   //perform when condition is true
}
```

- Checks a conditional for *true*
- Runs a block of code if *true*
- Conditional
   - Any statement that returns a boolean
   - Boolean *true* or *false*

--

## if Flow Chart

![If Flow Chart](/site/images/intro-to-java//if-flow-chart.png)

--

## Relational Operators

|Rel Op|Meaning|Example|
|-------------------|-------|-------|
|==|Equal to|1 == 1|
|!=|Not equal to| 1 != 2|
|>|Greater than|1 > 0|
|<|Less than|1 < 5|
|>=|Greater than or equal to| 1 >= 1|
|<=|Less than or equal to|1 <= 1|

--

## Relational Operators

- Used to perform a comparison
- Result in *true* or *false*
- Equals is a double equals sign ==
- ! not operator

--

## Examples

```java
if (x < y) {
  System.out.println("X is less then Y");
}

if (x != y) {
  System.out.println("X and y are not equal");
}

if (x == 1) {
  System.out.println("X equals one");
}
```

---

## If Else

```
if( < condition equals true > ) {
   //perform when condition is true
}
else {
   // Perform when condition is false
}
```

- Adds a alternate path
- Useful for doing something in either case
- Do one thing on *true* or alternate on *false*


--

## if else Flow Chart

![If Flow Chart](/site/images/intro-to-java//if-else-flow-chart.png)

--

## Examples

```java
if (x <= y) {
  System.out.println("X is less then Y");
} else {
  System.out.println("Y is greater then X");
}
```

---

## If, Else If, Else

```java
if( < condition equals true > ) {
   //perform this action
}
else if( < condition equals true > ) {
   //perform this action instead
}
else {
   // Perform when condition is false
}
```

- Used when multiple conditions needed
- First *true* one will run
- No limit to the number of *else if* statements

--

## Example

```java
if (x < y) {
  System.out.println("X is less then Y");
} else if (x > y) {
  System.out.println("Y is greater then X");
} else {
  System.out.println("X and Y are equal");
}
```

---

## Nested if

```
if( < condition equals true > ) {
   if( < condition equals true > ) {
      //perform this action instead
   }
}
```

- *if* inside of an *if* block
- Allows performing some other check
   - If first check *true*

---

## Logical Operators

|Operator|Meaning|Example|
|------|-------|-------|
|&&|and| x > 1 && x < 10|
|\|\||or|x > 1 \|\| x < 10|

- Join conditionals
- Build more complex comparisons

--

## Logical Opertor &&

```java
if( x > 1 && x < 10 ) {
   ...
}
```

- Read as and
- Both must be *true* to be *true*
- Stops on first *false* item

--

## Logical Opertor ||

```java
if( x > 1 || y < 10 ) {
   ...
}
```

- Read as or
- One must be *true* to be *true*
- Stops on first *true* item

---

## Operator Precedence

- Order matters in conditionals
- ! is evaluted first
- Relational Operators
- && and || last
- () can control order

--
## Operator Precedence Example

```java
if( x == y || y == 10 ) {}

if( x == y || ( y == 10 && z == 12 ) ) {}

if( !(x == y) && z == 12 ) {}
```

---
## *if* Single Code Line

```java
if (x < y)
  System.out.println("X is less then Y");
else if (x > y)
  System.out.println("Y is greater then X");
else
  System.out.println("X and Y are equal");
  System.out.println( "this isn't part of the if");
```

- last line is not part of *if* statement

---

## Conditional Operators

```java
< condition > ? < true code > : < false code >;
```

- Simple cases
- ? is the true path
- : is the false path

--

## Conditional Operators Example

```java
public static void conditionalOperatorExample(String input) {

 String output = input.toLowerCase().contains("hello")
      ? "Hello back"
         : "Good Bye";

 System.out.println(output);
}
```

---

## Switch Statement

```java
switch (< expression >) {
 case < value1 >:
   // code to execute
   break;

 case < value2 >:
   // code to execute
   break;

 default:
   //code to execute
   break;
}
```

--

## Switch Statement

- works like if, else if, else
- expression can be
   - *char*, *byte*, *short*, *int* or *String*
- *.equals* or *==* comparison

--

## Switch Statement Example

```java
String output = "";
switch (input.toLowerCase()) {
  case "hello":
   output = "Hello There";
   break;

  case "goodbye":
   output = "See you later.";
   break;

  default:
   output = "Sorry I don't understand that.";
   break;
}
```

---

## Conclusion

- Conditionals are important
- Heavily used in programming
- Can become very complex

{{< /revealjs >}}
