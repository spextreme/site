---
title: "Conditional"
date: 2021-02-01T10:08:56+09:00
description: Details about conditional statements and decision structures.
draft: false
collapsible: false
weight: 15
---

## Conditionals

Decision logic is one of the most important parts of programming. It provides a way to determine if something is *true* or *false* and if one thing or another should be done.

In this section, we'll focus on conditional logic and how you can make a decision using different conditions.

## If

*if* is used to determine if a condition is true, then execute some block of code.  When the *if* statement is *false*, then the block of code it contains will not be executed.

This allows the developer to create conditions on when to perform a set of actions or not based on some rules the application requires.

The syntax of an *if* statement is as follows

```
if( <condition equals true> ) {
   //perform when condition is true
}
```

The code to execute when the condition equals *true* is what belongs in the { ... } braces.

A conditional statement is any statement that returns a boolean result.  Remember a boolean is *true* or *false*.   Boolean values are produced by methods and comparisons.  Remember our example in Strings:

```java
"Hello".equals( "Hello" )
```

This would return *true* and be a good *if* condition.

*if* statements can be easily represented using a flow chart.

![If Flow Chart](/images/intro-to-java/if-flow-chart.png)

To understand how to create a conditional statements we need to understand relational operators.  Relational operators are the operators used to perform a comparison that will then return a boolean result.

|Relational Operator|Meaning|Example|
|-------------------|-------|-------|
|==|Is equal to|1 == 1|
|!=|Is not equal to| 1 != 2|
|>|Is greater than|1 > 0|
|<|Is less than|1 < 5|
|>=|Is greater than or equal to| 1 >= 1|
|<=|Is less than or equal to|1 <= 1|

Relational operators are used to perform the comparison.   Notice that the equals uses two equals signs.  This is because a single equals sign is used for assigning a value to a variable.   Also notice that *!* means *not* when combined with an equals sign.

Here are a few examples to show the different relational operations:

```java
if (x < y) {
  System.out.println("X is less then Y");
}

if (y > x) {
  System.out.println("Y is greater then X");
}

if (x != y) {
  System.out.println("X and y are not equal");
}

if (x == 1) {
  System.out.println("X equals one");
}
```

Now that we understand relational operators and the *if* statement, we can start to expand on this.

### If, Else

With the *if* statement being a simple do this code or not, there may be times when we need to do one thing for the *true* case and another thing when it's *false*.  This is where the *else* statement comes in.

```java
if( <condition equals true> ) {
   //perform when condition is true
} 
else {
   // Perform when condition is false
}
```

All this will do is, when the condition is *true*, it will act on the first code block after the if statement.   If the condition is *false*, then it will act on the block after the *else* statement.

This is useful when you want to perform one action but then handle the opposite for other case.

This can be represented as a flow chart as well.

![If-Else Flow Chart](/images/intro-to-java/if-else-flow-chart.png)

Here is an example to show how the *if else* statement works:

```java
if (x <= y) {
  System.out.println("X is less then Y");
} else {
  System.out.println("Y is greater then X");
}
```

### If, Else If, Else

There are cases where a go, no go path isn't enough.  Maybe there are multiple conditions that need to be handled.  This is where you can build a set of conditions and check against different ones until one is *true*.  In this case it takes the form:

```java
if( <condition equals true> ) {
   //perform this action
} 
else if( <condition equals true> ) {
   //perform this action instead
} 
else {
   // Perform when condition is false
}
```

In this example, the code will evaluate the first condition, and if *true*, it proceeds into the code block.  But if it is *false*, it will then evaluate the second else if condition, and if that is *true*, then it will proceed into that code block.  If both of them are *false*, then the *else* condition code block will be executed.  There is no limit to the number of *else if* statements that may be defined.

```java
if (x < y) {
  System.out.println("X is less then Y");
} else if (x > y) {
  System.out.println("Y is greater then X");
} else {
  System.out.println("X and Y are equal");
}
```

### Nested if

When you perform an *if* condition you have a code block that will be executed.  There are times when you want to perform another conditional check *if* the first condition is *true* (or *false*).  In this case you can nest *if* statements.

```java
if( <condition equals true> ) {
   if( <condition equals true> ) {
      //perform this action instead
   }
} 
```

This provides a way to add more advanced conditional checking and handle different alternative points as needed.  There is no limit to the amount that may be nested.

### Logical Operators

*if* statements that are nested can sometimes become overly complex and could potentially be combined into a single *if* statement.  You can also combine conditions to handle more complex cases.  Thinking about the examples we have had *x > y*, maybe we need to handle a case where z is between x and y.

The *if* condition has two unique operators that allow combining different boolean values into one check.  This way you can combine conditions to make one big conditional statement.

|Operator|Meaning|Example|
|------|-------|-------|
|&&|and| x > 1 && x < 10|
|\|\||or|x > 1 \|\| x < 10|

These are used to combine conditional statements to make a more complex conditional.    You can mix and match as needed, but it can quickly become hard to understand.   The thing to remember is that each part has to evaluate to *true* or *false*, then the *and* and *or* are evaluated.  They are evaluated until the condition is met.  

```java
if( x > 1 && x < 10 ) {
   ...
}
```

In the above example, it will first check if *x* is greater then 1 and then check if *x* is less then 10.   If both are *true*, then this condition is *true*.  If either of these is *false*, then the and condition is also *false*.  Really, if the statement runs and x > 1 is false, it stops checking since there is no way for the overall condition to evaluate to *true*.  This is done to improve performance.

```java
if( x > 1 || y < 10 ) {
   ...
}
```

Now if we use the *or* operator. If *x > 1* is *true* and *y < 10* is *false*, this will evaluate to *true*.   In this case, as long as one of the statements is *true*, then this will evaluate to *true*.  This too is optimized and will stop checking the condition as soon as one of them evaluates to *true*.

As you can see, conditionals can become quite complex.  As we work through exercises in class, we'll most likely hit some of these types of cases since this can be very useful.  As you get more comfortable with conditionals, these will start to feel more natural and easier to understand and put together.

### Operator Precedence

With the *&&* (read as and) and *||* (read as or) operator, you'll need to understand what the order of precedence is, meaning what condition will run first, second, and so on.

The *!* (read as not) operator has the highest precedence, so it will be evaluated first.  The *&&* and *||* have the lowest and will be evaluated after all relational operators (*==*, *>*, *<*, etc.).

If you need to control the order to force a different evaluation sequence, then you can wrap things in parentheses *()*.

Here are a few examples:

```java
if( x == y || y == 10 ) {} // order: x == y, y == 10, ||

if( x == y || ( y == 10 && z == 12 ) ) {} // order:  y == 10,  z == 12, &&, x == y, ||

if( !(x == y) && z == 12 ) {} // order:  x == y, not, z == 12, && 
```

### *if* Single Code Line

In all of the examples shown, the curly brackets were used to wrap the *if*, *else* and *else if* code blocks.  This is good practice and highly recommended because it makes the code easier to read, better to maintain, and reduces error.

However, you should be aware that the Java syntax allows an *if* statement to be followed by a line of code without the curly brackets.   In this case, the single line up to the semicolon will belong to the *if* statement.

```java
if (x < y)
  System.out.println("X is less then Y");
else if (x > y)
  System.out.println("Y is greater then X");
else
  System.out.println("X and Y are equal");
  System.out.println( "this isn't part of the if");
```

The last line is not part of the *else* in the above example.   This is why you could potentially have an error.  The formatting makes you believe it is under the *else* but when the code runs, that line will always be printed no matter what condition is hit because it really is executing this.

```java
if (x < y)
  System.out.println("X is less then Y");
else if (x > y)
  System.out.println("Y is greater then X");
else
  System.out.println("X and Y are equal");
  
System.out.println( "this isn't part of the if");
```

This is where formatting can really help, but again, if you always use the curly brackets, you'll never accidentally do something like the above.

## Conditional Operators

As mentioned, the *if else* construct in Java is quite powerful and makes the language able to achieve some very complex things.   There is a shorthand operator to perform an *if else* without all the keywords and run in a single line.  It uses the *?* and *:* operators in a special syntax.

```
<condition> ? <true code> : <false code>;
```

In this context, the condition gets evaluated like an if condition to *true* or *false*.   If the condition is *true* then the part after the *?* is executed.   If the condition is *false* then the part after the *:* is executed.    

This has to be single line and should only be used for simple executions.  It is generally used to initialize variables.

Here is an example:

```java
public static void conditionalOperatorExample(String input) {

 final String output = input.toLowerCase().contains("hello") ? "Hello back" : "Good Bye";

 System.out.println(output);
}
```

## Switch Statement

A *switch* statement is another form of conditional logic.   A *switch* statement evaluates the input value and compares it against a set of cases until a match is found.  If no match is found, then it runs the default case.   This works similar to an *if*, *else if*, *else* setup.

The switch statement can evaluate *char*, *byte*, *short*, *int* or *String*.  Longs, Floats, and Doubles can't be used due to their length and precision.

```java
switch (<expression>) {
 case <value1>:
   // code to execute
   break;

 case <value2>:
   // code to execute
   break;

 default:
   //code to execute
   break;
}
```

The way this works is it reads in the expression and compares it to each case statements value until a match is found.   This means it's doing a *==* or *.equals* evaluation.  If none of the cases match, then the default path will be taken.

Here is an example of passing in a string, and if it is *'hello'* or *'goodbye'*, then a case will be hit.  If it is any other string, then the default path will be executed.

```java
 public static void switchExample(String input) {

    String output = "";

    switch (input.toLowerCase()) {
      case "hello":
        output = "Hello There";
        break;

      case "goodbye":
        output = "See you later.";
        break;

      default:
        output = "Sorry I don't understand that.";
        break;
    }

    System.out.println(output);
  }
```

After a *case:* statement you can define as many lines to be executed.  It will run until the *break;* statement is reached.

## Conclusion

This was a lot on conditions, and hopefully you now understand how to make a computer make a choice.   That is all a condition is.   In Java, the *if* statement provides a host of ways to evaluate a boolean result to determine what should be executed.

These are things you'll use heavily in programming, so they are worth getting a strong understanding of.   Also, remember that Eclipse will help in generating and formatting the syntax for you to help guide you in the beginning.