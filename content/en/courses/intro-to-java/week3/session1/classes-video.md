---
title: "Classes - Video"
date: 2021-01-28T10:08:56+09:00
description: Introduction to classes and packages.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/classes-screen.png)](https://youtu.be/9pKcxlhDoqk)

## Slides

{{< revealjs >}}

## Introduction

- Classes are the foundation of Java
- Allow us to create objects
- Foundation for everything
- Focusing on the  details of a class

---

## Class

- Class is a Blue Print
- A blue print tells how to build a house
- A builder uses the blue print to build the house
- They can build as many houses as required
- A class instantiation creates an object
- An object can now exist and hold data

```java
MyFirstClass myClass = new MyFirstClass();

// Alternate way
MyFirstClass myClass = null;

myClass = new MyFirstClass();
```

--

## Class Creation

```java
< scope > class < name > {
   < fields >
   < constructors >
   < methods >
}
```

- Class name has some rules
- IDE will help greatly here
   - Wizard to build the class
   - Creates the file
   - Populates the class details

--

## Eclipse Class Creation

![Eclipse Error In Code](/site/images/intro-to-java//eclispe-new-class-dialog.png)

--

## Eclipse Class Creation

File: MyFirstClass.java
```java
/**
 *
 */
public class MyFirstClass {

}
```

--

## Constructor

- Used to initialize the object
- Called during the new operation
- Method name matches the class name
- Doesn't define a return type

```
< scope > < classname >( < 0..+ parameters > ) {

}

--

## Constructor

```java
public class MyFirstClass {

  public MyFirstClass() {

  }

  public MyFirstClass(int value) {

  }
}
```

--

## Class Structure

- Organization of the class is important
- Highly variable but many standards and guidance exist
- Order
   - Fields
   - Constructor
   - Methods
- Auto formatters can handle this for you

--

## *this* operator

```java
// This code will not work
public class Configuration {

  private int autoSaveRate = -1;

  public void setAutoSaveRate( int autoSaveRate ) {
   autoSaveRate = autoSaveRate;
  }
```

- The above will not work
- Think about autoSaveRate = autoSaveRate
- This is a name conflict
- The *this* operator forces class level reference

--

## *this* operator fixed

```java
// This code will not work
public class Configuration {

  private int autoSaveRate = -1;

  public void setAutoSaveRate( int autoSaveRate ) {
   this.autoSaveRate = autoSaveRate;
  }
```

--

## Overloading

- Used for methods or constructors
- Means to define multiple of the same
- Must vary by parameter
- Return type not included

```java
  public void initialize() {

    dbType = "";
    dbConnStr = "";
    autoSaveRate = 15;
  }

  public void initialize(File f) {
    // Read from file and set values.
  }
```

--

## Static

- *static* is a special keyword
- Placed between the scope and (return) type
- The item is accessible without instantiation

--

## Static Example

```java
public class StaticExample {
  public static String helloString = "Hello World";

  public static void main(String[] args) {
    System.out.println(helloString);
  }

  public static boolean saysHello(String message) {
    return message.toLowerCase().contains("hello");
  }
}
```

--

## toString()

- Allows defining how to convert the object to a string
- Concatenate the data together
- Eclipse can auto generate this for you

--

## equals

- Define what makes two object equal
- Default is same memory address
- true/false result

```java
public boolean equals(Object obj)
```

---

## Method Review

```
< scope > < return type > < name >( < parameters > ) { }
```

- Pass by Reference
- Pass by Value

---

## Packages

- A organization model for objects
- Group related objects together
- Used for package private
- Disambiguate class names
- Always a good idea to use packages

```java
package com.spextreme;
```

--

## Java Packages

|Package|Description|
|-------|-----------|
|java.lang|Core language features|
|java.io|Input/Output objects|
|java.net|Network objects|
|java.math|Math objects|
|java.utils|Utility objects|

--

## Importing Packages

- Must be imported to access a class
- .* to get all class in package
- Recommend to import each class
- Eclipse can automate this
- Object fully qualified name

```java
import < package name >;
```
--

## Import Package Example

```java
package classes;

import java.util.Collection;

public class ImportExample {
  public Collection< String > collection;

}
```

---

## Designing Objects

- Focus on a a goal
- What data does the goal need?
- What actions are taken?

---

## Conclusion

- Where do we go from here?
- Overarching project
   - To start building
   - See how all this fits together

{{< /revealjs >}}
