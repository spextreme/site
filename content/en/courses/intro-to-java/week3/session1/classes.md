---
title: "Classes"
date: 2021-01-28T10:08:56+09:00
description: Introduction to classes and packages.
draft: false
collapsible: false
weight: 15
---

## Introduction

Classes are the foundation of everything in Java.  As discussed during the Object Oriented Programming section, they are what allows us to create objects. In this section, we will go into the details of classes and all the parts that make them as powerful as they are.

There is a lot in this section, but it will be the foundation of everything else, so make sure you get a good handle on this topic.  

## Class

Think of a Class as a blueprint.  A blueprint is used to design what a house will look like when it's constructed.   Once the blueprint is finalized, it can be used to create as many homes as the builder needs.

This is how a class works.  The class is the blueprint, and each instantiation of the class creates a new object.  To create a new object from class you'll use the keyword *new* and it works exactly like any other variable (field) that you need to define.  If you remember, we create a variable with the following syntax.

```
<type> <name> = <value>;
```

To create an object, you'll use the class name as the type, then define the name = new class-constructor.

```java
MyFirstClass myClass = new MyFirstClass();

// Alternate way 
MyFirstClass myClass = null;

myClass = MyFirstClass();
```

The two methods for creating an instance are identical. The *null* keyword is a way to establish a variable that does not yet point to anything in memory.  *null* is used for any object (not primitive types), and if you declare a variable and don't plan to create the new instance right then, make sure you assign *null*.

Now that we know how to create a object from a class, the next step is to understand how to create a class (i.e. The blueprints).

### Class Creation

To understand how to create a class, we need to understand what parts make up a class.  If you remember back to the Object Oriented Programming (OOP) section, a class is made up of [Fields](courses/intro-to-java/week1/session2/content/#fields) and [Methods](courses/intro-to-java/week1/session2/content/#methods).  

To create the core of any class you'll use the following structure.

```
<scope> class <name> {
   <fields>
   <constructors>
   <methods>
}
```

Scope will be one of the Java keywords (*public*, *protected*, *private*) or will be left off to do *package private*.   Typically, it will almost always be public, since the purpose of making a class is so it can be instantiated and used.

The keyword *class* defines that this is a class.

The name can be any string desired but it does have some rules.   First, it can not contain any special characters or spaces.  It may use numbers, letters, and underscores (_) as long as the number isn't first in the class name.

Class names should capitalize the first letter of the name as well as the first letter of each word.  This is a standard defined by Java and must be followed. 

Finally, a class must be contained in a file with the same name as the class name with the .java extension.  This is required, and the compiler will report an error if this isn't followed.

As we discussed, IDEs will help here in many ways.   If you create a new class using Eclipse, it will allow you to enter the name in a dialog and click create.  It will, in turn, create the file with the correct extension, populate the file with the class structure, and any other option chosen will also be set up.  This is where using an IDE will help you.

Lets create our first class definition.

```java
/**
 * 
 */
public class MyFirstClass {

}
```

As this shows, there isn't much to creating a class.  The above was generated with Eclipse using the following options.

![Eclipse Error In Code](/images/intro-to-java/eclispe-new-class-dialog.png)

Now that we have a class, we can start to populating it with our functionality.

### Constructor

Objects have to be created (with the *new* operator), which means there is an initialization step performed during this.   This initialization is done with the classes' defined constructor.  A constructor is a special method defined in the class that will be called when using the *new* operator. A class may contain as many constructors as needed, but they must vary by the method signature and different parameters (See overloading for more details).

When defining a constructor, it is done by using the class name as the method name and then providing 0 or more parameters with no return type.

```
<scope> <classname>( <0..+ parameters> ) { 

}
```

```java
/**
 * My First class.
 */
public class MyFirstClass {

  /**
   * Default constructs this object.
   */
  public MyFirstClass() {

  }

  /**
   * Alternate constructs this object.
   *
   * @param value a value to set on this class during construction.
   */
  public MyFirstClass(int value) {

  }
}
```

As the example shows, this has defined two constructors: one that takes no parameters and one that takes one parameter.  If the class contains zero constructors, then a default constructor is created which will initialize all fields to their default values.  

When making a call to create a new instance of a class, you can call the desired constructor by calling with the aligned parameters.

```java
// This calls the constructor that takes the one parameter.
MyFirstClass class1 = new MyFirstClass(1);

// This calls the constructor that takes no parameters.
MyFirstClass class2 = new MyFirstClass();
```

Overall, the constructor is nothing more than another method with special rules.

### Class Structure

Now that we have a better idea of what a class is, there are some basic concepts that should be understood (Java has a lot of standards and guidance rules).   This is in relationship to how we should organize the fields and methods in our class.   This can be highly variable based on personal preference, employer guidelines, or instructor rules.   However, over time, many recommendations have become the de facto standard.

1. Fields listed first - In a class, the fields should be at the top of the class, above the constructor and all the methods.   Eclipse has an auto-formatter that will handle doing this for you.
2. Constructors - Listed above all other methods and after the fields.
3. Methods - With all the fields at the top of the class, the methods should obviously be after them or the constructors.

There are other practices that again are more personal preference but can help improve the maintainability of the code.   Eclipse has many style formatters available, and there are published style guides available.   Google is one that has produced a well defined style guide that many people leverage, and there are tool configurations to help with the checking and formatting of it.  Using this and being consistent is important, especially with a project or an organization.

### *this* operator

When creating a class, we may define fields that are well named.   Then we create a constructor or a method to take in the value for that field.   We have defined a really good field name, so what happens if we use it as the parameter name also in our constructor and field?  Lets look at an example.

```java
// This code will not work
public class Configuration {

  private int autoSaveRate = -1;
  
  public void setAutoSaveRate( int autoSaveRate ) {
   autoSaveRate = autoSaveRate;
  }
```

The above code will not work as you would think.  The set method for *autoSaveRate* will not set the class field.  This is because the *autoSaveRate* value in the set method exists within the boundary of the method and therefore doesn't have access to the *autoSaveRate* outside of the method.  Think of it like this: the method *setAutoSaveRate* defines its own autoSaveRate and therefore is unable to see past its named item.

Luckily, Java realized this could be a potential issue and created a way to work around it.  It provides a way to access the class level fields from anywhere in the class by giving the class a special name, *this*, meaning this class. When a name conflict occurs, then the *this* operator can be used.  In theory it is also possible to always use the *this* operator to reference any class field or method from within a class, but it is not specifically required in non-conflicting cases.

Now with the *this* operator, we can fix our broken class from above.

```java
public class Configuration {

  private int autoSaveRate = -1;
  
  public void setAutoSaveRate( int autoSaveRate ) {
   this.autoSaveRate = autoSaveRate;
  }
```

That simple keyword is now causing the set method to say this configuration class field autoSaveRate equals the local methods autoSaveRate.

Also note that Eclipse will automatically use it for setter classes it generates.   You'll need to realize when you have a conflict and set it in all other cases.

### Overloading

Overloading a constructor or method is a concept of defining the method more then once.  The condition is that the parameters must be unique.  By overloading a method or constructor, it allows you to create different ways to use the object's method. 

For example, lets say we have a *Configuration* class that holds configuration options for our application.  We may have a way to initialize a default instance of the *Configuration* class, where all the values are set to a good base value.  A second initialization method may exist that takes in a file path which will read the contents and set the configuration values. Finally, there is a third method that allows passing in the set of values to set each one through one call.  This way, our application may use the initialize during first load (never been used before), then it could use the file for all future launches of the application.  Finally when the users use our application to edit the configuration, this can call the method to just pass all values to use and then save it.

Below is an example of the overloaded methods for this.  As mentioned, they all have the same name, but the parameters are different.  Also note that the return time can be unique or the same for each call, but it has no bearing on the uniqueness of the method.

```java
  public void initialize() {

    dbType = "";
    dbConnStr = "";
    autoSaveRate = 15;
  }

  public void initialize(File f) {
    // Read from file and set values.
  }
```

### Static 

*static* is a special keyword that, when applied to a method or field of a class, will exist independent of the instantiation.  This means the method or field can be called without creating an instance of the class.  This is done to allow access to a method or field that is not dependent on the class's data.  For example the Java Math object doesn't need to know anything to perform a calculation for square root.  The Math square root signature is *Math.sqrt( double a )*.

Marking something static allows it to be created once in the JVM and held no matter how many times the backing class is instantiated.  This is beneficial when you want to provide a method that isn't reliant on an instantiation or data in the class.   In fact, we've used static before, in the *main* method that is created as part of a class to allow the JVM to run our application, before it knows which class to instantiate.  Another reason to use static methods or fields is to allow it to be use without creating an instance of the class.   

To use the *static* keyword, it is added between the scope and type for a field, or between the scope and return type for a method.

```java
/**
 * A class that uses all static fields/methods for examples.
 */
public class StaticExample {
  /**
   * A static string to print out in the main method.
   */
  public static String helloString = "Hello World";

  /**
   * JVM launch point to print hello world.
   *
   * @param args The command line args. Not used.
   */
  public static void main(String[] args) {
    System.out.println(helloString);
  }

  /**
   * A simple utility method that see if the word hello appears in the string.
   *
   * @param message The message to check.
   * @return <code>true</code> if this has the word hello in the message.
   */
  public static boolean saysHello(String message) {
    return message.toLowerCase().contains("hello");
  }
}
```

This is a complete example showing the main method, a field, and another method that are all static.

### toString

When we discussed the String object, we made reference to the fact that all objects can be turned into a String.  If an Object doesn't define how to output itself as a string, then the object's package + name and memory address is used.

Therefore, when creating a class, the toString() method may be defined.   This method allows the developer to define the details of what this object looks like when being turned into a String.  In many cases, the Eclipse IDE can automatically generate the *toString* method for you, and then it will concatenate a string with the class name and all the field values. 

In other cases, you can define what the string should look like to represent this object.  For example, a simple rectangle class toString method may just output the x and y position and the width and height value along with the area. 

```java
public String toString() {

 return String.format("x: %d, y: %d, w: %d, h: %d, a: %d", 
   x, y, height, width, 
   height * width);
}
```

### Equals

Equality between two objects is a very interesting concept.   What should it mean to make two objects equal?  For our Rectangle example, if they have the same x and y and width and height then they probably are identical.   

All objects in Java have the ability to create an *equals* method.  By default, all objects are equal if they are the same object in memory (i.e. they point to the same memory address).  

To define an equals method, the signature looks like this.

```java
public boolean equals(Object obj)
```

It is then the goal to define what equality means by doing comparisons and returning *true* or *false*.  The nice part is Eclipse can generate this by providing a list of fields to choose what means equal, and it will generate all of the comparison and result for you.

```java
public boolean equals(Object obj) {
 if (this == obj) {
   return true;
 }
 if (obj == null) {
   return false;
 }
 if (getClass() != obj.getClass()) {
   return false;
 }
 final Rectangle other = (Rectangle) obj;
 if (height != other.height) {
   return false;
 }
 if (width != other.width) {
   return false;
 }
 if (x != other.x) {
   return false;
 }
 if (y != other.y) {
   return false;
 }
 return true;
}
```

As this shows, it checks if the same object is passed in. Then it makes sure they are both Rectangle objects.   Finally, it checks each value, and, if everything is equal, it then determines the objects are equal.  

## Method Review

In this section, we discussed a lot about classes and the fields and methods that make them up. Before this, however, we glossed over methods and some of the details to keep things simplistic.   Now that we have a better understanding of classes, you may be wondering about passing and returning objects to and from methods.

As we discussed, a method takes the form

```
<scope> <return type> <name>( <parameters> ) {

}
```
What may not have been completely obvious is that the return type and parameters can be objects.   But what does that really mean to the caller and user of these objects?

When you pass in an object to a method, that object is said to be passed by reference.   This means that the object that was created outside of the method is handed to the method by pointing to the same spot in memory where the object was first created (not a copy).   This means that, if the method makes changes to the object, then it's affecting the instance that existed from the caller.

The reverse concept is pass by value, which means a copy of the object is passed to the method so that any updates to the data in the object would be isolated to the copy and not affect the original one from the caller.  

In Java, this is not the case, as everything is passed by reference.  If you need to achieve a pass by value, then a new object with all the same data (a copy) would need to be created and passed.  This is rarely done, however.

As mentioned, methods may also return something from the method.   An object can be passed back, and again, it is passed back by reference.

## Packages

Packages are designed to allow grouping of related classes.   It is mostly an organizational concept but does offer some features to the language.  As discussed in the section on Scope, package private does make use of the package concept.

To create a package you'll use the keyword *package* and then define the name of the package

```
package <name>;
```

Packages are also useful when you have to classes that are the same name.   In this case, if they are in different packages, the package path + name makes it unique.

It is always good to define a package.  Also, the Java standard recommends using the reverse of your domain name.  For example, *spextreme.com* becomes *completeness* for the package name.  From there, you can define any other package groups you need.

```java
package com.spextreme;
```

Java has defined packages for everything in their framework.   They all start with *java.* as the root.  For example *String* is in *java.lang*.  Java has a very large collection of objects and packages.  Here are a few of the most used:

|Package|Description|
|-------|-----------|
|java.lang|Core language features. This is where String and System come from.|
|java.io|Input/Output objects.|
|java.net|Network objects.|
|java.math|Math objects.|
|java.utils|Utility objects.|

### Importing Packages

Packages are great, but now that classes are stored in them, you'll need a way to tell your class how to access the package.  This is done using the keyword *import*.

```java
import <package name>;
```

For example if we want to bring in the *Collection* object from the *java.util* package we'll need to define the package at the head of our file using the import.

```java
package classes;

import java.util.Collection;

public class ImportExample {
  public Collection<String> collection;

}
```

It is possible to use a wild card (\*) to import all classes from a package. This way, you don't have to call out each class you want to import.

```java
package classes;

import java.util.*;

public class ImportExample {
  public Collection<String> collection;

}
```

In general, your IDE will automatically import for you and do it by named class.  This is the recommended method, and it's a bad practice to use wildcard (.*) for importing.

One last note is that you can avoid doing imports by fully qualifying the class name when used.   

```java
package classes;

public class ImportExample {
  public java.util.Collection<String> collection;

}
```

This is usually only used when you have a class name conflict and you need both in your file.  


## Designing Objects

Now that we understand packages and classes, the next big question is, where do we begin?  When you start programming, you won't be thinking much about designing an application.  The examples and work done so far have been mostly isolated, simple exercises to get a basic understanding of the language.

The thing to focus on when creating an object is to make sure it is self contained and focused on achieving one goal.  Putting to much into one object is a bad practice and will increase its complexity, making it harder to maintain.   The data (fields) and methods should be very focused on servicing the defined goal.

If we begin with the goal of an object, then we can think about what type of data is needed to service the goal.  Then, we can think about what actions can be taken to service the goal and act on the data.   Once we have these pieces figured out, we can start to turn them into our classes, fields and methods.

As we progress through the course, we'll start to build up an overarching application and all the objects that make it up.   This will give us a working example to run through and see how all this is put together.
