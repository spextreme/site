---
title: "Networking - Video"
date: 2021-03-06
description: Java network programming.
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/networking-screen.png)](https://youtu.be/rV3WhbYQeUQ)

## Slides

{{< revealjs >}}

## Networking

- Computers almost always supported networks
- Birth of the Internet changed it all
- Computers now can barely be used without a network
   - Shared resources, communication, music, video, etc...

---

## Network Programming

- Similar to working with Files
- Streams for communications
- Difference in how to access
  - Files: path starting from root (/)
  - Network: Protocol, IP, Port, Context
   
-- 

## Network Address

- Internet Protocol - IP
- Set of 4 numbers separated by dot (.)
  - 0 to 255
  - localhost - 127.0.0.1
  - google.com - 172.217.6.238
- Domain Name System
  - Named mapping to IP

--

## Ports

- A unique id on a server
- Severs bind to listen for messages
- A number from 0 to 65353
  - 0 to 1023 reserved for well known ports
  - SMTP (email) - 25
  - http (web) - 80
- Combined with the IP makes a full address

--

## Protocol

- Communications nothing more then a bag of bytes
- Need a method to standardize communications
- Defines how to communicate
  - http:// - Web sever access
  - file:// - File system access
   
--

## Context

- Represents the path after the address
- Contains details for the server
- Used by the server to know what to do
  - Path to a page
  - Parameters to process

---

## URL

- Universal Resource Locator
- Network communications model
- Combines the protocol, IP, port, and context

```
< protocol >://< server id >:< port >/< context >
```

---

# Java Network Programming

- Java Framework prebuilt objects
- *java.net*

-- 

# Java Objects

- DatagramSocket
- DatagramPacket
- MulticastSocket
- ServerSocket
- URL 
- HttpURLConnection

---

## Network Server Example

```java
final DatagramSocket socket = new DatagramSocket(12345);
final byte[] buffer = new byte[256];
final DatagramPacket packet = new DatagramPacket(buffer, 
                                             buffer.length);
try {

   System.out.println("Server: Waiting for message...");
   socket.receive(packet);

   System.out.println("Server: Message Content: " 
                        + new String(packet.getData()));
} catch (final IOException e) {

   System.out.println("Server: Had an IO error: " 
                                    + e.getMessage());
}
```
---

## Conclusion

- Only a starter
- Lot to network programming
- Java simplifies the process


{{< /revealjs >}}