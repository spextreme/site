---
title: "Networking"
date: 2021-03-06
description: Java network programming.
draft: false
collapsible: false
weight: 15
---

## Introduction

In the early days of computers, they used to sit on a desk and be used by the user to do standalone things like word processing, video editing, and music creation.  Today, however, everything is largely based on the Internet, and now computers are pretty useless without connecting over a network to other systems.  The Internet largely changed this model to focus on networking of systems.

Computers have always been able to connect over a network, it is used more globally now with the Internet.   Networked computers can work collectively, share data, and perform tasks.   Users use tools to communicate over the network like texting, email, streaming videos or music, file sharing, and much more.

Without network programming, this couldn't be archived, and Java has a collection of objects to support working over the network.

## Network Programming

Programming to communicate over a network is largely similar to working with a file.   The communications are performed using byte data and streams.   To make a request over the network to another computer, it simply opens a connection and writes out using an output stream.   The response comes back and, that is then read with an input stream.   If you need a refresher, look at [Streams](/courses/intro-to-java/week7/session1/files/#stream/intro-to-java/week7/session1/files/#stream) under the [File](/courses/intro-to-java/week7/session1/files/) session.

The main difference between working with the file system and a network is mainly how a connection is made.  The file system is based on a path to where the file or directory resides, starting from root (*/*).  A network connection requires an protocol, network address, port, and context.

### Network Address
A network begins with a network address, which is called an Internet Protocol (IP) address.   This is a sequence of numbers (0-255) separated by dots which uniquely identify everything on a network.   *127.0.	0.1*  for example, is your computer's localhost address. *google.com* resolved (at the time of this writing) *172.217.6.238*.    These numbers are used by the computer to identify a remote computer somewhere on the network.  These can be very difficult to remember, and this is where domain names were born to simplify the address.  For example, *google.com* is much easier to remember then *172.217.6.238*.   When a connection request is made to *google.com*, it goes and asks for the IP address from a domain name system (DNS) which provides the computer with the IP to make the connection.

### Ports

The next part on network programming to understand is the port that the connection will be made to.   In a computer, if it could only have one connection at a time on its IP, it would be largely unusable.    Ports are an integer from 0 to 65353 with the lower ranges 0 to 1023 reserved for well known ports used by different protocols.  For example, the email protocol (Simple Mail Transfer Protocol - SMTP) is set to 25 while the HTTP (Hypertext Transfer Protocol) is 80.  By using ports, the computer can open up and hold a port to wait for a connection.   When making a request to connect, the port is used with the IP to know what specific computer to hit and where they are listening for a request.

### Protocol

With network communications being nothing more then a collection of bytes (bag of bytes), standards have formed around this communications type to help improve network services.   It would be similar to if everyone spoke a different language; you wouldn't have anyone understanding anyone else.   This is the same for network communications.   The protocol model was created to standardize these communication models.  For example, HTTP is a standard protocol that all web servers have the ability to talk over, and a web browser will interpret them.   This is how the web works to give you access to all the web pages you view.

The protocol can be used to inform the server how to handle something, or it can be used to improve the routing of a request.

### Context

The context refers to the path after the network connection details.   The context can be thought of like the path to a file with the first slash being the root of the file system. In a network connection, it is the path to follow after the connection is made.  For example, when you connect to a web server, the context path is the path to the web page you want.   For example, to view this page, you access it at the path */site/courses/intro-to-java/week8/session1/networking/*.  This is how the web server knows the page you're looking for.

## URL

Universal Resource Locator (URL) is a network address model that is a standard model for how computers communicates and has grown into many other uses around network programming.  The URL is put together with the protocol, IP, port, and context in a standard format.

```
<protocol>://<server id>:<port>/<context>
```

As this shows, it puts together all the parts and makes it a standard that many things can use to understand what we are talking to (protocol), where it is (IP and port>, and where to find it within that server.


# Java Network Programming

With Java, they have built many objects around network programming and put them in a package called *java.net*.   Some useful network objects in this package are:

- DatagramSocket - Used to bind to a port on a machine to wait for a message.
- DatagramPacket - A bag of bytes to be sent to a server port.
- MulticastSocket - Used for multicast message listening.
- ServerSocket - Waits for a message to be received on a port.
- URL - The Universal Resource Locator implementation.
- HttpURLConnection - An abstract class for other services to extend for web(HTTP) connections.

There is a lot to network programming but this is just to scratch the surface.

## Network Server Example

When building a server to listen for messages on a network, you'll have to build a server that can bind to a port and wait for incoming messages.   This is known as a server process, which, when launched, will bind to the defined port and wait for a message to come in.  When a message hits that port on the server, the service will begin processing that bag of bytes.  In Java, setting up such a server is very easy with the DatagramSocket.

```java
final DatagramSocket socket = new DatagramSocket(12345);
final byte[] buffer = new byte[256];
final DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

try {

   System.out.println("Server: Waiting for message...");
   socket.receive(packet);

   System.out.println("Server: Message Content: " + new String(packet.getData()));
} catch (final IOException e) {

   System.out.println("Server: Had an IO error: " + e.getMessage());
}
```

This sets up a DatagramSocket on port 12345 and then waits for a message to be received.   If the message comes in, it will put the data into the DatagramPacket which holds an array of bytes.  We have allocated 256 bytes for this.  If more data comes in, that will be an issue, but for this example it's good enough.

## Conclusion

There is a lot to programming and creating good quality software.  This is true for making advanced network-based applications, but with Java, it does make it easier with the collection of prebuilt objects to support it.   This only started to introduce basic network programming.  As you get more advanced, there are many more objects and libraries available to help create complex network services.