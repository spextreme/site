---
title: "Java Refresher - Video"
date: 2021-03-12
description: Take a step back and relearn everything
draft: false
collapsible: false
weight: 10
---

## Video

[![Watch the video](/images/intro-to-java/videos/java-refresher-screen.png)](https://youtu.be/MehiXgh4ce4)

## Slides

{{< revealjs >}}

## Introduction

- Half way through
- Covered all the basics of the language
- Touched on the Java Framework
- Let's take a step back

---

## Things to Remember

- Basic rules of the language
- Language must abide by the rules 
- Provide a way to convert the language to machine code
   
---

## JVM

- *java* command line
- Runs the compiled .class byte code
- Virtual every Operation System has a JVM

Remember: **JVM = java = platform independence**

---

## Source Lines End with ;

- A source line is a single statement
  - Can span multiple lines
  - Can have multiple on a line

Remember: **Semicolon on every source statement**

---

## Primitive types

- int
- long
- short
- float 
- double
- byte
- boolean
- char

Remember: **int & boolean, enough for 90% of use cases**

---

## Variable definition
  
- variables, fields same thing
- *type name = value;*
- *type* may be any primitive or object
- *name* can be any string
- Scope may be applied first for fields
   - public
   - private
   - protected
   - default

Remember: **type name = value;**

---

## Arrays use []

- [] used for arrays

```java
// Create an int array of size 3
int [] myArray = new int[3];

// Create an int array size 3 with data populating each item.
int [] myArray = {1, 2, 3};

// Adding an item to index 0 and setting the value to 1
myArray[0] = 1;

// Get the value at index 1
int value = myArray[1];
```

Remember: **Arrays use []**

---

## Instantiating a Class

- Classes created with the *class* keyword
- *new* is used to instantiate a class

Remember: **class name = new class();**

---

## Call a method

- Methods are on a class
- Call with the dot *.* character
   - variable dot method(params)
- Parameters passed in the (...)

Remember: **name.method();**

---

## Static means call without instantiating

- *static* keyword
- call a class method without *new*

Remember: **static means call without instantiating**

---

## Main Starts It All

- *main* is the entry point into the application
- First thing called by the JVM
- A static method

Remember: **public static void main(String[] args)**

---

## Java Framework

- The collection of prebuilt objects
- Very large
- Use Google to find what you need
- Remember String and Object

Remember: **Java Framework, *String*, *Object* and Google for the rest**

---

## Conclusion

- A lot to java
- Remember the basics
- The rest will come later

{{< /revealjs >}}