---
title: "Java Refresher"
date: 2021-03-05
description: Take a step back and relearn everything
draft: false
collapsible: false
weight: 15
---

## Introduction

We're a little more then halfway through the course.  At this point, we've covered all the basics, so we'll take a step back, reflect, and start over.   This way, we can learn the foundational stuff again, but this time, since it's already been seen, it should become clearer.

## Things to Remember

In Java, there are basic rules to the language.   Just like in the English language, there are rules for writing a paragraph.   Java has its own rules that make up the language.  Remember that the language has to abide by these rules because the computer has to be able to compile the source into something the machine will understand (byte code).   Understanding the syntax rules will help to archive that.

### JVM

The Java Virtual Machine (*java*) will read in and run your application.  It reads in the .class files which are the byte code generated from compiling *.java files.   There is a JVM for virtually every environment today which is how it becomes platform independent.

Remember: **JVM = java = platform independence**

### Source Lines End with ;

All source lines should be terminated with a semicolon (*;*).   This doesn't mean every line, it only means source statements. A complete source statement is something like a variable definition, a object construction, or a function call.    On a for loop there may be multiple semicolons because it has the syntax of a variable init and a condition check combined in one.   Since it is made up of multiple statements, that is why there are multiple semicolons.   

Remember: **Semicolon on every source statement**


### Primitive types

Primitive types are the only things not object-based in Java.   There are 8 of them.

- int
- long
- short
- float 
- double
- byte
- boolean
- char

Remember: **int & boolean, you know enough for 90% of use cases**


### Variable definition
  
Variables or fields are defined with the syntax of *type name = value;*   The type is any Java object or primitive type.    The name can be any string you desire, so long as it doesn't contain spaces or special characters.  The only exception here is that the underscore character may be used.  Numbers are also allowed, as long as they are not the first character in the name.

Scope may also be applied when defining a field in a class.   This goes before the type to define who may access it from outside of the class.

Remember: **type name = value;**


### Arrays use []

When you need to use an array of items, you use the square brackets for most of it.   They are used in the creation, assigning values, getting values out, and defining it as an array.  

```java
// Create an int array of size 3
int [] myArray = new int[3];

// Create an int array size 3 with data populating each item.
int [] myArray = {1, 2, 3};

// Adding an item to index 0 and setting the value to 1
myArray[0] = 1;

// Get the value at index 1
int value = myArray[1];
```

Remember: **Arrays use []**


### Instantiating a Class

A class is any Java object that has been created using the *class* keyword.   When you want a new object from a given class, you instantiate it with the *new* keyword.  Think of it like asking for a drink at a restaurant.  You ask for a water and they bring you a new glass with water in it.   This is what you are doing, asking Java for a new object.

Remember: **class name = new class(); (where class is the type name you are trying to created).**


### Call a method

Once you have an instance of an object with the *new* construction, you can call any method publicly available on that class by using a dot (*.*) on the variable name.   After the dot, the method name follows.  In Eclipse, this is where the context helps provide support.  Once you have the method, you can pass in any variables between the *(...)*.  To pass zero parameters, simply using the *()* is good.

Remember: **name.method();**


### Static means call without instantiating

When the keyword *static* is put in front of a method, it means that method may be called on a class without creating an instance (instantiating) of the class.

Remember: **static means call without instantiating**


### Main Starts It All

The *main()* method is what first gets called when you launch your application.   It is a static method that the JVM will call first and execute whatever is inside.

Remember: **public static void main(String[] args)**


### Java Framework

The programming language of Java is only a small part of the language.   The biggest part is that Java has built a framework with thousands of objects available for use.   You don't need to remember all of them and can Google when you need to find one that will do what you need.   The main ones to remember are *String* and *Object*.

Remember: **Java Framework, *String*, *Object* and Google for the rest**
