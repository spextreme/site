+++
author = "Steve"
title = "Meeting the experts"
draft=true
date = "2020-02-29"
# description = ""
tags = [
    "diabetes",
]
+++

In the beginning, I got a lot of advice from people that I spoke to and mentioned I just found out I'm a diabetic.  Most would say I don't fit the category because I'm in shape and very active.   People think of Type 2 diabetes when they hear it, which is usually brought on by diet and lack of exercise.  For me, that wasn't the case and I'm not the normal diabetes patient.

In the conversations I had with people, everyone seems to know someone with diabetes or is themselves a diabetic, and they all have advice and recommendation. I don't mind this as I really don't know what I'm doing so any thoughts are welcome at this point.

What I was greatly lacking was an expert that can help me better understand what I should be doing.   When I was first diagnosed I was told I needed to meet with a Diabetes Nurse and a Nutritionist but I couldn't get an appointment for over a month.   Finally I got lucky and they fit me in so I had that appointment this week.  

I meet my nurse who was very helpful and gave me a lot of information.   We talked about many details and she gave me a lot of the ins and outs to be prepared.   This includes things about future problems (feet, blindness, heart disease, etc...) and the best ways to handle low blood sugar.  It was really nice to finally get some of these details and a person I can reach out to.

Right after that appointment I met with the nutritionist.  This was funny because she was talking about all these food groups, carb (carbohydrate) counting and balance meals.   I had to interject and say that this is nothing I've ever worried about so you're going to have to start at the beginning.   I was always in shape and never cared about any of this type of stuff, so it wasn't something I tried to understand.  She helped by going through the basics (what food has protein?) and how to count carbs.   Overall this was great and I finally had a plan.  The nutritionist laid out the daily meal plan which included 3 main meals, and 3 snacks.    It broke down the amount of carbs for each of those with a total of 240g of carbs per day.  This was to remain at my current weight and manage my condition.  At the end of this I finally felt like I was starting to get some direction.