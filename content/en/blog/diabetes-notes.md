+++
author = "Steve"
title = "Daibetes Notes"
draft=true
date = "2020-03-14"
# description = ""
tags = [
    "diabetes",
]
+++


The rest of the week I found that in my old diet, hitting the 240g a day was easy.  However, now I was not able to break 190g of carbs.  Before this my diet was pretty simple. It included Orange Juice, Cereal, Pepsi, Apples, PB and Jelly, a balanced dinner and ice cream.    Now as a diabetic I was told I shouldn't drink my carbs so that took out the Orange Jice, Milk, and Pepsi.   Cereals are loaded with sugar, so those are out too.   When you take all that stuff out, the carbs were cut almost in half to about 150g.

Now the issue I was having is that I would constantly hit a low (< 70 mg/dl) because of basic things like walking up the stairs, vaccuming, or trying to exercise.  This had me panicked cause I am physically active a lot and to do basic things I had to go eat a bunch of carbs.  I felt like I was forcing myself to eat more then I normally would.   

I was back to feeling lost and uncertain of how this was going to work. I don't curently see a path where I can play tennis, work in the yard or even go for a walk with my wife.    Something will have to change but what and how?



This means, that in me, when I take insulin my blood glucose drops. This is fine when I am in the high 100s mg/dl (milligrams per deciliter) but then as I continue to drop it can get very unsettling.   When I go below 70 mg/dl I start to shake and tremble.  If I go below 60 mg/dl then I start to get light headed and woozy.  That isn't a feeling I ever want to hit.

So over the last couple of weeks I have been following the doctor's order and injecting insulin (Bolis 1u) before each main meal of the day.  I end up trying to eat a normal meal but after about an hour and half I drop below 80 mg/dl.  I then eat more to compensate and get me back up.

This roller coaster is the part I'm trying to get under control.   It includes watching the Dexcom and eating more then I normally do to keep my levels up.  This is not a fun ride, and I'm ready to get off.




When the diagnosis was first given, I was given a CGM (Continuous Glucose Monitor), the FreeStyle Libre, placed on 10 units of Basal insulin taking it once a night and 4 units of Bolis insulin when I started eating a meal.   About a week and a half later, I was switched (by my insurance company) to the Dexcom G6.

Now that I'm able to see my levels and have insulin to help drive me down, it's time to begin figuring out how to manage myself.   I used the app (yes CGMs can hook into your phone) to track my level and track the carb intake and insulin doses.   After a few days, I kept going low (below 70 mg/dl).  I then spoke to the doctor who lowered my meal time insulin (Bolis) to 1 unit.

This helped but I still kept going low.  At this point I was guessing and asking the doctor for guidance.   Every day I feel like I was eating more than I normally would to just keep my levels up.  Finally the doctor told me to stop taking meal time insulin and lower the night time to 8 units.

This helped but still I feel like I'm guessing and not making any progress.   I guess time will tell but I feel like I've got a long way to go.




Date
February  26 meet with nurse and nutritionist
March 18 - Good Doctors apt with stop all insulin
March 19 - Insulin fast acting cause at 208....but after droped me way low.
		No insulin after and have been fairly stable.

Dexcom CGM
The Dexcom G6 Continuous Glucose Monitor (CGM) is a implanted sensor with a transmitter and a display device.  The Dexcom will check the glucose levels every 5 minutes and reports it to the display device.

I was able to connect the Dexcom sensor and trasmitter without to much trouble but found that its really not accurate the first day.