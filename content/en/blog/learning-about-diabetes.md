+++
author = "Steve"
title = "Learning about Diabetes"
draft=true
date = "2020-02-25"
# description = ""
tags = [
    "diabetes",
]
+++

As my journey continues, I have learned very little about how to deal with being a diabetic.   I've spent a lot of time watching my FreeLibre Continuous Glucose Monitor (CGM), trying to learn what I can eat, and wondering how I'm going to live with this disease.

What I've learned so far is that I fall into the category of Type 1.5 Diabetes or LADA (Latent Autoimmune Diabetes in Adults).  This is a form of Diabetes where your immune system attacks the beta cells in the pancreas that produce insulin.  Therefore, when eating carbohydrates (carbs), which your body turns into glucose that goes into your blood stream, your body doesn't have enough insulin to pull the glucose into the cells for energy.  This is why my glucose levels were so high when the test was taken a couple weeks ago.

Now that I understand what I have, managing it becomes the next big challenge so that I can begin to get back to a normal life.   Unfortunately that isn't as easy to solve.  

The primary issue is that humans are made up of a complex chemical balance and how they work is individual and complex.   How the body reacts to the intake of food varies in everyone.  What this means for a new diabetic is that it's a long-term science experiment with many variables and factors.   

For me, this now means I'm counting the carbs I put in, taking insulin before I eat a main meal, and taking insulin before going to bed.  There are two types of insulin that are used.  There is Basal, which is a long acting insulin used to stabilize diabetics throughout the day.  Bolis, a mealtime insulin, acts quickly and lasts about 4 hours.

Watching the CGM monitor, my levels go up and down.  I have no idea what normal should look like and I haven't found much online of what a non-diabetic's levels should be.   The spikes that occur after a meal and then the drops are expected, I think, but is it too high, too sharp?   All of this is really unclear and unique.

The last thing I have learned is that this slow learning, along with the trial and error, are part of the process.   So are the monitoring, seeing how the body reacts to the insulin, and the carb counting.   Only time will tell, but it's all part of the slow learning process to understanding yourself with Diabetes.
