+++
author = "Steve"
title = "Life Changes in an Instant"
draft=true
date = "2020-02-22"
# description = ""
tags = [
    "diabetes",
]
+++

This is where it all starts.  I'm not sure what I'll write about or how this will go.   But since life is an endless journey of experiences, and my life drastically changed in February, maybe this is where to begin.

To open, we'll go back to January.   Life was good, I was headed into two planned ski vacations and coming off of a 3 week Christmas break.   I went back to work for the first week after break.  The next Monday, driving home I noticed all the road signs were really blurry and I couldn't read them.  This seemed odd, but I didn't think much of it.   I thought being 45 my eyes were just starting to get old.   I promptly made an eye doctor appointment but I had to wait a week because I was going to Gore Mountain to ski with my brother and cousin.

After the Gore Trip (awesome vacation), I went to the eye doctor.   There was nothing much to report but the doctor casually asked if I was a diabetic, to which I replied 'no'.  So he decided to run some more tests at a follow up appointment scheduled for 2 weeks later.   I finished out the week and was ready for ski trip number 2 to Camelback mountain with my brother and both our families (another great trip). However during this trip my wife recommended to be safe and ask my doctor to order a blood test for when I get home.   I said sure, thinking there was no way I could be a diabetic.

Finally when I was back from all my trips, I got my blood work taken the next day.   The following Monday it was time to go back to work after all my vacations (I love ski season).   This was also the day I had my eye follow up appointment.   While I was sitting in the waiting room, I got an email that my blood work results were in, so I quickly took a look.  The results were that I had a 244 glucose level after fasting, and there was a message saying a normal fasting level should be less then 125.   There was also a message saying "You may have diabetes, contact your doctor to discuss".   Not what I was expecting, but then the eye doctor was ready for me.   I talked with him and mentioned the results.  We decided to not do much more until I could talk with my primary doctor.

I called and scheduled an appointment with my primary physician, which was now planned for Thursday. I was anxiously waiting to find out what this really meant.  Could this be wrong? Is there another explanation?

Something to know about my primary doctor is he is very even keel and more matter of fact type.  So on Wednesday when I got home I had 4 missed calls, 2 voicemails and 1 email from him.  I was now in a panic and I called the office.  He was busy and the nurse said he'd call me back.   About 20 minutes later, I got a call back and the conversation was not what I was expecting.

He informed me I needed to see an endocrinologist before the end of the week.  He couldn't understand how only 9 months ago my levels were perfect (98 after fasting) and now they were like this.   From his review of the results, he believed 3 months ago my body stopped producing insulin.   I was in shock as I hung up the phone, not sure what to think.

The next morning, their office called to tell me they got me into the endocrinologist on Friday and they canceled my Thursday appointment since it was no longer needed.  Now more waiting and still lots of questions.

Finally, Friday was here and I was praying they were going to tell me it could be wrong, lets perform more tests.  I was called back and met with the nurse practitioner.   They began talking about how to manage Type I Diabetes and started describing how this would work.  I asked if there was any way this could be wrong, but they informed me that my A1C levels showed that over the last 3 months my body no longer was providing enough insulin.   They showed me how to do a finger stick and checked my level (240mg/dl in the office).  They got me setup with my first overnight insulin injection and I did the shot myself.  

With Type 1 Diabetes (T1D), it can just spontaneously occur.   Environment or genetics, it really isn't fully known what triggers the immune system to change.  With T1D, the immune system attacks the insulin the pancreas is producing, killing off the cells and insulin.  

Leaving the office, I had to head to Rowan to teach my class.  I was completely in shock, not really ready to think about this or understanding what it meant.  I Still had lots of questions and really didn't know where to go next.  So I ignored it, went to class, then played soccer and finally got home late from a life changing day.   Saturday is another day and we'll see where this leads me.