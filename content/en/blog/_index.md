---
title: Blog
description: Blogged content
blogHeaderType: img
header:
  - type: text
    height: 200
    paddingX: 50
    align: center
    title:
      - Blog
    subtitle: 
      - adsf
    titleColor: 
    titleFontSize: 44
    subtitleColor: 
    subtitleFontSize: 20
    spaceBetweenTitleSubtitle: 16
  
  - type: img
    imageSrc: images/background.jpg
    imageSize: cover
    imageRepeat: no-repeat
    imagePosition: center
    height: 280
    paddingX: 50
    paddingY: 0
    align: center
    title:
      - Paulin's Mind
    subtitle:
      - Where I often get lost
    titleColor: 
    titleShadow: true
    titleFontSize: 44
    subtitleColor:
    subtitleFontSize: 16
    spaceBetweenTitleSubtitle: 20
---
