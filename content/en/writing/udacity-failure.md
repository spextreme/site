---
title: "How Udacity Failed Me"
description: "Details on how Udacity didn't meet my learning expectations"
date: 2021-06-21
draft: false
enableToc: false
---

After a few months in the Udacity AWS Cloud Developer Nano-Degree program, I have decided to withdraw from the program because of its lack of good learning, poor setup, and frustration with many parts of Udacity. My company was paying for this program, this write-up is to fully document the challenges with the program, so my organization may use it to work with Udacity to hopefully improve the program for others.   I'm not sure what will come of it, but know that I am withdrawing 2 project shy of completion, which was a difficult decision for me.

To understand more about what occurred, I'll begin with a brief introduction of myself and what I was expecting from this program.  I have been a software developer for a few decades and I am constantly learning new technology.  I have recently been heavily involved in Microservice development, cloud architecture, and moving large legacy products to Microservices.   I am also a adjunct professor at Rowan University, teaching in the computer science department. 

My organization offered the opportunity to be in the pilot program with Udacity as a way to grow our business expertise in a multitude of technologies.  I was lucky enough to be selected to be part of the first group to go through.  I was excited because I love learning, growing my skills and it would give me a better hands on understanding of AWS.   As stated this is not how it turned out.

The overall structure of Udacity is setup as a web site that you are logged into and you have access to your program.   The program guides you through a set of different parts, with projects for each part.  In the AWS Cloud Developer there were 6 parts with 4 guided projects and one Capstone project.  You can work at your own pace, but its roughly about 1 part due each month. Once we were approved and the program started, I was ready to go and that first week I jumped right in cutting through part 1 and 2 to get to the project the first weekend.

## Project 1 - Deploy Static Website on AWS

On the first day, I got started by watching all of the videos in part 1, "Welcome to the Cloud Developer Nano-Degree" and part 2, "Cloud Fundamentals", which was 136 videos.  I took the top learner in my organization that week.  My initial impression was that the videos were well done and the speakers have a good understanding of how to use AWS. 

Now that I got through all the videos, I jumped right into the first project.  The goal of this project was to create an S3 bucket to host a website and use CloudFront to make it available on the web.  Udacity provided the web site content so that was nice since it allowed us to focus on the AWS side of things.  Also included with the projects was a description, and a detailed Rubric that must be followed.  This is what the reviewers use to asses the project submission against.

Since I am an automation junkie, I decided to work toward an automation approach using the command line tools instead of the web interface.  This way I could learn a little more since the videos largely focused on the AWS console.  I ended up using Cloud Formation which was only mentioned but no details were provided. I search Google and worked toward this solution and was able to make it fully automated by creating a single file that is passed to Cloud Formation and it sets up the S3 bucket, the permissions (IAM) and other configuration needed. The only other step was to use the command line tool (*aws*) to upload the website and I was done project 1.

I then hopped to the rubric and created all the required screenshots (13 in total) and a README.md file that detailed what I did,  the commands used, description of each of the screenshots, and the S3 and CloudFront links required for the project.  I put all this together, including a log of the Cloud Formation run to show how I went over and above to do this project. This was submitted to the Udacity Project 1 website and I was anxiously awaited the feedback.

The next day I got a response on my project submission, stating I didn't complete it properly.  This threw me because I hit everything I could on the rubric with a lot of extra stuff. I logged into Udacity and reviewed the comments.  They stated I missed three rubric items.

- Rubric: The S3 bucket is configured to support static website hosting. 
  > "This requirement is missing"
- Rubric: The bucket should allow public access. The S3 bucket has an IAM bucket policy that makes the bucket contents publicly accessible. 
  > "This requirement is missing"
- Rubric: The website should be accessible to anyone on the Internet via a web browser. The student should have provided the CloudFront domain name URL and website-endpoint URL for the website. 
  > "You must allow the website access through CloudFront only."

This confused me since my S3 bucket was setup with public access, so I wasn't clear on what could have been wrong.   On the third one, I figured they didn't want me to included the S3 bucket URL so I removed it from the README.md and resubmit with no other changes.

This time, the review response stated I had done the S3 Bucket portions correct (remember I changed nothing here) but the last rubric about the website accessibility, the comment was: 

> CloudFront domain name URL. This works great
> According to the review rubric you should provide the website-endpoint URL, this means that you need to include in the README file the URL of the S3 bucket static website

OK, so I shouldn't have removed the URL from the README.md.  I put it back in and submitted for a third time.   The review then came back on this submission was largely unhelpful.

> "Please only allow access from the CloudFront (using origin access Identity OAI)."

So do they want 1 URL (CloudFront) or 2 (CloudFront and S3).   Nothing is clear at this point because they are contradicting reviews, so I wrote a detailed comment for the 4th submission with no content changes.

> "I'm completely lost at this point as I've changed nothing on AWS side but just guessed at what is missing. I've had multiple URLs provided in the doc but yet I'm still told not right. I need more details or some help since apparently the instructions are not clear enough to describe what truly is needed to say complete."

I got this in response

> "I recommend you read through the CloudFormation best practices here:
https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/best-practices.html <br />
Stay Safe and healthy."

OK, now we are off topic in the response, since Cloud Formation was the extra stuff I did and still was failing on the Cloud Front URL.   The next day I got an email from Amazon saying I went past the Free Tier usage limit, so I went in and disabled access to to the S3 to make sure I wasn't charged any more.  That night I submitted this as my next submissions.

> "At this point I am completely annoyed at this whole experience. I've gotten very little assistance, the reviews have been crap and there is no one to talk to. The details of what is need is not accurate or helpful. The reviews have not pointed to anything and I have about given up. My AWS account is past the usage limit so I can't have the site be active any more. It was active for the first three reviews and the links worked as the screenshot show. So you have to base it on that as I'm not paying to keep the site active since this is suppose to be for learning. The screenshots and readme has all the details of what I believe was asked for. If the reviewer isn't going to read it and give real feedback then either pass this project or reach out to me and have a conversation."

This was clearly stating why I had to shutdown the site, that I need more details and that I was losing patience with the lack of support and this program.  And here was the response I received

> "Unable to review <br />
Your project could not be reviewed. Please resubmit after you address the issue noted below by the reviewer. <br />
Unfortunately, This submission can not be reviewed. <br />
I went through all the previous submissions and you have not submitted the proper CloudFront URL and this is a mandatory point. You do not need to worry about incurring costs because you should be using CloudFront free tier and be under the 50Gb in the S3. <br />
To learn more about AWS prices, check these two links. <br />
CloudFront
S3 Prices
Please resubmit including the CloudFront URL."
 
This response infuriated me.  They are not answering what is wrong with my project, telling me that an email Amazon sent me, should not be true, and I still don't know what is wrong with my project.   I finally wrote this response as my 6th submission and I'm about ready to quit the program.

> "And another review that isn't helpful.<br />
I can only tell you what AWS sent me. My usage limit is high because S3 was public and what you pointed seems accurate, it doesn't change the fact that AWS emailed me and said I'm about to pass my free tier limit. <br />
Dear AWS Customer,
Your AWS account has exceeded 85% of the usage limit for one or more AWS Free Tier-eligible services for the month of February.
Product AWS Free Tier Usage as of 02/23/2021 Usage Limit AWS Free Tier Usage Limit
AmazonS3 1700.0 Requests 2000.0 Requests 2,000 Put, Copy, Post or List Requests of Amazon S3 <br />
I have screenshots and documentation on everything that was done and the site had been live for a week but no one told me what was really wrong. I still don't know why any of my submissions aren't correct. The only reason the site isn't active now is cause S3 is set to private to stop from exceeding the quota. But as the screenshots show it was active on the address and configured per the project specifications. I even have the cloud formation script that generated most of it.<br />
In a normal learning env, you get help and guidance and from all the videos and stuff that seems like it should be available but I've gotten none so far. No one to talk to, no one responds, the message board seems dead and the feedback provided seems like a set of canned responses then personalized.<br />
If you really want to help, then I expect the following.<br />
Guidance on what really is wrong (not site not accessible or requirement not meet). I am a university professor and when a student has a question or problem or get something wrong you try to help by answering and guiding them to an answer. That is how they learn.
Review my content and Readme fully to understand what was done and provide specific areas its wrong since it details each step and had the urls used and pointers to the screenshots that show it was working before taking it down.
Reach out to help work with me. I can do a live session and walk through any of the setup to show that it was working or what ever is need."

And the response on this one was:

> "Dear Sir,
Well done :clap: <br />
Congratulations on completing the project!
I offer my apologies, that you had such an experience. All the screenshots and data were enough to pass the project. It's true that I wasn't able to access the websites. I was getting AccessDenied error, as you have changed the permission of the S3 bucket to private. <br />
Later in the course, you will learn more concepts of AWS like, AWS CLI, dynamic hosting, etc." 

I was glad to finally be done this ordeal, but it really proved that the Reviewers were not doing a very good job.  As the response shows, this reviewer looked at my comments, read it and reviewed the screenshots.  On the Udacity site they actually attached the screenshot for each rubric and highlighted the area that proved I meet the requirements.  This was a much better review but I'm still unclear why the first review didn't have this as it was the same content in the end.

Udacity has a survey pop up after each section.  They also have a more detailed survey at the end of a project.   In this survey, I blasted this entire project, the reviews and my unhappiness with this first project.  I was severely unhappy and ready to quit the program but I decided maybe it would get better.

The next day, their Program Experience Manager, Mohit Das, apologizing for the experience and stated the review concerns I reported have been documented and shared with the reviewer operations team. Hopefully this means improvements will be forthcoming.

## Project 2 - Udagram: Your Own Instagram on AWS

This section, part 3, began mostly the same, by watching all the material and then starting the project.  This time the project was intended to use modern development practices using branching for development and creating a NodeJS application from their started code by filling in all the TODO blocks and publish the application using Elastic Beanstalk.  I completed this project pretty easily and created the GitHub project and branch.  Next was to publish the modified started code with a README.md to the GitHub project on the dev branch.

This time the review came back and said my README.md was empty.   As my submission comment stated, the reviewer should look at the 'dev' branch since the Udacity page only allows posting the GitHub project without specifying a branch.   Again, I resubmitted making the same statement that the reviewer will need to switch the branch to 'dev' to perform the review. Not sure why I have to explain to the reviewer what the project rubric had required but the second submission got accepted and this project was now complete.

## Project 3 - Monolith to Microservices at Scale  

This was the topic I was most interested in, because this is an area I work with teams on in my organization often.  I went through the videos and was largely unimpressed with the content.   It really didn't give much in the way of taking a monolithic and breaking it apart.  It does highlight what they are and when it makes sense to use a monolith verse Microservices, but there was really no good techniques provided on how to go about splitting up a project to get closer to Microservices.  Maybe I expected to much, but I really was expecting more about techniques that may be used or patterns that may be leveraged.  Instead it largely talked about containers, segregating REST services, and separating business logic.

For this project, we had to focus on taking their starter code and separating it into different REST endpoints, package into different containers including adding a load balance and the frontend, and then automating the build and deploying to a Kubernetes cluster hosted on AWS.

To begin this project, I simply got a copy of the starter code, and tried to run it before making any changes to understand how it worked.   Good thing I started this way, because running an *npm install* on their source failed. An *npm install* is suppose to pull down all the dependent content listed in the *package.json* file to allow the application to then be run.  I tried this on every machine I had (Windows 10, CentOS 7, Fedora 34, and CentOS 8) with different versions of NodeJS (10, 12, 14, 15). It was able to install only on the NodeJS 10.  I spent about 4 hours of Googling and found the issue to be with a out dated package (*bcrypt*).   I also posted a question to the Udacity mentors section, to try and get some guidance. Unfortunately that didn't provide much help, other then to say they didn't have a problem and they were using Node version 12.  By the time I got the code working I had figured out the following.

1. Node 10 would compile it as is but there videos were using Node 13.
1. Node 12, 14 and 15 wouldn't succeed.
1. Had to upgrade *bcrypt* to version 5.0.0
1. Finally took my newest machine (Fedora 34) downgraded Node to version 12 with the bcyrpt upgrade.

This effort of debugging their code took about 20 extra hours over a week and a half before I could even begin the project.   This is very frustrating since Udacity touts teaching modern technology and skills but yet their code is out dated and doesn't work with the current versions of tools.   This makes it seem like they develop the course, get it published and then reuse it for years without updating it.  Technology is constantly and rapidly evolving and this model will never be good especially for students.  As a teacher, I can understand the challenges of making videos and source for a class but every year I work to update as much as possible since the goal is to help the students learn current technology.

Finally with the application running, its time to work on the project and separate the application into the two different endpoints (/user & /feeds) and then turn them into containers using *Docker* and automate the deployment using *TravisCI*.  This part wasn't to bad since it was easy to duplicate the code, create a *Dockerfile* for the containers and get them to run.  

Next I setup *TravisCI* after a lot of trial and error, then got it to deploy the different NodeJS apps (4 in total) in container form, to *DockerHub*.   Finally its time to work with AWS and setup a Kubernetes cluster.  Kubernetes is something that I've been working with for a while so I was expecting this be easy.  Alas, this didn't go very well and I end up losing about 3 more weeks.

I tried to setup a KubeCluster in AWS and couldn't get anything to work.   Following most of what was in the videos didn't help and there was always an issue with either connecting to it from a local system or having the cluster nodes fail to connect. Their videos glossed over to much in this area and skipped all the parts that made this difficult. I finally posted a question for a mentor using the Udacity mentor forum.   This didn't provide much useful information and a few other student joined in looking for help also.  One of the students started over and had success so this was what I tried after a lot of Google searches.  I deleted everything in AWS and started over using *EKS* to get everything setup. I ended up writing [EKS Hardship](https://spextreme.gitlab.io/site/writing/ekshardship/) to help others through this nightmare of a project because this wasn't even talked about in the course  material.  Again I learned everything to do this project on my own outside of the course.  The content for this part is largely outdated and didn't cover much of what was really needed for this project.  Kubernetes isn't easy to setup and AWS does add some challenges to it, which EKS really does solve.  The material should have covered this as a better solution but again it is outdated and EKS probably didn't exist when it was created.

After spending a few weeks on this, I had fully automated a way to create a AWS KubeCluster to try and deploy the project containers.   I ended up recreating the cluster 4 times to make this work.   It was mostly around issues with not scaling properly, invalid resource sizes and things related to parts that were not fully known since I didn't develop the application.  None of this was in the material covered but I figured it all out and finally was successful in creating a KubeCluster and had the 4 Microservices running.   It was finally time to submit this project content.  I was very nervous when I submitted this and was not prepared for more challenges with reviews.  Luckily this review was a "Success" on the first submission.  

## Project 4 - Serverless Application

Serverless is the final directed project, which focuses on Serverless technology within AWS.  I started this by watching all the videos and going through the material.  This seemed to spend some time on Serverless technology but then ventured into NoSQL databases.  I think this was so they could setup more for the project but really didn't seem important to learn Serverless concepts.  This project was to build a TODO application using Serverless Framework, DynamoDB and Lambda functions.

This was the point I was really tired of this program.  The material in this section, seemed to do as many of the others, only scratch the surface on the technology and then direct a project to use many areas that are not covered in the material provided.   For this project, we had to build a databases schema, write JavaScript code to build out the application and then deploy it into AWS.  It wasn't very clear what the project required us to do so I made a list from the rubric.

- Create a database using DynamoDB.
- Create a JSON schema for the request/response.
- Build a CRUD set of REST APIs (Create, Read, Update, Delete ToDo items).
- Setup Auth0 account.
- Create each CRUD item as a Lambda Function.
- Separate into business and presentation logic.
- Setup API Gateway.
- Handle Async/Promise based functions.
- Implement Logging.
- Setup an S3 bucket.
- Deploy To AWS using Serverless Framework.

As you can see from the list, there are only a small set of things related to Serverless concept and mostly it should be handles using Serverless framework.  So really, 2 step out of 11 are focuses on what we are learning.   I got started by trying to figure out how to stand up a local DynamoDB (fairly easy) and a development environment (really hard).  This was all going to be needed to develop the CRUD functions, test it before trying to use AWS.  You may ask, "Why not just use AWS?", and this may have been the intention but to develop, you have to hit the end points to test it.  In doing so, AWS will be keeping track and after enough calls, you'll be charged because you blow past the free-tier.   I had already been hit by this, so there was no way I was going to develop on my AWS system till the functions worked.

After trying to stand up a full Serverless development environment, I was completely fed up.  I wanted to learn Serverless (which I think I have a good handle on) but I didn't really want to waste 3 weeks trying to learn how to and then build out my own development environment, when they didn't cover any of this in this part.  Why I had to build any of the JavaScript and DB is beyond me since that was not the intent of this course.  Especially with the rubric requirements stating they wanted us to subdivide into business layer and db layer.   It really just made the project more of a JavaScript design project over Serverless.

At this point, I decided its time to give up.  I wasn't getting anything out of this so I informed my organization lead that I was unhappy with the program and why.   They are going to be working with Udacity since they have a large number of other people still involved.

## Project 5 -  Capstone

I never looked at the capstone, but based upon my past experience I'm sure it would require some unique challenges that we didn't learn. I'm really glad I don't have to figure them out.

## Conclusion

As I worked through these projects, I went from excited to completely frustrated within the first project and it carried through each project.  As I got further in, I found the content and videos to be largely out dated because they are a few years old and mostly not providing much of the important values for AWS cloud.   This caused challenges and really limited the work on the goal of learning AWS Cloud.

In the end, I was very disappointed I had to give up so close to the end but since making the decision to leave my stress level has gone way down and I'm now focusing on Cloud development activities that I can really learn from.

If Udacity would like to improve, my recommendations are:

1. Provide a better way for your students to get help in a more realistic way.  The mentors seem hit or miss, reviewers provide little feedback and the forums seems largely unattended.
1. Update and keep the content, videos and projects current so they stay more inline with new technology.  Most of the AWS UI have changed and the project code should stay updated so students don't have to downgrade or debug the starter code.
1. Focus the projects on what really should be learned.  If Serverless is the goal, then don't require students to build out a JavaScript application and design a database or stand up a development environment.
1. Train Reviewers on how to provide good quality reviews that guide students toward the rubric tasks and can better learn from their mistakes.

If you choose to go with Udacity, I hope your experience is better then mine.  If you don't want to use Udacity, you can largely learn these technologies and many others with some good Google searches and watching videos on *Youtube*.   

Happy Learning.

