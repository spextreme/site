---
title: "About Me Writeups"
date: 2022-04-24
draft: true
enableToc: false
---

# About Me

The below are a collection of about me for different context.  The context is the title.

## Mediaum Bio Page

Welcome. My name is Stephen (Steve) Paulin. I've been a creative person for my entire life and this is what drove me to become a software developer and writing. Early in my career I focused mostly on programming to help in my early career but have evolved into a mentor and change agent for my organization. 

I attended Drexel University and received a Bachelors (1998) and Masters (2001) degree in Information Systems. I then obtained a Data Analytics Master (2020) degree from Rowan University. After getting my second masters degree I began working as an adjunct Professor in the Computer Science department of Rowan Univeristy.

I enjoy mentoring and helping to drive innovative solutions to existing problems. My focus as a professor, is to help students understand how their world is impacted by technology and how having a deeper understanding of it, will help with their future. My work for Lockheed Martin and the government has provided me with an avenue to help them evolve and leverage new techonology in this technologically advanced world.

In my spare time, you can find me cooking good food for my family or working on some home improvement project.

It all comes back to the creativity which drives me. Writing, cooking and coding to help educate and inspire individuals.

## Gitlab About Me

I have been professionally programming for over 20 years, and I’ve always loved creating things. Programming was my way of creating new content ever since I was a child. Much of what I do today is help push modern concepts and technologies including DevOps, Agile, Cloud Architecture, and Automation. I consider myself a change agent to help keep people on their toes by pushing forward with new technology.

When I think back, I’m not always clear how I got where I am, but it has been a slow steady climb. Programming as a child, I worked to learn Basic and evolved into C/C++. In college, web development became the new thing and this is where I learned C# and the .NET Framework. For the last 20 years I’ve been using Java.

In all of this, my desire to create has never changed. This is probably why I find so much enjoyment in writing and woodworking.

Recently, after completing my second master’s degree in Data Analytics, I became an adjunct professor. I’m hoping to pass on my passion for programming and creating new solutions.

## Book About

Stephen (Steve) Paulin has been developing software for over 25 years.  He began his career building web applications when the web was really new and then transition to Lockheed Martin where he built tactical applications.

