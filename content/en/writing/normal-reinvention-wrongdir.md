---
title: "Normal Reinvention"
description: ""
date: 2021-06-27
draft: true
enableToc: false
---

## Normal Reinvention

Have you ever had an idea that you made a reality? Maybe it was writing some code, building something, or even something as simple as a homework assignment when you were in school.  We have all had something that we thought was a great idea and ended up creating.   

When you are done, you’re usually proud of the creation your idea formed and you usually learned something in the process.  But from what you learned while building it, you also know how you can make it better, things you would change, improvements you could make and mistakes you made that you would correct.

From here you take that idea and keep building onto it and learning more.  Eventually you have a monolithic solution that you’re proud of, but you know all it’s faults, problem areas and things you wish you could redo.

Personally, I love construction and doing home remodeling projects.  I recently gutted a house and redid most rooms (new kitchen, 3 new bathrooms, all new flooring and painted everything).   After the 6 week project I was finally done, and the place looked great but I knew all the mistakes and problem areas.  Anyway, it was my rental property so I was willing to live with the imperfections (caulk is a wonderful thing).   Less then a week later, a waterline snapped and the entire first floor was flooded with 2 inches of water.  I now had to rip out all the flooring I just put in and then spent the week drying out the place.  Lucky insurance came through and I got to redo the Pergo flooring.  This time I bought a portable table saw.   Man did it make this flooring project so much easier and I really don’t know how I did the floor the first time without it.  This was a blessing in disguise.  After this was done I can honestly say this time I didn't make any of the same mistakes.

Bringing this back to Software, this holds true but can usually be much simpler.  Software is highly malleable and easy to create fresh or update.  It's also easy to experiment and the plethora of tools available make it even easier.

However, many organizations believe this to be true and treat it more like building a widget.   Much of our code is written and then we typically don't want to touch or change it much after a release.  Change is seen as a high risk endeavor and because of the lack of testing it can be very unnerving to the person who has to make the next change.   Also it is usually challenging to get approval to do general improvements that won’t fix a bug or add new capabilities.

In recent years this has started to change with automation and Agile/DevOps practices.  The thing we have to remember as that the really true constant is change.  Any software that has no request for features, no bugs, and never changes, usually means that software isn't being used.  This means we really want to be changing it often to keep it relevant.

With that in mind, as software engineers we need to do a few things.

1. Develop to support change
2. Educate others on how software should change
3. Constantly learn and evolve


### Support Change

As discussed software is malleable and intended to constantly change.  This is so we can improve it, add features and make it more useful for our customers.

As software developers we need to make sure out code can be maintained and support the constant change.  This means that we are responsible for making sure we produce good quality code, that is easy to follow and maintain.   This is done by creating descriptive variables and functions, good internal documentation (comments, doc solutions, etc...) and adding high quality unit tests.   Automation is also important to make sure every change will be run through the tests available and confirm the change didn't break things.

Putting this in place will make sure you and others are more willing to change the code in the future and helps with the overall maintainability.    If this is a project others will be involved in these things will make them much easier for all.   As a good developer these are your unwritten guidelines for everything you create.

### Educate Others

Most will not understand software or how easy it is to change but how difficult it can be to make it right.   Coding is complex and there are a many factors involved in making something work.   

Its your goal to help guide others to understand that software does take skill to create and shouldn't be expected to be "complete".   Helping to understand that software is and should remain malable and be constantly evolving.   No code is perfect and design choices will affect the long term maintainability for future changes. Making sure business leaders understand that can help to make sure funding and support are continually added to things that provide value.

Helping them understand will assist in making sure the product can improve and stay current.  

### Constantly Learn

As a software developer, it is very important to make sure you are constantly learning and improving.   New techniques, languages, products are constantly becoming available and staying up to date is work.  Its your job to make sure you stay relevant and to do this staying current.

If you start to lag you'll be left behind quickly because of how rapidly the technology is changing.  Sitting still why everything changes around you is like moving backwards in the tech industry.

## Conclusion

Expecting change as the normal 
