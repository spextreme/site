---
title: "I Want..."
description: "An opinion artical on how to succeed"
date: 2020-11-01T14:09:21+09:00
draft: true
enableToc: false
---

I Want...I Want...I Want...

If you've ever had young kids you know the word I WANT.   Well as humans we never really grow out of that but we get more tactful and we can then start to getting the things we want ourselves.

Recently I got to thinking that what I really want isn't something I can easily change or control.   To solve this, I am putting these down in an electronic form to make it real.   That is the first step but then the question becomes whats the second step.

You may be asking yourself what I really want but if I immediatly say it you'll discregaurd it or fully agree so I'm dealaying to convince you that you'll want this too.

So working at a large company (and I mean very large by number of people in the 100K) you see things occur that you feel you can help or want to improve.  You want to see the company be successful and you want to help your teamates get better.  The issue is you also have the complete opposite with people don't want to change, it costs money, and the worse oppinions 
