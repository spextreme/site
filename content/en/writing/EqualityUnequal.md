---
title: "Equality Unequal"
description: ""
date: 2020-11-01
draft: true
enableToc: false
---

We can not be equal.  Genetics makes sure of this.  If you believe differently then try to convince me differently.  But when you're born, no matter what you will not be as good as someone else at something.   YOu will be better then someone else at other things.

So why do we think we all should be equal?
