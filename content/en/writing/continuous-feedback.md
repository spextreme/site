---
title: "Continuous Feedback"
description: "Developers get feedback from CI, why not from teammates."
date: 2021-07-04
draft: false
enableToc: false
---

## Netflix No Rules Rules

I recently started reading the book from Reed Hasting and Erin Meyer called [No Rules Rules - Netflix and the Culture of Reinvention](https://www.amazon.com/No-Rules-Netflix-Culture-Reinvention/dp/1984877860).   I'm still reading it, but chapter 2, *Say What You Think (With Positive Intent)*, got me thinking about how in most businesses this is a vary challenging model.  As the book describes, it's very difficult for people to take candor feedback and it may be even harder to provide it.  However most developers use tools like Continuous Integration (CI) systems to get a constant and highly beneficial feedback loop.   Could this be translated to start changing peoples behavior and promote what Netflix has learned about candor feedback for improving the business culture?

## CI Feedback

As developers, we are all accustom to the feedback provided by our compiler, integrated development environment, unit tests and even our CI systems. The feedback provided by any of these, helps developers learn and improve regularly.  A failed compile, test or other audit, teaches developers more then getting it correct.

The CI feedback loop is one of the major benefits of automation, but you probably haven't realized that this is a form of candor feedback.  A failure in your CI system tells you and the team something is wrong.  You don't typically take offense or feel humiliated by this feedback or being publicly shown that the build failed.

The real questions becomes why and how to use this model for other forms of improvement.

##  Trained Behavior

Looking at the reason why developers don't get offended or humiliated by the CI system seems to be "Developers expected and want this feedback."

As Reed/Meyer explains, being candor is very difficult for people to speak up and even accept.  This drives the natural human response of fight or flight in the brain. Learning to accept and give feedback is very difficult because of this natural human psyche.

However, as we use tools that provide feedback like a CI system, this doesn't elicit the same response.   This is because we go into the situation knowing the outcome (pass or fail).  This allows us to then rationally use the outcome to improve.

This is what is explained as the culture of Netflix, and its part of their cultural foundation.   As employees start, they see leaders and other employees providing feedback with acceptance from the recipient.  This provides a natural environment to grow this behavior and it becomes normal.  It therefore becomes a known situation like a CI system result.

## Improve

Being able to provide candor feedback is hard for most of us, but accepting it can be even harder.  As a team member, you use the CI feedback as a way to improve because you learn from the mistakes.   For you and your team, you should work to get a feedback loop into the team to gain the same improvement benefits.

You should model your team after the CI system, by working to expect feedback on events (triggers in CI) and sharing feedback for those events (results).   By being open to this, it can foster a relationship in the team to help all improve.  By establishing this foundation with the member of the team, the feedback loop can become very real and beneficial.

When doing this, it is recommended to establish ground rules to make sure it can be successful.  Like the CI systems tasks, these rules will help guide the feedback, how it is provided, keeping it constructive and making sure its valuable for the individual.  Its also important that feedback should be given immediately so it can be most useful in the situation.

Reed/Meyer explains there are four main things to focus on in giving and receiving feedback.

Giving Feedback
- Aim to Assist
- Actionable 

Receiving Feedback
- Appreciate 
- Accept or Discard

For the team to provide feedback, it should be focused on how the individual could improve.   Providing feedback with clear understanding of what can be improved and how to achieve it, will be more easy to receive.

When receiving feedback, stay open to it and show that it is appreciated is key.  This will foster the teams willingness to continue.  It should also be clear that the individual receiving the feedback can decide to take action.  The provider of feedback should not feel offended it wasn't acted upon.  This is why showing appreciation for the feedback is important to at least show they were heard and valued.

Remember the goal of this, is to foster a team dynamic that is open to candor feedback.   Use it to help each member of the team so you can all be successful.  Practices it so it becomes as natural as using your CI system.




