---
title: "Architecting for Containers"
description: "A presentation on how the world changed and containers made the orchestration layer possible."
date: 2020-11-01T14:09:21+09:00
draft: true
enableToc: false
---

= Architecting For Containers
Stephen Paulin
Version 0.1, 22.10.2020

:sectnums:
:toc:
:toclevels: 4
:toc-title: Agenda
                          
:description: The world of Containers is changing how systems are being managed and how they are architected.  With the advent of cloud orchestration solutions, containers are the main way to manage and build applications.   This means, a change in the way we architect the systems being produced.
:keywords: containers architect kubernetes docker cloud software
:imagesdir: ./img

== Software Revolution

* Changed Everything 
* Monolithic Applications are bad
* Systems are to complex
* Smaller is better for complexity and scale
* Networks and containers set the stage

[.notes]
****
* The Internet drove modern software companies to find a better way of doing things.   Hardware is no long king, as the basic system is now intended to be generic, with the software being the uniqueness of the overall capability.   Specialized hardware is to expensive to create and maintain. Software is cheaper to create, easier to advance and takes much less research and development then in the past.   
* With this, the old model of one big monolithic application is showing its age.  The issue isn't its size, but the complexity it contains and the portability becomes challenging.   Change is also much more complex for a monolithic application because of the amount of code, its design decisions and in many cases because of the environment around it.  
* When the environment changed from a single desktop computer to large scale network applications, the monolithic became to costly to convert and recreate to meet the network centric model.   This is why the world changed to focus on small independent processes that can communicate over a network and work together to scale and create a system.  It has the added benefit of breaking down the complexity and when something outlives it's usefulness it is much easier (with less guilt) to replace.
* From and organizational perspective this also means that smaller items being replaced have less cost and risk to the overall goals of the company.   It increases the ability of reuse and decreases the complexity of maintaining the smaller parts.  It also opens the door to a host of other advancements.


* Internet changed the rules
* Modern software companies need a better solution
* Hardward is no longer as important and intended to be generic
* Monlithic no longer work
* Its not the size but complexity built in
* Its not portable or easy to adapt
* As environment changes, the monthlics can't keep pace
* The world changed from desktops to large scale networks
* Monolithics couldn't change fast enough to work in this model
* This is why smaller is better
* Smaller also breaks down the complexity
* For the business, smaller means less cost 
* It is also cheeper to replace with less guilt
* Increases reuse and lowers maintenance
* Opens the door to many advancements
****


== Nothing Is New

* Small services is an old concept
* Do one things and do it well
* Unix has had this model forever
** bash , ls, vi, ssh, tree, the list is endless 
* Small things are much easier  to replaced ( Remember telnet...)
* Communications through command line, memory or networking

[.notes]
****
* This concept of small services is not a new concepts.  Looking at one of the oldest computing systems, Unix, is just that.  A large collection of small independent services.  For example bash, ls, vi, ssh, tree, are all very small single purposed applications that do focus on the concept of do one things and do it well.  Not to say these in themselves aren't complex (vi has a lot of options) but it makes it easier to evolve and change or replacement (Remember telnet).
* With this concept of small independent things working together in the operating system, they can share information through memory or on the command line interface.  All that hasn't changed in the Internet based world.  It just became more distributed across a networking.   The networking model is just another form to communicate over.
* While networking, computing systems and small independent applications are not new, the world of architecting systems has changed to better align to this advancement.


* Small Services is not a new concept
* Think about Unix
* Its just a collection of small indepentent process
* bash, ls, vi, ssh, the list is endless
* Each one is very small and does one thign well
* They may still be complex but they are very focused complexity
* Not complex cause it performs to many unique things
* These small things are easier to evolve or replace.
* Rember telnet.  
* Now SSH is the primary solution
* In this concept small things can work together 
* They can talk through memory or on the command line
* Piping input and output between them.
* In the network based model it just became distributed
* The network just became another way to communicate
* While this isn't new 
* How the world architects to it has changed
* Mainly to better align to this model
****
 
 
== Modern Systems

* A collection of small things
* Network is the primary communications medium
* Abstract loosely coupled communications
* Complexity is now at the system level

[.notes]
****
* Modern systems leverage these networking concepts by going back to the roots of making and keeping things small to simplify the complexity of the independent items.  Networking has become the primary mode for these services to communicate with one another.  But an interesting thing has occurred with this larger scale communications, the messaging has become generic to allow these services to remain loosely coupled.
* Loosely coupling the communications means that each service is able to more freely evolve over time.  Versioning of these communications models also means that one service can advance without breaking the communications to older services.   This is an important concept because change is expected and promoted with this model.   Older services can then evolve at their own pace.
* Complexity is removed from the smaller services but is not gone from the system.   The workload will to be processed could pass through hundreds of services.   When things don't work as expected, this can be very difficult to understand where it broke down.  Also with services being distributed, the path taken may not be fully understood or easily determined and may change with each request.
* This requires the system to better protect itself from failure and improve the orchestration of all these services.   Tracing and logging become very important at the system level but they themselves are small independent services.

* This is a small animation of Netflix communications across its services
* Each dot is a service - 116 of them.
* And the communictions traffic passing through
* Only a small portion of the large system
*
* Networking concepts become most important
* By focusing on small and leveraging the network it simplifies the peice parts
* Independent things complexity is almost removed
* The focus on the one purpose they exist for
* Then they provide a communications path
* This gave birth to a generic communciations model
* Provides loose coupling
* Also if done well, its versions
* This is important because it allows servics to evolve independently from one another
* Older service have time to evolve at thier own pace
* Complexity is removed from the individual parts
* It does still exist but now its in the system
* A task could pass through many smaller services
* May not even go through teh same path every time
* When things break down it may be difficult to understand why or where
* With everything distributed it may not be easy to find the root cause
* The system also has to be better at protecting itself from failures (bad requests/responses, no responses, etc.)
* Orchestration of all this is important 
* So is better logging and tracing.
* But that is not part of the independent services themselves.
****


== Orchestrating a System

* Managing the system needs a solution
* Containers set the foundation
* Pods emerge as the way to manage services
* Orchestration manages the Pods
* And the magic happens from there

[.notes]
****
* With all these services in a system, a better form of managing them is required.  Containers made these services self contained so its only obvious that the orchestration should manage containers.   The standard model for this is to create a pod as a definition of the capability by managing the containers and their purpose.
* The Pods can then be managed by the orchestration layer which manages how many should exist, how they communicate, routing traffic and ultimately monitoring their health.  Running multiple pods then means its possible to guarantee the system remains operational during any potential issue.  Since it is abstracted from the underlying system (Hardware, OE, etc...) this becomes much easier scale.
* Redundancy is no longer important, as resiliency has taken the place for handling failures.   A pod dying can not impact the system because the orchestration will make sure that the pods service is still available somewhere in the system if that is the desired effect.
* Pod death and rebirth can happen quickly and easily at scale, with the correct design of the services contained within the pods.

* A better solution was needed to manage all these services
* Containers offered a way to more easily manage the needs of the service
* A container fully contains everything the service should need to run
* Runtime dependencies, data storage, etc...
* This is why orchastration starts with the container
* The Pod wraps around containers to build out the capability
* The Pod is the tangible unit that the orchestration layer manages
* This means they can be started/stopped and monitors.
* It does much more by routing traffic, managing the quantity and can constrain to limits
* Redundancy is replaced with resiliancy.  
* Failures are quickly recoverable by pods being spun up or down within the system as needed
* This also mean scale and growth is much easier.
****


== Designing Pods

* Containers contain a single application to execute
* Pods may contain multiple containers to create a service
** Track Service can listen for and provide track data.
** A database connection for the service then persist received data
* Pods are the building blocks to be orchestrated


[.notes]
****
* Containers typically contain a single application to perform one function during execution.  Let say for example our service is a simple capability to manage track information.  Tracks are generated from somewhere in the system, and this service just waits to be told of one (listener).   It then holds that track in an underlying database that is external to its services.   It can then be asked for history on tracks its managing.   The container holds nothing more then the service execution.
* A pod then may be setup to have this service and a the underlying database.   The service doesn't care about the database used, just that a connection to one is available.  The pod joining the two things so it can now deploy it as a complete service.
* Pods can be single containers too.  The pod is just one part of the building block to allow the management of the smaller pieces.   The orchestration of Pods is where the magic happens.

* Containers contain a single application
* A single entry point to run something
* For example we may have a track service that listens and records track data
* the container doesn't care about the database just that a connection exists.
* On receipt it will record the data
* A pod could be setup to contain both the container application and a database container
* Or it could be just the track service in the pod
* In any case, the orchestration will use the pod or pods to setup the workign system
* Based on the configuration our service could be setup to run 1 or any number 
* This way if a request gets sent there will always be one available to record it.
****


== Orchestration Layer

* Made of a collection of computing resources and pods
* Controls what runs, how many and within what constraints
* Services are defined to guide the orchestration
* Orchestration then manages everything

[.notes]
****
* The orchestration layer is told what pods to run, how many instance should exist, and what their resource bounds should be.  It also is told of what is available for it to work within (compute resources).
* From this information the orchestration layer is able to control the service desired (the pod with its details).  It can start services to meet the needs, it can kill them because they are misbehaving or not responding, it can increase them to handle extra load in the system, or spinning things down when there is no need for more.
* This makes the service resilient so the needs can always be met and adjust as the needs change.
* Networking is abstracted as is persistent storage for managing the communications and data.
* All of this is provided so the services can focus on what is important.  Building small things to service the larger system.
*
* Orchestration is configured to run the set of pods
* Told how many to run
* What their resource limits should be
* The orchestration layer also knows the details of whats available
* The resources it has. 
* Its environment so to speak
* knowing this information allows it to manage the services
* And the resiliancy of it
* It can bring up instance to handle extra load
* It can take down when the load decreases or a service is unhealthy
* Communications is abstracted and routed
* Data storage is abstracted and routed
* This is all handled by the orchestration layer
* Services can then focus on the core logic for that service to support the larger system
****


== Then A System Is...

* Not what it use to be
* A puzzle of services pieced together
* Abstracted communications
* Orchestration manages the execution lifecycle
* All this working together provides the system's capabilities

[.notes]
****
* So then what is a system?  Not what it use to be. 
* It is nothing more then a puzzle of all the smaller pieces fit together using an abstract communications model to allow each to perform the function it is intended for without impacting the rest of the system.
* The orchestration layer manages the life span of the service, its service availability and what should be done when the service is not performing properly.
* Putting it all together then allows the capability designed for the system to begin servicing its true intent.
*
* So whats a system
* Well obviously not like it use to be
* The system is now a collection of small indpentent peices
* Each performing its function with a little outside help
* Communciations is abstracted to keep services loosly coupled
* The orchestration can then manage the life cycle of the service
* Confirming they are healthy, growing and shrinking as needed
* Putting all this toghether then provides the overall system 
* It works together and orchestrates to make sure its available
****


== A New Meaning

* Keep things excessively simple 
** Complexity at the system level is inevitable
* Services should be focused
** Don't worry about other stuff
* Build a little and deploy often
** Learn and evolve, don't over design


[.notes]
****
* Therefore the new system model is to break things down into small excessively simple services.   Each service needs to stay focus on a single thing to make sure it can remain simplistic over time.   By doing this, creation and maintenance become easier and help to reduce the issues with it.  The system will be complex enough, which is why the services should not.
* With services focused on one thing, they can do that very well and completely.   It should not worry about other areas (databases, logging, security).   Adding these unique things add complexity to the service and prevent it from doing one thing well.   Externalize the parts that aren't part of the core goal for the service. 
* Building a service should be doable quickly (hours), deploying it should be automatic, and learning should be quick.    Doing this over and over on a daily basis will help this to evolve quickly, while maintaining quality.   Don't get distracted by perceived need and therefore over design.   For example, if a service needs to get position data, then build it to get position data.   Then deploy it.   Find out it gets position data but it takes 2 second to get the data.  Now worry about making it faster, refactor to get it in .5 seconds.  Is that good enough?   Iterate until it meets the system needs.  Don't over design from the start.  Use facts and learning to evolve the system.
* Leverage and reuse is crucial here.   The commercial industry has grown leaps and bounds and these capabilities are all coming from there.   Leverage more so the focus can be put on building value.  Cloud system are all based on Kubernetes which was open sourced by Google and every vendor who offers a solution is built on this single architecture so they can add their value on top.  
* Our goal is to figure out the real value and focus on building that, without building anything extra.
*
* Keep things small and execessively simple
* Each service should focus on one thing and doing it well
* This will make creating fast, learning faster, and maitenance cheaper
* The system will be complex enough
* Don't let it touch your services.
* Building services should be doable quickly (hours)
* For example someone built a gyro service in 4 hours.
* Got deployed and found it took about 1 second to respond for data
* Then they were told it was to slow
* Four hours later a new service was writen
* Exceeded the timing requirements (microseconds)
* We built it learned and rebuilt it in a day.
* Reuse is cricible too
* The industry has built so much so why reinvent
* Take it and use it till it doesn't work then replace it.
* Don't need to over analize but use the system to fact find
* Look at cloud.  
* Every cloud vendor now uses Kubernetes as the foundation
* Each then builds value on top of that.
* It should be our goal to figure out what the value is
* And build as little to get that value.
* Leverage the rest.
****
