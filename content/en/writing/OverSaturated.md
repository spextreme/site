---
title: "Over Saturationion"
description: "Why developers are the reason for so many issues in todays tech world."
date: 2020-11-01T14:09:21+09:00
draft: true
enableToc: false
---

Oversaturation

I've been a software person for as long as I can remember but recently with the advant of "<Insert Keyworld Here> As Code" it got me thinking its time to start changing to do my presentations as code.  So like most I jump online did some searching and came up with

Well there in lies the fundamental problem.   Software folks we need to stop.  Every where you turn someone revents something but doesn't do anything but make yet another complex system that someone else when then reinvent.   So what is the point.  Millions of Software people around the world just recreating something that alreadxy exists.  Its time to stop



Everywhere you turn there are way to many options for anything. If you ask someone in the business world about making a presentation they will say PowerPoint. Maybe one will say something else.   But not if you want to do a prentation as code we'll you would think there is a clear solution.   RevealJs seems to be in the top but then there is Markdown, Ascidoc, and Slides.com, and oh my.....   Gets even harder when you through in Git<keyword> pages.   Its just a bigger mess cause now I'm not even sure what git hosting service I should leverage.

Why do we need so many options.  I'm guesing cause there billions of people world wide and no one is ever happy.  Lets reinvent the wheel a hundred times over just cause every other person doesn't like the shape.   

So that brings me to my point.  Why can't we just start to consolidate and stop all the reinventing the same crap in different forms.  Are there no new ideas?  I'm really convinced there aren't.    Software is one of the easiest things to create but with that ease it just keeps people from using their imagingation and they just recreate whats already there.  Why can't people take that energy and start to use it to invent new things.   Yeah I get that you think you can do a better job then what perviously exist. Before you do that ask yourself why. 

