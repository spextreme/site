---
title: "A Life With No Direction"
description: ""
date: 2020-11-05T11:47:21+05:00
draft: true
enableToc: true
---

# Start

Your life is hard. Your family died and your have no reason to live. You wonder each day why you're still here. You take many risks and don't care if it will be the last one. Your home brings back too many painful memories; not wanting to go home you have been wandering around the city. At the end of the cold, dark, rainy street there is a light that you mindlessly walk toward. As you near the steet lamp, you notice that the only thing that is luminated by this light is a sign.

"Huh?", you begin say to yourself, "This could be a good thing. Why not. I have nothing to lose."

You walk down the street to see a man sitting by a door. He smells bad and looks dirty. He has a brown bag rapped around what looks like a bottle. He asks you for some change.

You give him a few dollars from your wallet.

"Thank you kind sir. Where are you headed? If it is in there you will not be coming back. That is a place of evil. It has continued to bring people in and I have never seen any of them return. If this doesn't scare you then you must be very stupid or eager to.... Ahh forget I said anything."

"No tell me. I am not stupid but curious."

"Ahh curiosity killed the cat, my boy. That is what many people have said as they enter. I am curious to know what they do, too. I can only assume that they do experiments on people. The people are kept because society would not accept them because they are so horid. Thus, they kill them or continue to do experiments. That is why I don't see anyone come out."

"So you say they kill the people in there. Well when I come back out I will tell you what goes on in there."

"I hope so......"

As you close the door behind you, you notice the bright lights reflecting off the polished white tile on the floor. As you look up you see two great big glass tubes with flourescent lights in them. They seem to flicker with a faint buzzing sound. As you look around you jump when you hear a voice from behind you. You turn around to see a man in a white lab coat standing there. He speaks to you.

"Hello. I am sorry if I startled you."

"Oh. That is OK. I have just had a tough day. A little stress never killed anyone." You realize that may not have been the best thing to say from what the man ouside the building told you.

"Oh well, that is probably true. So, are you here to travel and get away form this place?"

You and the man begin walking down the hallway. The tile is bright and shiny while the walls are white and smooth. The man continues to walk without saying a word. You are a little nervous but don't let it bother you. Since you have no reason to be in this strange place you see it as another of your many risks, testing the limits of death. There is a man standing in front of a door at the end of the long, white hallway. The two men speak but you can't make out what they are saying. Then all of a sudden the door opens and you enter while the man that braught you here follows you in. There are two seats and a table. You and the man sit down, and he begins to ask you some questions.

"Do you have any family?"

"No"

"Do you drink, smoke, have any physical or mental problems?"

"No" You answer wondering what the questioning is for.

"Do you get motion sickness?"

"No, why all the questions?" You finally ask.

"Just routine." He replies.

The questioning continues for about 10 minutes. When you are finally done you realize that you said no a lot. You get up and leave this room to go to another room where they give you a set of clothes to wear. You put them on. They are a little tight but the blue jeans and the white shirt seem to stretch as you move. They take you back to the section where you first came in. You walk toward the two glass tubes that you noticed earlier. When you get to them you notice what looks like a small booth. As you walk further you notice something that resembles a gun only it is much larger and has wires that run from it to the booth. You ask them what it is.

They explain. "It's the time machine. Basically, the way it works is, it transfers you from the booth to the laser which then makes you travel to a time that we specify."

You nod, hoping that it is as easy as it was to say. They begin working around the booth as they then motion for you to come and enter it. You begin to have second thoughts but those are quickly faded away by the thought of your family's death. You look inside the plain booth as the men move toward the control panel. They wave to you and you return with a nod. Suddenly you feel like you are being compressed and stretched at a rapid pace.

Then it all goes dark for a second...

You feel strange as you begin to feel pins and needles in your toes. You open your eyes to see the bright blur of light above you. It takes a few minutes for your eyes to adjust, at which time you begin to look around. You notice a burnt car sitting in the street with rubble all around it as well as numerous destroyed buildings.

Then you look around more and...

Ahhh! What was that? You run and dive behind the car. Shots begin after hearing the explosion. You can't see where the shots are coming from. They stopped almost as quickly as they started. You sit down, with your back against the car and you close your eyes as you begin praying to not have your head shot off.

"Put up your hands!" a synthetic voice tells you.

You open your eyes to see a metal machine standing in front of you holding a gun pointed at your head. You immediately raise your hands and begin to pray even more.

"Come with me." The metallic machine tells you.

You get up slowly trying not to make any sudden moves. Your hands are still in the air as you take a step forward. You feel the machine push the gun into your back, forcing you to walk forward. You start to walk and begin to think to yourself. I think I can take the gun and get away. Do you dismiss the idea and continue walking with the machine?

You continue to walk past a row of burnt out shops. The machine tells you to turn down the alleyway. After you turn the corner, the machine tells you to continue walking. You ponder the thought of where the hell it is taking you since there is a solid brick wall straight ahead. As you approach the wall the machine tells you to stop.

You feel a sharp pain in the back of your neck and you black out.

You wake up in a room laying on a couch. Your head hurts and you wonder where you are. Your eyes are still blurry so you can't see much. You rub your eyes trying to clear them but it isn't working.
"Hello there. I see that you are awake. How are you feeling?"

"Ahh...I am OK. Where am I? Who are you? What happen?" You ask while you strain to correct your vision.

"Well, I am Mark and we found you in an alley unconscious so we brought you back here to try and clean you up. We thought you were a goner when we saw the retriever take you." Mark explains.

"What is a retriever?" You ask curiously.

"Well it was that metal thing that took you to the alley. They aren't killing machines though. That is what the Executioners are for. We thought the retriever was taking you back to be killed. Their purpose is to gather information and take prisoners. You were lucky that he decided not to take you."

"So what is going on here? Why are there machines running around killing people?" you ask trying to piece together the situation.

"Where have you been? They must have hit you harder then we thought. Well we are at war with the machines. In 2023 we mastered the production of robots that could think and accomplish tasks that humans used to do. Some people didn't like this. It put many people out of work. The workers decide to get even with the company by blowing up a factory. The entire factory was run by machines so the workers didn't think that they would hurt anyone. Unfortunately the machines did take it personally. They tried to take out the people that were at the factory. They almost accomplished it but one person got away and told some people, everything went down hill from there. Is anything coming back to you?"

"Ahh, yeah I think so. Basically there is a war and I just was in the wrong place at the wrong time?"

"OK? Yeah I guess you can say that. So anyway what were you doing there in the first place?" he asks curiously.

You begin to answer......

After you finish telling him about how you got there, he just sits and nods his head in disbelief. You can understand how he may not believe it since time travel isn't a common occurrence. Or is it? You remember that you came from that past so you would think that it was here in the future. "I guess that you have never heard of time travel?"

"No it isn't that. We had time travel but it was to dangerous so they dismantled it. I am just surprised that you are still alive. Every time I heard they tried to send someone it turned out to be a disaster. How is it that you survived?"

"Ahh. I didn't know that. They didn't tell me that when I signed up for it." You say looking very shocked.

"Oh. Well then you just seem to be very lucky. Surviving time travel and the Retriever. Well I hope your luck doesn't run out." He tells you crossing his fingers.

"Me too. Well do you know how I can get out of here or where I can find another time machine to take me back home?"

"Well like I said I thought that they dismantled all of them. I don't think there are any in existence anymore."

A man comes running over saying "Are you talking about time machines. I think there still is one over in the old warehouse. We saw something that resembles what you described. Would you like us to take you to it?"

# Notes

## Emotion

What is it?

Depression, dispair

## Idea

Blake a risk taker ex marine who meets Jake a volunteer in a research project accidently sends him to a future that is disasteriuos with him trying to survive.



## Outline

- Blake is looking around and sees a world that is destroyed 
- Not sure what is going on
- All of a sudden gunshots are heard
- Blake reacts to find shelter
- He looks around for a weapon 
- His combat training has kicked in
- All of a sudden he hears something behind him and feels a pinch in his neck
- He turns around to see a large robotic structure and then he passes out
- 
- Show a memory from Blake
- Meet Jack someone Blake new a long time ago
- Chance run in at a bar
- Get to chatting and drinking
- Little tipsy Jack decides to show Black his research
- The head into research space with a wierd contraption
- Jack starts to explain what it is. 
- Blake doesn't believe
- Jack shows him by sending him 5 minutes into the future
- Blake hears some noise and then all of a sudden it stops
- He looks up and Jake is gone
- Blake yells where are you?
- Jake stand up from behind the console.
- What happen.  Nothing you just jumped ahead 5 minutes.  I had to wait
- Really...
- Jake says look at your watch.
- Jakes watch is 5 minuts faster.
- No way.
- Do it again
- Jack sets it up again and triggers it
- Being a little tipsy he must have screwed up
- 


- Blake is a ex combat marine who is suicidal and depressed.  
- Recently returned from active duty and not sure how to aclemate back into society
- Has no one and is a risk taker by nature
- Drinking at a bar a random stranger starts chatting him up
- By the end of hte night they are best friends
- They leave the bar and new freind Jake takes him to a warehouse owned by a big company
- Jakes shows Blake what he's been tasked with 
- An experiment to do time travel
- Both a little to drunk to be here 
- Blake sites in the cockpit 
- Jake is explaining more about the experiment
- Blake is interest and starts to convince Jake to do a simple run.
- After a little convinsing, Jake says sure and say we'll send you a minute ahead
- Jake does but this time when Blake looks up the build he was in is now gone
- Blake was sent to far into the future
- World is destroid


## Thoughts

Emotion main focus
Engaging first line/paragraph
Try to raise tension with every scene
Character driven
Character has changed or the read's understanding of char changed
Character through specific with unique traits
