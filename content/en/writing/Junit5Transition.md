---
title: "Transitioning to JUnit 5"
description: "A highlevel overview of moving from JUnit 4 to JUnit 5"
date: 2020-11-14T19:00:00+05:00
draft: false
enableToc: true
---

## Transitioning to JUnit 5

Any software development activity is not worth doing if testing isn't part of it.  With modern development practices, building small, simple applications is critical to keep complexity down. 

Frameworks, like JUnit, have emerged to help support simplifying unit testing.   JUnit uses a basic assert model to prove something works.  In the assert model, there are methods to handle every type of case like true/false, null or not, equality, same, etc.

JUnit has been around for a very long time (it was first released in 2002), and it is one of the most-used unit test frameworks today.  Therefore many applications have it in place to do their unit testing, but it may be based on the older version.

This may mean the product sticks with an older version.  Staying on something old is usually not a good option over the long term.  The goal of this is to highlight what's new and how to convert to the new version.

### JUnit 5

JUnit 5 is a major new edition of the popular unit test framework.  This is a major update for a few reasons.  The biggest reason is it is now moving forward and only supports Java 8 and above.   The backward portability of JUnit to older JRE versions has been a major blocker for JUnit to take advantage of new language features.

The other area that has changed is the new package naming structure.   Moving from *org.junit*, the new package name for the api is at *org.junit.jupitor.api*.  This was done to allow JUnit to support different parts of the JUnit eco system and different versions of the framework.

The next area to change is the renaming of some core annotations to help make them more descriptive.  The following are a few items that have changed that are usually the most commonly used.

- @Ignore -> @Disabled
- @BeforeClass -> @BeforeAll
- @AfterClass -> @AfterAll
- @Before -> @BeforeEach
- @After -> @AfterEach

There is a lot more to JUnit 5, but the goal is to focus on integrating JUnit 5 into a project that is built on an older version of JUnit.

### Old Project

An old project of mine is a Weather Archiver that I wrote to collect weather data for my area and record it every 15 minutes.  This is a micro-service built using Spring Boot, but it has a collection of JUnit 4 tests.   The goal will be to take this service as is and integrate JUnit 5 into it.   This will then show how it can be integrated with very little change to existing tests and then can offer a path forward for leveraging JUnit 5.

### Updating the Dependency

The first step is to update the project to use JUnit 5 which is easy as changing the dependency in the pom.

```xml
<dependency>
   <groupId>junit</groupId>
   <artifactId>junit</artifactId>
   <version>4.13</version>
   <scope>test</scope>
</dependency>
```

Becomes

```xml
<dependency>
   <groupId>org.junit.jupiter</groupId>
   <artifactId>junit-jupiter-engine</artifactId>
   <version>5.7.0</version>
   <scope>test</scope>
</dependency>
```

Now lets build our project.

```
mvn clean package
```

That worked better then expected.  Everything still builds (no api change) and all the test still pass.  At this point JUnit 5 is integrated into the project and we didn't have to update anything but a dependency.   JUnit is fully backward compliant in this case.   

### Change over an existing test

Since no changes were required, let's begin by modifing and existing test to just get our feet wet with JUnit 5.

The old test for the controller's convert method:

```java
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;

public class WeatherRetrieverTest {

  private WeatherRetriever retriever;

  @Before
  public void setUp() throws Exception {

    retriever = new WeatherRetriever();
  }

  @Test
  public void testConvert() {
    final OwmWeatherReport report = new OwmWeatherReport();

    final WeatherReportEntity entity = retriever.convert(report);

    assertNotNull(entity);
  }

}
```

To update this we'll change the imports, update the *@Before* annotation and use the new api for *assertNotNull* and others.

```java
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class WeatherRetrieverTest {

  private WeatherRetriever retriever;

  @BeforeAll
  public void setUp() throws Exception {

    retriever = new WeatherRetriever();
  }

  @Test
  public void testConvert() {
    final OwmWeatherReport report = new OwmWeatherReport();

    final WeatherReportEntity entity = retriever.convert(report);

    assertNotNull(entity);
  }
}
```
As demonstrated above, the imports changed to the new package name and the @Before annotation become @BeforeAll.  No other changes were required to make this function under JUnit 5.  That can't be any easier.

### Finally

As this illustrates, changing over to JUnit 5 can be as easy as updating a dependency. For any project, this should be the minimal step and should be as simple as shown unless more advanced features are used.

After integrating JUnit 5, all new tests should be writen using it.   Typically it's not worth converting older code to JUnit 5 unless there is something that requires it.   But switching to JUnit 5 should be done so that new features can be leveraged in the future.

If more information is required or you want to dig into the new features, JUnit publishes a very helpful guide with all the details necessary to start taking advantage of the improvements available.

[JUnit 5 User Guide](https://junit.org/junit5/docs/current/user-guide/)
