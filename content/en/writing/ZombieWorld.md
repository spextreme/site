---
title: "Zombie World"
description: ""
date: 2020-11-01T14:09:21+09:00
draft: true
enableToc: false
---

In the year 2035 a biological outbreak caused the dead to rise and the world changed forever.   Shortly after that the world's economy crumbled and all systems collapsed.   The governments of the world tried to set up safe zones but they didn’t succeed at first because of the panic and fear of the people.  Most reverted to basic instincts and the fighting and animal nature caused more harm than good. 

Outline
- Year 2035 zombie apocalypse outbreak
- Out of the ashes rose a new world
- Safe zones
- Companies focused on safety and security
- Cities no longer exist
- All people know hand to hand combat and weapons for defense
- Main Char works for the focus company
- Discovers something

