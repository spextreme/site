---
title: "Untitled - Jasmine Taylor"
description: ""
date: 2021-06-22
draft: true
enableToc: false
---

## Just Write

Jasmine Taylor (Jaz) is 26 years old, intelligent woman with a PHD in bio tech engineering and physics.  Very confident and physically fit due to her love of martial arts.  Was severely injured at 16 in a car accident where she was thrown from a car and it exploded, killing her mother and sister.  She was severely injured and was left with a physical disability and amnesia.  She was hospitalized for 9 months in a comma.   The martial arts was her way to regain here physical abilities and full recovery.  She doesn't remember much of her life before the accident and was raised by her father.  Good relationship with her father.  Doesn't remember much about her mother and sister other then what her father has told her and pictures she's seen.

Now ten years later, she is working for a company called Nova Space which has a corporate run space station.  In college, she and here best friend Erica conceived a concept to create a living ship with advance Artificial Intelligence. Her and Erica's, PhD thesis on Artificial Intelligence (AI) and Jaz's PhD in connecting living cells with technology provided the basis of this project.  Jaz and Erica were hired by Nova Space to developer there thesis into a reality which became Nova Space's Nova S project. They are very close to completion and have been holding many test flights, demonstration, and press conferences.

Jaz and Erica have been the star employee for the corporation because the Nova S project.   The company knows this project will put theme way ahead in the market for many things included this new space craft they have been developing.  In fact many sub-projects have already show success in the market leveraging aspects of the research. 

She and Erica are often working late with very little oversight.  They have the space craft, which they call Shadow, nearly complete and the AI system is functional but adjustments and testing are still on going.  Their current challenge point is binding the AI into the bio-tech Jaz developed as that interface between them has proven more challenging. At this point they have the AI working independent of the ship integration.  Overall the challenge has been its to massive for Shadow.

One evening, Erica and Jaz finally had a revelation to simplify the interface by create smaller isolated interfaces for subsections of the interface.  This way they can work independently reducing the amount of data required. To achieve this Jaz begins the growing phase of these new parts while Erica works on reducing the interface to these new parts.  The growth process is going to take a few hours and they are really excited so they head out to grab dinner and plan to come back later to check everything out.

They head to their normal hang out on the space station, Its called Lucky Jack's, which is a unique place because it has a restaurant and bar, but also has billiards, darts, arcade games and a section to throw knife and ax.   Being on a space station people needed ways to take out frustration and the owner, Jack "Rabbit" McQuaid, sold the corporation on the concept.  Jaz and Erica loved this place but they also have become really close with Jack.  He is like a second father to both of them, always looked out and providing them with much needed wisdom.  Jack Rabbit as the call him is an older man who never had the time for kids but in these two he has grown a real close attachment.

They walk in and head up to the bar and chat with Jack who pours them each a drink.  After a few minutes they grab their drinks and find an open table near the knife throwing section which is their normal spot.  One of the regular waitresses head over chat with them and takes their order.

A short while later, they have finished eating when they notice a few guys throwing knives and they begin to watch as one of them notices and another barely hits the target.  He says something to his friends as they each look.  Jaz not being shy doesn't even turn away.   One of them makes a comment about not being able to do much better.   Jaz looks at Erica who just shakes her head.   Jack is watching what's happen and also just smirks knowing what is about to happen.

Jaz and Erica stand up, walks over and take the knifes from the hand of the guy who made the comment.  He steps back to give her room, as she says "Lets make it interesting" as she reaches for the bowl of peanuts from the table.  She holds the bowl in front of one of the others and says throw a handful.    He looks surprised and confused but says OK.  He grabs a handful and tosses them upward toward the target and before he can even lower his arm he sees 3 peanut pinned to the wall with the knives Jaz was just holding.   The guys all look in disbelief as Jaz says I think you owe us a round.

At the end of the evening Jaz and Erica say goodnight and head back to the lab.  It was a fun break and had gotten a little later then they planned but they wanted to check the progress of their solution and see if they could get it install and try it out. They get back to the lab and check over everything.  The growth rate was good and Erica check over her code.  The begin to install it and after a few more hours they have Shadow working.

They run some more tests and finish things out for the night.  They shoot off a message to the team about the progress and that would like to show it off tomorrow after running some more diagnostics and tests.   Everything is all set for that afternoon.

The next day everything goes well with the demonstration.  The business types are thoroughly impressed.  They start making plans for the how they are going to capitalize from Shadow.  There are a lot of people hanging around and congratulating them.  Things are looking really good.   Finally as the crowd dies down they start to cleanup from the day.   The check over the system and lock everything down.   Jaz has an uneasy feeling but she rights it off as just feeling like the project is coming to an end with it mostly complete.  Jaz and Erica say goodnight to Shadow and head out for the night.  Like usual they end up at Jack's and tell him all the success.  They are exhausted so they leave early and head back to their apartments. 

The next morning, Erica knocks on Jaz's door and she comes in while Jaz is getting ready.   Jaz starts to tell Erica she had the craziest dream about Shadow.  It was like she was in Shadows mind and seeing how everything worked.  It was so odd cause it was like I could feel everything Shadow felt.   Shadow was scared but not because I was there but something else.  We then left the hanger and started flying around and Shadow was then calm and peaceful just floating in space.  Beside the feeling of Shadow I thought it was awesome cause it was like I was one with Shadow.  Feeling that rush of speed and void of space.

Erica listened but really wanted to get moving.  She pushed Jaz a little to hurry up now that her story is done.  They finally head out and get to the entrance but there are a lot of security and their boss is there.  They head over to find out what is going on and their boss explains that Shadow is gone.  They aren't sure what happen or how but someone must have stolen her.  Jaz and Erica can't believe it but security comes over and wants to speak with them.

After a couple of hours, they are finally free to start figuring out what happen on their own.  Security lets them into the lab and they start looking into things.  Erica asks Jaz if she thought her dream had anything to do with it.  You said Shadow was scared maybe you felt that.  Jaz writes it off.

After looking over some data they finally leave and head to Jack's to consult with him.  Jack can tell they are very upset.  He offers some advice and suggestions and tries to reassure them that it will work out.
 
While they are talking Erica says something to Jaz and she doesn't respond.  Erica looks at her and instinctively notices something is wrong.   Jaz, Jaz, Jaz, she says as she shakes here.  Erica looks at Jack and then back and Jaz.  She just has the blank stare like no one is home.  Erica is really concerned, and then all of a sudden, Jaz stands up says, Come to Shadow.   She then grabs a few knives from the bar, and begins to walk out.

Erica is chasing Jaz as she heads toward a transfer shuttle.  Erica follows her in as she starts to strap her self into the pilot chair.  Erica sits beside her and says where are we going.   Won't we get in trouble for taking this out.  Jaz doesn't respond and starts it up and takes off into space.

In the big black void of space, Erica is feeling confused and scared not understanding what her friend is doing.  Jaz is instantly focused and Erica can't seem to break into her thoughts. 

Erica is amazed but she see Shadow off in the distance.  Jaz pilots the ship to dock with Shadow.   As she shutdown the craft, she all of a sudden turns to see Erica and say "What happen...where am I"

Erica looks at her confused but says we just docked with Shadow. How did you know where she was?  Jaz replies with "I have no idea.  Last thing I remember is being at Jack's"

They get off the pod and into the bridge.  Erica connects in and starts looking over information.  The start trying to understand what happen.  Asking Shadow what happen.

Shadow is concerned and shows the outside, where there is a ship attacking another.   "Shadow must help", Shadow says as she starts to move toward the conflict.  Erica begins to protest.   Jaz seems to still be feeling Shadow and understands.   She says "Shadow we can't just run in there without a plan.".  Shadow slows and says "Plan?  Shadow has a plan".  Erica is completely lost and not sure how Jaz can be so calm.

Just then the other craft is stopped by the other ship.  "Shadow must go now..." and she starts moving faster toward the conflict.  At this point they can see the ship that was attacking is a pirate ship and the other seems to be a hauler.   Jaz attempts to explain but Shadow won't hear it.  As Shadow gets closer a shot is fired from the pirate ship and Shadow attempts to evade but it grazes the side of Shadow.   As they get closer, Shadow hides behind the hauler tucks up next to it. Shadow extends a connection tube to the hauler and makes a connection.   All of a sudden Jaz hears Shadow in her head.  Jaz exclaims what the hell. Erica looks at her and doesn't understand.  Jaz looks at Erica and say she can hear Shadow in her head.  Shadow says "We are same."   Jaz says what does that mean but Shadow doesn't answer.

Shadow tells Jaz to go look in the cargo bay. Jaz starts to leave an Erica asks where she's going.  Jaz answers and then Erica and her head to the cargo bay.   In there is a set of crates.   Jaz finds once with weapons of here liking, knives.  She avoids the guns and takes some knives and gears up.  Erica asks what she is doing.   Jaz explains that Shadow isn't going to give up till something is done so she's going to move this forward.  Erica says that is crazy but she knows Jaz.  Jaz starts to head to the docking port and she tells Erica to head back to the bridge.  Jaz says something else but Erica can't make it out.  

Jaz enters the hauler and Shadow is giving her details of the ship.   She asks how Shadow knows and Shadow explains that she got into the computer and can access any of the information available.  She gives Jaz the lay of the land and where people are.  As Jaz turns the corner she sees a group of the pirates and decides to chose and alternate path.  As she makes her way she finally reaches the bridge and asks Shadow to release the door.  The door unlocks and she stands to the side as she starts to open it.   A shot is fired and ricochet off the steel of the hull.  She then finishes open the door.   She asks them not to shoot and says she is here to help.  With her hands raised she peaks in the doorway and the guy with the gun is holding steady.  She explains that she isn't with the pirates and has a separate ship that they can escape on.  The crew captain says they can't leave but to take his crew.  She explains that her ship isn't going to like that option.  The crew captain seems confused but doesn't ask. She says she'd like all to go but the captain persists.  She looks at the crew who are also trying to convince him to leave.  She says last chance and starts to lead them back the way she came.

As they are heading out, they hear voices from the pirates heading toward the bridge.  Jaz starts to lead the crew in the opposite direction.  As they head toward the connection port, they hear the captain scream and then a gunshot. The continue and then come to a corner where Jaz peaks around it and sees 2 pirates.  She tells the crew to pause and pulls out one of the knives from her belt.  She tells the guy closes to wait here.

She stands up turns the corner and throws the knife.  She then takes off running as the butt of the knife clocks the first pirate in the back of the head and he falls to the ground conscienceless.   As the other pirate realizes what is happening and turns, Jaz throws a kick knocking him off balance and then punches him as he falls to the ground unconscious.  She then calls for the crew to get moving.  They reach the port and everyone heads into Shadow.   Shadow disconnects and starts to head out.  Erica helps guide the people to the mess hall to give them a place to relax.  

Erica comes out to find Jaz and starts grilling her on what is going on.  Jaz tries to explain but she isn't very sure what is going on either or how Shadow can speak to her almost like telepathy.  Erica can't understand but they decide to head to the bridge.

They try to talk to Shadow to get an understanding of what is going on and why she is here.  Shadow doesn't response.  Erica asks more questions but Shadow doesn't respond and Erica getting frustrated with each passing question.   She looks at Jaz but still getting no where.   Jaz then says, "Shadow, what did you mean when you said 'We are same'?"

"We Are Same" Shadow replies. Jaz still is confused.  "This how connect with you."

Jaz asks what that means.   Shadow isn't making sense but Jaz really would like to understand.  She looks at Erica who look as lost as Jaz.  Jaz wonders what Shadow could mean. 



Big reveal.  Jaz is a android father built because she died in the car accident and the ship she built is her "child" because she can't have any but didn't know why.   All subconscious.


## High Level outline

- Jaz in car accident no memory before it
- Jump ahead to working for space corp
- Friend and her build a living ship from doctoral thesis
- Living ship disappears
- Draws Jaz to ship
- Erica tags along
- Head into conflict
- Rescue most (captain stays)
- Escape conflict 
- Shadow acts like child
- Take crew to a port
- Head back to corporation
- Find out corporation plans to misuse Shadow
- Jaz figures out she is like shadow (BioTech)
- Needs to reach out to father
- Have to head back to corp
- Talk to father to get details
- Get corporation off back (somehow)
    + Legal action?
    + Corporation does lot of illegal stuff
        * Shadow collected a lot of data
        * Jaz knows what Shadow has
        * Corp owned/ran pirates to steal other corp tech
        * Plan to use Shadow for illegal purposes
        * Was going to use Shadow for espionage
        * Lets her keep the ship if she signs NDA
- She heads off with Shadow, and Erica


A beautiful day after a fresh rain has fallen. Jaz is enjoying the ride with her mother down a winding stretch of road on the mountains edge.  They were headed to town to pick up a few things for the week and were not far from their home.  As they are talking, her mother glides around a bend and a deer jumps out into the road.  Jaz braces in her seat, as her mother attempt to swerve to miss the deer.  They feel the car start to slide on the dampness of the road and it hits the edge and begins to roll down the mountain side.  Jaz feels he body being pulled to the left then upside down with the seat belt strap digging in on every direction. As a full rotation of the car completes her head smashes against the window as the car slams against a tree and she blacks out.

"Mom" she thinks as she opens her eyes to see the ceiling of her bedroom.  This is the memory of the day her life changed forever.  Her mother died, and she had a long road of recovery.  She has no memory from before the accident and her body was beat to a pulp requiring her to fight her way back.  Her father helped her the whole way and got her into exercising, martial arts and weapons.  She used these skills to get strong and become the confident force she is.

Jasmine Taylor got her nickname Jaz in college from her best friend Erica.  They met when they had to work on a project in their freshman year and have been friends ever since.   Erica was studying Artificial Intelligence (AI).

Jaz was never one to sit still.  At the start of college she studied everything she could.  Learning came naturally to her and she found it very difficult to stick to one major.  This caused her to get a degree in biology, and engineering with a minor in computer science.  She would have majored in it also but other things got in the way.

For their graduate studies, she and Erica came up with a doctoral thesis that compliment each other passions.   It focused on using technology and AI to create a living bio-mechanical entity.  Jaz thesis was on the marriage of technology and biology while Erica was focus on using advanced AI to control living tissue.  This strengthen their friendship and also got the attention of some powerful corporations.

Jaz and Erica both started working for Black Box Industries after completing their doctoral programs.  They couldn't believe the opportunity, and this gave them a way to create what they theorized in their thesis.

After two years they had moved to one of the commercial space stations that Black Box and other large organizations used for research and development.  Life on a space station was an unbelievable experience for them.  They had everything they could desire in a research facility and awesome apartments to live in.  Since this was a collection of commercial space stations, there was a thriving environment with nightlife and travel.  When the girls weren't working they could travel to the other space stations, Mars and the Moon.  The night life also let them meet others who are living up here.

Their favorite place was a bar called "Jack Rabbit's" owed by Jack "Rabbit" McQuaid.  He was the first to setup in space to give people an escape from life up here.  He setup a restaurant and bar that also house billiards, darts, arcade games and a knife/ax throwing.  This is one of the main reasons Jaz enjoyed this place, besides it was hosted on their space station so they could easily get to it.

With their frequent time spent there, Jack and the girls became very close.  The girls think of Jack as a second father. Jack never had children but these two were like daughters to him.

One defining moment that sealed it for Jack, was an early event when some guys were throwing knives and Jaz and Erica were watching them.  They guys noticed the girls watching them.  The guys trying to act skilled, start talking to the girls.   One of them underestimating Jaz, bets her she couldn't hit the target.  Erica smirks, as Jaz stand up, walks over and takes the knifes from one of the guy.  "Lets make it more interesting" as she reaches for a bowl of peanuts on one of the bar tables.  She holds the bowl in front and says to the guy, "Throw a handful up toward the target.  I bet you drinks, I can put a peanut on the bulls-eye".  They guys look at each other thinking there is no way.  One of them grabs a handful and tosses them upward toward the target and before he can even lower his arm he sees 3 peanut pinned to the wall all tied very tightly to the bulls eye.   The guys all look in disbelief as Jaz says "I think you owe us a round."  Jack's been impressed with Jaz's confidence and skill ever since.

As Jaz gets up and starts her day, she steps to the window and admires her view which shows a section of the space station looking out toward the darkness of space.  She can see a planet off in the distance and thinks to herself how luck she is to be here.  She heads to take a shower and get ready for the day.  Emerging from her apartment she heads down the hall to go get Erica.  She knocks on the door, it opens and she enters.  Like normal Erica is running around her apartment getting ready.  "I'll be ready in a minute", Erica announces.  After a few minutes, Erica and Jaz start heading to their lab.

Their lab is more like a warehouse then a lab.  The small door into the area is dwarfed by the immense space it leads into.  With as large as this space is, its filled with the space craft that was designed and built by the pair.  This ship has been the work of the last two years, trying to create a living ship they designed in their thesis.  The living ship, which they call Shadow, is a engineering and biological marvel.  Jaz has been able to create a method to combine living tissue with technology and has been able to grow most of the parts of Shadow.  Shadow is also made from an advanced metal alloy that comprises the shell of the ship that the parts grow into.   Erica's artificial intelligence has given life to Shadow pulling from massive amounts of data and a complex collection of algorithms to give Shadow the ability to learn, think and even feel.

After two years at Black Box building out their thesis, they are finally reaching the point where Shadow is ready for the public. Today they will be showing Shadow to the organization to show how Shadow has grown and learned.  Many in the organization are well aware of the Shadow project and of Jaz and Erica.  Because of this they will have a large showing for this unveiling.

By the end of the day, everyone in the organization is talking about Shadows and what the girls have achieved.  Many offered their congratulations and are amazed at what they are seeing.  The business has been using some of the technology along the way in many other applications but the creation of Shadow surpassed everyone expectations.  Finally the lab is empty, Erica and Jaz grab there things and head to Jack's to grab something for dinner.  They were still wired from everything.

At Jack's, they tell him about the success and he just absorbs it all.  Happy for them and thrilled they are so successful in their dreams.  They eat dinner and have a few drinks.  They decide to head out.  Jaz says good night to Erica, and heads to her apartment.  Once there she sends a message to her dad back on earth to give him the news who promptly calls her.  Finally done talking with her father, she relaxes for a few and then heads to sleep.

--

"Shadow, what are you doing?" Jaz asks without speaking.  "Leaving" she hears in her mind.  Shadow begins to move out of the lab and into space.  The lab bay door is open and Shadow zooms out into the vastness of space.  "Where are you going" she asks again.  "Leaving...Not Safe".  Jaz feels the calm and peaceful of just floating out into space.  All of a sudden, there is this rush as Shadow accelerates into the void of space.

The next morning, Jaz grabs Erica and heads to the lab. As the enter the lab they are horrified to see Shadow is gone.

They look at each other in disbelief and immediately call security and every other person the can think of.  As they wait Jaz mentions the dream she had.  Just then a few people and security walk into the lab.  Seeing the empty area where Shadow should be they start to ask the girls questions.

"When was the last time you saw Shadow?" asks one of the security guards.

"We were here until about seven last night and everything was fine." replies Jaz.

"Who could have taken Shadow?" asks Erica

All of a sudden a guard comes in with a device.  He confirmed they left at 7:02pm but no one was in here between then and when the ship left.  The ship appears to just leave." says the security guard.  They all watch the screen as it shows the doors opening and Shadow leaving.  The lead guard asks a few more questions and scour the area.   The excuse the girls who say they will see what they can find.  The girls tell their boss they are going for a walk. 

They leave the lab and just start aimlessly walking. Jaz explains the details of her dream to Erica and how it feel like she and Shadow were sharing a conscientiousness.  She felt Shadow was concerned and felt trapped.  She couldn't understand what this all meant and why she would have the dream. It felt so real and so weird she couldn't understand it.  The finally find a place to sit down.




A short while later, they have finished eating when they notice a few guys throwing knives and they begin to watch as one of them notices and another barely hits the target.  He says something to his friends as they each look.  Jaz not being shy doesn't even turn away.   One of them makes a comment about not being able to do much better.   Jaz looks at Erica who just shakes her head.   Jack is watching what's happen and also just smirks knowing what is about to happen.

Jaz and Erica stand up, walks over and take the knifes from the hand of the guy who made the comment.  He steps back to give her room, as she says "Lets make it interesting" as she reaches for the bowl of peanuts from the table.  She holds the bowl in front of one of the others and says throw a handful.    He looks surprised and confused but says OK.  He grabs a handful and tosses them upward toward the target and before he can even lower his arm he sees 3 peanut pinned to the wall with the knives Jaz was just holding.   The guys all look in disbelief as Jaz says I think you owe us a round.

At the end of the evening Jaz and Erica say goodnight and head back to the lab.  It was a fun break and had gotten a little later then they planned but they wanted to check the progress of their solution and see if they could get it install and try it out. They get back to the lab and check over everything.  The growth rate was good and Erica check over her code.  The begin to install it and after a few more hours they have Shadow working.

They run some more tests and finish things out for the night.  They shoot off a message to the team about the progress and that would like to show it off tomorrow after running some more diagnostics and tests.   Everything is all set for that afternoon.

The next day everything goes well with the demonstration.  The business types are thoroughly impressed.  They start making plans for the how they are going to capitalize from Shadow.  There are a lot of people hanging around and congratulating them.  Things are looking really good.   Finally as the crowd dies down they start to cleanup from the day.   The check over the system and lock everything down.   Jaz has an uneasy feeling but she rights it off as just feeling like the project is coming to an end with it mostly complete.  Jaz and Erica say goodnight to Shadow and head out for the night.  Like usual they end up at Jack's and tell him all the success.  They are exhausted so they leave early and head back to their apartments. 

The next morning, Erica knocks on Jaz's door and she comes in while Jaz is getting ready.   Jaz starts to tell Erica she had the craziest dream about Shadow.  It was like she was in Shadows mind and seeing how everything worked.  It was so odd cause it was like I could feel everything Shadow felt.   Shadow was scared but not because I was there but something else.  We then left the hanger and started flying around and Shadow was then calm and peaceful just floating in space.  Beside the feeling of Shadow I thought it was awesome cause it was like I was one with Shadow.  Feeling that rush of speed and void of space.

Erica listened but really wanted to get moving.  She pushed Jaz a little to hurry up now that her story is done.  They finally head out and get to the entrance but there are a lot of security and their boss is there.  They head over to find out what is going on and their boss explains that Shadow is gone.  They aren't sure what happen or how but someone must have stolen her.  Jaz and Erica can't believe it but security comes over and wants to speak with them.

After a couple of hours, they are finally free to start figuring out what happen on their own.  Security lets them into the lab and they start looking into things.  Erica asks Jaz if she thought her dream had anything to do with it.  You said Shadow was scared maybe you felt that.  Jaz writes it off.

After looking over some data they finally leave and head to Jack's to consult with him.  Jack can tell they are very upset.  He offers some advice and suggestions and tries to reassure them that it will work out.
 
While they are talking Erica says something to Jaz and she doesn't respond.  Erica looks at her and instinctively notices something is wrong.   Jaz, Jaz, Jaz, she says as she shakes here.  Erica looks at Jack and then back and Jaz.  She just has the blank stare like no one is home.  Erica is really concerned, and then all of a sudden, Jaz stands up says, Come to Shadow.   She then grabs a few knives from the bar, and begins to walk out.

Erica is chasing Jaz as she heads toward a transfer shuttle.  Erica follows her in as she starts to strap her self into the pilot chair.  Erica sits beside her and says where are we going.   Won't we get in trouble for taking this out.  Jaz doesn't respond and starts it up and takes off into space.

In the big black void of space, Erica is feeling confused and scared not understanding what her friend is doing.  Jaz is instantly focused and Erica can't seem to break into her thoughts. 

Erica is amazed but she see Shadow off in the distance.  Jaz pilots the ship to dock with Shadow.   As she shutdown the craft, she all of a sudden turns to see Erica and say "What happen...where am I"

Erica looks at her confused but says we just docked with Shadow. How did you know where she was?  Jaz replies with "I have no idea.  Last thing I remember is being at Jack's"

They get off the pod and into the bridge.  Erica connects in and starts looking over information.  The start trying to understand what happen.  Asking Shadow what happen.

Shadow is concerned and shows the outside, where there is a ship attacking another.   "Shadow must help", Shadow says as she starts to move toward the conflict.  Erica begins to protest.   Jaz seems to still be feeling Shadow and understands.   She says "Shadow we can't just run in there without a plan.".  Shadow slows and says "Plan?  Shadow has a plan".  Erica is completely lost and not sure how Jaz can be so calm.

Just then the other craft is stopped by the other ship.  "Shadow must go now..." and she starts moving faster toward the conflict.  At this point they can see the ship that was attacking is a pirate ship and the other seems to be a hauler.   Jaz attempts to explain but Shadow won't hear it.  As Shadow gets closer a shot is fired from the pirate ship and Shadow attempts to evade but it grazes the side of Shadow.   As they get closer, Shadow hides behind the hauler tucks up next to it. Shadow extends a connection tube to the hauler and makes a connection.   All of a sudden Jaz hears Shadow in her head.  Jaz exclaims what the hell. Erica looks at her and doesn't understand.  Jaz looks at Erica and say she can hear Shadow in her head.  Shadow says "We are same."   Jaz says what does that mean but Shadow doesn't answer.

Shadow tells Jaz to go look in the cargo bay. Jaz starts to leave an Erica asks where she's going.  Jaz answers and then Erica and her head to the cargo bay.   In there is a set of crates.   Jaz finds once with weapons of here liking, knives.  She avoids the guns and takes some knives and gears up.  Erica asks what she is doing.   Jaz explains that Shadow isn't going to give up till something is done so she's going to move this forward.  Erica says that is crazy but she knows Jaz.  Jaz starts to head to the docking port and she tells Erica to head back to the bridge.  Jaz says something else but Erica can't make it out.  

Jaz enters the hauler and Shadow is giving her details of the ship.   She asks how Shadow knows and Shadow explains that she got into the computer and can access any of the information available.  She gives Jaz the lay of the land and where people are.  As Jaz turns the corner she sees a group of the pirates and decides to chose and alternate path.  As she makes her way she finally reaches the bridge and asks Shadow to release the door.  The door unlocks and she stands to the side as she starts to open it.   A shot is fired and ricochet off the steel of the hull.  She then finishes open the door.   She asks them not to shoot and says she is here to help.  With her hands raised she peaks in the doorway and the guy with the gun is holding steady.  She explains that she isn't with the pirates and has a separate ship that they can escape on.  The crew captain says they can't leave but to take his crew.  She explains that her ship isn't going to like that option.  The crew captain seems confused but doesn't ask. She says she'd like all to go but the captain persists.  She looks at the crew who are also trying to convince him to leave.  She says last chance and starts to lead them back the way she came.

As they are heading out, they hear voices from the pirates heading toward the bridge.  Jaz starts to lead the crew in the opposite direction.  As they head toward the connection port, they hear the captain scream and then a gunshot. The continue and then come to a corner where Jaz peaks around it and sees 2 pirates.  She tells the crew to pause and pulls out one of the knives from her belt.  She tells the guy closes to wait here.

She stands up turns the corner and throws the knife.  She then takes off running as the butt of the knife clocks the first pirate in the back of the head and he falls to the ground conscienceless.   As the other pirate realizes what is happening and turns, Jaz throws a kick knocking him off balance and then punches him as he falls to the ground unconscious.  She then calls for the crew to get moving.  They reach the port and everyone heads into Shadow.   Shadow disconnects and starts to head out.  Erica helps guide the people to the mess hall to give them a place to relax.  

Erica comes out to find Jaz and starts grilling her on what is going on.  Jaz tries to explain but she isn't very sure what is going on either or how Shadow can speak to her almost like telepathy.  Erica can't understand but they decide to head to the bridge.

They try to talk to Shadow to get an understanding of what is going on and why she is here.  Shadow doesn't response.  Erica asks more questions but Shadow doesn't respond and Erica getting frustrated with each passing question.   She looks at Jaz but still getting no where.   Jaz then says, "Shadow, what did you mean when you said 'We are same'?"

"We Are Same" Shadow replies. Jaz still is confused.  "This how connect with you."

Jaz asks what that means.   Shadow isn't making sense but Jaz really would like to understand.  She looks at Erica who look as lost as Jaz.  Jaz wonders what Shadow could mean. 



Big reveal.  Jaz is a android father built because she died in the car accident and the ship she built is her "child" because she can't have any but didn't know why.   All subconscious.