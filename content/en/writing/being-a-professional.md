---
title: "Professionalize the Software Developer Role"
description: "Increase the quality of the software development profession."
date: 2021-07-06
draft: false
enableToc: false
---

## Software Developer

I've been a software developer for a couple of decades and one thing that has always felt odd is how people seem to treat software development as “anyone can do it”. With software being a major part of our life, languages being easier to write and tools to assist, it seems like this belief is being perpetuated. While it may seem like anyone can develop software, we need to work to change the perception and bring back the "professional" part of software development.

## Being a Professional

Think about a surgeon who spends a large percentage of their life in school and then an apprenticeship to learn hands on skills. After many years of this, they finally advance to doing surgery on their own and eventually may become a renowned surgeon. Even after achieving this, they continue to study, perform research and learn so they may continue to advance their skills and career.

Similarly think about a professional sports player. They work exceedingly hard to become the best in their sport to eventually make a professional team. Then they continue to work excessively hard to continuously improve and be the best against all others in the professional level.

Being a professional takes drive, determination, skill and a constant drive to improve. As with a surgeon or a professional sports player, they are still pushing to become better and try to become the top in their career.

## Software Developer

If you look at software development as a career, it seems to lack this level of professionalism. Many believe you can sit in front of a computer and simply start Googling for examples and playing around. While this is a good way to start it doesn't mean you’re a professional developer.

Getting a job as a developer may seem like the path to being a professional but this is where the misconception comes from. A professional doesn't simply mean you have a job for that skill.

There are many skills required to be a professional. Learning how to code is only a small part, but then there is the understanding of technology, theory of computers, advance algorithms, etc… Many may feel this is not important today, but how would you feel if you needed surgery and you surgeon thought biology wasn't important?

## Be Professional

Taking this into account, to really be a professional means that you have a deep understanding of coding, know the past and current technology, and the most important part, continue to work to advance your skills. Using those skills to work for a company to bring value into the business and for others around you to learn from.

If we use this concept as being a professional software developer, then simply coding and collecting a paycheck isn't truly a professional developer. This would be like playing soccer on the weekends and claiming you are a pro.

To truly be a professional software developer means you are working constantly to improve, advance, learn and adding value to the profession.

## Change

With the world being largely software based, we need to grow software developers into a mindset of being a professional. If you look at the major technology companies today, they want the best and brightest people possible ([Reed Hasting on the Rock-Start Principle](https://www.cnbc.com/2020/09/08/netflix-ceo-reed-hastings-on-high-salaries-the-best-are-easily-10x-better-than-average.html)). Older companies which didn’t start out in the software business, have a long way to catch up but they should expect the same.

If you want to write code and develop software, you should aim to be a professional. It is up to you to put in the work and become like a professional sports player or a surgeon. Don’t cut corners, continue to learn and lets change the way software developers are perceived throughout the industry.
