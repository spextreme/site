---
title: "Take on Success"
description: "Theories on how to become successful in your career."
date: 2021-06-27
draft: false
enableToc: false
---

## Take on Success

Everyone dreams of having a good job, making good money, and being happy. If you can do all these at once then life is perfect. In the 24 years I've been working, I feel like I've achieved these goals. I love my job and the place I work, I make a good living, and I'm pretty happy.

For many years I have been a mentor to a lot of new hires and junior employees because I really want to help them grow and be able to achieve success in the organization. This is probably why I also enjoy being a college professor. The people that I mentor often ask how to be successful at work or how I got to where I am.

To answer this question, it makes me think back to when I started my career a long time ago. Through that time, these are the things that I believe have helped set the stage for my success and I truly believe this will help others. I don't claim to know the secret sauce to a good career, but I do believe these things will help anyone improve and head toward success.

### Be Honest

This may seem easy, but over time your ego and opinions can cause you to stretch the truth. This may be because of a mistake and you're to proud to admit it or misinterpret something and push it to far. Whatever the reason, you should always own it, admit when you're wrong, and be overly honest.

As you work with people, they will learn this about you and it will increase their respect and trust in you. Having this trust and respect goes a long way. Realize that in a working environment, you have leaders or managers that you report to but won't work with regularly. Your teammates and other co-workers may be asked to provide feedback. Better yet, they may respect you so much, they offer feedback to others. This unsolicited promotion is always helpful to you.

For me, this has always been something my parents instilled in me as a child and to this day, it has served me quite well. I will be the first to admit when I make a mistake and will own that mistake. You learn more from making mistakes anyway. I have obtained the respect of many people over my career largely because of this trait.

### Never Say No

This doesn't mean you always answer 'Yes', but it does mean you have to answer more creatively. Often times, saying 'No' is the easiest answer and many gravitate to this before really thinking about a better response. Being someone who removes 'No' from their vocabulary will end up changing the way you think about responding.

The perception of this goes a long way because people will see you as someone they want to ask. Leaders will also notice this because it provides them a resource that they can rely on when things arise or critical issues need attention.

It also has a by product of answering people with a more detailed response when the answer can't be a simple 'Yes'. This will sometimes provide the person who asked, with details they may not have thought about or information they may use to better the situation.

On the flip side, saying 'Yes' has an affect on you because it will push you to situations you may not have thought you would want to attempt. It can challenge you and it will help you to conquer your fears. Fear drives us constantly to avoid situations which again 'No' is the easy way to evade them. But pushing yourself and taking those situations on, will help you grow, improve, and get past those fears. Fears are mostly caused because of what is unknown. Forcing yourself is always a good thing because of the challenges and as you do it more, you will become less fearful of future activities and will improve your skill set.

The people around you will also be impressed and will ultimately rely on you. This occurs because the perception and reality of this is that you provide guidance and assistance when asked. This is a good thing, but it may also have some drawbacks. I would argue it's still better to be the type of person people rely on, but it will mean, you may be over occupied and distracted by these requests.

During my career, I have always taken on roles that I may not have wanted to and become someone that many teammates rely on and look up to. As I mentioned, I mentor a lot of younger employees and this is why. I even have senior members who rely on me for many things because they know I will help. Since I have the skills to fit into many situations due to the roles I have taken on in the past and I try to never let my fears stop me.

### Push Yourself

If you get into a role and never challenge yourself, it will have two major affects. First you'll become lazy and eventually unhappy. The second is you'll be seen as unmotivated and falling behind.

Getting into a roll and learning it, is a challenging thing that may be unsettling, energizing, and exciting. Once you learn it, you will start to relax and can largely flow through without much thought. This then means you start to atrophy and loose motivation which is a bad thing. Especially if your goal is to continue to improve, grow, and become successful. Sitting still is not success so don't let that comfort blind you. Also, if everyone else is moving forward it makes it appear as if you're falling behind.

Staying motivated is difficult but if you constantly push yourself in your role and always look ahead this will help keep you energized and engaged. Pushing yourself is better because as you start to learn that role you are challenging yourself. Once you become comfortable, then it is time to change. If you can become comfortable with change, this will also have the opposite effect of become uncomfortable with the relax state.

In my career, I've always pushed myself into unfamiliar activities to keep me on my toes. This way I don't become stagnant and it has largely benefited me. I believe this comes from my inability to sit still but it has served me very well.

Don't go overboard here though. There are roles you may get into that once they become comfortable, still need you to be there, but its up to you to make sure they still challenge you. If you jump as soon as it becomes comfortable, people may perceive you as someone who is not dependable because you jump off of things to frequently.

### Never Stop Learning

From birth we start learning by observing and imitating. This is largely how we learned to walk and talk. Eventually we then expand on that by learning more words and more concepts until eventually we gain the skills that get us into the role that we desire. This doesn't mean we should stop learning because we achieved a goal. It does mean we should continue to learn new things so we can continue to grow and become better with many more opportunities.

Depending upon the career you are in, what you learn may vary greatly but its still very important to keep up on trends in your field. To do this, learning, experimenting, and practicing are still a large part of it.

In my life I use my free time to try and constantly learn new things. I research online, read books, and play with technology. In the world today, there is so much free information, there is really no reason to stop learning.

## Conclusion

As discussed, these key activities will help you become a better person and grow in your career. Overall these activities will help you to evolve, grow and become successful. Keep in mind, success is different for each person, so use these concepts to help guide you to your version of success. Make them your own and use them as they best suit you.
