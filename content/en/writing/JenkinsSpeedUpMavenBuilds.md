---
title: "Jenkins Optimizing for Speed"
description: "How to speed up maven Automated builds."
date: 2020-11-11T11:47:21+05:00
draft: true
enableToc: true
---

Removing extra logging
Other tricks for speeding up maven
Etc�

https://www.jrebel.com/blog/how-to-speed-up-your-maven-build

 

MAVEN_OPTS= -XX:+TieredCompilation -XX:TieredStopAtLevel=1 

mvn -T 1C install -- will use 1 thread per available CPU core

 

https://blog.sebastian-daschner.com/entries/analyze_maven_build_time 

<build>
    <extensions>
        <extension>
            <groupId>co.leantechniques</groupId>
            <artifactId>maven-buildtime-extension</artifactId>
            <version>2.0.2</version>
        </extension>
    </extensions>
</build>