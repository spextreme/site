<?xml version='1.0'?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  version="1.0">
  <xsl:template match="/">
  <xsl:for-each select="/booklist/book">
    <b><xsl:value-of select="@title"/></b><br/>
    <xsl:value-of select="summary"/>
    <table border="1">
      <xsl:for-each select="author">
        <tr>
          <td>
            <xsl:value-of select="@name"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>
    <hr noshade="true" />
  </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>