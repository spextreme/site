---
title: "College Failure"
description: ""
date: 2021-09-04
draft: true
enableToc: false
---

## 

Its been a long time coming, but my first daughter has entered college this fall.  While this is a joyous moment as a parent and an exciting time for my daughter, I have come to really loath the entire process.

My daughter deserves college.  She is highly intelligent, strong willed, confident and very motivated to learn.   I have no doubts that she will do very well in college and life. My biggest disappointment is that college will fail her.

College use to be a predigest thing that meant you worked hard and achieved something.  Now, it something that anyone can go to as long as you're willing to pay.  College has become big business and the price tag of many schools proves that.  Going into college for 4 years will having the expense of a 30 year mortgage.  This is ridiculous.  Why do we keep supporting this?

## Basics to Finance

I was taught at a young age to not spend more then you make.  A general roal of thumb is that you shouldn't be spending more then 1/3 of your salary on a mortgage.   But for college, people are charging 

