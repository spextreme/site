---
title: "Writing"
description: "My experiements in writing."
date: 2020-11-01T14:09:21+09:00
draft: false
enableToc: false
---

A section for things I’ve written or stuff I’m working on. This can include draft work and writing I’m currently experimenting on.
