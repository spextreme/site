---
title: "Plan to Constantly Change"
description: "Developing software solutions, plan to change or replace them from the start."
date: 2021-07-29
draft: false
enableToc: false
---

Have you ever had an idea that you turned into a reality? Maybe it was writing some code, building something with your hands, or even completing a homework assignment. We have all had something we thought was a great idea and ended up turning it into something real.
When you are done, you’re usually proud of the creation your idea gave birth to, and you usually learned something in the process. But from what you learned while constructing it, you also know how you can make it better, things you would change, improvements you could make, and mistakes you made that you would correct.

From here you take that idea and keep building onto it and learning more. Eventually you have a monolithic solution that you’re proud of, but you know all it’s faults, problem areas and things you wish you could redo.

Personally, I love construction and doing home remodeling projects. I recently gutted a house and redid most rooms (new kitchen, 3 new bathrooms, all new flooring and painted every room). After the 6 week project, I was finally done, and the place looked great but I knew all the mistakes and problem areas. It was my rental property so I was willing to live with the imperfections (caulk is a wonderful solution). Less then a week later, a waterline snapped and the entire first floor was flooded with 2 inches of water. I now had to rip out all the flooring I just put in and then spent the week drying out the place. Luckily, insurance came through and I got to redo the Pergo flooring. This time I bought a portable table saw. Man, did it make this flooring project so much easier (right tool for the job) and I really am unsure how I did the floor the first time without it. This was a blessing in disguise. After this was done, I can honestly say this time I didn’t make any of the same mistakes on the floor.

Bringing this back to software, this concept holds true but can usually be much simpler. Software is highly malleable and easy to create fresh or update. It’s also easy to experiment and the plethora of tools available make it even easier.

However, many organizations believe this to be untrue and treat software more like building a widget. Much of our code is written and then typically try to limit change. Change is seen as a high risk endeavor and because of the lack of testing it can be very unnerving to the person who has to make the next modifications. Also, it is usually challenging to get leadership approval to do general improvements that won’t fix a bug or add new advancements.

In recent years this has started to change with automation and Agile/DevOps practices. The thing we have to remember is the real constant is change. Any software that has no request for features, no bugs, and never changes, usually means the software isn't being used. This means we really want to be changing it often to keep it relevant or it’s time to throw it away and start over.

## Expect Change

It is important to realize that with software, it doesn’t follow the normal manufacturing process. When the software industry began, it was largely following the manufacturing process because it’s what was known and many of the companies where focused on only writing software to support their hardware.

As the industry evolved, because software is so easy to create and modify, it needed to be treated differently. Making a code change, recompiling and deploying is really easy, compared to replacing a piece of hardware. While it wasn't perfect, this method did change to create its own model and automation became key. Now with containers and cloud added to the mix, change is truly the normal flow.

## Reinvention

Reinvention is a bad word for many organizations because it equals a new cost and the past sunk cost. After building software products, you have invested a large amount to create it, so it may be very risky to recreate or abandon the solution. That sunk cost can be a hindrance to progress. For this reason, many software products will last much longer then they should, even with all their known defects. Obviously, if a software product is providing value, there is no guarantee replacing it will have the same value. Replacing it though will allow it to remove past design decisions and help make it fit better into modern practices (Automation, Cloud, etc…). This also opens the door to new languages and new technology solutions. Remember too, that taking the knowledge and not the code can be very beneficial for a product and company.

Microservices largely opened the door to better support for reinvention. This is because replacing a small single focused service is easier. This greatly reduces the sunk cost in most cases. Over time it should be reworked or even replaced and that sunk cost is no longer a major factor in the decision. Also with the singular focus, it is easier to replace because of the reduced complexity of the service.

Having the ability to more easily reinvent solutions is a powerful capability that shouldn’t be ignored. Companies make money by supporting their customers and being able to adjust, realign and better support the goals of the business. Organizations that can do this can more easily expand and stay relevant. It’s also important to provide the path to reinvent because part of staying relevant means you have to change or you’ll fall behind.

## Plan To Change

As we progress, we learn more, get better, and know how to improve. Use that by planning to focus on smaller reusable services, so you can quickly adapt, adjust and improve, while removing the risk and guilt of sunk costs. Planning to replace an item from the start, will help to make sure you don’t over engineer a solution and will keep a open path to staying relevant.

As your organization tries to improve, moving to reinvention and accepting constant change will prove more powerful. Software has changed the game in the technology industry and the successful companies have figured this out. Software allows them to rapidly change and make sure they stay relevant and advance capabilities for their customers.

