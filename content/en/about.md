---
title: "About"
description: "About Me"
date: 2020-11-01T14:09:21+09:00
draft: false
enableToc: false
---

I have been professionally programming for over 20 years, and I've always loved creating things.   Programming was my way of creating new content ever since I was a child.  Much of what I do today is help push modern concepts and technologies including DevOps, Agile, Cloud Architecture, and Automation.  I consider myself a change agent to help keep people on their toes by pushing forward with new technology.

When I think back, I'm not always clear how I got where I am, but it has been a slow steady climb.   Programming as a child, I worked to learn Basic and evolved into C/C++.  In college, web development became the new thing and this is where I learned C# and the .NET Framework.   For the last 20 years I've been using Java.

In all of this, my desire to create has never changed.  This is probably why I find so much enjoyment in writing and woodworking.

Recently, after completing my second master's degree in Data Analytics, I became an adjunct professor. I'm hoping to pass on my passion for programming and creating new solutions.
